The main file is nl.vu.recoprov.CompletePipeline.
You can configure the folder that you want to access using "config.txt".

For example, if you want to read a local folder, you should write:

Local
folder="/Users/user/myfolder"

Otherwise, if you want to access a Dropbox folder, you should write:

Dropbox
folder="/eswc2011-demo-paper/"