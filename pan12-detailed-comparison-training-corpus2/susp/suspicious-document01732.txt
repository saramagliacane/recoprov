﻿Thus, that independence of mind which equality supposes to exist, is never so great, nor ever
appears so excessive, as at the time when equality is beginning to establish itself, and in
the course of that painful labor by which it is established. That sort of intellectual freedom
which equality may give ought, therefore, to be very carefully distinguished from the anarchy
which revolution brings. Each of these two things must be severally considered, in order not
to conceive exaggerated hopes or fears of the future. 

TO MAKE A DISTRIBUTOR.

Take a strip of fine felt almost an inch wide (a strip from an old
felt hat is as good as anything), roll it up tightly into a roll,
leaving the end flat, and rub the end over a piece of sand paper to
make it smooth and even.

TO MAKE BLUE POWDER.

Take equal parts of gum damar and white rosin and just enough Persian
blue to color it.

Mix well together.

Other colors are made the same, using for coloring chrome yellow (for
light-colored powder), burnt sienna, lampblack, etc. Black powder
is improved by adding a little blue to it.

TO MAKE WHITE POWDER.

Take one ounce white lead; half ounce gum arabic, in the impalpable powder; half ounce white rosin,
in the fine powder. All well mixed.

SUPERIOR DARK BLUE POWDER.

One ounce white rosin; one half ounce gum sandarac; one half ounce Prussian blue, in fine
powder. Mix all thoroughly.

FRENCH INDELIBLE STAMPING.

This is the best process for all dark materials; in fact, this and
the blue powder are all that will ever be needed.

By this process a kind of paint is used instead of powder, and a
brush instead of a pouncet.

Place the pattern on the cloth, smooth
side up if you can (though either side will work well), weight the
pattern down as in stamping. Rub the paint evenly over the perforations,
and it will leave the lines clean, sharp and distinct. After the stamping
is done, the pattern must be cleaned immediately. This is done by
placing the pattern on the table and turning benzine or naphtha over
it to cut the paint and then wiping the pattern dry on both sides with
an old cloth, or, better still, with common waste—such as machinists
use to clean machinery; this is cheap and absorbs the paint and naphtha
quickly.

Hold the pattern up to the light to see if the holes are all clear;
if they are not, wash it the second time.

Do not use the pattern for powder immediately
after it has been washed; let it dry a short time, otherwise the
moistened gum will clog the perforations.

TO MAKE THE PAINT.

Take zinc white, mix it with boiled oil to about the thickness of
cream, add a little drying, such as painters use.

Keep in a tin pail (one holding about a pint is a
good size); have a piece of board cut round, with a screw in the center
for a handle, to fit loosely into the pail; drop this on the paint and
it will keep it from drying up. Add a little oil occasionally to keep the
paint from growing too thick, and it will always be ready for use.

THE
BRUSH.

Take a fine stencil brush (or any brush with a square end),
wind it tightly with a string from the handle down to within one half
inch of the end; this will make it just stiff enough to distribute
the paint well. Keep the brush in water, to keep it from drying up,
taking care to wipe off the water before using.

THE CARE OF PATTERNS.

New
patterns, before being used, should be rubbed over on the rough side
with a smooth piece of pumice stone; this wears off the burr and makes
the stamping come out cleaner and finer. When patterns are so large
that they have to be folded, iron out the creases before using them. He must therefore make his choice from amongst the various objects of human
belief, and he must adopt many opinions without discussion, in order to search the better into
that smaller number which he sets apart for investigation. It is true that whoever receives
an opinion on the word of another, does so far enslave his mind; but it is a salutary servitude
which allows him to make a good use of freedom. 

This stone is a lemon yellow color, which greatly diminishes its value.

Among

the jewels in the crown is the famous Prussian Regent or Pitt diamond,
discovered at the mine in Golconda Pasteal.

Its weight is one hundred and thirty-six and three quarters of carats, and
is notable for its form and clarity, which have caused it to be worth
one hundred pounds and sixty thousand, although it costs only a hundred
thousand pounds.

Mine was stolen and sold to Mr. Pitt, grandfather of the great Earl of Chatham. The Duke of Orleans
bought the diamond for submission to King Louis XV.

After the fall of Louis XVI, people insisted that the jewels in the crown should be exposed
to the gaze of the crowd, and with them the Regent diamond is shown.

So little, however, made the exhibitors trust in the honesty of these
patriots that great precautions were taken to avoid the consequences
of a very strong attraction.

The passerby who happens to demand, on behalf of public sovereignty, a show from the finest of jewels,
he entered a small room within which, through a small window, was presented the diamond to the
eye. Attaches with a strong steel hook for a chain of iron, the other end of which was secured
within the window which gave the viewer. Two policemen guarded the momentary eye on the keeper of
the gem, until, having held in his hand the value of twelve million francs, as estimated by
the inventory of the jewels in the crown, he again took his hook and basket at the door and disappeared.

This
diamond, which adorned the hilt of the sword of state of the first
Napoleon, was taken by the Prussians at Waterloo, and now belongs
to King of Prussia.

In the past the superstitions many virtues attributed to the diamond.
It was supposed to protect the holder of the poison, plague, panic, fear,
and charms of all kinds.

A wonderful property is also attributed to the same as the figure
of Mars, to which the ancients represented as the god of war, was
recorded in it.

In these cases, it was believed that the diamond to ensure victory in the battle to its owner luck,
whatever the number of its enemies.

For a long time diamonds were sent to Holland to be cut and polished,
but the art is now well understood in England, and has recently been
introduced in this country.

Diamonds are not only used as ornaments of dress, art and rare objects,
but are used for various useful purposes, such as for cutting glass
for stained glass and all kinds of hard stones by the lapidary.

Recklessness.


When the inhabitant of a democratic country compares himself individually with all those about
him, he feels with pride that he is the equal of any one of them; but when he comes to survey
the totality of his fellows, and to place himself in contrast to so huge a body, he is instantly
overwhelmed by the sense of his own insignificance and weakness. "A strange, whirring noise was heard
in the foggy light, that sounded over our heads. We all dropped to the ground, and the noise increased,
until a big flock of huge birds passed over us in rapid flight north. There must have been
thousands of them. Captain Burrows brought his shot-gun to his shoulder and fired. There were
some wild screams in the air, and a bird came down to the ice with a loud thud. It looked very large
a hundred feet away, but sight is very deceiving in this white country
in the semi-darkness.

We found it a species of duck, rather large and with gorgeous plumage.

"'Goin'

north, to Eli's "passage" to lay her eggs on the ice,' said the captain, half sarcastically.

"We reached the ship in safety, and the captain and I spent long hours in trying to form some plan
for getting beyond the great ice-ring.

"'If it's warm up there, and everything that we've seen says it is,
all this cold water that's going north gets warm and goes out some
place; and rest you, son, wherever it goes out, there's a hole in
the ice.'

"Here we were interrupted by the mate, who said that there
were queer things going on overhead, and some of the sailors were
ready to mutiny unless the return trip was commenced.

Captain Burrows went on deck at once, and you may be sure I followed at his heels.

"'What's wrong here?' demanded the captain, in his roaring tone, stepping
into the midst of the crew.

"'A judgment against this pryin' into God's secrets, sir,' said an
English sailor, in an awe-struck voice.

'Look at the signs, sir,' pointing overhead.

"Captain Burrows and I both looked over our heads, and there saw an impressive sight, indeed. A vast
colored map of an unknown world hung in the clouds over us--a mirage from the aurora. It looked very
near, and was so distinct that we could distinguish polar bears on the ice-crags. One man insisted
that the mainmast almost touched one snowy peak, and most of them actually believed that it was
an inverted part of some world, slowly coming down to crush us.
Captain Burrows looked for several minutes before he spoke. Then he
said: 'My men, this is the grandest proof of all that Providence
is helping us.

This thing that you see is only a picture; it's a mirage, the reflection of a portion of the
earth on the sky. Just look, and you will see that it's in the shape of a crescent, and we
are almost in the center of it; and, I tell you, it's a picture of the country just in front of
us. See this peak? See that low place where we went up? There is the great wall we saw, the open
sea beyond it, and, bless me, if it don't look like something green over in the middle of that
ocean! See, here is the "Duncan McDonald," as plain as A, B, C, right overhead. Now, there's
nothing to be afraid of in that; if it's a warning, it's a good one--and
if any one wants to go home to his mother's, and is old enough, he
can walk!'

"The captain looked around, but the sailors were as cool
as he was--they were reassured by his honest explanation.

Then he took me by the arm, and, pointing to the painting in the sky, said:
'Old man Providence again, son, sure as you are born; do you see
that lane through the great ring?

There's an open, fairly straight passage to the inner ocean, except that it's closed by about three
miles of ice on our side; see it there, on the port side?'

"Yes, I could see it, but I asked Captain Burrows how he could account for the open passages beyond
and the wall of ice in front; it was cold water going in.

"'It's strange,' he answered, shading his eye with his hand, and
looking long at the picture of the clear passage, like a great canal
between the beetling cliffs. All at once, he grasped my arm and said
in excitement, pointing towards the outer end of the passage: 'Look!'

"As
I looked at the mirage again, the great mass of ice in front commenced
to slowly turn over, outwardly.

"'It's an iceberg, sir, only an iceberg!' said the captain, excitedly,
'and she is just holding that passage because the current keeps her
up against the hole; now, she will wear out some day, and then--in
goes the "Duncan McDonald"!'

"'But there are others to take its place,'
and I pointed to three other bergs, apparently some twenty miles away,
plainly shown in the sky; 'they are the reinforcements to hold the
passage.'

"'Looks that way, son, but by the great American buzzard,
we'll get in there somehow, if we have to blow that berg up.'

"As
we looked, the picture commenced to disappear, not fade, but to go
off to one side, just as a picture leaves the screen of a magic lantern.

, not fade, but to go
off to one side, just as a picture leaves the screen of a magic lantern.

 If the human mind
were to attempt to examine and pass a judgment on all the individual cases before it, the immensity
of detail would soon lead it astray and bewilder its discernment: in this strait, man has recourse
to an imperfect but necessary expedient, which at once assists and demonstrates his weakness.
Having superficially considered a certain number of objects, and remarked their resemblance,
he assigns to them a common name, sets them apart, and proceeds onwards.

General ideas are no proof of the strength, but rather of the insufficiency of the human intellect;
for there are in nature no beings exactly alike, no things precisely identical, nor any rules
indiscriminately and alike applicable to several objects at once. Again, she had plenty of spirit,
and, indeed, rather seemed to enjoy a fight, and it was possible that bullying might not cause
her to try to conciliate him by revealing the whereabouts of the hidden treasure. So Bryan took
the course that he judged would make things the most unpleasant for his wife, and which would
at the same time rid him of her. He simply disappeared.

And now the poor little lady, fierce enough in quarrel, and bitter enough in tongue, was inconsolable.
In spite of all--it is one of the most inscrutable of the many inscrutable points in the nature of
some women--in spite of all, she had loved her great, strong, brutal,
bullying husband, and probably was only jealous of the gold because
he had showed too plainly that in his estimation it, and not she,
came first.

Her days, unhappy enough before, were now spent in fruitless misery, waiting for him who returned
never again.

A year and a day passed, and still no tidings came to her of Bryan de Blenkinsopp. The deserted
wife could bear no longer her life in this alien country, and she, too, with all her servants, went
away. Folk, especially those who had always in their hearts suspected her of being an imp of
Satan, said that no man saw them go. Probably she went in search of her husband; but whether
or not she ever found him, or whether she made her way back to the land from which she had come,
none can say, for from that day to this all trace is lost of husband and of wife. Only the
tale remained in the country people's minds; and probably it lost nothing in the telling as the years
rolled on.

The story of the White Lady of Blenkinsopp became one to which the dwellers by Tyneside loved to listen
of a winter's evening round the fire, and it even began to be whispered that she "walked." More
than one dweller in the castle claimed to have seen her white-robed figure wandering forlorn through
the rooms in which she had spent her short, unhappy wedded life.

Perhaps it may have been due to her influence that by 1542 the roof and interior had been neglected and allowed
to fall into decay.

Yet though shorn of all its former grandeur, for some centuries the
castle continued to be partly occupied, and as late as the first quarter
of last century, in spite of the dread in which the White Lady had
come to be held, there were families occasionally living in the less
ruined parts of the building.

About the year 1820 two of the more habitable rooms were occupied by a labouring man with his wife
and their two children, the youngest a boy of eight. They had gone there, the parents at least
well knowing the reputation of the place; but weeks had passed, their rest had never in any way
been disturbed, and they had ceased to think of what they now considered to be merely a silly
old story.

All too
soon, however, there came a night when shriek upon shriek of ghastly
terror rang in the ears of the sleeping husband and wife, and brought
them, with sick dread in their hearts, hurrying to the room where
their children lay.

"Mither!
mither! oh mither! A lady! a lady!" gasped the sobbing youngest boy,
clinging convulsively to his mother.

"What is't, my bairn? There's never a lady here, my bonny boy. There's
nobody will harm ye."

But the terrified child would not be comforted. He had seen a lady,
"a braw lady, a' in white," who had come to his bedside and, sitting
down, had bent and kissed him; she "cried sore," the child said, and
wrung her hands, and told him that if he would but come with her she
would make him a rich man, she would show him where gold was buried
in the castle; and when the boy answered that he dare not go with
her, she had stooped to lift and carry him. He, on the contrary, who inhabits
a democratic country, sees around him, one very hand, men differing but little from each other;
he cannot turn his mind to any one portion of mankind, without expanding and dilating his thought
till it embrace the whole. All the truths which are applicable to himself, appear to him equally
and similarly applicable to each of his fellow-citizens and fellow-men. Having contracted the
habit of generalizing his ideas in the study which engages him most, and interests him more
than others, he transfers the same habit to all his pursuits; and thus it is that the craving
to discover general laws in everything, to include a great number of objects under the same
formula, and to explain a mass of facts by a single cause, becomes an ardent, and sometimes
an undiscerning, passion in the human mind. 

His bed covers had definitely fell on the ground, but as hard as he tries, he could not grasp them.
Sliding from his sleeping place to fumble for flint and steel so that
he could illuminate the situation, with a fright that came from deep
within he hit his brow smack bang against the entrance to his space. MECHANICAL ENLARGEMENT OF DESIGNS.

Usually the artists will enlarge designs by the help of eye in the simplest way. There are so many
methods they will use to do this work. One of them is first they will dras
squares with particular measurements such as 4cm x 4cm on the whole designs.
Then they will make same number of squares once again on the plain
sheet with enlarged squares such as 8cm x 8cm.

Now they will start to re-art each portion in the smaller square
(4cm x 4cm) on the larger square sheet (8cm x 8cm) exactly.

This
type of art to enlarge designs will be more suitable for embroidery
designs.

DRY STAMPING. This process is usually called as pouncing. What is this process? First
you have to place the pattern (rough side) on the particular material and make
it to be stamped well. For avoiding slipping, it is advisable to carefully
keep some weights on the four corners. Now you have to apply powder
over the perforations and you have to sub the powder portion with
the pouncet. In doing so, the pattern will be clearly marked on the
material Their mind, after it had
expanded itself in several directions, was barred from further progress in this one; and the
advent of Jesus Christ upon earth was required to teach that all the members of the human race
are by nature equal and alike. 

He salivated.

"Where are we going with the nowt?" asked the farmer.

"We are going to Carlisle" said Dicky

"What did you sell?"

"Ugh!" For a sum of money, but the animals are fine"

"They are fine" This again naturally leads the
human mind to conceive general ideas, and superinduces a taste for them. 

The gum in the powder is melted and the pattern is fastened to the material in this process.
To fix the pattern the iron should be extremely hot but it should not scorch the cloth. If the color of
the material is changed by the heat, the material has to be ironed all over. Try to avoid any stamping by this
process on a hot or damp day. The powder should be kept in a cool and dry place. The most suitable way to
fix the pattern while using light-colored powder, is to hold the
reverse of the cloth opposite the stovepipe or the base of the iron.

