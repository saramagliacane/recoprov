﻿Sc. i. merely continues the love-making of Act III. Sc. ii. gives another glimpse of the good
Duke's court; in sc. iii. the love of Phebe bears fruit in a letter to Ganymede, and Oliver
finds his way to the forest. The bad Duke's intentions toward Orlando in sending Oliver after
him are, however, frustrated by the sudden change of heart against a bad Duke is a good Duke.
Contrast their actions throughout the play. Contrast also the two brothers, Orlando and Oliver.
What are the resemblances between the characters of Oliver and Duke Frederick?--between Orlando
and the banished Duke? Is Orlando's rebellion against his brother's injustice or the banished
Duke's acceptance of his brother's injustice the more to be praised? Compare his attitude with
that of Prospero under similar circumstances. Whose repentance is the more sincere, Oliver's
or Duke Frederick's? Note that Oliver has lost all when he repents, while the Duke gives up
everything just as he is about to realize his aim. Is the repentance of the usurping Duke merely
a ruse of Shakespeare's to bring the play to a happy ending? In Lodge's story he does not repent,
but is proceeded against by his brother. Contrast Jaques and Touchstone. Is Jaques's melancholy
affected? What is the main difference between Rosalind and Celia? Which is the more the friend
of the other? (For valuable suggestions on these points see 'Characters in "As You Like It,"'
Poet-lore, Vol. IV. pp. 31 and 81, Jan. and Feb., 1892.)

QUERIES FOR DISCUSSION

Which is the better philosopher, Jaques or Touchstone, and which is more closely related to
the philosophy of the play?

The characters of the two Dukes are not developed; they are merely walking gentlemen, whose
office it is to keep the play in motion.

2. The Lovers of the Play.

The Different Kinds of Love in 'As You Like It.' Examples of love at first sight in Shakespeare.
Note Orlando's surprise at the suddenness of Oliver's and Celia's love. Was his own less sudden?
Consider Hymen's song and Jaques's remarks in the last scene as descriptive of the various
couples. Does the comic element of the play, as represented by Touchstone, discredit sentiment
in the play? Notice the madrigal in Lodge's novel (given in Poet-lore, Vol. III., in the article
on Lodge, Dec, 1891), and consider whether Shakespeare has borrowed anything from it in characterizing
Rosalind's wooing? Contrast Lodge's Montanus as a lover with Shakespeare's Silvius. Is Montanus
too much of a "tame snake" to be natural? Or does this constancy in love make him a superior
figure? Is it a sign of Silvius's inferiority that love has its own way with him? Can love
be true that changes if it is unrequited?

Are those actors right, do you think, who play Oliver as guessing who Ganymede is when she
swoons? Is Rosalind's conduct unwomanly? Is her disguise unlikely?

QUERIES FOR DISCUSSION

It is best for the man to love the most; and therefore has Silvius and Phebe's unequal love-match
a better chance for happiness than Rosalind's and Orlando's?

VII

THE PASTORAL ELOPMENT

The Rise of Pastoral Poetry, and Shakespeare's Use of it in 'As You Like It.'

Compare Spenser's 'Shepherd's Calendar,' Fletcher's 'Faithful Shepherdess,' etc. Point out
any differences you find between Shakespeare's and Spenser's pastoral poetry. Modern literary
use of the pastoral element, Wordsworth's 'Michael.' Is the pastoral life of literature always
artificial? Can a progress toward realism be shown? The humor of the play. Discuss in particular
the humorous comments on contrasts between court and country life. Compare modern instances
of the refinements and artifices of city life and the crudeness of work and pleasure in the
country.

Special Points.--1. The Forest of Arden: Is it in England, France, or Shakespeare's imagination?
2. "Old Robin Hood of England." What are the legends concerning him? 3. The archaic words in
the play. (See Prof. Sinclair Korner's 'Shakespeare's Inheritance from the Fourteenth Century,'
in Poet-lore, Vol. II., p. 410, Aug., 1890.)

QUERY FOR DISCUSSION

Is the opposition shown in the play between life at court and in the country truly shown to
be to the advantage of the country.

VIII

THE MORAL ELEMENT

The moral side of the Play consists, according to the Introduction in the First Folio Edition,
in its persuasion toward an Arden of the disposition, or a spirit of happy good will toward
all men. How far does this cover the lesson of the Play?

What is to be thought of the idea in the 'Ethics of "As You Like It"' (Poet-lore, Vol. III.,
p. 498, Oct., 1891), that Touchstone's opinion of a shepherd's life (III. ii.) is the key-note
of the play? Are the references to fortune in the play significant? Dr. F.J. Furnivall says:
"What we most prize is misfortune borne with cheery mind, the sun of man's spirit shining through
and dispersing the clouds which try to shade it. This is the spirit of the play." Of this Dr.
Ingleby says: "The moral of the play is much more concrete than this. It is not how to bear
misfortune with a cheery mind, but how to read the lessons in the vicissitudes of physical
nature." C.A. Wurtzburg says: "The deep truths that may be gathered from the play are the innate
dignity of the human spirit, before which every conventionality of birth, rank, education,
even of natural ties, must give way." Give arguments drawn from the play in favor of or against
all of these suggestions. Is it an evidence of Shakespeare's intention to be a moral teacher
that he altered the fate of Duke Frederick?

QUERY FOR DISCUSSION

Has the play any moral that is not gently satirized in it?

IX

THE SOURCE OF THE PLOT

Shakespeare's Variations from Lodge.

Compare Lodge's 'Rosalind' with 'As You Like It.'

(For this story, see "Shakespeare's Library" or Extracts in Notes and Comment in Sources in
"First Folio Edition").

Is the story better without the parts Shakespeare leaves out (e. g., Adam's proposal to Rosader
to cut his veins and suck the blood; his nose-bleed; the incident of the robbers accounting
for Aliena's sudden love, etc.)? Why is the "Green and gilded snake" added? Isn't the "lioness"
enough? Is Rosader or Orlando the finer character, and why? The new characters introduced--Audrey
and William--considered as embodying real instead of ideal pastoral life. Do Shakespeare's
changes affect the plot, the characters, or the moral of the story? (For an examination of
the plot of the play, see 'An Inductive Study of "As You Like It,"' in Poet-lore, Vol. III.,
p. 341.)

A Sketch of Lodge's Life and Work. (See 'An Elizabethan Lyrist: Thomas Lodge,' in Poet-lore,
Vol. III., p. 593, Dec, 1891.)

QUERY FOR DISCUSSION

Is Shakespeare's framing of the plot of 'As You Like It' not to be admired, because it is borrowed?

X

THE MUSIC OF THE PLAY

This may consist of a brief paper on the subject illustrated by a program of the songs with
the old and more modern settings. (See New Shakespeare Society's Papers, on this subject; 'Shakespeare
and Music,' by E.W. Naylor.)

TWELFE NIGHT

The winsomeness of this poetic comedy rightly makes the reader or the hearer hesitate to count
its petals or scrutinize the stages of its growth, which are marked by its acts as symmetrically
as leaf buds are ranged about a stalk. And yet, one may find that to take note of such beautiful
orderliness in the delicate structure and sprightly blossoming of the poet's design enhances
the appreciation of its artistic quality. Regarding it first as a whole, sum up the stages
of the action, first; then the caprices its allusions denote; then the characters; and finally
the poetic fancy and wit exhaled by the whole play like a fragrance.

I

THE STORY OF THE PLAY

Act I. scene i. puts us in possession of what facts concerning the Duke and Olivia? What do
we learn from the conversation of Viola and the Captain in scene ii., and what course does
Viola decide upon? What do we discover from scene iii. in regard to the state of things in
Olivia's household? In scene iv., what relation has been established between the Duke and Viola?
What three new characters are introduced in scene v., and what is the event of the scene? Act
II. scene i.: What is learned of Sebastian and his intentions? In scene ii., what are shown
to be the feelings of Olivia? In what previous scene was this prepared for? Does scene iii.
advance the story at all? What is it taken up with? Does scene iv. advance the story? Of what
scene is it almost a repetition? If it does not advance the action, what does it do? Of what
previous scene is scene v. the result? What previous scene leads up to scene i. of Act III?
and of what scene is it in purpose a repetition? What new turn is given to affairs in scene
ii., and through whom is it brought about? Whose doings do we get a glimpse of in scene iii?
Of whose plot do we see further developments in scene iv? What other issues in the progress
of events come to a climax in this Act? Act IV. scene i.: Describe the complication of affairs
which arises in this scene. What previous scenes do we see the result of in scene ii? and what
happens that will bring about a change in the situation? What important event occurs in this
scene iii? Act V. scene i.: Describe how in this scene all the complications are unravelled,
and by what means all the characters are brought upon the stage. What do you think of the device
to call Malvolio upon the stage? Does it not seem rather clumsy, or do you think it a further
humorous touch that Viola should have to depend on Malvolio to find her 'woman's weeds again'?

What becomes evident after tracing the events of the play through in this way? That the interest
of the play does not depend so much upon the story itself, as, first, upon the amusing situations
resultant from the story, and, second, upon the scenes which introduce the characters in Olivia's
household who are really not at all concerned in the development of the plot, but who are the
occasion of many added amusing situations.

What constitutes the real interest of the two short scenes between Sebastian and Antonio? Their
bearing, mainly, on scene iv. of Act III. By means of them we are shown that Antonio has an
enemy in Orsino, and thus his arrest is prepared for, also how Antonio gives his purse to Sebastian,
the real purpose of the arrest being to bring about a reason for Antonio's requiring his purse
again from Cesario, whom he takes for Sebastian, and so to add complication to the situation
arising from the resemblance between the brother and sister.

What are the situations which the story gives Shakespeare a chance to develop? On the one hand,
is the Duke pouring out his love for another woman to his supposed page, who is in love with
him, and thus giving rise to the series of scenes between the Duke and Viola. On the other
hand, is the supposed page pressing his master's suit to a woman who loves the supposed page,
and thus giving rise to the series of scenes between Viola and Olivia. Out of this love of
Olivia for Viola grows the absurd situation of Viola's being obliged to fight a duel, which
is made still more ridiculous through the circumstance of her challenger being a fool. Out
of Viola's resemblance to her brother and her disguise grows the absurd situation of Olivia's
claiming her as a husband, and that of Sir Andrew taking for his unwilling duellist the all-too-willing
Sebastian.

To these situations which naturally result from the story, Shakespeare has added in Olivia's
household a set of characters whose personality is such that amusing situations are multiplied.
Thus we may say that the play is one of situation rather than of action, since whatever of
action there is in it leads to situation, and whatever of character there is in it leads also
to situation.

QUERIES FOR DISCUSSION

1. If attention is constantly given to creating humorous situations, will character-development
necessarily suffer? 2. Do you agree with the Shakespearian critic Verplanck that this play
bears no indication either of an original groundwork of incident, afterwards enriched by the
additions of a fuller mind, or of thoughts, situations, and characters accidentally suggested,
or growing unexpectedly out of the story, as the author proceeded?

II

THE WHIMSICAL AND OTHER ALLUSIONS IN THE PLAY

Pick out and explain the curious allusions in the play, noticing that these may be classed
as geographical, mythological, astrological, or referable to persons or customs of the time,
or books of the day. For examples of the latter class, note Sir Toby's 'diluculo surgere' (II.
iii.), for 'Saluberrimum est dilucolu surgere,' an adage from Lilly's Grammar, doubtless one
of Shakespeare's text-books at the Edward VI. School in Stratford; and Viola's 'Some Mollification
for your giant sweet lady' (I. v.),--an allusion to the innumerable romances whose fair ladies
are guarded by giants; for Maria, being very small, Viola ironically calls her giant, and asks
Olivia to pacify her because she has opposed her message. (For Shakespeare's education and
school-books, see Bayne's remarks on this subject in Brit. Encyc. art. Shakespeare.) The whole
incident of the 'possession' of Malvolio, and the visit of Sir Topas, probably alludes to a
tract published in 1599 by Dr. Harsnett,--'A Discovery of the Fraudulent Practices of John
Darrel,'--in which is narrated how the Starkeys' children were possessed by a demon, and how
the Puritan minister, Mr. Darrel, was concerned in it. For examples of allusions to contemporary
customs, see Sir Toby's mention of dances no longer known,--'Galliard,' 'Coranto,' etc. As
an example of allusions to persons of that time, Sir Toby's reference to 'Mistress Mall's picture,'--Mary
Frith, born in 1584, died in 1659, a notorious woman who used to go about in man's clothing
and was the target for much abuse. Astrological allusions: 'Were we not born under Taurus?'
'That's sides and hearts,' which refers to the medical astrology still preserved in patent-medicine
almanacs, where the figure of a man has his various parts named by the signs of the Zodiac.
'Diana's lip' (I. iv.), ('Arion on the Dolphin's back' I. ii.), are examples of mythological
allusions. Of the geographical allusions there are two kinds, the real and the sportive,--Illyria,
an example of the one, the 'Vapians' and the 'Equinoctial of Queubus,' of the other. Go on
through the play classifying and commenting on the allusions. What was a 'catch'? Give an example.

QUERIES FOR DISCUSSION

Are the odd allusions in the play a result of the corrupt text, ignorance, ridicule of learning?
Or are they introduced to give a lively and contemporaneous effect?

III

THE DUKE AND SEBASTIAN

