﻿I have no objection to your use of my name if you think fit to publish it.

                                                    Your obedient servant,
                                    Thomas Waddington, M.D., of Wakefield.
                                  Morley’s Hotel, Charing Cross, March 26.

To the above letter the following reply was sent to the Times.

                 “Alleged Accident on the Great Northern.
                      “To the Editor of the Times.

“Sir,—The Directors of the Great Northern railway will feel much obliged by the insertion of
the following statement in the Times to-morrow relative to a letter which appeared therein
to-day, signed ‘Thomas Waddington, M.D., of Wakefield,’ and headed, ‘Accident on the Great
Northern railway.’

There was no accident whatever yesterday on the Great Northern railway.

The trains all reached King’s Cross with punctuality, the most irregular in the whole day being
only five minutes late.  No such person as Thomas Waddington is known at Morley’s Hotel, whence
the letter in question is dated.

                                              I am, Sir, yours faithfully,
                                           Seymour Clark, General Manager,
                                                   King’s Cross, March 27.

In the Times on the day following appeared a letter from the real Dr. Waddington, of Wakefield,
(Edward not “Thomas”) confirmatory of the impudence of the hoax.

           “The alleged Accident on the Great Northern railway.
                      “To the Editor of the Times.

“Sir,—My attention has been called to a letter in the Times of yesterday (signed ‘Thomas Waddington,
M.D., of Wakefield’) the signature of which is as gross and impudent a fabrication as the circumstances
which the writer professes to detail.  I need only say there is no ‘M.D.’ here named Waddington
but myself, and that I was not on the Great Northern or any other Railway on the 26th inst,
when the accident is alleged to have occured.

Having obtained possession of the original letter, I have handed it to my solicitors, in the
hope that they may be enabled to discover and bring to justice the perpetrator of this very
stupid hoax.

                                         I am, Sir, your obedient servant,
                                                   Edward Waddington, M.D.

   Wakefield, March 28.

A’PENNY A MILE.

Two costers were looking at a railway time-table.

“Say, Jem,” said one of them, “vot’s P.M. mean?”

“Vy, penny a mile, to be sure.”

“Vell, vot’s A.M.?”

“A’penny a mile, to be sure.”

SINGULAR FREAK.

In October, 1857, Mr. Tindal Atkinson applied to Mr. Hammill, at Worship Street Police Court,
to obtain a summons under the following strange circumstances:—

“Mr. Atkinson stated that he was instructed on behalf of the Directors of the Eastern Counties
Railway Company to apply to the magistrate under the terms of their Act of Incorporation, for
a summons against Mr. Henry Hunt, of Waltham-Cross, Essex, for having unlawfully used and worked
a certain locomotive upon a portion of their line, without having previously obtained the permission
or approval of the engineers or agents of the company, whereby he had rendered himself liable
to a penalty of £20.  He should confine himself to that by stating that in the dark, on the
night of Thursday, the 1st instant, a locomotive engine belonging to Mr. Hunt was suddenly
discovered by some of the company’s servants to be running along the rails in close proximity
to one of the regular passenger trains on the North Woolwich line.  So great was the danger
of a collision, that they were obliged to instantly stop the train till the stranger engine
could get out of the way, to the great terror of the passengers by the train, and as he was
instructed it was almost the result of a merciful interposition of Providence that a collision
had not occurred between them, in which event it would probably have terminated fatally, to
a greater or lesser extent.  He now desired that summonses might be granted not only against
the owner of the engine so used, but also against the driver and stoker of it, both of whom,
it was obvious, must have been well aware of their committing an unlawful act, and of the perilous
nature of the service in which they were engaged when they were running an engine at such a
time and place.

“Mr. Hammill said it certainly was a most extraordinary proceeding for anyone to adopt, and
after the learned gentleman’s statement he had no hesitation whatever in granting summonses
against the whole of the persons engaged in it.”

A.B.C. AND D.E.F.

A gentleman travelling in a railway carriage was endeavouring, with considerable earnestness,
to impress some argument upon a fellow-traveller who was seated opposite to him, and who appeared
rather dull of apprehension.  At length, being slightly irritated, he exclaimed in a louder
tone, “Why, sir, it’s as plain as A.B.C.”  “That may be,” quietly replied the other, “but I
am D.E.F.”

NATIONAL CONTRAST.

The contrast which exists between the character of the French and English navvy may be briefly
exemplified by the following trifling anecdote:—

“In excavating a portion of the first tunnel east of Rouen towards Paris, a French miner dressed
in his blouse, and an English “navvy” in his white smock jacket, were suddenly buried alive
together by the falling in of the earth behind them.  Notwithstanding the violent commotion
which the intelligence of the accident excited above ground, Mr. Meek, the English engineer
who was constructing the work, after having quietly measured the distance from the shaft to
the sunken ground, satisfied himself that if the men, at the moment of the accident, were at
the head of “the drift” at which they were working, they would be safe.

Accordingly, getting together as many French and English labourers as he could collect, he
instantly commenced sinking a shaft, which was accomplished to the depth of 50 feet in the
extraordinary short space of eleven hours, and the men were thus brought up to the surface alive.

The Frenchman, on reaching the top, suddenly rushing forward, hugged and saluted on both cheeks
his friends and acquaintances, many of whom had assembled, and then, almost instantly overpowered
by conflicting feelings—by the recollection of the endless time he had been imprisoned and
by the joy of his release—he sat down on a log of timber, and, putting both his hands before
his face, he began to cry aloud most bitterly.

The English “navvy” sat himself down on the very same piece of timber—took his pit-cap off
his head—slowly wiped with it the perspiration from his hair and face—and then, looking for
some seconds into the hole or shaft close beside him through which he had been lifted, as if
he were calculating the number of cubic yards that had been excavated, he quite coolly, in
broad Lancashire dialect, said to the crowd of French and English who were staring at him,
as children and nursery-maids in our London Zoological Gardens stand gazing half-terrified
at the white bear, “YAW’VE BEAN A DARMNATION SHORT TOIME ABAAOWT IT!”

                                       Sir F. Head’s Stokers and Pokers.

REMARKABLE ACCIDENT.

The most remarkable railway accident on record happened some years ago on the North-Western
road between London and Liverpool.  A gentleman and his wife were travelling in a compartment
alone, when—the train going at the rate of forty miles an hour—an iron rail projecting from
a car on a side-track cut into the carriage and took the head of the lady clear off, and rolled
it into the husband’s lap.  He subsequently sued the company for damages, and created great
surprise in court by giving his age at thirty-six years, although his hair was snow white.
It had been turned from jet black by the horror of that event.

ENGINEERING LOAN, OR STAKING OUT A RAILWAY.

“Beau” Caldwell was a sporting genius of an extremely versatile character.  Like all his fraternity,
he was possessed of a pliancy of adaptation to circumstances that enabled him to succumb with
true philosophy to misfortunes, and also to grace the more exalted sphere of prosperity with
that natural ease attributed to gentlemen with bloated bank accounts.

Fertile in ingenuity and resources, Beau was rarely at his wit’s end for that nest egg of the
gambler, a stake.  His providence, when in luck, was such as to keep him continually on the
qui vive for a nucleus to build upon.

Beau, having exhausted the pockets and liberality of his contemporaries in Charleston, S.C.,
was constrained to “pitch his tent” in fresh pastures.  He therefore selected Abbeville, whither
he was immediately expedited by the agency of a “free pass.”

Snugly ensconced in his hotel, Beau ruminated over the means to raise the “plate.”  The bar-keeper
was assailed, but he was discovered to have scruples (anomalous barkeeper!)  The landlord was
a “grum wretch,” with no soul for speculation.  The cornered “sport” was finally reduced to
the alternative of “confidence of operation.”  Having arranged his scheme, he rented him a
precious negro boy, and borrowed an old theodolite.  Thus equipped, Beau betook himself to
the abode of a neighbouring planter, notorious for his wealth, obstinacy, and ignorance.  Operations
were commenced by sending the nigger into the planter’s barn-yard with a flagpole.  Beau got
himself up into a charming tableau, directly in front of the house.  He now roared at the top
of his voice, “72,000,000—51—8—11.”

After which he went to driving small stakes, in a very promiscuous manner, about the premises.

The planter hearing the shouting, and curious to ascertain the cause, put his head out of the
window.

“Now,” said Beau, again assuming his civil engineering pose, “go to the right a little further—there,
that’ll do.  47,000—92—5.”

“What the d---l are you doing in my barn-yard?” roared the planter.

Beau would not consent to answer this interrogation, but pursuing his business, hallooed out
to his “nigger”—

“Now go to the house, place your pole against the kitchen door, higher—stop at that.  86—45—6.”

“I say there,” again vociferated the planter, “get out of my yard.”

“I’m afraid we will have to go right through the house,” soliloquized Beau.

“I’m d--d if you do,” exclaimed the planter.

Beau now looked up for the first time, accosting the planter with a courteous—

“Good day, sir.”

“Good d---l, sir; you are committing a trespass.”

“My dear friend,” replied Beau, “public duty, imperative—no trespass—surveying railroad—State
job—your house in the way.  Must take off one corner, sir,—the kitchen part—least value—leave
the parlour—delightful room to see the cars rush by twelve times a day—make you accessible
to market.”

Beau, turning to the nigger, cried out—

“Put the pole against the kitchen door again—so, 85.”

“I say, stranger,” interrupted the planter, “I guess you ain’t dined.  As dinner’s up, suppose
you come in, and we’ll talk the matter over.”

Beau, delighted with the proposition, immediately acceded, not having tasted cooked provisions
that day.

“Now,” said the planter, while Beau was paying marked attention to a young turkey, “it’s mighty
inconvenient to have one’s homestead smashed up, without so much as asking the liberty.  And
more than that, if there’s law to be had, it shan’t be did either.”

“Pooh! nonsense, my dear friend,” replied Beau, “it’s the law that says the railroad must be
laid through kitchens.  Why, we have gone through seventeen kitchens and eight parlours in
the last eight miles—people don’t like it, but then it’s law, and there’s no alternative, except
the party persuades the surveyor to move a little to the left, and as curves costs money most
folks let it go through the kitchen.”

“Cost something, eh?” said the planter, eagerly catching at the bait thrown out for him.  “Would
not mind a trifle.  You see I don’t oppose the road, but if you’ll turn to the left and it
won’t be much expense, why I’ll stand it.”

“Let me see,” said Beau, counting his fingers, “forty and forty is eighty, and one hundred.
Yes, two hundred dollars will do it.” Unrolling a large map, intersected with lines running
in every direction, he continued—“There is your house, and here’s the road.  Air line.  You
see to move to the left we must excavate this hill.  As we are desirous of retaining the goodwill
of parties residing on the route, I’ll agree on the part of the company to secure the alteration,
and prevent your house from being molested.”

The planter revolved the matter in his mind for a moment and exclaimed:—

“You’ll guarantee the alteration?”

“Give a written document.”

“Then it’s a bargain.”

The planter without more delay gave Beau an order on his city factor for the stipulated sum,
and received in exchange a written document, guaranteeing the freedom of the kitchen from any
encroachment by the C. L. R. R. Co.

Before leaving, Beau took the planter on one side and requested him not to disclose their bargain
until after the railroad was built.

“You see, it mightn’t exactly suit the views of some people—partiality, you know.”

The last remark, accompanied by a suggestive wink, was returned by the planter in a similar
demonstration of owlishness.

Beau resumed his theodolite, drove a few stakes on the hill opposite, and proceeded onward
in the fulfilment of his duties.  As his light figure receded into obscurity and the distance,
the planter caught a sound vastly like 40—40—120—200.—And that was the last he ever heard of
the railroad.

                              Appleton’s American Railway Anecdote Book.

MR. FRANK BUCKLAND’S FIRST RAILWAY JOURNEY.

Mr. Spencer Walpole remarks:—“Of Mr. Buckland’s Christ Church days many good stories are told.
Almost every one has heard of the bear which he kept at his rooms, of its misdemeanours, and
its rustication.  Less familiar, perhaps, is the story of his first journey by the Great Western.
The dons, alarmed at the possible consequences of a railway to London, would not allow Brunel
to bring the line nearer than to Didcot. Dean Buckland in vain protested against the folly
of this decision, and the line was kept out of harm’s way at Didcot.  But, the very day on
which it was opened, Mr. Frank Buckland, with one or two other undergraduates, drove over to
Didcot, travelled up to London, and returned in time to fulfil all the regulations of the university.
The Dean, who was probably not altogether displeased at the joke, told the story to his friends
who had prided themselves in keeping the line from Oxford.  ‘Here,’ he said, ‘you have deprived
us of the advantage of a railway, and my son has been up to London.’”

SCENE BEFORE A SUB-COMMITTEE ON STANDING ORDERS. PETITIONING AGAINST A RAILWAY BILL, 1846.

“Well, Snooks,” began the Agent for the Promoters, in cross-examination, “you signed the petition
against the Bill—aye?”

“Yees, zur.  I zined summit, zur.”

“But that petition—did you sign that petition?”

“I do’ant nar, zur; I zined zummit, zur.”

“But don’t you know the contents of the petition?”

“The what, zur?”

“The contents; what’s in it.”

“Oa!  Noa, zur.”

“You don’t know what’s in the petition!—Why, ain’t you the petitioner himself?”

“Noa, zur, I doan’t nar that I be, zur.”

[“Snooks!  Snooks!  Snooks!” issued a voice from a stout and benevolent-looking elderly gentleman
from behind, “how can you say so, Snooks?  It’s your petition.”  The prompting, however, seemed
to produce but little impression upon him for whom it was intended, whatever effect it may
have had upon the minds of those whose ears it reached, but for whose service it was not intended].

“Really, Mr. Chairman,” observed the Agent for the Bill, who appeared to have no idea of Burking
the inquiry, “this is growing interesting.”

“The interest is all on your side,” remarked the Agent for the petition (against the Bill).

“Now, Snooks,” continued the Agent for the Bill, “apply your mind to the questions I shall
put to you, and let me caution you to reply to them truly and honestly.  Now, tell me—who got
you to sign this petition?”

“I object to the question,” interposed the Agent for the petition.  “The matter altogether
is descending into mean, trivial, and unnecessary details, which I am surprised my friend opposite
should attempt to trouble the Committee with.”

“I can readily understand, sir,” replied the other, “why my friend is so anxious to get rid
of this inquiry—simple and short as it will be; but I trust, sir, that you will consider it
of sufficient importance to allow it to proceed.  I purpose to put only a few questions more
on this extraordinary petition against the Bill (the bare meaning of the name of which the
petitioner does not seem to understand) for the purpose of eliciting some further information
respecting it.”

The Committee being thus appealed to by both parties, inclined their heads for a few moments
in order to facilitate a communication in whispers, and then decided that the inquiry might
proceed.  It was evident that the matter had excited an interest in the minds and breasts of
the honourable members of the Committee; created as much perhaps by the extreme mean and poverty-stricken
appearance of the witness—a miserable, dirty, and decrepit old man—as by the disclosures he
had already made.

“Well, Snooks, I was about to ask you (when my friend interrupted me) who got you to sign the
petition, or that zummit as you call it?”

“Some genelmen, zur.”

“Who were they—do you know their names?”

“Noa, zur, co’ant say I do nar ’em a’, zur.”

“But do you know any of them, was that gentleman behind you one?”

[The gentleman referred to was the fine benevolent-looking individual who had previously kindly
endeavoured to assist the witness in his answers, and who stood the present scrutiny with marked
composure and complaisance].

“Yees, zur, he war one on ’em.”

“Do you know his name?”

“Noa, zur, I doant; but he be one of the railway genelmen.”

“What did he say to you, when he requested you to sign the petition?”

“He said I ware to zine (pointing to the petition) that zummit.”

“When and where, pray, did you sign it?”

“A lot o’ railway genelmen kum to me on Sunday night last; and they wo’ make me do it, zur.”

“On Sunday night last, aye!”

“What, on Sunday night!” exclaimed one honourable member on the extreme right of the Chairman,
with horror depicted on his countenance; “are you sure, witness, that it was done in the evening
of a Sabbath?”

“The honourable member asks you, whether you are certain that you were called upon by the railway
gentlemen to sign the petition on a Sunday evening?  I think you told me last Sunday evening.”

“Oa, yees, zur; they kum just as we war a garing to chapel.”

“Disgraceful, and wrong in the extreme!” ejaculated the honourable member.

“And did not that gentleman” (continued the Agent for the Bill), “nor any of the railway gentlemen,
as you call them, when they requested you to sign, explain the nature and contents of the petition?”

“Noa, zur.”

“Then you don’t know at this moment what it’s for?”

“Noa, zur.”

“Of course, therefore, it’s not your petition as set forth?”

“I doant nar, zur.  I zined zummit.”

“Now, answer me, do you object to this line of railway?  Have you any dislike to it?”

“O, noa, zur.  I shud loak to zee it kum.”

“Exactly, you should like to see it made.  So you have been led to petition against it, though
you are favourable to it?”

The petitioner against the Bill did not appear to comprehend the precise drift of the remark,
and his only reply to the wordy fix into which the learned agent had drawn him was made in
the dumb-show of scratching with his one disengaged hand (the other being employed in holding
his hat) his uncombed head—an operation that created much laughter, which was not damped by
the Agent’s putting, with a serious face, a concluding question or remark to him to the effect
that he presumed he (the witness) had not paid, or engaged to pay, so many guineas a day to
his friend on the other side for the prosecution of the opposition against the Bill—had he;
yes, or no?  The witness’s appearance was the only and best answer.

The petition, of course, upon this exposé, was withdrawn.

This, the substance of what actually took place before one of the Sub-Committees on Standing
orders will give some idea of the nature of many of the petitions against Railway Bills, especially
on technical points.  It will serve to show in some measure what heartless mockeries these
petitions mostly are; the moral evils they give birth to—and that, even while complaining of
errors, they are themselves made up of falsehood.

AN IDEA ON RAILWAYS.

A happy comment on the annihilation of time and space by locomotive agency, is as follows:—A
little child who rode fifty miles in a railway train, and then took a coach to her uncle’s
house, some five miles further, was asked on her arrival if she came by the cars.  “We came
a little way in the cars, and all the rest of the way in a carriage.”

BURNING THE ROAD CLEAR.

It is related of Colonel Thomas A. Scott, that on one occasion, when making one of his swift
trips over the American lines under his control, his train was stopped by the wreck of a goods
train.  There was a dozen heavily loaded covered trucks piled up on the road, and it would
take a long time to get help from the nearest accessible point, and probably hours more to
get the track cleared by mere force of labour.  He surveyed the difficulty, made a rough calculation
of the cost of a total destruction of the freight, and promptly made up his mind to burn the
road clear.  By the time the relief train came the flames had done their work and nothing remained
but to patch up a few injuries done to the track so as to enable him to pursue his way.

HARSH TREATMENT OF A MAN OF COLOUR.

