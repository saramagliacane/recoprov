﻿He now, however, at length bent himself, with the whole strength of his mind, and the whole
force of his empire, to prepare for this final and decisive undertaking. The gun-boats in the
Bay of Gibraltar, where calms are frequent, had sometimes in the course of the former war been
able to do considerable damage to the English vessels of war, when they could not use their
sails. Such small craft, therefore, were supposed the proper force for covering the intended
descent. They were built in different harbours, and brought together by crawling along the
French shore, and keeping under the protection of the batteries, which were now established
on every cape, almost as if the sea-coast of the channel on the French side had been the lines
of a besieged city, no one point of which could with prudence be left undefended by cannon.
Boulogne was pitched upon as the centre port, from which the expedition was to sail. By incredible
exertions, Bonaparte had rendered its harbour and roads capable of containing two thousand
vessels of various descriptions. The smaller sea-ports of Vimereux, Ambleteuse, and Etaples,
Dieppe, Havre, St. Valeri, Caen, Gravelines, and Dunkirk, were likewise filled with shipping.
Flushing and Ostend were occupied by a separate flotilla. Brest, Toulon, and Rochefort, were
each the station of as strong a naval squadron as France, had still the means to send to sea.

A land army was assembled of the most formidable description, whether we regard the high military
character of the troops, the extent and perfection of their appointments, or their numerical
strength. The coast, from the mouth of the Seine to the Texel, was covered with forces; and
Soult, Ney, Davoust, and Victor, names that were then the pride and the dread of war, were
appointed to command the army of England, (for that menacing title was once more, assumed,)
and execute those manoeuvres, planned and superintended by Bonaparte, the issue of which was
to be the blotting out of Britain from the rank of independent nations.

Far from being alarmed at this formidable demonstration of force, England prepared for her
resistance with an energy becoming her ancient rank in Europe, and far surpassing in its efforts
any extent of military preparation before heard of in her history. To nearly one hundred thousand
troops of the line, were added eighty thousand and upwards of militia, which scarce yielded
to the regulars in point of discipline. The volunteer force, by which every citizen was permitted
and invited to add his efforts to the defence of the country, was far more numerous than during
the last war, was better officered also, and rendered every way more effective. It was computed
to amount to three hundred and fifty thousand men, who, if we regard the shortness of the time
and the nature of the service, had attained considerable practice in the use and management
of their arms. Other classes of men were embodied, and destined to act as pioneers, drivers
of wagons, and in the like services. On a sudden, the land seemed converted to an immense camp,
the whole nation into soldiers, and the good old king himself into a general-in-chief. All
peaceful considerations appeared for a time to be thrown aside; and the voice, calling the
nation to defend their dearest rights, sounded not only in Parliament, and in meetings convoked
to second the measures of defence, but was heard in the places of public amusement, and mingled
even with the voice of devotion--not unbecoming surely, since to defend our country is to defend
our religion.

Beacons were erected in conspicuous points, corresponding with each other, all around and all
through the island; and morning and evening, one might have said, every eye was turned towards
them to watch for the fatal and momentous signal. Partial alarms were given to different places,
from the mistakes to which such arrangements must necessarily be liable; and the ready spirit
which animated every species of troops where such signals called to arms, was of the most satisfactory
description, and afforded the most perfect assurance, that the heart of every man was in the
cause of his country.

Amidst her preparations by land, England did not neglect or relax her precautions on the element
she calls her own. She covered the ocean with five hundred and seventy ships of war of various
descriptions. Divisions of her fleet blocked up every French port in the channel; and the army
destined to invade our shores, might see the British flag flying in every direction on the
horizon, waiting for their issuing from the harbour, as birds of prey may be seen floating
in the air above the animal which they design to pounce upon. Sometimes the British frigates
and sloops of war stood in, and cannonaded or threw shells into Havre, Dieppe, Granville, and
Boulogne itself. Sometimes the seamen and marines landed, cut out vessels, destroyed signal
posts, and dismantled batteries. Such events were trifling, and it was to be regretted that
they cost the lives of gallant men; but although they produced no direct results of consequence,
yet they had their use in encouraging the spirits of our sailors, and damping the confidence
of the enemy, who must at length have looked forward with more doubt than hope to the invasion
of the English coast, when the utmost vigilance could not prevent their experiencing insults
upon their own.

During this period of menaced attack and arranged defence, Bonaparte visited Boulogne, and
seemed active in preparing his soldiers for the grand effort. He reviewed them in an unusual
manner, teaching them to execute several manoeuvres by night; and experiments were also made
upon the best mode of arranging the soldiers in the flat-bottomed boats, and of embarking and
disembarking them with celerity. Omens were resorted to for keeping up the enthusiasm which
the presence of the First Consul naturally inspired. A Roman battle-axe was said to be found
when they removed the earth to pitch Bonaparte's tent or barrack; and medals of William the
Conqueror were produced, as having been dug up upon the same honoured spot. These were pleasant
bodings, yet perhaps did not altogether, in the minds of the soldiers, counterbalance the sense
of insecurity impressed on them by the prospect of being packed together in these miserable
chaloupes, and exposed to the fire of an enemy so superior at sea, that during the chief consul's
review of the fortifications, their frigates stood in shore with composure, and fired at him
and his suite as at a mark. The men who had braved the perils of the Alps and of the Egyptian
deserts, might yet be allowed to feel alarm at a species of danger which seemed so inevitable,
and which they had no adequate means of repelling by force of arms.

A circumstance which seemed to render the expedition in a great measure hopeless, was the ease
with which the English could maintain a constant watch upon their operations within the port
of Boulogne. The least appearance of stir or preparation, to embark troops, or get ready for
sea, was promptly sent by signal to the English coast, and the numerous British cruisers were
instantly on the alert to attend their motions. Nelson had, in fact, during the last war, declared
the sailing of a hostile armament from Boulogne to be a most forlorn undertaking, on account
of cross tides and other disadvantages, together with the certainty of the flotilla being lost
if there were the least wind west-north-west. "As for rowing," he adds, "that is impossible.--It
is perfectly right to be prepared for a mad government," continued this incontestable judge
of maritime possibilities; "but with the active force which has been given me, I may pronounce
it almost impracticable."

Before quitting the subject, we may notice, that Bonaparte seems not to have entertained the
least doubts of success, could he have succeeded in disembarking his army. A single general
action was to decide the fate of England. Five days were to bring Napoleon to London, where
he was to perform the part of William the Third; but with more generosity and disinterestedness.
He was to call a meeting of the inhabitants, restore them what he calls their rights, and destroy
the oligarchical faction. A few months would not, according to his account, have elapsed, ere
the two nations, late such determined enemies, would have been identified by their principles,
their maxims, their interests. The full explanation of this gibberish, (for it can be termed
no better, even proceeding from the lips of Napoleon,) is to be found elsewhere, when he spoke
a language more genuine than that of the Moniteur and the bulletins. "England," he said, "must
have ended, by becoming an appendage to the France of my system. Nature has made it one of
our islands, as well as Oleron and Corsica."

It is impossible not to pursue the train of reflections which Bonaparte continued to pour forth
to the companion of his exile, on the rock of Saint Helena. When England was conquered, and
identified with France in maxims and principles, according to one form of expression, or rendered
an appendage and dependency, according to another phrase, the reader may suppose that Bonaparte
would have considered his mission as accomplished. Alas! it was not much more than commenced.
"I would have departed from thence [from subjugated Britain] to carry the work of European
regeneration [that is, the extention of his own arbitrary authority] from south to north, under
the Republican colours, for I was then Chief Consul, in the same manner which I was more lately
on the point of achieving it under the monarchical forms." When we find such ideas retaining
hold of Napoleon's imagination, and arising to his tongue after his irretrievable fall, it
is impossible to avoid exclaiming, Did ambition ever conceive so wild a dream, and had so wild
a vision ever a termination so disastrous and humiliating!

It may be expected that something should be here said, upon the chances which Britain would
have had of defending herself successfully against the army of invaders. We are willing to
acknowledge that the risk must have been dreadful; and that Bonaparte, with his genius and
his army, must have inflicted severe calamities upon a country which had so long enjoyed the
blessings of peace. But the people were unanimous in their purpose of defence, and their forces
composed of materials to which Bonaparte did more justice when he came to be better acquainted
with them. Of the three British nations, the English have since shown themselves possessed
of the same steady valour which won the fields of Cressy and Agincourt, Blenheim and Minden--the
Irish have not lost the fiery enthusiasm which has distinguished them in all the countries
of Europe--nor have the Scots degenerated from the stubborn courage with which their ancestors
for two thousand years maintained their independence against a superior enemy. Even if London
had been lost, we would not, under so great a calamity, have despaired of the freedom of the
country; for the war would in all probability have assumed that popular and national character
which sooner or later wears out an invading army. Neither does the confidence with which Bonaparte
affirms the conviction of his winning the first battle, appear go certainly well founded. This,
at least, we know, that the resolution of the country was fully bent up to the hazard; and
those who remember the period will bear us witness, that the desire that the French would make
the attempt, was a general feeling through all classes, because they had every reason to hope
that the issue might be such as for ever to silence the threat of invasion.

The next most important occurrence that claims our notice in this volume, and which fully delineates
the nature and character of this wonderful and ambitious individual, is the account of his
declaration as Emperor of France, and his subsequent Coronation.

CORONATION OF NAPOLEON.

Measures were taken, as on former occasions, to preserve appearances, by obtaining, in show
at least, the opinion of the people, on this radical change of their system. Government, however,
were already confident of their approbation, which, indeed, had never been refused to any of
the various constitutions, however inconsistent, that had succeeded each other with such rapidity.
Secure on this point, Bonaparte's accession to the empire was proclaimed with the greatest
pomp, without waiting to inquire whether the people approved of his promotion or otherwise.
The proclamation was coldly received, even by the populace, and excited little enthusiasm.
It seemed, according to some writers, as if the shades of D'Enghien and Pichegru had been present
invisibly, and spread a damp over the ceremony. The Emperor was recognised by the soldiery
with more warmth. He visited the encampments at Boulogne, with the intention, apparently, of
receiving such an acknowledgment from the troops as was paid by the ancient Franks to their
monarchs, when they elevated them on their bucklers. Seated on an iron chair, said to have
belonged to king Dagobert, he took his place between two immense camps, and having before him
the Channel and the hostile coasts of England. The weather, we have been assured, had been
tempestuous, but no sooner had the Emperor assumed his seat, to receive the homage of his shouting
host, than the sky cleared, and the wind dropt, retaining just breath sufficient gently to
wave the banners. Even the elements seemed to acknowledge the imperial dignity, all save the
sea, which rolled as carelessly to the feet of Napoleon as it had formerly done towards those
of Canute the Dane.

The Emperor, accompanied with his Empress, who bore her honours both gracefully and meekly,
visited Aix-la-Chapelle, and the frontiers of Germany. They received the congratulations of
all the powers of Europe, excepting England, Russia, and Sweden, upon their new exaltation;
and the German princes, who had everything to hope and fear from so powerful a neighbour, hastened
to pay their compliments to Napoleon in person, which more distant sovereigns offered by their
ambassadors.

