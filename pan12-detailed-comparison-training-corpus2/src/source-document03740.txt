﻿The impulsive individual must be rushed. His emotions are very responsive, easily aroused,
and, as, a rule, when aroused take a strong hold upon him. It is the impulsive person's tendency
always to act quickly and to act in response to his strong feelings. The impulsive man discharges
his feelings with speed in action, and they rapidly evaporate. Therefore, desire, when aroused,
must be quickly ripened into decision and action or it soon cools, and it is too late. As a
general rule, the impulsive person is well supplied with fears, and if he is given time to
think the matter over his lack of courage begins to assert itself. Fears of possible or impossible
disaster begin to take form until the feelings of fear and apprehension entirely overshadow
the desires which have been created.

Mark Twain's story of his attendance at a missionary meeting is typical. After the speaker
had been talking for half an hour, Mark was in such hearty sympathy with him and the cause
for which he plead that he decided to put one dollar in the collection box when it came around--but
the man kept on talking. At the end of three-quarters of an hour, Mark decided he would give
only fifty cents. At the end of an hour, he decided that he would give nothing, and when, at
the end of an hour and a half, the collection box finally did come around, Mark took out a
dollar to pay himself for his pains.

INDICATIONS OF IMPULSIVENESS

Here are some of the indications of impulsiveness: blonde coloring, especially if accompanied
by a florid skin; small, round, retreating chin; small size; fineness of texture; elasticity
of consistency; short head; short, smooth fingers, with tapering tips; a keen, alert, intense
expression. The impulsive person's movements are also impulsive. He walks with a quick step,
sometimes almost jerky. His gestures are quick, and if he is very impulsive, he always has
the air of starting to do things before he has properly considered what he is going to do.

THE DELIBERATE MAN

The deliberate individual is the opposite of the impulsive. His feelings may be strong, but
he has them well under control. He may think slowly or he may think quickly, but he always
acts with deliberation and always after he has thought very carefully. Once he has determined
to act, he may act far more energetically, and certainly more persistently, than the impulsive
person. The thing to remember about him is that he is constitutionally opposed to hasty decision
and action. Even when his mind is made up and his desires are strong, he is very likely to
postpone action until his resolution has had an opportunity to harden. Oftentimes these deliberate
people are, or seem to be, incorrigible procrastinators. It is useless to try to rush them.
Give them time to think and consider.

INDICATIONS OF DELIBERATION

These are some of the indications of deliberation: dark coloring, with an inclination to pallor;
a long, strong, prominent chin and well-developed jaw; large size; medium or coarse texture;
hard consistency; a long, square head; long, knotty fingers, with square tips; slow, deliberate,
rhythmical movements; a calm, poised expression, and either an absence of gesture or gesture
of a slow, graceful character.

Looking around amongst your friends and acquaintances, you will readily see that few, if any,
have all of the characteristics of impulsiveness in a marked degree, and an equally small number
all of the characteristics of deliberation in a marked degree. The majority of people probably
have a combination of these characteristics--some indications of impulsiveness and some of
deliberation. In such cases, the question is answered by a preponderance of evidence.

OBSTINATE PEOPLE

Some people are remarkably obstinate. If given their own way, they are agreeable and amiable,
but when opposed, they are exceedingly difficult to persuade. If such persons are of the positive
type and like to feel that they are doing the thing and that no one else is influencing or
coercing them, then they must be handled by an adroit suggestion similar in principle to that
described in the case of the automobile salesman on page 380. On the other hand, in case these
obstinate people are somewhat negative in character, without much initiative or aggressiveness
but with a very large degree of stubbornness, then care must be taken not to antagonize them
or to oppose them--always gently to lead them and never to try to drive them.

Argument is probably the most useless waste of energy possible in attempting persuasion. Your
own experience teaches you that argument only leaves each party to the controversy more strongly
convinced than ever that he is right. This is true no matter what the character of the arguers
be. It is especially and most emphatically true when either one or the other, or both, who
participate in the argument are of the obstinate type.

The obstinate person may be amenable to reason if reasons are stated calmly, tactfully, and
without arousing his opposition. His emotions of love, sympathy, generosity, desire for power
and authority may be successfully appealed to and he may be gently led to a decision by way
of minor and seemingly insignificant points.

INDICATIONS OF OBSTINACY

These are the indications of obstinacy: dark coloring; a prominent chin; a head high in the
crown; hard consistency; a rigidity of the joints, especially of the joints in the hands and
fingers. Perhaps the most important and most easily recognized indication of a domineering,
obstinate, determined will is the length of line from the point of the chin to the crown of
the head. When this line greatly exceeds in length that from the nape of the neck to the hair
line at the top of the forehead, you have an individual who desires to rule and bitterly resents
any attempt on the part of others to rule him.

The indications of a positive, aggressive, dominating will are these: blonde color; prominent
chin; a large, bony nose, high in the bridge; high forehead, prominent at the brows and retreating
as it rises; medium or small size; medium fine, medium or coarse texture; hard consistency,
rigid joints; a head wide just above and also behind the ears and high in the crown; a keen,
penetrating, intense expression of the eyes, and positive, decided tones of voice, movements
and gestures.

The individual who is negatively stubborn may have a small or sway-back nose; may have a high
forehead, flat at the brows and prominent above; may have elastic or soft consistency; may
have a head narrow above and behind the ears. Obstinacy will be shown in the length of line
from the point of chin to the crown of head and in the rigidity of the joints of the hands
and fingers.

THE INDECISIVE

The gentleman mentioned at the opening of this chapter belongs to the indecisive class.

We have often watched boys in swimming. In every crowd there are always a few of these timorous
mortals who "shiver on the brink and fear to launch away." As a general rule, some of their
companions usually come up behind them and give them a strong push, after which they are pleased
and happy enough in the water. We have seen boys who seemed to be waiting for someone to push
them in. No doubt they were. Certain it is that grown up men and women who suffer in an agony
of indecision usually like to have someone take the matter out of their hands.

In the case of the gentleman to whom we have referred in the opening of this chapter, the real
estate agent one day walked into his office, laid a contract down on the desk in front of him,
and said, very impressively: "This thing has got to be settled up to-day. Just sign your name
right there." And, with a feeling of intense relief and satisfaction, our friend did sign his
name "right there." To the best of our knowledge and belief, he has been glad of it ever since.

HOW ONE SALESMAN OVERCAME INDECISION

We once knew a salesman of the positive, domineering type. He was selling an educational work.
Now, education is a thing everyone needs but few will take the trouble and find the money to
purchase unless they are very strongly persuaded. Men who would readily spend fifty or seventy-five
dollars for a night's carousal will hesitate, and find objections, and back and fill for weeks,
or even for months, before they spend thirty or forty dollars on a bit of education which they
well know they ought to have. Our friend, therefore, was met over and over again with the temporizing
excuse: "Well, I will have to think this matter over. I cannot decide it to-day, but you come
in and see me again." Almost without exception, this excuse means that the man who makes it
knows, deep down in his heart, that he ought to make his decision--that he will profit by it
in many ways. He fully intends to make his decision some time, or else he would not ask the
salesman to come back and see him again. But he is a little weak-kneed. He lacks something
in decisiveness. Our friend treated practically all of these indecisive prospects of his in
the same way.

"I am sorry," he would say, "but I can't come back to see you again. My time is limited. There
are plenty of people who want to know about my proposition and who are eager to take it. I
must get around and see them. I can't afford to go back on my track and spend time with people
to whom I have already explained the whole thing. You want this and you know you want it. You
intend to have it, or you would not ask me to come back and see you again. There is no good
reason why you should not have it now, and you know there is not. Furthermore, if you do not
take it now and I do not come back to see you--and I won't--then you will never take it. That's
plain enough. You feel more like taking it right now, to-day, while I am talking to you, than
you will later, when you have forgotten half of what I have said. If there is any question
you want to ask about this, ask me now and I will answer it. But there isn't any, because I
have already answered your questions. You are satisfied. Your mind is made up. There is no
reason for delay--just sign your name right there, please." And only about four per cent of
those to whom he talked that way refused to sign when he told them to.

The indecisive person wants someone always to decide for him. If you are trying to persuade
such a person, then you must decide for him. Do it as tactfully as you can. Sometimes these
people want others to decide for them and, at the same time, to make the situation look as
if they had decided for themselves. They realize their own indecisiveness. They are ashamed
of it, and they do not like to be reminded of it. Rare, indeed, is the person who has all of
these indications. So rare, in fact, that he is scarcely a normal being if he has them all
in a marked degree.

THE BALANCED TYPE

There are some people of an evenly balanced type. They are neither violently impulsive nor
ponderously deliberate. They are interested in facts and pass their judgment upon them, but
they are also interested in theories and willing to listen to them. They are practical and
matter-of-fact, but they also have ideals. They have clean, powerful emotions, fairly well
controlled, and yet, when their judgment has been satisfied, they are perfectly willing to
act in response to their feelings. They are neither easy, credulous and impulsive nor suspicious,
obstinate and procrastinating. The way to persuade them is first to present the facts and show
them the reasons why. Then, by suggestion and word-painting, to stimulate their desire and
give them an opportunity to decide and act. Such people are medium in color, with forehead,
nose, mouth and chin inclining to the straight line; medium in size; medium in build; fine
or medium fine in texture; elastic in consistency; moderately high, wide, long, square head;
a pleasant but calm and sensible expression of face and eyes; quiet, well-timed walk and gestures;
well-modulated voice.

THE EASY MARK

When the person to be persuaded is indecisive and also has large, wide-open, credulous eyes;
a hopeful, optimistic, turned-up nose, and a large, round dome of a head just above the temples,
he is the living image of the champion easy mark. What he needs is not so much to be persuaded
as to be protected against himself. He, and the greedy, grasping, cunning but short-sighted
individual, who is always trying to get something for nothing, constitute that very large class
of people of whom it has been said that there is one born every minute.

ADVANTAGE OF PERSUADER'S POSITION

In closing this chapter, we cannot forego the opportunity for a word of counsel to you in your
efforts to persuade others. Remember that if you do your work well in securing favorable attention,
arousing interest, and creating desire, the person with whom you are dealing is like a man
standing on one foot, not quite knowing which way he will go. Even if he is more or less obstinate
and should be on both his feet, he is at least standing still and considering which direction
he will take. If this is not true, then you have failed to create a desire, or, having created
it, have not augmented it until it is strong enough. But, granting that this is true, do you
not see what an advantage it gives you? The man who is standing on one foot, undecided, is
quickly pulled or pushed in the way you want him to go if you yourself vigorously desire it.
Even the man who stands obstinately on both feet is at a disadvantage if he does not know which
way to go, and you very decidedly know which way you want him to go.

THE VALUE OF COURAGE

We have seen more sales skillfully brought up to the point of desire and then lost through
the indecision, the wavering, the fear, or the hesitation of the salesman than for any other
one cause. Of all of the qualities and characteristics which contribute to success in the persuasion
of others, there is, perhaps, none more powerful than that courage which gives calmness, surety
of touch, decisiveness, and unwavering, unhesitating action.

Some years ago we saw a huge mob surround a building in which a political speaker was trying
to talk upon an unpopular subject. The longer the mob remained waiting for their victim to
come out, the more violent and the more abusive it became. There was an angry hum, sounding
above the occasional cries and shouts, which betokened trouble. Presently a large man scrambled
upon the pedestal of a statue in front of the building and began to harangue the crowd. He
argued with them, he pleaded with them, he threatened them, he tried to cajole them. But through
it all he could scarcely make himself heard and the mob remained solidly packed about the door.
Then the police were brought and attempted to force a passageway for the escape of the speaker,
whose address inside the building was nearing a close. But the police were powerless and some
of them were badly hurt.

Then a quiet little man came down the steps of the building. He was dressed in ordinary clothing
and was unarmed. His open hands hung idly at his side. He stood near the bottom step, where
he could just look over the heads of the crowd. He stood perfectly still, perfectly calm, and
yet with a look of such iron resolution on his countenance as we have seldom seen. Those next
him grew strangely quiet. Then the semi-circle of silence spread until the entire mob stood
as if holding its breath waiting to see what this man would do.

"Make a passageway there," he said in a matter-of-fact tone of voice; "there is a carriage
coming through."

Instantly the crowd parted, a carriage was driven up to the steps, the speaker came down and
entered it, and it was driven rapidly away, followed only by a few hisses and cat-calls.

When all is said and done, that is the spirit which secures the decision and action of others.

CHAPTER V

EFFICIENT AND SATISFACTORY SERVICE

Marshall Nyall was an excellent workman. He was keen, quick of comprehension, practical in
his judgment, and unusually resourceful. He was energetic, industrious, and skillful. Being
blessed with considerable idealism, he took pride and pleasure in putting a fine artistic finish
on everything he did. He studied his work in all its aspects and was alert in finding ways
of saving time, materials, energy, and money. He was, therefore, personally efficient. As an
employee of the Swift Motor Company, he rose rapidly until he became superintendent. In that
position he made a good record. So valuable was he that the White Rapids Motor Company coveted
him and its president and general manager began to lay plans to entice him away. Negotiations
were begun and continued over a period of weeks. Larger and larger grew the inducements offered
by the White Rapids Motor Company until, finally, Nyall's employers felt that they could not
afford to meet them any longer, and this highly efficient man became works manager for the
White Rapids Motor Company, at a very greatly increased salary.

Now, the White Rapids Motor Company was larger and wealthier than the Swift Motor Company.
The position of works manager was a more important and responsible position than that of superintendent.
Nyall was accordingly delighted and had high ambitions as to his career with his new employers.

HOW THE TROUBLE STARTED

"You have a reputation," said the president and general manager to Nyall, "for efficiency.
Efficiency is what we want in the works here, and if you can put these factories on as efficient
a basis as you did the shops of the Swift Motor Company, your future is assured."

"I can do that all right, Mr. Burton," Nyall replied confidently, "provided I get the right
kind of co-operation from the front office."

"Call on us for anything you want, Nyall," returned the president sharply. He was a proud,
positive man. He loved power. He had the ability to lead and to rule, and he resented even
the slightest imputation that any lack of co-operation on his part might defeat his plans for
efficient management.

A few days later Nyall made some changes in the plan of routing the work through the factories.
These changes were rather radical and sweeping and necessitated a considerable initial expense.
Naturally, Burton was not long in hearing about it. Instantly he summoned his works manager.

"Haven't you begun your work here in a rather drastic manner?" he inquired. "Surely you have
not studied this situation carefully enough in a few days to justify you in making such sweeping
changes in the system which we have built up here after years of patient study and research.
I have given the routing of the work through the factories days and nights of careful study,
Nyall, during the years that we have been standardizing it. I believe that it was just as nearly
perfect as it can be just as we had it."

"Your system was all wrong, and I can prove it to you," returned Nyall. "Just wait a minute
until I bring you in my charts."

RUBBING IT IN

