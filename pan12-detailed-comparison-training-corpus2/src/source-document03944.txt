﻿Put half a pound of rice in hot milk till it has absorbed all it can and is tender. Beat lightly
the yolks of three eggs, beating in a lump of fresh butter the size of a pullet's egg; add
powdered sugar and the whites of the eggs well beaten. Put the rice into this mixture and place
all in a mold. Cook it gently for twenty-five minutes. Meanwhile take some very perfect yellow
plums, skin and stone them and heat them in half a bottle of light white wine that you have
seasoned with a little spice. Turn out the rice, put the yellow plums on the top and pour round
the sauce, strained through muslin. Very good cold.

BRABANT PANCAKE

Butter first of all your pancakes, and you should have proper pancake saucers fit to go to
table. Heat half a pint of sweetened milk and melt a quarter of a pound of salt butter with
it. When well melted pour it into a basin and sprinkle in nearly three ounces of flour. Beat
up the yolks of three large or four small eggs and incorporate them, then add the whites well
beaten. Put a spoonful or two on each saucer and set to bake in the oven for ten minutes and
when done place each saucer on a plate with a good lump of apricot jam on each. If you have
no pancake saucers, put the apricot preserve on one half of each pancake and fold it up.

[Jean O.]

DELICIOUS SAUCE FOR PUDDINGS

To a large wineglassful (say a glass for champagne wine) of new Madeira add the yolks only
of two eggs. Put in a very clean enamel saucepan over the fire and stir in powdered sugar to
your taste. Whisk it over the fire till it froths, but do not allow it even to simmer. Use
for Genoese cakes and puddings.

[Madame Groubet.]

FRUIT JELLIES

Jellies that are very well flavored can be made with fresh fruit, raspberries, strawberries,
apricots, or even rhubarb, using the proportions of one ounce of gelatine (in cold weather)
to every pound of fruit puree. In hot weather use a little less gelatine. As the fruit generally
gives a bad color, you must use cochineal for the red jellies and a little green coloring for
gooseberry jellies. The gelatine is of course melted in the fruit puree and all turned into
a mold. You can make your own green coloring in this way. Pick a pound of spinach, throwing
away the stalks and midrib. Put it on in a pan with a little salt and keep the cover down.
Let it boil for twelve minutes. Then put a fine sieve over a basin and pour the spinach water
through it. Strain the spinach water once or twice through muslin; it will be a good color
and will keep some time. Orange and lemon jellies are much more wholesome when made at home
than those made from bought powders. To the juice of every six oranges you should add the juice
of one lemon, and you will procure twice as much juice from the fruit if, just before you squeeze
it, you let it soak in hot water for three or four minutes.

[Pour la Patrie.]

STRAWBERRY FANCY

Take a slice or two of plain sponge cake and cut out rounds two inches across. Then whip up
in a basin the whites only of four eggs, coloring them with the thinner part of strawberry
jam. As a rule this jam is not red enough, and you must add a little cochineal. Put the pink
mixture in high piles on the cakes.

[Pour la Patrie.]

PINK RICE

This sweet is liked by children who are tired of rice pudding. Boil your rice and when tender
mix in with it the juice of a boiled beetroot to which some sugar has been added. Turn it into
a mold and when cold remove it and serve it with a spoonful of raspberry preserve on the top
or with some red plums round it.

[Pour la Patrie.]

MILITARY PRUNES

Take some of the best French preserved prunes, and remove the stones. Soak them in orange curacoa
for as long a time as you have at your disposal. Then replace each stone by a blanched almond,
and place the prunes in small crystal dishes.

[Pour la Patrie.]

MADELINE CHERRIES

Take some Madeleine cakes and scoop them out to form baskets. Fill these with stoned cherries
both white and black that you have soaked in a good liqueur--cherry brandy is the best but
you may use maraschino. Place two long strips of angelica across the top and where these intersect
a very fine stoned cherry.

[Pour la Patrie.]

STRAWBERRY TARTLETS

It often happens that you have among the strawberries a quantity that are not quite good enough
to be sent to table as dessert, and yet not enough to make jam of. Put these strawberries on
to heat, with some brown sugar, and use them to fill small pastry tartlets. Pastry cases can
be bought for very little at the confectioner's. Cover the top of the tartlet when the strawberry
conserve is cold with whipped cream.

[Pour la Patrie.]

MADEIRA EGGS OR OEUFS A LA GRAND'MERE

Break the yolk of an egg in a basin and be sure that it is very fresh; beat it up, adding a
little powdered sugar, and then, drop by drop, enough of the best Madeira to give it a strong
flavor. This makes a nice sweet served in glass cups and it is besides very good for sore throats.

[Pour la Patrie.]

BUTTERFLIES

You will get at the confectioner's small round cakes that are smooth on the top; they are plain,
and are about two and one-half inches across. Take one and cut it in halves, separating the
top from the bottom. Cut the top pieces right across; you have now two half moons. Put some
honey along the one straight edge of each half moon and stick it by that on the lower piece
of cake, a little to one side. Do the same with the second half moon, so that they both stick
up, not unlike wings. Fill the space between with a thick mixture of chopped almonds rolled
in honey, and place two strips of angelica poking forward to suggest antennae. A good nougat
will answer instead of the honey.

[Pour la Patrie.]

CHERRY AND STRAWBERRY COMPOTE

Take half a pint of rich cream and mix with it a small glassful of Madeira wine or of good
brandy. Pick over some fine cherries and strawberries, stoning the cherries, and taking out
the little center piece of each strawberry that is attached to the stalk. Lay your fruit in
a shallow dish and cover it with the liquor and serve with the long sponge biscuits known as
"langues de chat" (Savoy fingers).

[Amitie aux Anglais.]

CHOCOLATE CUSTARD

To make a nice sweet in a few minutes can be easily managed if you follow this recipe. Make
a custard of rich milk and yolks of eggs, sweeten it with sugar, flavored with vanilla, and
if you have a little cream add that also. Then grate down some of the best chocolate, as finely
as you can, rub it through coarse muslin so that it is a fine powder. Stir this with your custard,
always stirring one way so that no bubbles of air get in. When you have got a thick consistency
like rich cream, pour the mixture into paper or china cases, sprinkle over the tops with chopped
almonds. There is no cooking required.

GOOSEBERRY CREAM WITHOUT CREAM

Take your gooseberries and wash them well, cut off the stalk and the black tip of each. Stew
them with sugar till they are tender, just covered in water. Do not let them burn. If you have
not time to attend to that put them in the oven in a shallow dish sprinkled with brown sugar.
When tender rub them through a fine sieve at least twice. Flavor with a few drops of lemon
juice, and add sugar if required. Then beat up a fresh egg in milk and add as much arrowroot
or cornflour as will lie flat in a salt spoon. Mix the custard with the gooseberries, pass
it through the sieve once more and serve it in a crystal bowl.

[Mdlle. B-M.]

CHOCOLATE PUDDINGS

