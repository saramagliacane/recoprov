﻿The next expansion came in 1933 when the committee rooms adjacent to the main reading room
were taken over and portion of the walls removed to give an open area. In 1938 the Library
took over the remainder of the attic and portion of the first floor vacated by the Health Department.
Though other alterations were made to increase shelving, no further space was taken over until
1950 when a further committee room was given to the Library. About the same time earthquake
risk and alteration to the building caused the removal of books from a portion of the attic
to the basement where further space had been made available. Other rooms have more recently
been provided to store the books and periodicals in the Library and constant ingenuity is necessary
to see that the most economical use is made of the area available.

The reasons for the expansion of the Library can be found in the increased interest in libraries
generally, and in the increased vote which resulted. The fund received £600 until 1920 when
it was raised to £800. It was reduced to £700 in 1922 and remained at that figure until 1929
when it was raised to £900, though it suffered the depression cuts.

These amounts were not sufficient to adequately finance the purchase of books needed for the
service the Library was expected to give, and in 1938 the grant was once again raised, this
time to £1,250. Further increases were made in 1947 (£2,000), 1949 (£2,250), 1952 (£3,000),
and 1955 (£4,500).

In addition there has been considerable expansion in the exchange arrangements, Government
publishing having increased considerably in the United Kingdom and the United States. Arrangements
for the exchange of official publications with Australia were made in 1952, while during 1957
the Canadian Government made the General Assembly Library a select depository for its publications.

Another source of material for the Library has been by gift either of individual books or of
collections. They have been many and varied, and it is safe to say that the Library would not
possess the wide variety of stock it does had it not been for the kindness and generosity of
many donors.

                          COPYRIGHT DEPOSIT

The Copyright Act has also provided the Library with an increasing amount of material. Like
so many other of the Library's activities, this was foreshadowed in the days when James Collier
was Librarian. In his report for 1888, he suggested that the time was ripe for the enacting
of a Colonial Copyright Act. Whatever was done about this there was one thing that ought to
be done immediately and that was the passing of a law making provision for the deposit of one
copy of every colonial publication in a central library, which library could only be the Parliamentary
Library.

A letter was written from the chairman of the Library Committee to the Premier asking for instructions
to be given to the Solicitor-General to prepare a Copyright Act, but nothing was done. The
matter was raised again by the Acting Librarian in 1891 and 1894. In 1895 Mr W. Hutchison,
M.H.R. for Dunedin, introduced the Literary Copyright Act requiring the deposit in the Library
of two copies of works published in New Zealand. Nothing came of the Bill, which was discharged,
though the Library Committee in welcoming it had, however, considered one copy sufficient.

There the matter rested until 1903 when two vigorous supporters of the Library, the Hon. R.
McNab and the Hon. John Rigg, introduced the General Assembly Library Bill requiring publishers
to present two copies of their books to the Library. The Bill passed without difficulty and
became law on 30 October 1903. Though there was some argument whether the Act required the
deposit of issues of periodicals, the Act was generally welcomed, and increased the amount
of New Zealand material reaching the Library.

There has been little change in the provisions affecting deposit, though the previous Act is
no longer in force, and has been replaced by section 52 of the Copyright Act 1913.

In the 55 years during which deposit has been required the Library has taken its responsibility
for preservation seriously and now possesses thousands of volumes not only of books, but of
newspapers, periodicals, and pamphlets. In addition, every attempt has been made to obtain
material which for various reasons was not obtained at the time of publication. While not by
any means perfect, the New Zealand collection of the Library is probably without equal.

                           THE LIBRARIANS

Mr Charles Wilson had a considerable interest in literature as such and contributed a literary
column to a Wellington weekly for many years. Though he had an excellent knowledge of literature,
library technique generally in New Zealand was not at its best, and not all the work done in
the Library was of the highest standard.

He was responsible for further attempts to buy the more important New Zealand books still missing
from the Library and for housing them in special cases where they were available for consultation
but were not permitted to leave the Library. From this has grown the special New Zealand collection
with its own rooms.

Mr Wilson introduced the present system of alternate weekly shifts for the staff working nights.
Previously the staff worked broken shifts which meant that some often had "all nighters" without
breaks and were called on to make their appearance fairly early the following day. If the House
sits late, the present system relieves the night staff when the House rises or at 8 a.m. and
they are not required till 5.30 p.m.

Stocktaking was a major task of the staff. The Library did not possess a shelf list and the
system used was slow. It did, however, indicate that constant vigilance was necessary--and
still is--to prevent books going astray.

Mr James continued as Assistant Librarian until 1923. His later years were marked with frequent
periods of illness which told on the standard of his work.

Mr Wilson retired in March 1926 and his successor, Dr G. H. Scholefield, O.B.E., commenced
duties in May. He was even then the author of two books on New Zealand and the Pacific and
had been New Zealand Press Association representative in London. For the next 22 years the
Library was under his care. Hampered by depression and war, the development of the Library
was not as rapid as it could have been.

The principal change in the Library during this time was probably in the staff. Members of
the staff, mostly in senior positions, had held degrees, but generally they had not been recruited
from university graduates and had picked up such library technique as they could at work. A
university degree now became essential, and in addition, outside studies of library science
were favoured as being of value both to the member of the staff and to the Library. Mr A. D.
McIntosh, now head of the Department of External Affairs, for example, was given leave in 1932
after receiving a Carnegie grant to attend the Library School at the University of Michigan.

Dr Scholefield was also responsible for the introduction of women to the staff. Though a Mrs
North had been employed as a clerk for six months in 1900, the hours of duty had made the Library
a man's world. In 1926 Miss Q. B. Cowles, from the Turnbull Library, was the first of the many
young ladies who since then have been members of the staff.

The other change was in the reference service. The Library came to be called on more and more
for research and information. These calls came not only from members of Parliament, but also
from Government Departments and from the public. The staff naturally had to be more highly
trained to carry out these tasks and had to spend more time to answer the inquiries. After
Mr McIntosh's return the reference staff was reorganised and a collection of quick reference
books made. In addition, not only did the staff carry out research but it began to summarise
and rewrite the results of its research ready for immediate use by honourable members.

Dr Scholefield, with his keen interest in biography, was instrumental in obtaining for the
Library many collections of personal papers of New Zealand statesmen. Among these are the papers
of Sir John Hall, William Rolleston, and Sir Julius Vogel, not to mention the wonderful papers
written and collected by the Richmond and Atkinson families over nearly 50 years. These documents
are already proving valuable to political and historical scholars.

Dr Scholefield was also Controller of Dominion Archives and for some years these were housed
in the Library. During his period as Chief Librarian, in addition to several editions of Who's
Who in New Zealand, Dr Scholefield published his monumental Dictionary of New Zealand Biography
and two other works of biography.

On his retirement in October 1947 Dr Scholefield was succeeded by Mr W. S. Wauchop, M.A., who
had joined the staff in 1924 as Assistant Chief Librarian. Freed from the restraints of war,
and with a larger grant, the Library expanded rapidly. The Library Committee, which had for
some years taken a less important rôle in the control of the Library, once again came to the
fore. It was instrumental in obtaining much needed space and assisting generally in the progress
which took place.

Mr Wauchop was also responsible for obtaining the microfilm camera which is today reducing
the bulk of New Zealand newspapers received in the Library to manageable proportions for storage.
Great steps forward were also taken in the indexing of New Zealand newspapers and for the first
time in its history the Library had a complete index to all news in two (later three) of the
more important newspapers in the Dominion. Mr Wauchop retired at the beginning of 1955.

                       FIRE AND FIRE INSURANCE

No history would be complete without some mention of the fire of the early morning of 11 December
1907 which destroyed most of Parliament Buildings. It began in the old portion formerly occupied
by the Library at about 2 a.m. and rapidly spread to the Legislative Council on one side and
the House of Representatives on the other. Both these portions were of wood and burned fiercely.

Though the Library was in the brick portion, fire danger had still been considered to be great
so that earlier in the year the stackroom windows overlooking the courtyard had been bricked
up. In addition, the entrance door was protected by a steel blind.

It appeared at first that the Library was in no danger and no attempt was made to remove books.
Eventually, about 4 a.m. the roof of the new committee rooms and entrance was in danger of
catching alight, and Mr Wilson decided to clear the building. With the help of some of the
staff and the general public, some 15,000 volumes were taken either to the Government Buildings
or to houses in Hill Street. Though the rear portion of brick with wooden floors and partitions
caught fire about 5 a.m. and damage was done to the roof, the Library was seen to be in no
further danger and the clearance was stopped.

Some slight damage was done to these books, but insurance covered this, and generally little
damage was done to the Library itself. The removal of the wooden portion has reduced the risk
of fire considerably, and although the rear portion still has wooden floors, little of value
is stored here. If any future outbreak occurred it is probable that more damage would be done
by water. To prevent this a large drain was recently made in the basement to allow water to
escape readily.

After the fire there was some discussion on the possibility of using the reading room as the
Chamber of the House of Representatives, but Government House was finally chosen. The brick
building was repaired and a covered access way provided across Sydney Street from the Library
to the Chamber.

At the time of the fire the Library was insured for £4,000, a small portion of its true value.
This insurance was continued until 1928 when the cover was raised to £10,000, still much below
the cost of replacement. In 1942, with the introduction of war damage insurance and the consequent
increase of premiums, it was decided that the Library should, like other Government Departments,
not be insured, the Government carrying the risk itself.

                               GENERAL

Circulation of Books

The first library rules that can be discovered today are those for 1869. Though it is certain
that borrowing was permitted before this, members were permitted by these rules to borrow two
books for a period of a fortnight. Even so, the privilege of borrowing was restricted to the
session.

It is doubtful if the rules were strictly enforced for as early as 1873 Mr T. Kelly from New
Plymouth moved that the Library Committee should be instructed to allow members outside Wellington
the right to take out books and to keep them for two months. Though the motion was not approved
it appears that members residing in Wellington did have books at their homes.

No great change was made in the wording of the rules, but it appears that at the end of the
session members were taking books away, and in 1886 Mr James Macandrew from Dunedin admitted
doing so. In the recess of 1885-86 Sir James G. Wilson (Bulls) had written to the Librarian
asking for books to be sent to his home. The request was refused but following it the House
passed a motion recommending the Joint Committee to prepare regulations for lending books during
the recess to members living outside Wellington.

The Committee, however, did not favour the idea and reported that there were so many difficulties
in the way that they would not carry it out. On the motion of the Premier, Sir Robert Stout,
the House reluctantly agreed with the report.

There the matter rested until the session of 1891 when it was raised in a question addressed
to a Minister. As a result the Committee brought down a report saying that they had agreed
to a scheme for circulating up to six books at a time to members in the recess. Certain reference
and valuable books, newspapers, and periodicals were excluded, but most other works could be
borrowed. The Library would provide boxes or baskets for the transmission of the books, and
six dozen were obtained for the following recess. During it 34 members borrowed 438 volumes,
not one being lost, though two were damaged.

Both House and Council agreed to the scheme, though certain members were violently opposed
to it. Since then it has provided members with reading material during many recesses. Certainly,
some books have been lost, but probably there would be an even greater chance of losses if
the practice of recess borrowing had not been regularised. In any case, books often disappear
from the shelves in libraries with the best oversight and supervision and are never seen again.

Fiction

The provision of fiction in the Library has been criticised, but novels have been purchased
since the early seventies. The numbers purchased have always been small, and have given well
earned relaxation and pleasure to legislators as well as building up what is the only collection
of the minor nineteenth century classics that exists in the Dominion. These books are frequently
in demand by students of nineteenth century English literature.

Inter-library Loan

