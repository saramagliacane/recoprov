﻿It then became a question of defending the fatherland--our fair France--against all Europe.
They didn't like our laying down the law to the Russians, and our driving them back across
their borders, so that they couldn't devour us, as is the custom of the North. Those Northern
peoples are very greedy for the South, or at least that's what I've heard many generals say.
Then Napoleon saw arrayed against him his own father-in-law, his friends whom he had made kings,
and all the scoundrels whom he had put on thrones. Finally, in pursuance of orders from high
quarters, even Frenchmen, and allies in our own ranks, turned against us; as at the battle
of Leipsic. Common soldiers wouldn't have been mean enough to do that! Men who called themselves
princes broke their word three times a day.

Well, then came the invasion. Wherever Napoleon showed his lion face the enemy retreated; and
he worked more miracles in defending France than he had shown in conquering Italy, the East,
Spain, Europe, and Russia. He wanted to bury all the invaders in France, and thus teach them
to respect the country; so he let them come close to Paris, in order to swallow 'em all at
a gulp and rise to the height of his genius in a battle greater than all the others--a regular
mother of battles! But those cowardly Parisians were so afraid for their wretched skins and
their miserable shops that they opened the gates of the city. Then the good times ended and
the "ragusades" began. They fooled the Empress and hung white flags out of the palace windows.
Finally the very generals whom Napoleon had taken for his best friends deserted him and went
over to the Bourbons--of whom nobody had ever before heard. Then he bade us good-by at Fontainebleau.
"Soldiers!"

I can hear him, even now. We were all crying like regular babies, and the eagles and flags
were lowered as if at a funeral. And it was a funeral--the funeral of the Empire. His old soldiers,
once so hale and spruce, were little more than skeletons. Standing on the portico of his palace,
he said to us:

"Comrades! We have been beaten through treachery; but we shall all see one another again in
heaven, the country of the brave. Protect my child, whom I intrust to you. Long live Napoleon II!"

Like Jesus Christ before his last agony, he believed himself deserted by God and his star;
and in order that no one should see him conquered, it was his intention to die; but, although
he took poison enough to kill a whole regiment, it never hurt him at all--another proof, you
see, that he was more than man: he found himself immortal. As he felt sure of his business
after that, and knew that he was to be Emperor always, he went to a certain island for a while,
to study the natures of those people in Paris, who did not fail, of course, to do stupid things
without end.

While he was standing guard down there, the Chinese and those animals on the coast of Africa--Moors
and others, who are not at all easy to get along with--were so sure that he was something more
than man that they respected his tent, and said that to touch it would be to offend God. So
he reigned over the whole world, although those other fellows had sent him out of France.

Well, then, after a while he embarked again in the very same nut-shell of a boat that he had
left Egypt in, passed right under the bows of the English vessels, and set foot once more in
France. France acknowledged him; the sacred cuckoo flew from spire to spire; and all the people
cried, "Long live the Emperor!"

In this vicinity the enthusiasm for the Wonder of the Ages was most hearty. Dauphiny behaved
well; and it pleased me particularly to know that our own people here wept for joy when they
saw again his gray coat.

On the 1st of March Napoleon landed, with two hundred men, to conquer the kingdom of France
and Navarre; and on the 20th of the same month that kingdom became the French Empire. On that
day THE MAN was in Paris. He had made a clean sweep--had reconquered his dear France, and had
brought all his old soldiers together again by saying only three words: "Here I am." 'Twas
the greatest miracle God had ever worked. Did ever a man, before him, take an empire by merely
showing his hat? They thought that France was crushed, did they? Not a bit of it! At sight
of the Eagle a national army sprang up, and we all marched to Waterloo. There the Guard perished,
as if stricken down at a single blow. Napoleon, in despair, threw himself three times, at the
head of his troops, on the enemy's cannon, without being able to find death. The battle was lost.

That evening the Emperor called his old soldiers together, and, on the field wet with our blood,
burned his eagles and his flags. The poor eagles, who had always been victorious, who had cried
"Forward!" in all our battles, and who had flown over all Europe, were saved from the disgrace
of falling into the hands of their enemies. All the treasure of England couldn't buy the tail
of one of them. They were no more!

The rest of the story is well known to everybody. The Red Man went over to the Bourbons, like
the scoundrel that he is; France was crushed; and the old soldiers, who were no longer of any
account, were deprived of their dues and sent back to their homes, in order that their places
might be given to a lot of nobles who couldn't even march--it was pitiful to see them try!
Then Napoleon was seized, through treachery, and the English nailed him to a rock, ten thousand
feet above the earth, on a desert island in the great ocean. There he must stay until the Red
Man, for the good of France, gives him back his power. It is said by some that he is dead.
Oh, yes! Dead! That shows how little they know him! They only tell that lie to cheat the people
and keep peace in their shanty of a government.

This that I have told you is gospel truth; and all the other things that you hear about the
Emperor are foolish stories with no human likeliness. Because, you see, God never gave to any
other man born of woman the power to write his name in red across the whole world--and the
world will remember him forever. Long live Napoleon, the father of the soldiers and the people!