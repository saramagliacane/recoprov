﻿In 1853, young Arthur went to New York City, by the invitation of the Honorable Erastus D.
Culver, whose acquaintance he had made when that gentleman represented the Washington County
district, and Dr. Arthur was the pastor of the Baptist Church at Greenwich. Mr. Culver had
been noted in Congress as an advanced, anti-slavery man, and he was prompted to take an interest
in the son of a clergyman-constituent, who did not fear to express anti-slavery sentiments,
at a time when the occupants of pulpits were generally so conservative that they were dumb
upon this important question. Before the close of the year, young Arthur displayed such legal
ability and business tact, that he was admitted into partnership, and became a member of the
firm of Culver, Parker, and Arthur. The firm had numerous clients, and the junior partner soon
became a successful practitioner, uniting to a thorough knowledge of the law a vigorous understanding
and an untiring industry which gained for him an enviable reputation.

Among other cases on the docket of Culver, Parker, and Arthur, was one known as the Lemon slave-case.
A Virginian named Jonathan Lemon undertook to take eight slaves to Texas on steamers, by the
way of New York. While in that city a writ of habeas corpus was issued, and the slaves were
brought into the court before Judge Elijah Paine; Mr. Culver and John Jay appearing for the
slaves, while H.D. Lapaugh and Henry L. Clifton were retained by Lemon. Judge Paine, after
hearing long arguments, declared that the fugitive slave law did not apply to slaves who were
brought by their masters into a free State, and he ordered their release. The Legislature of
Virginia directed the attorney-general of that State to employ counsel to appeal from Judge
Paine's decision to the Supreme Court of the State of New York. Mr. Arthur, who was the attorney
of record in the case for the people, went to Albany, and after earnest efforts procured the
passage of a joint resolution, requesting the governor to employ counsel to defend the interests
of the State. Attorney-General Hoffman, E.D. Culver, and Joseph Blunt were appointed by the
governor as counsel, and Mr. Arthur as the State's attorney. The Supreme Court sustained Judge
Paine's decision. The slave-holder, unwilling to lose his "property," then engaged Charles
O'Conor to argue the case before the State Court of Appeals. There the counsel for the State
were again successful in defending the decision of Judge Paine, and from that day no slave-holder
dared to bring his slaves into the city of New York.

Mr. Arthur, who had naturally taken a prominent part in this case, was regarded by the colored
people of New York as a champion of their interests, and it was not long before they sought
his aid. At that time, colored people were not permitted to ride in the street-cars in New
York City, with the exception of a few old and shabby cars set aside for their occupation.
The Fourth-avenue line permitted them to ride when no other passenger made objection.

One Sunday, in 1855, Lizzie Jennings, a colored woman, returning from having fulfilled her
duties as superintendent of a colored Sunday-school, entered a Fourth-avenue car, and the conductor
took her fare. Soon after, a drunken white man objected to her presence, and insisted that
she be made to leave the car. The conductor pulled the bell, and when the car stopped, told
her that she must get out, offering to return her fare. She refused, and the conductor then
offered to put her off by force. She made vigorous resistance, exclaiming: "I have paid my
fare, and I have a right to ride." Finally, the conductor called in several policemen, and,
by their joint efforts, she was removed from the car, her clothing having nearly all been torn
from her in the struggle. When the leading colored people of the city heard of this, they sent
a committee to the office of Culver, Parker, and Arthur, and requested them to make it a test case.

Mr. Arthur brought suit against the railroad company for Miss Jennings, in the Supreme Court,
at Brooklyn. The case came on for trial before Judge Rockwell, who then sat upon the bench
there. He had just decided, in a previous case, that a corporation was not liable for the wrongful
acts of its agent or servant, and when Mr. Arthur handed him the pleadings, he said that the
railroad company was not liable, and was about to order a nonsuit. Mr. Arthur called his attention,
however, to a recently revised section of the Revised Statutes, making certain railroad corporations
which carried passengers liable for the acts of their conductors and drivers, whether wilful
or negligent, under which the action had been brought. The judge was silenced, the case was
tried, and the jury rendered a verdict of five hundred dollars damages in favor of the colored
woman. The railroad company paid the money without further contest, and issued orders to its
conductors to permit colored people to ride in its cars, an example that was followed by all
the other street railroads in New York. The colored people, especially "The Colored People's
Legal Rights Association," were very grateful to Mr. Arthur, and for years afterward they celebrated
the anniversary of the day on which he won the case that asserted their rights in public conveyances.

When a lad, young Arthur had always taken a great interest in politics, and it is related of
him that during the Clay-Polk campaign of 1844, while he and some of his companions were raising
an ash pole in honor of Harry Clay, they were attacked by some Democratic boys, when young
Arthur, who was the leader of the party, ordered a charge, and drove the young Democrats from
the field with sore heads and subdued spirits. His first vote was cast in 1852 for Winfield
Scott for President, and he identified himself with the Whigs of his ward when he located in
New York City. In those days the best citizens served as inspectors of elections at the polls,
and for some years Mr. Arthur served in that capacity at a voting-place in a carpenter's shop,
which occupied the site of the present Fifth Avenue Hotel. When, in 1856, the Republican party
was formed, Mr. Arthur was a prominent member of the Young Men's Vigilance Committee, which
advocated the election of Fremont and Dayton. It was during this campaign that he became acquainted
with Edwin D. Morgan, and gained his ardent life-long friendship.

Animated by a military spirit, Mr. Arthur sought recreation by joining the volunteer militia
of New York, and he was appointed judge-advocate-general on the staff of Brigadier-General
Yates, who commanded the second brigade. The general was a strict disciplinarian, and required
his field, line, and staff officers to meet weekly for drill and instruction. Mr. Arthur thus
acquired the rudiments of a military education, and became acquainted with many of those who
afterwards distinguished themselves as officers in the volunteer army of the Union. His heroic
death, in 1857, is recorded in history among those "names which will never be forgotten as
long as there is remembrance in the world for fidelity unto death." In command of the steamer
Central America, which went down, with a loss of three hundred and sixty lives, he stood at
his post on the wheelhouse, and succeeded in having the women and children safely transferred
to the boats, remaining himself to perish with his vessel. General Sherman has characterized
this grand deed of unselfish devotion as the most heroic incident in our naval history. Mrs.
Arthur was a lady of the highest culture, and in the varied relations of life--wife, mother,
friend--she illustrated all that gives to womanhood its highest charm, and commands for it
the purest homage. She died in 1880, after an illness of but three days, leaving a son and
a daughter, with a large number of mourning friends, not only in society, of which she was
an ornament, but among the poor and the distressed, whose wants and whose sufferings she had
tenderly cared for.

When the Honorable Edward D. Morgan was elected Governor of the State of New York, he appointed
Mr. Arthur engineer-in-chief on his staff, and when Fort Sumter was fired upon, the governor
telegraphed to him to go to Albany, where he received orders to act as state quartermaster-general
in the city of New York. General Arthur at once began to organize regiments,--uniform, arm,
and equip them,--and send them to the defence of the capital. His capacity for leadership and
organization was soon manifest. There was no lack of men or of money, but it needed organizing
powers like his to mould them into disciplined form, to grasp the new issues with a master-hand,
and to infuse earnestness and obedience into the citizens, suddenly transformed into soldiers.
His accounts were kept in accordance with the army regulations, and their subsequent settlement
with the United States, without deduction for unwarranted charges, was an easy task. It was
by his exertions, to a great extent, that the Empire State was enabled to send to the front
six hundred and ninety thousand men, nearly one fifth of the Grand Army of the Union.

There were, of course, many adventurers who sought commissions, and some of the regiments were
recruited from the rough element of city life, who soon refused to obey their officers. General
Arthur made short work of these cases, exercising an authority which no one dared to dispute.
Neither would he permit the army contractors to ingratiate themselves with him by presents,
returning everything thus sent him. Although a comparatively poor man when he entered upon
the duties of quartermaster-general at New York, he was far poorer when he gave up the office.
A friend describing his course at this period, says: "So jealous was he of his integrity, that
I have known instances where he could have made thousands of dollars legitimately, and yet
he refused to do it on the ground that he was a public officer and meant to be, like Caesar's
wife, above suspicion."

When the rebel ironclad steamer Merrimac had commenced her work of destruction near Fortress
Monroe, General Arthur, as engineer-in-chief, took efficient steps for the defence of New York,
and made a thorough inspection of all the forts and defences in the State, describing the armament
of each one. His report to the Legislature, submitted to that body in a little more than three
weeks after his attention was called to the subject by Governor Morgan, was thus noticed editorially
in the New York Herald of January 25, 1862:--

"The report of the engineer-in-chief, General Arthur, which appeared in yesterday's Herald,
is one of the most important and valuable documents that have been this year presented to our
Legislature. It deserves perusal, not only on account of the careful analysis it contains of
the condition of the forts, but because the recommendations, with which it closes, coincide
precisely with the wishes of the administration with respect to securing a full and complete
defence of the entire Northern coast."

