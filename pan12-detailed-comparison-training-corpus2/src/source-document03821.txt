﻿The sun had set when we reached Cologne. I gave my luggage to a porter, with orders to carry
it to a hotel at Duez, a little town on the opposite side of the Rhine; and directed my steps
toward the cathedral. Rather than ask my way, I wandered up and down the narrow streets, which
night had all but obscured. At last I entered a gateway leading to a court, and came out on
an open square--dark and deserted. A magnificent spectacle now presented itself. Before me,
in the fantastic light of a twilight sky, rose, in the midst of a group of low houses, an enormous
black mass, studded with pinnacles and belfries. A little farther was another, not quite so
broad as the first, but higher; a kind of square fortress, flanked at its angles with four
long detached towers, having on its summit something resembling a huge feather. On approaching,
I discovered that it was the cathedral of Cologne.

What appeared like a large feather was a crane, to which sheets of lead were appended, and
which, from its workable appearance, indicated to passers-by that this unfinished temple may
one day be completed; and that the dream of Engelbert de Berg, which was realized under Conrad
de Hochsteden, may, in an age or two, be the greatest cathedral in the world. This incomplete
Iliad sees Homers in futurity. The church was shut. I surveyed the steeples, and was startled
at their dimensions. What I had taken for towers are the projections of the buttresses. Tho
only the first story is completed, the building is already nearly as high as the towers of
Notre Dame at Paris. Should the spire, according to the plan, be placed upon this monstrous
trunk, Strasburg would be, comparatively speaking, small by its side.[B] It has always struck
me that nothing resembles ruin more than an unfinished edifice. Briars, saxifrages, and pellitories--indeed,
all weeds that root themselves in the crevices and at the base of old buildings--have besieged
these venerable walls. Man only constructs what Nature in time destroys.

All was quiet; there was no one near to break the prevailing silence. I approached the façade,
as near as the gate would permit me, and heard the countless shrubs gently rustling in the
night breeze. A light which appeared at a neighboring window, cast its rays upon a group of
exquisite statues--angels and saints, reading or preaching, with a large open book before them.
Admirable prologue for a church, which is nothing else than the Word made marble, brass or
stone! Swallows have fearlessly taken up their abode here, and their simple yet curious masonry
contrasts strangely with the architecture of the building. This was my first visit to the cathedral
of Cologne. The dome of Cologne, when seen by day, appeared to me to have lost a little of
its sublimity; it no longer had what I call the twilight grandeur that the evening lends to
huge objects; and I must say that the cathedral of Beauvais, which is scarcely known, is not
inferior, either in size or in detail, to the cathedral of Cologne.

The Hôtel-de-Ville, situated near the cathedral, is one of those singular edifices which have
been built at different times, and which consist of all styles of architecture seen in ancient
buildings. The mode in which these edifices have been built forms rather an interesting study.
Nothing is regular--no fixt plan has been drawn out--all has been built as necessity required.
Thus the Hôtel-de-Ville, which has, probably, some Roman cave near its foundation, was, in
1250, only a structure similar to those of our edifices built with pillars. For the convenience
of the night-watchman, and in order to sound the alarum, a steeple was required, and in the
fourteenth century a tower was built. Under Maximilian a taste for elegant structures was everywhere
spread, and the bishops of Cologne, deeming it essential to dress their city-house in new raiment,
engaged an Italian architect, a pupil, probably, of old Michael Angelo, and a French sculptor,
who adjusted on the blackened façade of the thirteenth century a triumphant and magnificent
porch. A few years expired, and they stood sadly in want of a promenade by the side of the
Registry. A back court was built, and galleries erected, which were sumptuously enlivened by
heraldry and bas-reliefs. These I had the pleasure of seeing; but, in a few years, no person
will have the same gratification, for, without anything being done to prevent it, they are
fast falling into ruins. At last, under Charles the Fifth, a large room for sales and for the
assemblies of the citizens was required, and a tasteful building of stone and brick was added.
I went up to the belfry; and under a gloomy sky, which harmonized with the edifice and with
my thoughts, I saw at my feet the whole of this admirable town.

From Thurmchen to Bayenthurme, the town, which extends upward of a league on the banks of the
river, displays a whole host of windows and façades. In the midst of roofs, turrets and gables,
the summits of twenty-four churches strike the eye, all of different styles, and each church,
from its grandeur, worthy of the name of cathedral. If we examine the town in detail, all is
stir, all is life. The bridge is crowded with passengers and carriages; the river is covered
with sails. Here and there clumps of trees caress, as it were, the houses blackened by time;
and the old stone hotels of the fifteenth century, with their long frieze of sculptured flowers,
fruit and leaves, upon which the dove, when tired, rests itself, relieve the monotony of the
slate roofs and brick fronts which surround them.

Round this great town--mercantile from its industry, military from its position, marine from
its river--is a vast plain that borders Germany, which the Rhine crosses at different places,
and is crowned on the northeast by historic eminences--that wonderful nest of legends and traditions,
called the "Seven Mountains." Thus Holland and its commerce, Germany and its poetry--like the
two great aspects of the human mind, the positive and the ideal--shed their light upon the
horizon of Cologne; a city of business and of meditation.

After descending from the belfry, I stopt in the yard before a handsome porch of the Renaissance,
the second story of which is formed of a series of small triumphal arches, with inscriptions.
The first is dedicated to Caesar; the second to Augustus; the third to Agrippa, the founder
of Cologne; the fourth to Constantine, the Christian emperor; the fifth to Justinian, the great
legislator; and the sixth to Maximilian. Upon the façade, the poetic sculpture has chased three
bas-reliefs, representing the three lion-combatants, Milo of Crotona, Pepin-le-Bref, and Daniel.
At the two extremities he has placed Milo of Crotona, attacking the lions by strength of body;
and Daniel subduing the lions by the power of mind. Between these is Pepin-le-Bref, conquering
his ferocious antagonist with that mixture of moral and physical strength which distinguishes
the soldier. Between pure strength and pure thought, is courage; between the athlete and the
prophet--the hero.

Pepin, sword in hand, has plunged his left arm, which is enveloped in his mantle, into the
mouth of the lion; the animal stands, with extended claws, in that attitude which in heraldry
represents the lion rampant. Pepin attacks it bravely and vanquishes. Daniel is standing motionless,
his arms by his side, and his eyes lifted up to Heaven, the lions lovingly rolling at his feet.
As for Milo of Crotona, he defends himself against the lion, which is in the act of devouring
him. His blind presumption has put too much faith in muscle, in corporeal strength. These three
bas-reliefs contain a world of meaning; the last produces a powerful effect. It is Nature avenging
herself on the man whose only faith is in brute force....

In the evening, as the stars were shining, I took a walk upon the side of the river opposite
to Cologne. Before me was the whole town, with its innumerable steeples figuring in detail
upon the pale western sky. To my left rose, like the giant of Cologne, the high spire of St.
Martin's, with its two towers; and, almost in front, the somber apsed cathedral, with its many
sharp-pointed spires, resembling a monstrous hedgehog, the crane forming the tail, and near
the base two lights, which appeared like two eyes sparkling with fire. Nothing disturbed the
stillness of the night but the rustling of the waters at my feet, the heavy tramp of a horse's
hoofs upon the bridge, and the sound of a blacksmith's hammer. A long stream of fire that issued
from the forge caused the adjoining windows to sparkle; then, as if hastening to its opposite
element, disappeared in the water.

[Footnote A: From "The Rhine." Translated by D.M. Aird.]

[Footnote B: One of the illustrations that accompany this volume shows the spires in their
completed state.]

ROUND ABOUT COBLENZ[A]

BY LADY BLANCHE MURPHY

Coblenz is the place which many years ago gave me my first associations with the Rhine. From
a neighboring town we often drove to Coblenz, and the wide, calm flow of the river, the low,
massive bridge of boats and the commonplace outskirts of a busy city contributed to make up
a very different picture from that of the poetic "castled" Rhine of German song and English
ballad. The old town has, however, many beauties, tho its military character looks out through
most of them, and reminds us that the Mosel city (for it originally stood only on that river,
and then crept up to the Rhine), tho a point of union in Nature, has been for ages, so far
as mankind was concerned, a point of defense and watching. The great fortress, a German Gibraltar,
hangs over the river and sets its teeth in the face of the opposite shore; all the foreign
element in the town is due to the deposits made there by troubles in other countries, revolution
and war sending their exiles, émigrés and prisoners. The history of the town is only a long
military record, from the days of the archbishops of Trèves, to whom it was subject....

There is the old "German house" by the bank of the Mosel, a building little altered outwardly
since the fourteenth century, now used as a food-magazine for the troops. The church of St.
Castor commemorates a holy hermit who lived and preached to the heathen in the eighth century,
and also covers the grave and monument of the founder of the "Mouse" at Wellmich, the warlike
Kuno of Falkenstein, Archbishop of Trèves. The Exchange, once a court of justice, has changed
less startlingly, and its proportions are much the same as of old; and besides these there
are other buildings worth noticing, tho not so old, and rather distinguished by the men who
lived and died there, or were born there, such as Metternich, than by architectural beauties.
Such houses there are in every old city. They do not invite you to go in and admire them; every
tourist you meet does not ask you how you liked them or whether you saw them. They are homes,
and sealed to you as such, but they are the shell of the real life of the country; and they
have somehow a charm and a fascination that no public building or show-place can have. Goethe,
who turned his life-experiences into poetry, has told us something of one such house not far
from Coblenz, in the village of Ehrenbreitstein, beneath the fortress, and which in familiar
Coblenz parlance goes by the name of "The Valley"--the house of Sophie de Laroche. The village
is also Clement Brentano's birthplace.

The oldest of German cities, Trèves (or in German Trier), is not too far to visit on our way
up the Mosel Valley, whose Celtic inhabitants of old gave the Roman legions so much trouble.
But Rome ended by conquering, by means of her civilization as well as by her arms, and Augusta
Trevirorum, tho claiming a far higher antiquity than Rome herself, and still bearing an inscription
to that effect on the old council-house--now called the Red House and used as a hotel--became,
as Ausonius condescendingly remarked, a second Rome, adorned with baths, gardens, temples,
theaters and all that went to make up an imperial capital. As in Venice everything precious
seems to have come from Constantinople, so in Trier most things worthy of note date from the
days of the Romans; tho, to tell the truth, few of the actual buildings do, no matter how classic
is their look. The style of the Empire outlived its sway, and doubtless symbolized to the inhabitants
their traditions of a higher standard of civilization.

The Porta Nigra, for instance--called Simeon's Gate at present--dates really from the days
of the first Merovingian kings, but it looks like a piece of the Colosseum, with its rows of
arches in massive red sandstone, the stones held together by iron clamps, and its low, immensely
strong double gateway, reminding one of the triumphal arches in the Forum at Rome. The history
of the transformation of this gateway is curious. First a fortified city gate, standing in
a correspondingly fortified wall, it became a dilapidated granary and storehouse in the Middle
Ages, when one of the archbishops gave leave to Simeon, a wandering hermit from Syracuse in
Sicily, to take up his abode there; and another turned it into a church dedicated to this saint,
tho of this change few traces remain. Finally, it has become a national museum of antiquities.
The amphitheater is a genuine Roman work, wonderfully well preserved; and genuine enough were
the Roman games it has witnessed, for, if we are to believe tradition, a thousand Frankish
prisoners of war were here given in one day to the wild beasts by the Emperor Constantine.
Christian emperors beautified the basilica that stood where the cathedral now is, and the latter
itself has some basilica-like points about it, tho, being the work of fifteen centuries, it
bears the stamp of successive styles upon its face....

The Mosel has but few tributary streams of importance; its own course is as winding, as wild
and as romantic as that of the Rhine itself. The most interesting part of the very varied scenery
of this river is not the castles, the antique towns, the dense woods or the teeming vineyards
lining rocks where a chamois could hardly stand--all this it has in common with the Rhine--but
the volcanic region of the Eifel, the lakes in ancient craters, the tossed masses of lava and
tufa, the great wastes strewn with dark boulders, the rifts that are called valleys and are
like the Iceland gorges, the poor, starved villages and the extraordinary rusticity, not to
say coarseness, of the inhabitants. This grotesque, interesting country--unique, I believe,
on the continent of Europe--lies in a small triangle between the Mosel, the Belgian frontier
and the Schiefer hills of the Lower Rhine; it goes by the names of the High Eifel, with the
High Acht, the Kellberg and the Nurburg; the upper (Vorder) Eifel, with Gerolstein, a ruined
castle, and Daun, a pretty village; and the Snow-Eifel (Schnee Eifel), contracted by the speech
of the country into Schneifel.

The last is the most curious, the most dreary, the least visited. Walls of sharp rocks rise
up over eight hundred feet high round some of its sunken lakes--one is called the Powder Lake--and
the level above this abyss stretches out in moors and desolate downs, peopled with herds of
lean sheep, and marked here and there by sepulchral, gibbet-looking signposts, shaped like
a rough T and set in a heap of loose stones. It is a great contrast to turn aside from this
landscape and look on the smiling villages and pretty wooded scenery of the valley of the Mosel
proper; the long lines of handsome, healthy women washing their linen on the banks; the old
ferryboats crossing by the help of antique chain-and-rope contrivances; the groves of old trees,
with broken walls and rude shrines, reminding one of Southern Italy and her olives and ilexes;
and the picturesque houses, in Kochem, in Daun, in Travbach, in Bernkastel, which, however
untiring one may be as a sightseer, hardly warrant one as a writer to describe and re-describe
their beauties. Klüsserath, however, we must mention, because its straggling figure has given
rise to a local proverb--"As long as Klüsserath;" and Neumagen, because of the legend of Constantine,
who is said to have seen the cross of victory in the heavens at this place, as well as at Sinzig
on the Rhine, and, as the more famous legend tells us, at the Pons Milvium over the Tiber.

The last glance we take at the beauties of this neighborhood is from the mouth of the torrent-river
Eltz as it dashes into the Eifel, washing the rock on which stands the castle of Eltz. The
building and the family are an exception in the history of these lands; both exist to this
day, and are prosperous and undaunted, notwithstanding all the efforts of enemies, time and
circumstances to the contrary. The strongly-turreted wall runs from the castle till it loses
itself in the rock, and the building has a home-like inhabited, complete look; which, in virtue
of the quaint irregularity and magnificent natural position of the castle, standing guard over
the foaming Eltz, does not take from its romantic appearance, as preservation or restoration
too often does.

Not far from Coblenz, and past the island of Nonnenwerth, is the old tenth-century castle of
Sayn, which stood until the Thirty Years' War, and below it, quiet, comfortable, large, but
unpretending, lies the new house of the family of Sayn-Wittgenstein, built in the year 1848.
As we push our way down the Rhine we soon come to the little peaceful town of Neuwied, a sanctuary
for persecuted Flemings and others of the Low Countries, gathered here by the local sovereign,
Count Frederick III. The little brook that gives its name to the village runs softly into the
Rhine under a rustic bridge and amid murmuring rushes, while beyond it the valley gets narrower,
rocks begin to rise over the Rhine banks, and we come to Andernach.

Andernach is the Rocky Gate of the Rhine, and if its scenery were not enough, its history,
dating from Roman times, would make it interesting. However, of its relics we can only mention,
in passing, the parish church with its four towers, all of tufa, the dungeons under the council-house,
significantly called the "Jew's bath," and the old sixteenth-century contrivances for loading
Rhine boats with the millstones in which the town still drives a fair trade. At the mouth of
the Brohl we meet the volcanic region again, and farther up the valley through which this stream
winds come upon the retired little watering-place of Tönnistein, a favorite goal of the Dutch,
with its steel waters; and Wassenach, with what we may well call its dust-baths, stretching
for miles inland, up hills full of old craters, and leaving us only at the entrance of the
beech-woods that have grown up in these cauldron-like valleys and fringe the blue Laachersee,
the lake of legends and of fairies. One of these Schlegel has versified in the "Lay of the
Sunken Castle," with the piteous tale of the spirits imprisoned; and Simrock tells us in rhyme
of the merman who sits waiting for a mortal bride; while Wolfgang Müller sings of the "Castle
under the Lake," where at night ghostly torches are lighted and ghostly revels are held, the
story of which so fascinates the fisherman's boy who has heard of these doings from his grandmother
that as he watches the enchanted waters one night his fancy plays him a cruel trick, and he
plunges in to join the revellers and learn the truth.

Local tradition says that Count Henry II. and his wife Adelaide, walking here by night, saw
the whole lake lighted up from within in uncanny fashion, and founded a monastery in order
to counteract the spell. This deserted but scarcely ruined building still exists, and contains
the grave of the founder; the twelfth-century decoration, rich and detailed, is almost whole
in the oldest part of the monastery. The far-famed German tale of Genovefa of Brabant is here
localized, and Henry's son Siegfried assigned to the princess as a husband, while the neighboring
grotto of Hochstein is shown as her place of refuge. On our way back to the Rocky Gate we pass
through the singular little town of Niedermendig, an hour's distance from the lake--a place
built wholly of dark gray lava, standing in a region where lava-ridges seam the earth like
the bones of antediluvian monsters, but are made more profitable by being quarried into millstones.
There is something here that brings part of Wales to the remembrance of the few who have seen
those dreary slate-villages--dark, damp, but naked, for moss and weeds do not thrive on this
dampness as they do on the decay of other stones--which dot the moorland of Wales. The fences
are slate; the gateposts are slate; the stiles are of slate; the very "sticks" up which the
climbing roses are trained are of slate; churches, schools, houses, stables are all of one
dark iron-blue shade; floors and roofs are alike; hearth-stones and threshold-stones, and grave-stones
all of the same material. It is curious and depressing. This volcanic region of the Rhine,
however, has so many unexpected beauties strewn pell-mell in the midst of stony barrenness
that it also bears some likeness to Naples and Ischia, where beauty of color, and even of vegetation,
alternate surprisingly with tracts of parched and rocky wilderness pierced with holes whence
gas and steam are always rising.

[Footnote A: From "Down the Rhine."]

BINGEN AND MAYENCE[A]

BY VICTOR HUGO

Bingen is an exceedingly pretty place, having at once the somber look of an ancient town, and
the cheering aspect of a new one. From the days of Consul Drusus to those of the Emperor Charlemagne,
from Charlemagne to Archbishop Willigis, from Willigis to the merchant Montemagno, and from
Montemagno to the visionary Holzhausen, the town gradually increased in the number of its houses,
as the dew gathers drop by drop in the cup of a lily. Excuse this comparison; for, tho flowery,
it has truth to back it, and faithfully illustrates the mode in which a town near the conflux
of two rivers is constructed. The irregularity of the houses--in fact everything, tends to
make Bingen a kind of antithesis, both with respect to buildings and the scenery which surrounds
them. The town, bounded on the left by Nahe, and by the Rhine on the right, develops itself
in a triangular form near a Gothic church, which is backed by a Roman citadel. In this citadel,
which bears the date of the first century, and has long been the haunt of bandits, there is
a garden; and in the church, which is of the fifteenth century, is the tomb of Barthélemy de
Holzhausen. In the direction of Mayence, the famed Paradise Plain opens upon the Ringau; and
in that of Coblentz, the dark mountains of Leyen seem to frown on the surrounding scenery.
Here Nature smiles like a lovely woman extended unadorned on the greensward; there, like a
slumbering giant, she excites a feeling of awe.

The more we examine this beautiful place, the more the antithesis is multiplied under our looks
and thoughts. It assumes a thousand different forms; and as the Nahe flows through the arches
of the stone bridge, upon the parapet of which the lion of Hesse turns its back to the eagle
of Prussia, the green arm of the Rhine seizes suddenly the fair and indolent stream, and plunges
it into the Bingerloch.

To sit down toward the evening on the summit of the Klopp--to see the town at its base, with
an immense horizon on all sides, the mountains overshadowing all--to see the slated roofs smoking,
the shadows lengthening, and the scenery breathing to life the verses of Virgil--to respire
at once the wind which rustles the leaves, the breeze of the flood, and the gale of the mountain--is
an exquisite and inexpressible pleasure, full of secret enjoyment, which is veiled by the grandeur
of the spectacle, by the intensity of contemplation. At the windows of huts, young women, their
eyes fixt upon their work, are gaily singing; among the weeds that grow round the ruins birds
whistle and pair; barks are crossing the river, and the sound of oars splashing in the water,
and unfurling of sails, reaches our ears. The washerwomen of the Rhine spread their clothes
on the bushes; and those of the Nahe, their legs and feet naked, beat their linen upon floating
rafts, and laugh at some poor artist as he sketches Ehrenfels.

The sun sets, night comes on, the slated roofs of the houses appear as one, the mountains congregate
and take the aspect of an immense dark body; and the washerwomen, with bundles on their heads,
return cheerfully to their cabins; the noise subsides, the voices are hushed; a faint light,
resembling the reflections of the other world upon the countenance of a dying man, is for a
short time observable on the Ehrenfels; then all is dark, except the tower of Hatto, which,
tho scarcely seen in the day, makes its appearance at night, amid a light smoke and the reverberation
of the forge....

Mayence and Frankfort, like Versailles and Paris, may, at the present time, be called one town.
In the middle ages there was a distance of eight leagues between them, which was then considered
a long journey; now, an hour and a quarter will suffice to transport you from one to the other.
The buildings of Frankfort and Mayence, like those of Liège, have been devastated by modern
good taste, and old and venerable edifices are rapidly disappearing, giving place to frightful
groups of white houses.

I expected to be able to see, at Mayence, Martinsburg, which, up to the seventeenth century,
was the feudal residence of the ecclesiastical electors; but the French made a hospital of
it, which was afterward razed to the ground to make room for the Porte Franc; the merchant's
hotel, built in 1317 by the famed League, and which was splendidly decorated with the statues
of seven electors, and surmounted by two colossal figures, bearing the crown of the empire,
also shared the same fate. Mayence possesses that which marks its antiquity--a venerable cathedral,
which was commenced in 978, and finished in 1009. Part of this superb structure was burned
in 1190, and since that period has, from century to century, undergone some change.

I explored its interior, and was struck with awe on beholding innumerable tombs, bearing dates
as far back as the eighteenth century. Under the galleries of the cloister I observed an obscure
monument, a bas-relief of the fourteenth century, and tried, in vain, to guess the enigma.
On one side are two men in chains, wildness in their looks, and despair in their attitudes;
on the other, an emperor, accompanied by a bishop, and surrounded by a number of people, triumphing.
Is it Barbarossa? Is it Louis of Bavaria? Does it speak of the revolt of 1160, or of the war
between Mayence and Frankfort in 1332? I could not tell, and therefore passed by.

As I was leaving the galleries, I discovered in the shade a sculptured head, half protruding
from the wall, surmounted by a crown of flower-work, similar to that worn by the kings of the
eleventh century. I looked at it; it had a mild countenance; yet it possest something of severity
in it--a face imprinted with that august beauty which the workings of a great mind give to
the countenance of man. The hand of some peasant had chalked the name "Frauenlob" above it,
and I instantly remembered the Tasso of Mayence, so calumniated during his life, so venerated
after his death. When Henry Frauenlob died, which was in the year 1318, the females who had
insulted him in life carried his coffin to the tomb, which procession is chiseled on the tombstone
beneath. I again looked at that noble head. The sculptor had left the eyes open; and thus,
in that church of sepulchers--in that cloister of the dead--the poet alone sees; he only is
represented standing, and observing all.

The market-place, which is by the side of the cathedral, has rather an amusing and pleasing
aspect. In the middle is a pretty triangular fountain of the German Renaissance, which, besides
having scepters, nymphs, angels, dolphins, and mermaids, serves as a pedestal to the Virgin
Mary. This fountain was erected by Albert de Brandenburg, who reigned in 1540, in commemoration
of the capture of Francis the First by Charles the Fifth.

Mayence, white tho it be, retains its ancient aspect of a beautiful city. The river here is
not less crowded with sails, the town not less incumbered with bales, nor more free from bustle,
than formerly. People walk, squeak, push, sell, buy, sing, and cry; in fact in all the quarters
of the town, in every house, life seems to predominate. At night the buzz and noise cease,
and nothing is heard at Mayence but the murmurings of the Rhine, and the everlasting noise
of seventeen water mills, which are fixt to the piles of the bridge of Charlemagne.

[Footnote A: From "The Rhine." Translated by D.M. Aird.]

FRANKFORT-AM-MAIN[A]

BY BAYARD TAYLOR

Frankfort is a genuine old German city. He agreed, and did it; and at the present time one
can distinguish a rude nine on the vane, as if cut with bullets, while two or three marks at
the side appear to be from shots that failed.

[Footnote A: From "Views Afoot." Published by G.P. Putnam's Sons.]

HEIDELBERG[A]

BY BAYARD TAYLOR

Here in Heidelberg at last, and a most glorious town it is. I forget exactly how many casks
it holds, but I believe eight hundred.

[Footnote A: From "Views Afoot."

[Footnote A: From "Sunny Memories of Foreign Lands." Mrs. Stowe published this work in 1854,
after returning from the tour she made soon after achieving great fame with "Uncle Tom's Cabin."

[Footnote A: From "Views Afoot." Published by G.P. Putnam's Sons.]

II

NUREMBERG

AS A MEDIEVAL CITY[A]

BY CECIL HEADLAM

In spite of all changes, and in spite of the disfigurements of modern industry, Nuremberg is
and will remain a medieval city, a city of history and legend, a city of the soul. She is like
Venice in this, as in not a little of her history, that she exercises an indefinable fascination
over our hearts no less than over our intellects. The subtle flavor of medieval towns may be
likened to that of those rare old ports which are said to taste of the grave; a flavor indefinable,
exquisite. Rothenburg has it; and it is with Rothenburg, that little gem of medievalism, that
Nuremberg is likely to be compared in the mind of the modern wanderer in Franconia. But tho
Rothenburg may surpass her greater neighbor in the perfect harmony and in the picturesqueness
of her red-tiled houses and well-preserved fortifications, in interest at any rate she must
yield to the heroine of this story.

For, apart from the beauty which Nuremberg owes to the wonderful grouping of her red roofs
and ancient castle, her coronet of antique towers, her Gothic churches and Renaissance buildings
or brown riverside houses dipping into the mud-colored Pegnitz, she rejoices in treasures of
art and architecture and in the possession of a splendid history such as Rothenburg can not
boast. To those who know something of her story Nuremberg brings the subtle charm of association.
While appealing to our memories by the grandeur of her historic past, and to our imaginations
by the work and tradition of her mighty dead, she appeals also to our senses with the rare
magic of her personal beauty, if one may so call it. In that triple appeal lies the fascination
of Nuremberg....

The facts as to the origin of Nuremberg are lost in the dim shadows of tradition. When the
little town sprang up amid the forests and swamps which still marked the course of the Pegnitz,
we know as little as we know the origin of the name Nürnberg. It is true that the chronicles
of later days are only too ready to furnish us with information; but the information is not
always reliable. The chronicles, like our own peerage, are apt to contain too vivid efforts
of imaginative fiction. The chroniclers, unharassed by facts or documents, with minds "not
by geography prejudiced, or warped by history," can not unfortunately always be believed. It
is, for instance, quite possible that Attila, King of the Huns, passed and plundered Nuremberg,
as they tell us. But there is no proof, no record of that visitation. Again, the inevitable
legend of a visit from Charlemagne occurs. He, you may be sure, was lost in the woods while
hunting near Nuremberg, and passed all night alone, unhurt by the wild beasts. As a token of
gratitude for God's manifest favor he caused a chapel to be built on the spot. The chapel stands
to this day--a twelfth-century building--but no matter! for did not Otho I., as our chroniclers
tell us, attend mass in St. Sebald's Church in 970, tho St. Sebald's Church can not have been
built till a century later?

The origin of the very name of Nuremberg is hidden in the clouds of obscurity. In the earliest
documents we find it spelt with the usual variations of early manuscripts--Nourenberg, Nuorimperc,
Niurenberg, Nuremberc, etc. The origin of the place, we repeat, is equally obscure. Many attempts
have been made to find history in the light of the derivations of the name. But when philology
turns historian it is apt to play strange tricks. Nur ein Berg (only a castle), or Nero's Castle,
or Norix Tower--what matter which is the right derivation, so long as we can base a possible
theory on it? The Norixberg theory will serve to illustrate the incredible quantity of misplaced
ingenuity which both of old times and in the present has been wasted in trying to explain the
inexplicable.

