﻿Casting about for a satisfactory supplement and complement for the public schools, we find
the public library ready to render exactly this service; to make it possible for the adult
to continue through life the growth begun in childhood in the public school. Only in this way
and by this means can we hope to continue the common American people as the most uncommon people
which the world has yet known.

Henceforth, then, these two must go hand in hand, neither trenching upon the field of the other,
neither burdening or hampering the other, each helping the other. The public school must take
the initiative, determining lines of thought and work, developing in each child the power to
act and the tendency to act, making full use of the public library as an effective ally in
all its current work, and making such use of it as to create in each pupil the library habit,
to last through life. The public library must respond by every possible supplementary effort,
by most intelligent co-operation, by most sympathetic and effective assistance, and by giving
pupils a welcome which they will feel holds good till waning physical powers make further use
of the library impossible.

NATIONAL EDUCATION ASS'N REPORT, 1906.

The most imperative duty of the state is the universal education of the masses. No money which
can be usefully spent for this indispensable end should be denied. Public sentiment should,
on the contrary, approve the doctrine that the more that can be judiciously spent, the better
for the country. There is no insurance of nations so cheap as the enlightenment of the people.

ANDREW CARNEGIE.

PUBLIC LIBRARY IS PUBLIC CO-OPERATION

A public library is the flower of the modern forms of co-operation, which secures for the individual,
luxuries which he could not afford otherwise.

Instead of buying so many books and magazines which wear out on the shelves after one reading,
let us "pool our issues" and put the multitude of small sums in one fund, buy the best at the
lowest prices, and then use the volumes so bought for the good of all. We need spend no more
money each year for literature, but we need to save the wastage due to unused books, foolish
purchases, book agents, commissions, and needless profits--and we can have a public library
without other cost.

A good public library in this town may help our neighboring farmers as well as our townspeople.
They cannot support public libraries in their small communities. Their small school libraries
give the children a taste for reading, but give them nothing to gratify that taste when they
leave school. Let us join our forces for mutual advantage and get a better library and a wider
community of interests.

WISCONSIN FREE LIBRARY COMMISSION.

USE OF LIBRARIES FOR REFERENCE

An ability to glean information quickly and accurately from books and periodicals, to catch
a fact when it is needed and useful, is an indispensable factor in that self-education which
all citizens should add to the education obtained in the schools. The schools cannot give a
wide range of knowledge, but they can give the desire for knowledge, and the library can give
the opportunity to gain it.

Nearly every branch taught in the schools may be lightened and made more interesting by supplementary
information gained from a good library. The pupil who is studying the life of Washington should
find many interesting facts concerning him and his time and associates, not given in any of
the formal biographies. He will find an article on Washington in the "Young Folks' Cyclopedia
of Persons and Places," but if he knows how to use the index he can find fourteen other articles
in the same volume in which Washington is mentioned. A large encyclopedia will give scores
of facts wanted, under various articles treating of important events in the latter colonial
and earlier national history of our country, in articles on places, customs, epochs, battles,
and soldiers and statesmen who were Washington's contemporaries.

A teacher cannot train a large number of young people to habits of thorough investigation in
a brief time, but she can easily train a few, one or two at a time, and they will help to train
others.

F. A. HUTCHINS.

THE MODERN LIBRARY MOVEMENT

The modern library movement is a movement to increase by every possible means the accessibility
of books, to stimulate their reading and to create a demand for the best. Its motive is helpfulness;
its scope, instruction and recreation; its purpose, the enlightenment of all; its aspirations,
still greater usefulness. It is a distinctive movement, because it recognizes, as never before,
the infinite possibilities of the public library, and because it has done everything within
its power to develop those possibilities.

Among the peculiar relations that a library sustains to a community, which the movement has
made clear and greatly advanced, are its relations to the school and university extension.
The education of an individual is coincident with the life of that individual. It is carried
on by the influences and appliances of the family, vocation, government, the church, the press,
the school and the library. The library is unsectarian, and hence occupies a field independent
of the church. It furnishes a foundation for an intelligent reading of paper and magazine.
It is the complement and supplement of the school, co-operating with the teacher in the work
of educating the child, and furnishing the means for continuing that education after the child
has gone out from the school. These are important relations. From the beginning the child is
taught the value of books. In the kindergarten period he learns that they contain beautiful
pictures; in the grammar grades they do much to make history and geography attractive; in the
high school they are indispensable as works of reference.

Were it not for the library, the education of the masses would, in most cases, cease when the
doors of the school swung in after them for the last time; but it keeps those doors wide open,
and is, in the truest sense of the word, the university of the people. The library is as much
a part of the educational system of a community as the public school, and is coming more and
more to be regarded with the same respect and supported in the same generous manner.

The public library of to-day is an active, potential force, serving the present, and silently
helping to develop the civilization of the future. The spirit of the modern library movement
which surrounds it is thoroughly progressive, and thoroughly in sympathy with the people. It
believes that the true function of the library is to serve the people, and that the only test
of success is usefulness.

JOSEPH LEROY HARRISON.

THE PEOPLE'S UNIVERSITY

There is no institution so intimately, so universally, so constantly connected with the life
of the whole people as the free public library--no instrumentality that can do so much to civilize
society. The public schools alone cannot accomplish the task of elevating mankind to even the
most modest ideal of a well ordered society.

Our public schools have been the chief source of the greater general intelligence and hence
the industrial superiority of our citizens over those of other countries. But the public schools
cannot accomplish impossibilities. They are not to blame for the fact that they can reach the
great majority during only six or eight years, or that only one and one half per cent of the
children in the United States go through the high school. But wherever there is a public library,
the teachers are to blame if they do not graduate all their pupils, at whatever age they may
leave school, into the People's University.

General intelligence is the necessary foundation of prosperity and social order.

The public library is one of the chief agencies, if not the most potent and far-reaching agency,
for promoting general intelligence.

Therefore, money devoted to the maintenance of a public library is money well invested by a
community.

F. M. CRUNDEN.

PUBLIC LIBRARY, A PUBLIC NECESSITY

Any consideration of a public library project is complimentary to a community, showing, as
it does, a sense of civic responsibility and a desire for future progress which are commendable.
No town can hope to live up to its greatest possibilities without a public library, and none
with a sincere desire need be denied the blessings which result from such an institution.

There are few communities which would not provide for a public library, if its advantages were
appreciated, for it is a remedy for many ills and is all-embracing in its scope. It vitalizes
school work, and receiving the pupil from the school, the library continues his education throughout
life. It is a home missionary, sending its messengers, the books, into every shop and home.
With true missionary zeal, it not only sends help, but opens its doors to every man, woman
and child. In most towns, there are scores of young men and boys whose evenings are spent in
loafing about the streets, and to these the library offers an attractive meeting place, where
the time may be spent with jolly, wise friends in the books. The library substitutes better
for poorer reading, and provides story hours for the children who are eager to hear before
they are able to read. It also increases the earning capacity of people, by supplying information
and advice on the work they are doing.

Increased taxation is one of the greatest hindrances to the opening of a public library, but
any institution which enriches and uplifts the lives of the people, is the greatest economy.
Any attempt to conduct civic affairs without a reasonable expenditure of money for such influences
is the grossest extravagance. No economy results from ignorance and vice, and the public library
has long since established its claim as one of the most potent remedies for such conditions.

It is no exaggeration to state that every dollar expended for library purposes is returned
to the community tenfold, not necessarily in dollars and cents, but in the more permanent,
more valuable assets of greater happiness, comfort and progress of the people. A city is the
expression of every life within its borders, and every increase in progress and efficiency
in the individual citizen, is progress for the whole.

The most valuable things usually are obtained at some sacrifice, and the many advantages from
a public library are certainly worth paying for. Hundreds of small cities and towns tax themselves
for electric plants and count themselves fortunate. No one seems to regret this taxation for
electric lights which illuminate the citizen's way at night. Should there not be an equal or
greater readiness on the part of a community to establish a library and so illuminate the mental
horizon of every citizen?

A public library is a necessity, not a luxury. Every community which realizes this and establishes
a library, proclaims itself an intelligent, progressive town and one worth living in.

CHALMERS HADLEY.

The opening of a free public library is a most important event in any town. There is no way
in which a community can more benefit itself than in the establishment of a library which shall
be free to all citizens.

WILLIAM McKINLEY.

PUBLIC LIBRARY, A PUBLIC OPPORTUNITY

Modern industrialism exacts from the artisan and the worker in every branch, skill and knowledge
not dreamed of years ago. He who would not be trampled under foot needs to keep pace with the
onward sweep in his particular craft. The public library furnishes to the ambitious artisan
the opportunity to rise. Upon its shelves he may find the latest and the best in invention
and in method and in knowledge. Never in the history of the country has there been such a desire
manifested among the adult population for continued education as may be noted to-day. Does
it not speak eloquently of ambition to rise above circumstances--that same spirit that we have
admired in our Franklins and our Lincolns and the long roll of self-made men whose lives we
are proud to recall? And so library extension takes note of adult education, and combining
its forces with university extension, realizes that broader movement variously termed home
education, popular education and the people's college.

The library gives heed to the future, and thus does not neglect the child. The intelligent
work of the children's librarian, supplementing the related work of the teacher, aims to develop
the individual talent or dormant resource which finds no chance for expression where children
are necessarily treated as masses. And we may never know what society has lost by failure to
quicken into life this dormant talent for invention, for art, for literature, for philosophy.
"The loss to society of the unearned increment is trivial compared to the loss of the undiscovered
resource." Had retarding influences affected half a dozen men whom we could readily name--Morse,
Fulton, Stephenson, Edison, Bell, Marconi--we might to-day be without the locomotive, the steamship,
the telegraph, the telephone--the myriad marvels of electricity that to-day seem commonplaces.
What we have actually lost during this great century of scientific development we can never
know. Nor must we forget that invention is the result of cumulated knowledge which the fertile
brain of man utilizes in new directions, and that the preservation of the knowledge and experience
of the centuries is the province of the public library, where all alike may have access to
its riches. The ideal democracy is the democracy of knowledge and of learning.

The library endeavors, by applying the traveling library principle to collections of pictures,
by means of the illustrated lecture and otherwise, to cultivate among the people an appreciation
of the beautiful and artistic that shall ultimately find expression in the home and its surroundings.

The library believes, too, that recreative reading is a legitimate function. We hold, with
William Morton Payne, that a sparkling and sprightly story, which may be read in an hour and
which will leave the reader with a good conscience and a sense of cheerfulness, has its merits.
In this work-a-day world of ours we need a bit of cheer for the hours which ought to be restful
as well as resting hours. Library extension is imbued with optimism; its broadening field is
educational, sociological, recreative. Unblinded to the evils of the day, its promoters realize
inability to amend them except by educational processes affecting all the people. They do not
preach the gospel of discontent, but seek realization of conditions which shall bring about
contentment and happiness. That, after all, for the welfare of the people, wants need be but
few and easily supplied. He who has food, raiment and shelter in reasonable degree, access
to the intellectual wealth of the world in public libraries, to the riches created by the master
painters and sculptors, found in public galleries and museums, to the untrammeled use of public
parks and drives, and the many other universal advantages which are now so increasingly many,
need not envy the richest men on earth. Many a millionaire is poorer than the most humble of
his employees, for excessive wealth brings its own train of evils to torment its possessor.
Commercial success is a legitimate endeavor among men, and thrift is to be commended, but when
these degenerate into greed, pity and not envy should be the meed of the man seized with the
money disease.

HENRY E. LEGLER.

THE LIBRARY AND THE WORKERS

My opinion of the public library from a workingman's standpoint is, that it is the greatest
boon that could possibly be conferred upon him. It places him at once upon the level with the
millionaire, the student and the philosopher. It opens for him (whose poverty would otherwise
debar him) the vast fields of literature. Here he may wander at will with the master minds
of humanity, hand in hand with the great thinkers of the ages, open his mind and heart to the
lessons taught by those great leaders of men who have conquered nations and shaped the destinies
of the human race. Here he may associate with the greatest, the wisest and the best. There
is no limit to the possibilities of possessing knowledge which is power, without money and
without price. The public library should be managed in the best interests of the workingman,
and the books should be purchased mainly with his welfare in view. The capitalist can buy and
own his own books. The workingman cannot do this. The children of the workingman must get from
the public library the general books of reference which the business man has in his home. The
children of the workingman must have these books in order properly to do their school work
and thoroughly understand it. Their teachers require this. The children of the workingman have
their schools as well as the library. Their work in the schools and the work in the library
go hand in hand, but the workingman himself has only the library for his school and must, of
necessity, go there. His schoolroom is the reference room, for the knowledge he gains in that
department he can at once put into practical use in any capacity in which he may be employed.

The question arises, having presented those opportunities to the workingman, will he take advantage
of them? I answer, he surely will. It is now more than twenty years since I joined a labor
organization, the "Stone-cutters' Union" of Minneapolis. Since that time I have always been
affiliated with organized workingmen. During all these years the workingman has taken advantage
of every opportunity to better the condition of himself, his fellow workman and his employer.
He has learned to be more patient, more conservative and more trustworthy. His hours of labor
have been shortened, his wages are higher, and labor-saving machinery has made his work lighter.
He lives in a better home, his family is better provided for and, best of all, his children
are better educated. What has wrought those great changes in the conditions of the workingman?
What has enabled him to keep up with the swift march of progress during these many years? I
will answer in one word, Education. Just such institutions as the public library have made
this possible, and the public library has given the largest share.

JOHN P. BUCKLEY.

A WORLD WITHOUT BOOKS

What if there were no letters and no books? Think what your state would be in a situation like
that! Think what it would be to know nothing, for example, of the way in which American independence
had been won, and the federal republic of the United States constructed; nothing of Bunker
Hill; nothing of George Washington; except the little, half true and half mistaken, that your
fathers could remember, of what their fathers had repeated, of what their fathers had told
to them. Think what it would be to have nothing but shadowy traditions of the voyage of Columbus,
of the coming of the Mayflower pilgrims, and of all the planting of life in the New World from
Old World stocks, like Greek legends of the Argonauts and of the Heraclidae! Think what it
would be to know no more of the origins of the English people, their rise and their growth
in greatness, than the Romans knew of their Latin beginnings; and to know no more of Rome herself
than we might guess from the ruins she has left! Think what it would be to have the whole story
of Athens and Greece dropped out of our knowledge, and to be unaware that Marathon was ever
fought, or that one like Socrates had ever lived! Think what it would be to have no line from
Homer, no thought from Plato, no message from Isaiah, no Sermon on the Mount, nor any parable
from the lips of Jesus!

Can you imagine a world intellectually famine-smitten like that--a bookless world--and not
shrink with horror from the thought of being condemned to it?

Yet the men and women who take nothing from letters and books are choosing to live as though
mankind did actually wallow in the awful darkness of that state from which writing and books
have rescued us. For them, it is as if no ship had ever come from the far shores of old Time
where their ancestry dwelt; and the interest of existence to them is huddled in the petty space
of their own few years, between walls of mist which thicken as impenetrably behind them as
before. How can life be worth living on such terms as that? How can man or woman be content
with so little, when so much is offered?

J. N. LARNED.

BOOKLESS HOMES

The bookless homes of the well-to-do people are familiar to all. Inside those walls no books
are to be found but a few gift books, chosen for their bindings rather than their contents,
and perhaps others which some agent has pressed upon them. What can be done to stimulate reading
in these homes? Ten-cent magazines and cheap stories are devoured by mother and daughters to
the destruction of sane thoughts and connected ideas. The man of the house each day reads his
newspaper, containing accounts of crimes, accidents and the funny paper. Happily, it also contains
articles of travel, invention and discovery, otherwise his brain would be weakened.

Young people come from these bookless homes to college each year, showing great confusion of
ideas, vacuity of mind and utter lack of information. They need us, need libraries, need the
force of the state to help them. Ninety-four per cent of our young people never get into college.
Ninety per cent, it is said, never go to school after they have passed the age of fourteen years.

The contribution of the library is to elevate the standard of the town. Books depicting noble,
earnest, well-meaning lives will cause the social standard to progress, and other standards
with it.

OREGON LIBRARY COMMISSION.

NEED OF FREE LIBRARIES

A library is an essential part of a broad system of education, and a community should think
it as discreditable to be without a well-conducted free public library as to be without a good
school. If it is the duty of the state to give each future citizen an opportunity to learn
to read, it is equally its duty to give each citizen an opportunity to use that power wisely
for himself and the state. Wholesome literature can be furnished to all the readers in a community
at a fraction of the cost necessary to teach them to read, and the power to read may then become
a means to a life-long education.

The books that a boy reads for pleasure do more to determine his ideals and shape his character
than the text-books he studies in the schools. Bad and indifferent literature is now so common
that the boys will have some sort of reading. If they have a good public library they will
read wholesome books and learn to admire Washington, Lincoln and other great men. Without a
library many of them will gloat over the exploits of depraved men and women, and their earliest
ambitions will be tainted.

Each town needs a library to furnish more practice in reading for the little folks in school;
it needs it to give the boys and girls who have learned to read a taste for wholesome literature
that informs and inspires; it needs it as a center for an intellectual and spiritual activity
that shall leaven the whole community and make healthful and inspiring themes the burden of
the common thought--substituting, by natural methods, clean conversation and literature for
petty gossip, scandal and oral and printed teachings in vice.

F. A. HUTCHINS.

THE LIBRARY AND BOYS

"In Madison, N. J., a bird club of boys met twice a week, once for study and once for an expedition,
and found the library's resources on this topic to be of interest and value. How to utilize
profitably the activities of a 'gang' of boys is worth much planning. One librarian is reported
to have started a chair-caning class to interest restless boys; another had a museum of flowers
and insects, another conducted a branch of the flower mission. Not less interesting, and perhaps
more instructive, is a series of talks on Indian legends accompanied by hunting expeditions
for the half-buried implements and relics found in almost every meadow in some parts of the
country. Boys are eager to learn about natural history and natural science, and they will be
encouraged at the public library."

IRENE VAN KLEECK.

THE LIBRARY

Get good books; give them a home attractive to readers of good books; name a friend of good
books as mistress of this home--and you have a library; all share in its support and all get
pleasure and profit from it if they will; without divisions religious, politic or social, it
unites all in the pursuit of high pleasure and sound learning, and gives that common interest
in a common concern which is the basis of all local pride.

If you have rightly read a book, that book is yours.

You cannot always choose your companions; you can always choose your books. You can, if you
will, spend a few minutes every day with the best and wisest men and women the world has ever
known.

The people you have known, the things you have said and done, and the books you have read,
all these are now a part of you.

You like yourself better when you are with people who are well-bred and clever; you respect
yourself more when you are reading a bright and wholesome book, for you are then in the company
of the wise.

J. C. DANA.

After the church and the school, the free public library is the most effective influence for
good in America. The moral, mental and material benefits to be derived from a carefully selected
collection of good books, free for the use of all the people, cannot be overestimated. No community
can afford to be without a library.

THEODORE ROOSEVELT.

SHALL WE BE LOYAL TO THE CITY OF OUR HOME?

The opportunity is at hand to answer this question. A generous gift is offered, shall we accept
it? We can have ---- dollars for a public use, if we will promise to support the use to which
this money is dedicated. Shall ---- have a free public library? It is up to us, her citizens.

We have passed the stage of a country town and are ranked and cataloged as a modern, progressive
city, enjoying many of the advantages of the larger cities. Why is this true? Because the progressive
spirit and sentiment have always triumphed in her onward march. Because, inspired by a public
spirit, her people have joined hands, and shoulder to shoulder labored for all that pertains
to religious, moral, social, industrial, educational and material development. Let us keep
marching on.

Many towns in the state, nearly all those in the counties surrounding us, are accepting Carnegie
gifts for libraries. Will it not humiliate and degrade us in the eyes of the people of the
state if we decree against a public library? Let us not detract from our well deserved and
established reputation for progressiveness by such a mistake. We appeal to public spirit; to
pride of city; to pride of home, and urge you to register your vote in favor of this enterprise.

IOWA LIBRARY COMMISSION.

The system of free public libraries now being established in this country is the most important
development of modern times. The library is a center from which radiates an ever widening influence
for the enlightenment, the uplift, the advancement of the community.

WILLIAM JENNINGS BRYAN.

THE SCHOOL'S GREATEST BOON

The greatest boon that the system of public schools, or the college, or the university, can
confer upon any boy or girl is to teach him or her to use a great collection of literature,
to teach them how to read; and to plant within their hearts an irresistible impulse and an
indestructible delight in so doing. What profits it a man to learn how to read if he does not
read? For what purpose is the mind trained and developed by the process of systematic study
in the schools if it is not inspired to go farther into the realms of knowledge? Is it a rational
procedure for one, upon the completion of his course of training, to discontinue all further
investigation and to lay aside what little love for learning and literature and philosophy
and science that may have been aroused in his bosom by school or college inspirations? And
how is this advancing and widening of one's horizon by means of the accumulated stores of knowledge
gathered by the previous generations of the world's strong thinkers and beautiful writers to
be secured, other than by a collection of good books, by a library?

C. C. THACH.

BOOKS AND STUDY WORK

Have our missionary societies access to Bliss's "Encyclopedia of Missions," or to Dennis's
great "Missions and Christian Progress"? Do our Bible students know Moulton's "Literary Study
of the Bible"?--a book so illuminating as to seem almost itself inspired. How many of the members
of the young people's societies of our churches have access to a standard concordance, Bible
dictionary, or a dictionary of sects and doctrines? Has the W. C. T. U. the reports of the
Committee of Fifty, that great committee of master minds, who made exhaustive investigation
and authoritative reports on the various aspects of the liquor question? Have the Masons a
history of free-masonry? Has the Shakespeare Club books on Shakespeare, and is the Political
Equality Club acquainted with standard works on political science and the franchise? Who has
a good "Cyclopedia of Quotations," or a "Reader's Handbook," where we can satisfy our curiosity
regarding allusions to "Fair Rosamond," "Apples of Hesperia," "Atlantis" and "Captain Cuttle"?

If we were to see a farmer laboriously cutting his wheat with a scythe, tying it into bundles
by hand, and then carrying the bundles on his back to the barn, we would think he was crazy.
Is it not as foolish, however, for us in our study work to do without the suitable tools and
helps which we might have in a public library?

HOLLEY (N. Y.) STANDARD.

WHY CITIES SUPPORT PUBLIC LIBRARIES

The proposition that only an enlightened and an intelligent people can make self-government
a success is so self-evident as to make argument but a vain repetition of empty words. And
yet we know that the public school side of our system of free public education is as yet only
able to secure five years' schooling for the average child in this country--an all too narrow
portal through which to enter upon successful citizenship. There is an imperative demand, then,
for the establishment and the development and for the wise administration of that other branch
of our system of free public education which we know as the public library.

We must understand clearly that the beneficent result of this system of education is just as
possible to the son of the peasant as to the son of the president, is just as helpful to the
blacksmith as to the barrister, to the farmer as to the philosopher; and in its possibilities
and in its helpfulness is a constant blessing to all and through all, and is needed by all alike.

The most worthy mind, that which is of most value to the world, is the well-informed mind which
is public and large. Only through the development of such, both as leaders and as followers,
can all classes be brought into an understanding of each other, can we preserve true republican
equality, can we avoid that insulation and seclusion which are unwholesome and unworthy of
true American manhood. The state has no resources at all comparable with its citizens. A man
is worth to himself just what he is capable of enjoying, and he is worth to the state just
what he is capable of imparting. These form an exact and true measure of every man. The greatest
positive strength and value, therefore, must always be associated with the greatest positive
and practical development of every faculty and power.

This, then, is the true basis of taxation for public libraries. Such a tax is subject to all
the canons of usual taxation, and may be defended and must be defended upon precisely the same
grounds as we defend the tax for the public schools.

JAMES HULME CANFIELD.

WHY MR. CARNEGIE ESTABLISHES LIBRARIES

I choose free libraries as the best agencies for improving the masses of the people, because
they give nothing for nothing. They only help those who help themselves. They never pauperize.
They reach the aspiring, and open to these the chief treasures of the world--those stored up
in books. A taste for reading drives out lower tastes.

Besides this, I believe good fiction one of the most beneficial reliefs to the monotonous lives
of the poor. For these and other reasons I prefer the free public library to most if not any
other agencies for the happiness and improvement of a community.

ANDREW CARNEGIE.

TO TEACHERS

Libraries are established that they may gather together the best of the fruits of the tree
of human speech, spread them before men in all liberality and invite all to enjoy them. The
schools are in part established that they may tell the young how to enjoy this feast. They
do this. They teach the young to read. They put them in touch with words and phrases; they
point out to them the delectable mountains of human thought and action, and then let them go.
It is to be lamented that they go so soon. At twelve, at thirteen, at fourteen at the most,
these young men and women, whose lives could be so broadened, sweetened, mellowed, humanized
by a few years' daily contact with the wisest, noblest, wittiest of our kind as their own words
portray them--at this early age, when reading has hardly begun, they leave school, and they
leave almost all of the best reading at the same time. If, now, you can bring these young citizens
into sympathy with the books the libraries would persuade them to read; if you can impress
upon them the reading habit; then the libraries can supplement your good work; will rejoice
in empty shelves; will feel that they are not in vain; and the coming generations will delight,
one and all, in that which good books can give; will speak more plainly; will think more clearly;
will be less often led astray by false prophets of every kind; will see that all men are of
the one country of humanity; and will--to sum it all--be better citizens of a good state.

I believe you will find there is something yet to do in reading in which the library can be
of help. Reading comes by practice. The practice which a pupil gets during school hours does
not make him a quick and skilful reader. There is not enough of it. If you encourage the reading
habit, and lead that habit, as you easily can, along good lines, your pupils will gain much,
simply in knowledge of words, in ability to get the meaning out of print, even though we say
nothing of the help their reading will give them in other ways.

J. C. DANA.

RIGHT USE OF BOOKS

When we consider how much the education that is continued after schooltime depends upon the
right use of books, we can hardly be too emphatic in asserting that something of that use should
be learned in the school. Yet almost nothing of the sort really is learned. The average student
in high school does not know the difference between a table of contents and an index, does
not know what a concordance is, does not know how to find what he wants in an encyclopedia,
does not even know that a dictionary has many other uses besides that of supplying definitions.
Still more pitiful is his naïve assumption that a book is a book, and that what book it is
does not particularly matter. It is the commonest of all experiences to hear a student say
that he has got a given statement from a book, and to find him quite incapable of naming the
book. That the source of information, as long as that information is printed somewhere, should
be of any consequence, is quite surprising to him, and still more the suggestion that it is
also his duty to have some sort of an opinion concerning the value and credibility of the authority
he thus blindly quotes. If the school library, and the instruction given in connection with
it, should do no more than impress these two elementary principles upon the minds of the whole
student body, it would go far towards accounting for itself as an educational means. That it
may, and should, do much more than this is the proposition that we have sought to maintain,
and we do not see how its essential reasonableness may be gainsaid.

DIAL, Feb. 1, 1906.

THE TRUE SPIRIT OF DEMOCRACY

The library supplies information for mechanics and workingmen of every class. Just as the system
of apprenticeship declines and employers require trained helpers, must the usefulness of the
library increase.

Library work offers great opportunity for philanthropy, and philanthropy of the higher form,
because its work is preventive, rather than positive. It anticipates evil by substituting the
antidote beforehand. It fosters the love of what is good and uplifting before low tastes have
become a chronic propensity. Pleasure in such books as the library would furnish to young readers
will interest the mind and occupy the thoughts exclusive of those evil practices invited by
the open door of idleness. The children generally come of their own free will; they are influenced
silently, unconsciously to themselves; they feel themselves welcome, loved, respected. Self-respect,
the mighty power to lift and keep erect, is fostered and developed.

The work of the library is for civic education and the making of good citizens, a form of patriotism
made imperative for the millions of foreigners coming yearly to our shores.

The public library offers common ground to all. There are no social lines to bar the entrance;
the doors open at every touch, if only the simple etiquette of quiet, earnest bearing is observed.
No creeds are to be subscribed to, the rich and poor meet together in absolute independence.
Even the aristocracy of intellect does not count in the people's university. The ideal public
library realizes the true spirit of democracy.

WALLER IRENE BULLOCK.

THE PUBLIC LIBRARY AS THE CENTER OF THE COMMUNITY

In more than one locality the local public library has come to be recognized as the natural
local center of the community, around which revolve the local studies, the local industries,
and all the various local interests of the town or village. Here, for instance, is the home
of the local historical society; here also is the home of the local camera club; of the natural
history society; of the study club and debating societies. Why is this? It is because those
in charge of the library have so thoroughly realized the fact that in a community the interests
of all are the interests of each, and that while this is true of other institutions as related
to each other, yet there is no one of them on which the lines of interest so invariably converge
from all the others--as "all roads lead to Rome."

W. E. FOSTER.

PUBLIC LIBRARIES

