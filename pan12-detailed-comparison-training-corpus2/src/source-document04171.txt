﻿[39] Ibid., 358-360.

[40] Moore, "Historical Notes," 19.

[41] Manuscripts in the Archives of Massachusetts, CXCIX, 80.

[42] Moore, "Historical Notes," 20.

[43] Laws of the State of New York, Chapter XXXII, Fourth Session.

[44] Sparks, "Correspondence of the American Revolution," III, 331.

[45] Moore, "Historical Notes," 20.

[46] Ibid., 21.

     Journals of the Continental Congress, 1779, pp. 386, 418.

[49] Sparks, "Writings of Washington," VIII, 322, 323.

[50] Ford, "Washington's Writings," VII, 371.

[51] Letter from the Adjutant General of the U.S. War Department.

[52] Schloezer's "Briefwechsel," IV, 365.

[52a] The Washington Manuscripts in the Library of Congress.

[53] "The Spirit of '76 in Rhode Island," 186-188.

[54] Sidney S. Rider, "An Historical Tract in the Rhode Island Series," No. 10.

[55] Marquis de Chastellux, "Travels," I, 454.

[56] Moore, "Historical Notes," 19.

[57] "The Spirit of Rhode Island in '76," 186-188.

[58] Washington, "The Story of the Negro," I, 311, Note.

[59] Moore, "Historical Notes," 22.

[60] Ibid., 16.

[61] Bancroft, "History of the United States," X, 133.

[62] Lecky, "American Revolution," 364.

[63] Austin Dabney, a remarkable free man of color, died at Zebulon. His remains repose, we
understand, near those of his friend Harris. The following account of Dabney, as given by Governor
Gilmer, may be interesting:

     In the beginning of the Revolutionary conflict, a man by the name of
     Aycock removed to Wilkes County, having in his possession a mulatto
     boy, who passed for and was treated as his slave. The boy had been
     called Austin, to which the captain to whose company he was attached
     added Dabney.

     Dabney proved himself a good soldier. In many a skirmish with the
     British and Tories, he acted a conspicuous part. He was with Colonel
     Elijah Clarke in the battle of Kettle Creek, and was severely wounded
     by a rifleball passing through his thigh, by which he was made a
     cripple for life. He was unable to do further military duty, and was
     without means to procure due attention to his wound, which threatened
     his life. In this suffering condition he was taken into the house of a
     Mr. Harris, where he was kindly cared for until he recovered. He
     afterwards labored for Harris and his family more faithfully than any
     slave could have been made to do.

     After the close of the war, when prosperous times came, Austin Dabney
     acquired property. In the year 18--, he removed to Madison County,
     carrying with him his benefactor and family. Here he became noted for
     his great fondness for horses and the turf. He attended all the races
     in the neighboring counties, and betted to the extent of his means.
     His courteous behavior and good temper always secured him gentlemen
     backers. His means were aided by a pension which he received from the
     United States.

     In the distribution of the public lands by lottery among the people
     of Georgia, the Legislature gave to Dabney a lot of land in the county
     of Walton. The Hon. Mr. Upson, then a representative from Oglethorpe,
     was the member who moved the passage of the law, giving him the lot
     of land.

     At the election for members of the Legislature the year after, the
     County of Madison was distracted by the animosity and strife of an
     Austin Dabney and an Anti-Austin Dabney party. Many of the people
     were highly incensed that a mulatto negro should receive a gift of
     the land which belonged to the freemen of Georgia. Dabney soon after
     removed to the land given him by the State, and carried with him
     the family of Harris, and continued to labor for them, and
     appropriated whatever he made for their support, except what was
     necessary for his coarse clothing and food. Upon his death, he left
     them all his property. The eldest son of his benefactor he sent to
     Franklin College, and afterwards supported him whilst he studied law
     with Mr. Upson, in Lexington. When Harris was undergoing his
     examination, Austin was standing outside of the bar, exhibiting great
     anxiety in his countenance; and when his young protégé was sworn in,
     he burst into a flood of tears. He understood his situation very
     well, and never was guilty of impertinence. He was one of the best
     chroniclers of the events of the Revolutionary War, in Georgia. Judge
     Dooly thought much of him, for he had served under his father, Colonel
     Dooly. It was Dabney's custom to be at the public house in Madison,
     where the judge stopped during court, and he took much pains in
     seeing his horse well attended to. He frequently came into the room
     where the judges and lawyers were assembled on the evening before the
     court, and seated himself upon a stool or some low place, where he
     would commence a parley with any one who chose to talk with him.

     He drew his pension in Savannah where he went once a year for this
     purpose. On one occasion he went to Savannah in company with his
     neighbor, Colonel Wyley Pope. They traveled together on the most
     familiar terms until they arrived in the streets of the town. Then
     the Colonel observed to Austin that he was a man of sense, and knew
     that it was not suitable to be seen riding side by side with a
     colored man through the streets of Savannah; to which Austin replied
     that he understood that matter very well. Accordingly when they came
     to the principal street, Austin checked his horse and fell behind.
     They had not gone very far before Colonel Pope passed the house of
     General James Jackson who was then governor of the state. Upon
     looking back he saw the governor run out of the house, seize Austin's
     hand, shake it as if he had been his long absent brother, draw him
     from his horse, and carry him into his house, where he stayed whilst
     in town. Colonel Pope used to tell this anecdote with much glee,
     adding that he felt chagrined when he ascertained that whilst he
     passed his time at a tavern, unknown and uncared for, Austin was the
     honored guest of the governor.

     White's "Historical Collections," 584.

FREEDOM AND SLAVERY IN APPALACHIAN AMERICA

To understand the problem of harmonizing freedom and slavery in Appalachian America we must
keep in mind two different stocks coming in some cases from the same mother country and subject
here to the same government. Why they differed so widely was due to their peculiar ideals formed
prior to their emigration from Europe and to their environment in the New World. To the tidewater
came a class whose character and purposes, although not altogether alike, easily enabled them
to develop into an aristocratic class. All of them were trying to lighten the burdens of life.
In this section favored with fertile soil, mild climate, navigable streams and good harbors
facilitating direct trade with Europe, the conservative, easy-going, wealth-seeking, exploiting
adventurers finally fell back on the institution of slavery which furnished the basis for a
large plantation system of seeming principalities. In the course of time too there arose in
the few towns of the coast a number of prosperous business men whose bearing was equally as
aristocratic as that of the masters of plantations.[1] These elements constituted the rustic
nobility which lorded it over the unfortunate settlers whom the plantation system forced to
go into the interior to take up land. Eliminating thus an enterprising middle class, the colonists
tended to become more aristocratic near the shore.

In this congenial atmosphere the eastern people were content to dwell. the East had the West
in mind and said much about its inexhaustible resources, but with the exception of obtaining
there grants of land nothing definite toward the conquest of this section was done because
of the handicap of slavery which precluded the possibility of a rapid expansion of the plantation
group in the slave States. Separated thus by high ranges of mountains which prevented the unification
of the interests of the sections, the West was left for conquest by a hardy race of European
dissenters who were capable of a more rapid growth.[2] these were the Germans and Scotch-Irish
with a sprinkling of Huguenots, Quakers and poor whites who had served their time as indentured
servants in the east.[3] The unsettled condition of Europe during its devastating wars of the
seventeenth and eighteenth centuries caused many of foreign stocks to seek homes in America
where they hoped to realize political liberty and religious freedom. Many of these Germans
first settled in the mountainous district of Pennsylvania and Maryland and then migrated later
to the lower part of the Shenandoah Valley, while the Scotch-Irish took possession of the upper
part of that section. Thereafter the Shenandoah Valley became a thoroughfare for a continuous
movement of these immigrants toward the south into the uplands and mountains of the Carolinas,
Georgia, Kentucky, and Tennessee.[4]

Among the Germans were Mennonites, Lutherans, and Moravians, all of whom believed in individual
freedom, the divine right of secular power, and personal responsibility.[5] The strongest stock
among these immigrants, however, were the Scotch-Irish, "a God-fearing, Sabbath-keeping, covenant-adhering,
liberty-loving, and tyrant-hating race," which had formed its ideals under the influence of
philosophy of John Calvin, John Knox, Andrew Melville, and George Buchanan. By these thinkers
they had been taught to emphasize equality, freedom of conscience, and political liberty. These
stocks differed somewhat from each other, but they were equally attached to practical religion,
homely virtues, and democratic institutions.[7] Being a kind and beneficent class with a tenacity
for the habits and customs of their fathers, they proved to be a valuable contribution to the
American stock. As they had no riches every man was to be just what he could make himself.
Equality and brotherly love became their dominant traits. Common feeling and similarity of
ideals made them one people whose chief characteristic was individualism.[8] Differing thus
so widely from the easterners they were regarded by the aristocrats as "Men of new blood" and
"Wild Irish," who formed a barrier over which "none ventured to leap and would venture to settle
among."[9] No aristocrat figuring conspicuously in the society of the East, where slavery made
men socially unequal, could feel comfortable on the frontier, where freedom from competition
with such labor prevented the development of caste.

The natural endowment of the West was so different from that of the East that the former did
not attract the people who settled in the Tidewater. The mountaineers were in the midst of
natural meadows, steep hills, narrow valleys of hilly soil, and inexhaustible forests. In the
East tobacco and corn were the staple commodities. Cattle and hog raising became profitable
west of the mountains, while various other employments which did not require so much vacant
land were more popular near the sea. Besides, when the dwellers near the coast sought the cheap
labor which the slave furnished the mountaineers encouraged the influx of freemen. It is not
strange then that we have no record of an early flourishing slave plantation beyond the mountains.
Kercheval gives an account of a settlement by slaves and overseers on the large Carter grant
situated on the west side of the Shenandoah, but it seems that the settlement did not prosper
as such, for it soon passed into the hands of the Burwells and the Pages.[10]

The rise of slavery in the Tidewater section, however, established the going of those settlers
in the direction of government for the people. The East began with indentured servants but
soon found the system of slavery more profitable. It was not long before the blacks constituted
the masses of the laboring population,[11] while on the expiration of their term of service
the indentured servants went west and helped to democratize the frontier. Caste too was secured
by the peculiar land tenure of the East. The king and the proprietors granted land for small
sums on feudal terms. The grantees in their turn settled these holdings in fee tail on the
oldest son in accordance with the law of primogeniture. This "West" was for a number of years
known as the region beyond the Blue Ridge Mountains and later beyond the Alleghenies. A more
satisfactory dividing line, however, is the historical line of demarcation between the East
and West which moved toward the mountains in the proportion that the western section became
connected with the East and indoctrinated by its proslavery propagandists. In none of these
parts, however, not even far south, were the eastern people able to bring the frontiersmen
altogether around to their way of thinking. Their ideals and environment caused them to have
differing opinions as to the extent, character, and foundations of local self-government, differing
conceptions of the meaning of representative institutions, differing ideas of the magnitude
of governmental power over the individual, and differing theories of the relations of church
and State. The East having accepted caste as the basis of its society naturally adopted the
policy of government by a favorite minority, the West inclined more and more toward democracy.
The latter considered representatives only those who had been elected as such by a majority
of the people of the district in which they lived; the former believed in a more restricted
electorate, and the representation of districts and interests, rather than that of numbers.[13]
Furthermore, almost from the founding of the colonies there was court party consisting of the
rich planters and favorites composing the coterie of royal officials generally opposed by a
country party of men who, either denied certain privileges or unaccustomed to participation
in the affairs of privileged classes, felt that the interests of the lowly were different.
As the frontier moved westward the line of cleavage tended to become identical with that between
the privileged classes and the small farmers, between the lowlanders and the uplanders, between
capital and labor, and finally between the East and West.

The frontiersmen did not long delay in translating some of their political theories into action.
The aristocratic East could not do things to suit the mountaineers who were struggling to get
the government nearer to them. At times, therefore, their endeavors to abolish government for
the people resulted in violent frontier uprisings like that of Bacon's Rebellion in Virginia
and the War of Regulation in North Carolina. In all of these cases the cause was practically
the same. These pioneers had observed with jealous eye the policy which bestowed all political
honors on the descendants of a few wealthy families living upon the tide or along the banks
of the larger streams. They were, therefore, inclined to advance with quick pace toward revolution.[14]
On finding such leaders as James Otis, Patrick Henry and Thomas Jefferson, the frontiersmen
instituted such a movement in behalf of freedom that it resulted in the Revolutionary War.[15]
These patriots' advocacy of freedom, too, was not half-hearted. When they demanded liberty
for the colonists they spoke also for the slaves, so emphasizing the necessity for abolition
that observers from afar thought that the institution would of itself soon pass away.[16]

In the reorganization of the governments necessitated by the overthrow of the British, however,
the frontiersmen were unfortunate in that they lacked constructive leadership adequate to having
their ideas incorporated into the new constitutions. In the other Atlantic States where such
distinctions were not made in framing their constitutions, the conservatives resorted to other
schemes to keep the power in the hands of the rich planters near the sea. When the Appalachian
Americans awoke to the situation then they were against a stone wall. The so-called rights
of man were subjected to restrictions which in our day could not exist. The right to hold office
and to vote were not dependent upon manhood qualifications but on a white skin, religious opinions,
the payment of taxes, and wealth. In South Carolina a person desiring to vote must believe
in the existence of a God, in a future state of reward and punishment, and have a freehold
of fifty acres of land. In Virginia the right of suffrage was restricted to freeholders possessing
one hundred acres of land. Senators in North Carolina had to own three hundred acres of land;
representatives in South Carolina were required to have a 500 acre freehold and 10 Negroes;
and in Georgia 250 acres and support the Protestant religion.[18] In all of these slave States,
suffering from such unpopular government, the mountaineers developed into a reform party persistently
demanding that the sense of the people be taken on the question of calling together their representatives
to remove certain defects from the constitutions. It was the contest between the aristocrats
and the progressive westerner. The aristocrats' idea of government was developed from the "English
Scion--the liberty of kings, lords, and commons, with different grades of society acting independently
of all foreign powers." In taking this position, these conservatives brought down upon their
heads all of the ire that the frontiersmen had felt for the British prior to the American Revolution.
The easterners were regarded in the mountains as a party bent upon establishing in this country
a régime equally as oppressive as the British government. The frontiersmen saw in slavery the
cause of the whole trouble. They, therefore, hated the institution and endeavored more than
ever to keep their section open to free labor. They hated the slave as such, not as a man.
On the early southern frontier there was more prejudice against the slaveholder than against
the Negro.[20] There was the feeling that this was not a country for a laboring class so undeveloped
as the African slaves, then being brought to these shores to serve as a basis for a government
differing radically from that in quest of which the frontiersmen had left their homes in Europe.

This struggle reached its climax in different States at various periods. In Maryland the contest
differed somewhat from that of other Southern States because of the contiguity of that commonwealth
with Pennsylvania, which early set such examples of abolition and democratic government that
a slave State near by could not go so far in fortifying an aristocratic governing class. In
Virginia the situation was much more critical than elsewhere. Unlike the other Atlantic States,
which wisely provided roads and canals to unify the diverse interests of the sections, that
commonwealth left the trans-Alleghany district to continue in its own way as a center of insurgency
from which war was waged against the established order of things.[21] In most States, however,
the contest was decided by the invention of the cotton gin and other mechanical appliances
which, in effecting an industrial revolution throughout the world, gave rise to the plantation
system found profitable to supply the increasing demand for cotton. In the course of the subsequent
expansion of slavery, many of the uplanders and mountaineers were gradually won to the support
of that institution. Realizing gradually a community of interests with the eastern planters,
their ill-feeling against them tended to diminish. Abolition societies which had once flourished
among the whites of the uplands tended to decline and by 1840 there were practically no abolitionists
in the South living east of the Appalachian Mountains.[22]

Virginia, which showed signs of discord longer than the other Atlantic States, furnishes us
a good example of how it worked out. The reform party of the West finally forced the call of
a convention in 1829, hoping in vain to crush the aristocracy. Defeated in this first battle
with the conservatives, they secured the call of the Reform Convention in 1850 only to find
that two thirds of the State had become permanently attached to the cause of maintaining slavery.[23]
Samuel McDowell Moore, of Rockbridge County in the Valley, said in the Convention of 1829-30
that slaves should be free to enjoy their natural rights,[24] but a generation later the people
of that section would not have justified such an utterance in behalf of freedom. The uplanders
of South Carolina were early satisfied with such changes as were made in the apportionment
of representation in 1808, and in the qualifications of voters in 1810.[25] Thereafter Calhoun's
party, proceeding on the theory of government by a concurrent majority, vanquished what few
liberal-minded men remained, and then proceeded to force their policy on the whole country.
Some of this sentiment continued in the mountains. There was an unusually strong anti-slavery
element in Davie, Davidson, Granville and Guilford counties. The efforts of this liberal group,
too, were not long in taking organized form. While there were several local organizations operating
in various parts, the efforts of the anti-slavery people centered around the North Carolina
Manumission Society. It had over forty branches at one time, besides several associations of
women, all extending into seven or eight of the most populous counties of the State. This society
denounced the importation and exportation of slaves, and favored providing for manumissions,
legalizing slave contracts for the purchase of freedom, and enacting a law that at a certain
age all persons should be born free.[28] That these reformers had considerable influence is
evidenced by the fact that in 1826 a member of the manumission society was elected to the State
Senate. In 1824 and 1826 two thousand slaves were freed in North Carolina.[29] Among the distinguished
men who at times supported this movement in various ways were Hinton Rowan Helper, Benjamin
S. Hedrick, Daniel R. Goodloe, Eli W. Caruthers, and Lunsford Lane, a colored orator and lecturer
of considerable ability.[30] They constituted a hopeless minority, however, for the liberal
element saw their hopes completely blasted in the triumph of the slave party in the Convention
of 1835, which made everything subservient to the institution of slavery.

In the mountains of Kentucky and Tennessee conditions were a little more encouraging, especially
between 1817 and 1830. The anti-slavery work in Kentucky seemed to owe its beginning to certain
"Emancipating Baptists." Early in the history of that State six Baptist preachers, Carter Tarrant,
David Darrow, John Sutton, Donald Holmes, Jacob Gregg, and George Smith, began an anti-slavery
campaign, maintaining that there should be no fellowship with slaveholders.[31] They were unable
to effect much, however, because of the fact that they had no extensive organization through
which to extend their efforts. Every church remained free to decide for itself and even in
Northern States the Baptists later winked at slavery. More effective than these efforts of
the Baptists was the work of the Scotch-Irish. Led by David Rice, a minister of the Presbyterian
Church, the anti-slavery element tried to exclude slavery from the State when framing its first
constitution in the Convention of 1792.[32] Another effort thus to amend the fundamental law
was made at the session of the legislature of 1797-98, and had it not been for the excitement
aroused by the Alien and Sedition Laws, the bill probably would have passed.[33]

Many successful efforts were made through the anti-slavery bodies. The society known as "Friends
of Humanity" was organized in Kentucky in 1807. It had a constitution signed by eleven preachers
and thirteen laymen. The organization was in existence as late as 1813. The records of the
abolitionists show that there was another such society near Frankfort between 1809 and 1823.[34]
Birney then appeared in the State and gave his influence to the cause with a view to promoting
the exportation of Negroes to Liberia.[35] A number of citizens also memorialized Congress
to colonize the Negroes on the public lands in the West.[36] In the later twenties an effort
was made to unite the endeavors of many wealthy and influential persons who were then interested
in promoting abolition. Lacking a vigorous and forceful leader, they appealed to Henry Clay,
who refused.[37] They fought on, however, for years to come. A contributor to the Western Luminary
said, in 1830, that the people of Kentucky were finding slavery a burden.[38] Evidently a good
many of them had come to this conclusion, for a bill providing for emancipation introduced
in the Legislature was postponed indefinitely by a vote of 18 to 11.[39] So favorable were
conditions in Kentucky at this time that it was said that Tennessee was watching Kentucky with
the expectation of following her lead should the latter become a free State as was then expected.

The main factor in promoting the work in Tennessee was, as in Kentucky, the Scotch-Irish Presbyterian
stock. They opposed slavery in word and in deed, purchasing and setting free a number of colored
men. Among these liberal westerners was organized the "Manumission Society of Tennessee," represented
for years in the American Convention of Abolition Societies by Benjamin Lundy.[40] The Tennessee
organization once had twenty branches and a membership of six hundred.[41] Among its promoters
were Charles Osborn, Elihu Swain, John Underhill, Jesse Willis, John Cannady, John Swain, David
Maulsby, John Rankin, Jesse Lockhart, and John Morgan.[42] They advocated at first immediate
and unconditional emancipation, but soon seeing that the realization of this policy was impossible,
they receded from this advanced position and memorialized their representatives to provide
for gradual emancipation, the abolition of slavery in the District of Columbia, the prevention
of the separation of families, the prohibition of the interstate slave trade, the restriction
of slavery, the general improvement of colored people through church and school, and especially
the establishment among them of the right of marriage.[43] To procure the abolition of slavery
by argument, other persons of this section organized another body, known as the "Moral Religious
Manumission Society of West Tennessee."[44] It once had a large membership and tended to increase
and spread the agitation in behalf of abolition.

In view of these favorable tendencies, it was thought up to 1830 that Tennessee, following
the lead of Kentucky, would become a free State.[45] But just as the expansion of slavery into
the interior of the Atlantic States attached those districts to the fortunes of the slaveholding
class, it happened in some cases in the mountains which to some extent became indoctrinated
by the teaching of the defenders of slavery. Then the ardent slavery debate in Congress and
the bold agitation, like that of the immediatists led by William Lloyd Garrison, alienated
the support which some mountaineers had willingly given the cause. Abolition in these States,
therefore, began to weaken and rapidly declined during the thirties.[46] Because of a heterogeneous
membership, these organizations tended to develop into other societies representing differing
ideas of anti-slavery factions which had at times made it impossible for them to cooperate
effectively in carrying out any plan. The slaveholders who had been members formed branches
of the American Colonization Society, while the radical element fell back upon organizing branches
of the Underground Railroad to cooperate with those of their number who, seeing that it was
impossible to attain their end in the Southern mountains, had moved into the Northwest Territory
to colonize the freedmen and aid the escape of slaves.[47] Among these workers who had thus
changed their base of operation were not only such noted men as Joshua Coffin, Benjamin Lundy,
and James G. Birney, but less distinguished workers like John Rankin, of Ripley; James Gilliland,
of Red Oak; Jesse Lockhart, of Russellville; Robert Dobbins, of Sardinia; Samuel Crothers,
of Greenfield; Hugh L. Fullerton, of Chillicothe, and William Dickey, of Ross or Fayette County,
Ohio. There were other southern abolitionists who settled and established stations of the Underground
Railroad In Bond, Putnam, and Bureau Counties, Illinois.[48] The Underground Railroad was thus
enabled to extend into the heart of the South by way of the Cumberland Mountains. The operation
of the system extended through Tennessee into northern Georgia and Alabama, following the Appalachian
highland as it juts like a peninsula into the South. Dillingham, John Brown, and Harriet Tubman
used these routes.

Let us consider, then, the attitude of these mountaineers toward slaves. All of them were not
abolitionists. Some slavery existed among them. The attack on the institution, then, in these
parts was not altogether opposition to an institution foreign to the mountaineers. The frontiersmen
hated slavery, hated the slave as such, but, as we have observed above, hated the eastern planter
worse than they hated the slave. As there was a scarcity of slaves in that country they generally
dwelt at home with their masters. Slavery among these liberal people, therefore, continued
patriarchal and so desirous were they that the institution should remain such that they favored
the admission of the State of Missouri as a slave State,[50] not to promote slavery but to
expand it that each master, having a smaller number of Negroes, might keep them in close and
helpful contact. Consistently with this policy many of the frontier Baptists, Scotch-Irish
and Methodists continued to emphasize the education of the blacks as the correlative of emancipation.
A number of persons united in 1825 to found an institution for the education of eight or ten
Negro slaves with their families, to be operated under the direction of the "Emancipating Labor
Society of the State of Kentucky." About the same time Frances Wright was endeavoring to establish
an institution on the same order to improve the free blacks and mulattoes in West Tennessee.
It seems that this movement had the support of a goodly number of persons, including George
Fowler, and, it was said, Lafayette, who had always been regarded as a friend of emancipation.
Exactly what these enterprises were, however, it is difficult to determine. They were not well
supported and soon passed from public notice. Some have said that the Tennessee project was
a money-making scheme for the proprietors, and that the Negroes taught there were in reality
slaves. Others have defended the work as a philanthropic effort so characteristic of the friends
of freedom in Appalachian America.[52]

The people of Eastern Tennessee were largely in favor of Negro education. Around Maryville
and Knoxville were found a considerable number of white persons who were thus interested in
the uplift of the belated race. Well might such efforts be expected in Maryville, for the school
of theology at this place had gradually become so radical that according to the Maryville Intelligencer
half of the students by 1841 declared their adherence to the cause of abolition.[53] Consequently,
they hoped not only to see such doctrines triumph within the walls of that institution, but
were endeavoring to enlighten the Negroes of that community to prepare them for the enjoyment
of life as citizens in their own or some other country.[54]

Just as the people of Maryville had expressed themselves through The Intelligencer, so did
those of Knoxville find a spokesman in The Presbyterian Witness.

From a group in Kentucky came another helpful movement. Desiring to train up white men who
would eventually be able to do a work which public sentiment then prevented the anti-slavery
minority from carrying on, the liberal element of Kentucky, under the leadership of John G.
Fee and his coworkers, established Berea College. This profession was not really put to a test
until after the Civil War, when the institution courageously met the issue by accepting as
students some colored soldiers who were returning home wearing their uniforms.[56] The State
has since prohibited the co-education of the races.

With so many sympathizers with the oppressed in the back country, the South had much difficulty
in holding the mountaineers in line to force upon the whole nation their policies, mainly determined
by their desire for the continuation of slavery. Many of the mountaineers accordingly deserted
the South in its opposition to the tariff and internal improvements, and when that section
saw that it had failed in economic competition with the North, and realized that it had to
leave the Union soon or never, the mountaineers who had become commercially attached to the
North and West boldly adhered to these sections to maintain the Union. The highlanders of North
Carolina were finally reduced to secession with great difficulty; Eastern Tennessee had to
yield, but kept the State almost divided between the two causes; timely dominated by Unionists
with the support of troops, Kentucky stood firm; and to continue attached to the Federal Government
forty-eight western counties of Virginia severed their connection with the essentially slaveholding
district and formed the loyal State of West Virginia.

In the mountainous region the public mind has been largely that of people who have developed
on free soil. They have always differed from the dwellers in the district near the sea not
only in their attitude toward slavery but in the policy they have followed in dealing with
the blacks since the Civil War. One can observe even to-day such a difference in the atmosphere
of the two sections, that in passing from the tidewater to the mountains it seems like going
from one country into another. There is still in the back country, of course, much of that
lawlessness which shames the South, but crime in that section is not peculiarly the persecution
of the Negro. Almost any one considered undesirable is dealt with unceremoniously. In Appalachian
America the races still maintain a sort of social contact. White and black men work side by
side, visit each other in their homes, and often attend the same church to listen with delight
to the Word spoken by either a colored or white preacher.

C. G. WOODSON

FOOTNOTES:

[1] Wertenbaker, "Patrician and Plebeian in Virginia," 31.

[2] Exactly how many of each race settled in the Appalachian region we cannot tell, but we
know that they came in large numbers, after the year 1735. A few important facts and names
may give some idea as to the extent of this immigration. The Shenandoah Valley attracted many.
Most prominent among those who were instrumental in settling the Valley was the Scotchman,
John Lewis, the ancestor of so many families of the mountains. The Dutchmen, John and Isaac
Van Meter, were among the first to buy land from Joist Hite, probably the first settler in
the Valley. Among other adventurers of this frontier were Benjamin Allen, Riley Moore, and
William White, of Maryland, who settled in the Shenandoah in 1734; Robert Harper and others
who, in the same year, settled Richard Morgan's grant near Harper's Ferry; and Howard, Walker,
and Rutledge, who took up land on what became the Fairfax Manor on the South Branch. In 1738
some Quakers came from Pennsylvania to occupy the Ross Survey of 40,000 acres near Winchester
Farm in what is now Frederick County, Virginia. In the following year John and James Lindsay
reached Long Marsh, and Isaac Larne of New Jersey the same district about the same time; while
Joseph Carter of Bucks County, Pennsylvania, built his cabin on the Opequon near Winchester
in 1743, and Joseph Hampton with his two sons came from Maryland to Buck Marsh near Berryville.
But it is a more important fact that Burden, a Scotch-Irishman, obtained a large grant of land
and settled it with hundreds of his race during the period from 1736 to 1743, and employed
an agent to continue the work. With Burden came the McDowells, Alexanders, Campbells, McClungs,
McCampbells, McCowans, and McKees, Prestons, Browns, Wallaces, Wilsons, McCues, and Caruthers.
They settled the upper waters of the Shenandoah and the James, while the Germans had by this
time well covered the territory between what is known as Harrisonburg and the present site
of Harper's Ferry. See Maury, "Physical Survey," 42; Virginia Magazine, IX, 337-352; Washington's
Journal, 47-48; Wayland, "German Element of the Shenandoah," 110.

[3] Wayland, "German Element of the Shenandoah," 28-30; Virginia Historical Register, III, 10.

[4] See Meade, "Old Families of Virginia," The Transalleghany Historical Magazine, I and II;
De Hass, "The Settlement of Western Virginia," 71, 75; Kercheval, "History of the Valley,"
61-71; Faust, "The German Element in the United States."

[5] Dunning, "The History of Political Theory from Luther to Montesquieu," 9,10.

[6] Not in Text

[7] Buchanan, the most literary of these reformers, insisted that society originates in the
effort of men to escape from the primordial state of nature, that in a society thus formed
the essential to well-being is justice, that justice is maintained by laws rather than by kings,
that the maker of the laws is the people, and that the interpreter of the laws is not the king,
but the body of judges chosen by the people. He reduced the power of the ruler to the minimum,
the only power assigned to him being to maintain the morals of the state by making his life
a model of virtuous living. The reformer claimed, too, that when the ruler exceeds his power
he becomes a tyrant, and that people are justified in rejecting the doctrine of passive obedience
and slaying him. See Buchanan, "De Jure Apud Scotos" (Aberdeen, 1762); Dunning, "History of
Political Theories from Luther to Montesquieu"; and P. Hume Brown, "Biography of John Knox."

