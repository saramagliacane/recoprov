﻿"So am I," grunted Buck, surlily. "No, I'm sorry he didn't live to torment you. No, the only
thing I'm really sorry about is that 'twas Brick Avery's money he got away with."

Avery sighed.

"But I want to say to you, Signory Rosy-elly," continued Buck, with a burst of pride quite
excusable, tipping his hat to one side and hooking his thumb into the armhole of his vest,
"it wasn't my money you got, and it never will be my money you'll get. You just made the mistake
of your life when you run away from me."

"He's got fifty thousand dollars in the bank," hoarsely whispered Avery, vicariously sharing
in this pride of prosperity--the prosperity beyond her reach.

"Uh-huh! Correct!" corroborated Buck, surveying her in increasing triumph. This moment was
really worth waiting through the years for, he reflected.

"Twenty can play as well as one," croaked the parrot, his beady eye pressed between the bars
of his cage.

The signora glanced up at this new speaker, eyed Elkanah with a sage look that he returned,
and then, after a moment's reflection, said:

"Thanks for the suggestion, old chap. That is to say, three can play as well as two, when there's
fifty thousand in the bank. Buck, you know I'm always outspoken and straight to the point.
No underhanded bluff for me. I'm going to sue you for ten thousand."

"Crack 'em down, gents!" remarked Elkanah, grimly.

Buck cast a malevolent look at the bird, and then, his cigar tip-tilted and the corner of his
mouth sarcastically askew, suggested with an air as though the idea were the limit of satiric
impossibility:

"I want to know! Breach of promise, I per-sume!"

"Good aim! You've rung the bell," rejoined the lady, coolly.

The unconscionable impudence of the bare suggestion fetched a gasp from both men. Plug Ivory's
assumption of dignity crumbled immediately. The years rolled back. He felt one of those old-time
fits of rage come bristling up the back of his head, the fury of old when he had tried to wither
that giddy creature in his spasms of jealousy. But now, as in the past, her calm assurance
put him out of countenance and his wild anathemas died away in sputterings.

"I know all that, Ivory Buck," she said, icily. "But how are you going to prove I was married?
Where are you going to hunt for witnesses? Professional people are like wild geese--roosting
on air and moulting their names like feathers. What proof of anything are you going to find
after all these thirty years? While I--I've got your letters, every one--all your promises.
Observe how I take my cue! Jury a-listening! I've been hunting the world over for you. You
hid here. Here I find you--this poor, deserted woman, whose life has been wrecked by your faithlessness,
finds you. Me, with a crape veil, a sniff in my nose, a crushed-creature face make-up, a tremolo
in my voice and a smart lawyer such as I know about! What can you two old fools say to a country
jury to block my bluff? Why, you can save money by handing me your bank book!"

In his fury Buck grabbed her chair and tipped it forward violently in order to dump her off
his sacred platform. She fled out into space with a flutter of skirts, landed lightly as a
cat and pirouetted on one toe, crooking her arms in the professional pose that appeals for
applause.

"This is the first time Signora Rosyelli, champion bareback rider, ever tried to ride a mule,"
she chirped, "but you see she can do it and make her graceful dismount to the music of the band."

Several villagers across the road were gaping at the scene. She inquired the way to the tavern,
one of them took her valise, and she went down the road, tossing a kiss from her finger tips
toward the two plug hats. Plug hats watched her out of sight and then turned toward each other
with simultaneous jerk.

"Don't that beat tophet and repeat?" they inquired, in exact unison.

"What are you going to do?" asked Plug Avery.

"Fight her! Fight her clear to the high, consolidated supreme court aggregation of the United
States, or whatever they call it!" roared Plug Ivory.

"Nobody has ever beat her yet, except Dellybunko, and we ain't in his class," sighed Avery,
despondently.

"You don't think, do you, that I'm going to lap my thumb and finger and peel her off ten thousand
dollars?"

"Why don't you and she get married and we'll all live here, happy, hereafter?" wistfully suggested
Avery. "If it was in a book it would end off like that--sure pop."

"This ain't no book," replied Buck, elbows on his knees, eyes moodily on the dusty planks.

"So you're bound to go to court?"

"Low court--high court--clear to the ridgepole--clear to the cupoly, and then I'll shin the
weather vane with the star spangled banner of justice between my teeth."

"I heard a breach of promise trial once," related Avery, half closing his eyes in reminiscence,
"and it was the funniest thing I ever listened to. 'Twas twenty years ago, and I'll bet that
the people down there laugh yet when they see that fellow walk along the street. Them letters
he wrote was certainly the squashiest--why, every one of them seemed to woggle like a tumbler
of jelly--sweet and sloppy, as you might say! It being so long ago, when you was having your
spell, I don't suppose you remember just what you wrote to her, do you?"

Avery still gazed at the same knothole, but a hot flush was crawling up from under his collar.
He took off his plug hat and scuffed his wrist across his steaming forehead.

"I remember that he called her 'Ittikins, Pittikins, Popsy-sweet.' Thought I'd die laughing
at that trial! Did you sling in any names like that, Ivory? You being so prominent now and
settled down and having money in the bank, them kind of names, if you wrote mushy like that,
will certainly tickle folks something tremendous."

A student in physiognomy might have read that memory was playing havoc with Buck's resolution.
Avery was knitting his brows in deep reflection, knuckling his forehead.

"Seems as if," he went on, slowly, "she told me you called her something like 'Sweety-tweety,'
or 'Tweeny-weeny Girlikins'--something like that. How them newspapers do like to string out
things--funny kind of things, when a man is prominent and well known, and has got money in
the bank! Folks can't help laughing--they just naturally can't, Ive! You'll be setting there
in court, looking ugly as a gibcat and her lawyer reading them things out. Them cussed lawyers
have a sassy way of----"

Buck got up, kicked his chair off onto the ground, and in choler uncontrollable, clacked his
fists under Avery's nose and barked:

"Twit me another word--just one other word--and I'll drive that old nose of yourn clear up
into the roof of your head!"

Then he locked his store door and stumped away across the field to the big barn, where the
remains of Buck's Leviathan Circus reposed in isolated state.

No one knows by just what course of agonized reasoning he arrived at his final decision, but
at dusk he came back to the store. With the dumb placidity of some ruminant, Avery was sitting
in his same place on the platform of the emporium.

"Brick," said Ivory, humbly, "I've been thinking back and remembering what I wrote to her--and
it's all of it pretty clear in my mind, 'cause I never wrote love letters to anyone else. And
I can't face it. I couldn't sit in court and hear it. I couldn't sit here on this platform
in my own home place and face the people afterward. I couldn't start on the road with a circus
and have the face to stand before the big tent after it and bark like I used to. They'd grin
me out of business. I'd be backed into the stall. No, I can't do it. Go down and see what she'll
compromise on."

Avery came back after two hours and loomed in the dusk before the platform. He fixed his eyes
on the plug hat that was still lowered in the attitude of despondency.

"I wrassled with her, Ivory, just the same as if I was handling my own money, and I beat her
down to sixty-six hundred. She won't take a cent less."

"I'll tell you what that sounds like to me," snarled Buck, after a moment of meditation. "It
sounds as if she was going to get five thousand and you was looking after your little old sixteen
hundred."

A couple of tears squeezed out and down over Avery's flabby cheeks.

"This ain't the first time you've misjudged me, when I've been doing you a favor," said he.
"And it's all on account of the same mis'able woman that I'm misjudged--and we was living so
happy here, me and you. I wish she was in----" His voice broke.

"I ain't responsible for what I'm saying, Avery," pleaded Buck, contritely. "You know what
things have happened to stir me up the last few hours--yes, all my life, for that matter. I
ain't been comfortable in mind for thirty years till you come here and cheered me up and showed
me what's what. I appreciate it and I'll prove that to you before we're done. We'll get along
together all right after this. All is, you must see me through."

Then the two plug hats bent together in earnest conference.

The next morning Avery, armed with an order on the savings bank at the shire for six thousand
six hundred dollars, and with Buck's bank book in his inside pocket, drove up to the door of
Fyles' tavern in Buck's best carriage, and Signora Rosyelli flipped lightly up beside the peace
commissioner.

He was to pay over the money on the neutral ground at the shire, receive the letters, put her
aboard a train and then come back triumphantly into that interrupted otium cum dignitate of
Smyrna Corner.

For two days a solitary and bereaved plug hat on the emporium's platform turned its fuzzy gloss
toward the bend in the road at the clump of alders. But the sleek black nose of Buck's "reader"
did not appear.

On the third day the bank book arrived by mail, its account minus six thousand six hundred
dollars, and between its leaves a letter. It was an apologetic letter, and yet it was flavored
with a note of complaint. Brick Avery stated that after thinking it all over he felt that,
having been misjudged cruelly twice, it might happen again, and being old, he could not endure
griefs of that kind. He had supported the first two, but being naturally tender-hearted and
easily influenced, the third might be fatal. Moreover, the conscience of Signora Rosyelli had
troubled her, so he believed, ever since the affair of the one thousand six hundred dollars.
So he had decided that he would quiet her remorse by marrying her and taking entire charge
of her improved finances. In fact, so certain was he that she would waste the money--being
a woman fickle and vain--that he had insisted on the marriage, and she, realizing her dependence
on his aid in cashing in, assented, and now he assured her that as her husband he was entitled
to full control of their affairs--all of which, so the letter delicately hinted, was serving
as retribution and bringing her into a proper frame of mind to realize her past enormities.
The writer hoped that his own personal self-sacrifice in thus becoming the instrument of flagellation
would be appreciated by one whom he esteemed highly.

They would be known at the fairs as Moseer and Madame Bottotte, and would do the genteel and
compact gift-sale graft from the buggy--having the necessary capital now--and would accept
the buggy and horse as a wedding present, knowing that an old friend with forty-three thousand
four hundred dollars still left in the bank would not begrudge this small gift to a couple
just starting out in life, and with deep regard for him and all inquiring friends, they were, etc.

In the more crucial moments of his life Buck had frequently refrained from anathema as a method
of relief. Some situations were made vulgar and matter-of-fact by sulphurous ejaculation. It
dulled the edge of rancor brutally, as a rock dulls a razor.

Now he merely turned the paper over, took out a stubby lead pencil, licked it and began to
write on the blank side, flattening the paper on his bank book.

    FOR SALE--1 Band Wagon, 1 Swan Chariot, 3 Lion Cages.

He paused here in his laborious scrawl and, despite his resolution of silence, muttered:

"It's going to be a clean sale. I don't never in all my life want to hear of a circus, see
a circus, talk circus, see a circus man----"

"Crack 'em down, gents!" squalled the parrot. It was the first time for many hours that he
had heard his master's voice, and the sound cheered him. He hooked his beak around a wire and
rattled away jovially. He seemed to be relieved by the absence of the other plug hat that had
been absorbing so much of the familiar, beloved and original plug hat's attention.

Ivory looked up at Elkanah vindictively and then resumed his soliloquy.

"No, sir, never! Half of circusing is a skin game all through--and I've done my share of the
skinning. But to be skinned twice--me, I. Buck, proprietor--and the last time the worst, but----"

"Twenty can play it as well as one!" the parrot yelled, cocking his eye over the edge of the cage.

It was an evil scowl that flashed up from under the plug hat, but Elkanah in his new joy was
oblivious.

"Me a man that's been all through it from A to Z--my affections trod on, all confidence in
females destroyed and nothing ahead of me all the rest of my life! No, sir, I never want to
hear of a circus again. Bit by the mouths I fed--and they thumbing their noses at me. That
trick----"

"It's the old army game!" squealed the parrot, in nerve-racking rasp.

Ivory Buck arose, yanked the bottom off the cage, caught the squawking bird, wrung his neck,
tossed him into the middle of the road, and then, sucking his bleeding finger, went on writing
the copy for his advertisement.

SUPPER WITH NATICA

By ROBERT E. MACALARNEY

It isn't at all pleasant to burn one's fingers, but it's worth while burning them now and then,
if you have to be scorched to be near a particularly attractive fire; at least I've found it
that way. All of which leads me to Natica Drayton--Melsford that was.

I think I'm the only one of the crew she dragged at her heels who hasn't forgot about things
and gone off after other game; some of them have been lashed to the burning stake of pretty
uncomfortable domesticity, too. As for me--well, I've simply gone on caring, and I think I
shall always go on.

Does she know it? Of course she knows it; always has known it, ever since that first summer
at Sacandaga. Not that I've been ass enough to say anything after the first time. I'm only
an ordinary sort of chap when it comes to intuition, but somehow I've never plucked up the
cheek to do any talking about my own miserable self; not since she let me down as gently as
she could, while I paddled her back from Birch Point to the canoe house, with Elephant Mountain
ragged-backed in the moon-haze. For the life of me I couldn't tell you what it was she said.
There was the drip of water from the paddle as I lifted it, stroke after stroke; the tiny hiss
of smother at the prow, and twisted through it all, like a gathering string, Natica Melsford's
voice, letting me down easy--as easily as she could.

After I had made fast, I remember feeling that somehow the moonlight had turned things extremely
cold; and I reached for my sweater that lay in the stern. I also laughed a great deal too much
around the logs at the bungalow fire, and then drank a deal more than too much at the clubhouse
before turning in. Maybe it was cowardly to sneak back to town a couple of days later, "on
business," of course--a shabby excuse for a chap that doesn't dabble in business more than
I do. But I honestly needed to go to get back my equilibrium. I got it, though, and I've kept
it pretty continuously. And this much is enough for that. Natica Melsford is the only interesting
bit about this story, and let's get back to her.

That winter she married Jack Drayton. The afternoon we rehearsed for the wedding I looked at
her, before we pranced down the aisle and endured the endless silly giggles of the bridesmaids,
and the usher louts who would fall out of step, and grew more peevish by the minute. I looked
her over then, and I said to myself: "You feeble paranoiac, imagine that girl tying up with
you." Well, I couldn't very well imagine it, although I tried. But I was extremely noisy, and
I heard two or three of the bridesmaids, to say nothing of the maid of honor and the bridegroom's
mamma, tapping their gentle hammers, at my expense, at the breakfast. It was a year afterward
that I began to fag regularly for the Drayton establishment.

Jack Drayton, by rights, ought to have been poisoned. He'd be the first to acknowledge it now.
Perhaps if he'd married a girl who insisted on having things out the moment they began, the
things wouldn't have happened. But Natica Melsford wasn't that sort. She was the kind that
simply looked scorn into and clear through you, when she thought you were acting low down.
This, with a man strung like Jack was, simply put the fat into the fire. It would have been
different with me. I'd--well--I'd have made an abject crawl, to be sure. You see, her knowing
this was the thing that must have always queered me with her. A woman prefers a man she can
get furious at and who'll stick it out a bit, to one who caves in at the first sign of a frown.
But Jack carried things too far.

No, he didn't mind my frequenting the house. He liked me and I liked him. But, all the same,
I knew he didn't regard me as a foeman worthy of his steel. And, although the knowledge made
me raw now and then, when he's come in with his easy, careless way, still I swallowed the mean
feeling because it gave me a chance to see her. And don't imagine I went around hunting for
trouble. It was at the club one night--I'd just come from the Draytons, and Jack hadn't been
home to dinner--that I heard Rawlins Richardson and Horace Trevano chattering about Maisie
Hartopp. The "Jo-Jo" song had made the biggest kind of a hit that winter at the Gaiety, and
the hit had been made by the Hartopp singing it to a stage box which the Johnnies scrambled
to bid in nightly.

It seemed like small game for Jack Drayton to be trailing along with the ruck--the ruck meaning
Tony Criswold and the rest of that just-out-of-college crew--but I didn't need signed affidavits,
after five minutes of club chatter, to know that he was pretty well tied to an avenue window
at Cherry's after the show. The Ruinart, too, that kept spouting from the bucket beside it,
was a pet vintage of the Hartopp.

There was a lot of that silly chuckle, and I recalled reading somewhere that there was a husband
belonging to the Hartopp, a medium good welterweight, who picked up a living flooring easy
marks for private clubs at Paterson, N. J., and the like, and occasionally serving as a punching
bag for the good uns before a championship mill. What the devil was there to do? I couldn't
answer the riddle.

It sounds like old women's chatter, the meddlesome way I scribble this down. It would take
a real thing in the line of literature to paint me right, anyway, I fancy. When a third party
keeps mixing in with husband and wife, he deserves all the slanging that's coming to him; which
same is my last squeal for mercy.

A month went by--two of them. Natica Drayton wasn't the strain that needs spectacles to see
through things. Then, too, I guessed the loving friend sympathy racket was being worked by
some of the bridge whist aggregation which met up with her every fortnight. She laughed more
than she ought to have done. This was a bad sign with her. Once or twice, when the three of
us dined together, and she was almost noisy over the benedictine, I could have choked Jack
Drayton, for he didn't see. It's not a pretty thing for an outsider to sit à trois, and see
things in a wife's manner that the husband doesn't or won't see; and worse than that, to know
that the wife knows you see it and that he doesn't. Speak to Jack? I wouldn't have done it
for worlds. As I said, I'm willing to burn my fingers and even cuddle the hurt; but I don't
meddle with giant firecrackers except on the Fourth of July, and that didn't come until afterward.

I was to take her to the opera one night--Drayton had the habit of dropping in for an act or
two and then disappearing--but on her own doorstep she tossed off her carriage wrap and sent
Martin back to the stables.

"Let's talk, instead," she said, and she made me coffee in the library, with one of those French
pots that gurgle conveniently when you don't exactly know what to say. That pot did a heap
of gurgling before we began to talk. When she spoke, what she said almost took me off my chair.

"Percy, have you seen the show at the Gaiety?" she asked.

I had seen it more than once, and I said so.

"They tell me there's a song there----" she went on.

"There are a lot of songs," said I.

"There's one in particular."

There wasn't any use in fencing, so I answered: "You mean the 'Jo-Jo' song. It's a silly little
ditty, and it's sung by----"

"A girl named Hartopp--Maisie Hartopp." She was speaking as if she were trying to remember
where she'd heard the name.

Of course, me for the clumsy speech. "She's a winner," I cut in.

She got up at that, and walked over to the fireplace. "She seems to be," she said, picking
at a bit of bronze, a wedding present, I think. Then she came over to where I was sitting and
put a hand on my shoulder. I'd have got to my feet if I hadn't been afraid to face her. "Percy----"
she began, and I felt the fingers on my shoulder quiver. I don't think the Apaches handed out
anything much worse in the torture line than the quiver of a woman's ringers upon your shoulder,
when you know that those fingers aren't quivering on your account. Maybe that occurred to her,
for a second later she took her hand away. "You once said something foolish to me, Percy,"
she said.

I nodded my head, my eyes upon an edge of the Royal Bokhara. "It was in a canoe, wasn't it?"
I replied. "There was a moon, of course, and the paddle blades went drip, drip."

"You meant what you said then, didn't you?"

My gaze was wavering from the rug by now. Little wonder, was it? "I meant it all right," I
got out after a while. "Do you want to hear me say my little speech over again?" Was it possible
that, after all, Natica Drayton had really decided to toss Jack over, and take on a fag, warranted
kind and gentle, able to be driven by any lady? But I forgot that foolish notion pretty nearly
right off.

"There is a husband," she went on, as if taking account of stock.

"There always is," I rejoined. "Some of 'em are good and the others are bad." I chuckled despite
me, as I put in my mean little hack.

"I mean the Hartopp's husband," she explained.

"There is," I said. "'Boiler-plate' Hartopp. His given name is James, and he prize-fights fair
to middling." All this wasn't quite good billiards, but we'd begun wrong that night, and we
might as well keep it up, thought I.

Natica Drayton was tapping her foot upon the fender. "H'm," she mused. "Some of those horrid
names sound interesting." Then she turned to me abruptly. "I think, perhaps, you ought to go
now," she suggested.

"I think so, too," I agreed, rising very hastily, and taking my leave.

"Have you Friday evening disengaged?" She flung this after me before I had got to the hall.

"Yes," said I, all unthinking.

"Then we'll do it Friday," she said.

"We'll do what?" I asked, coming back to her. For once I felt rebellious, and showed it, whereat
she smiled.

"Supper after the theater at Cherry's."

"Oh, well, I don't mind that," I volunteered.

"With 'Boiler-plate' Hartopp," she added.

The searchlight dawned upon me. It swung around the room once or twice, and that was enough.
I knew in the flood of sudden illumination that the girl had planned this thing in advance,
with the daring of despair--and a wife's despair, a very young wife's despair, is a more desperate
thing than the anger of any other woman. Natica had planned it all in advance; had figured
it, and the chances of it. And in the balance she had confidently thrown the asset of my assisting
her.

The right sort of a man, I suppose, would have become enraged because of her taking things
for granted. But I--I had been chained to her chariot too long a time to experience the mild
sensation of resentment.

Natica wished to face her husband in a crowded restaurant after the play. More than that, she
wished to face him in company with a man not of her sort, even as he--Drayton--was escorting
a woman whose lane of living did not rightly cross his. The coincidence of Natica's means-to-an-end
being the Hartopp's husband, was simply a gift of fate; an opportunity of administering poetic
justice, which could not be denied. Had the Hartopp not possessed a convenient husband, Natica
would have arranged for another companion. But even she had not dared to plan her coup alone,
with her chosen instrument of wifely retaliation. Through it all, she had confidently counted
on me, a discreet background, a pliant puppet.

She could not know what Drayton might do, after they had eyed one another from different tables.
She did not much care. But she would at least have the painful joy of the Brahmin woman's hope,
who trusts by some fresh incantation to secure a blessing, formerly vouchsafed her by the gods,
but which now old-time petitions fail to renew. It seemed cold-blooded, the entire arrangement,
and yet I knew it was not. She was far braver than I could have been, even to win her caring.
But I understood.

I must have been rough as I took her hand. "Look here," I said. "It's a desperate game, Natica.
You wouldn't have dared to say that to any other man than me. You've got used to seeing me
fag for you. And I'm going to do it this time, too. But if you weaken, by Heaven, you'll deserve
to lose for good. It's crazy, it's the act of a pair of paretics, but I'm going to see it through."

She was crying when I left her. "Percy, my dear," she said; then she began to laugh--that after
dinner benedictine laugh of hers. "If there weren't Jack, that speech of yours just now might
make me want to kiss you."

On the sidewalk I tried to figure out if there had been knockout drops in the coffee Natica
had brewed for me. In any one of the forty-eight hours ensuing, I might have rung up the Draytons'
on the telephone, and told her that I had come to my senses. But I didn't do anything of the
sort. Instead, I hunted up a newspaper chap I knew, and he put me next to "Boiler-plate" Hartopp
at the Metropole.

The bruiser wasn't as bad sort as I had fancied him. He was an Englishman all right--a cut
below middle class; you could tell that by the way he clipped his initial h's off and on. I
tried the ice at first--it's always best when you don't know the exact thickness of your frozen
water. The way I tried it was to toss a flower or two at Maisie Hartopp and her "Jo-Jo" song.

He rose sure enough, and it didn't take me a quarter hour to see that the pug was really bowled
out by the parcel of stage skirts who wore his name on the Gaiety bills. This made it a warmer
game than it might have been otherwise, but I was in for it now, and I made the date.

No, I didn't mention Natica. Even a broken-to-harness shawl carrier has a shred of cautious
decency about him. But I gabbled lightly about a certain feminine party who was keen on exemplars
of the genuine thing in the line of the manly art. Whereupon "Boilerplate" acquired a pouter-pigeon
chest, which fairly bulged over the bar railing, and gave me his word of honor he'd be waiting
at Forty-fourth Street about eleven on Friday. He intimated, ere I left, that he'd bring his
festive accouterments with him. And he did.

We were a bit late--Natica and I. It must have been a quarter past the hour when we drove up
to Cherry's. I felt reasonably certain that if Jack Drayton were guarding a champagne bucket
by the corner table that night, he was located then. In the offing, miserably self-conscious,
a crush hat on the back of his really fine head, and two or three small locomotive headlights
glinting from his broad expanse of evening shirt, was "Boiler-plate" Hartopp. The flunkeys
were regarding him curiously, and once a waiter-captain came out and gave him what seemed to
be an unsatisfactory report.

I think the man was just about to take the count from sheer nerves, when he made me out in
the doorway. Natica winked--actually winked at me--as he floundered over his share of the introduction.
Looking at her, and faintly divining her mood that night, I felt sorry for Jack, for "Boiler-plate"
and for myself. I left them for a moment and went in to see about my table. Two minutes later
I emerged, to face Drayton and the Hartopp unloading from an electric hansom. The under-toned
remark of one of the footman came to me: "A bit behind schedule time to-night, eh, Charley?"

There wasn't anything to do then, for they were fair inside. "Boiler-plate" was finishing some
elephantine pleasantry to Natica, when he saw what I saw. A foolish grin rippled across his
wide face. "Hullo!" he said to the Hartopp, who looked properly peevish, and then waspish,
as she let her glance travel to Natica, who stood perfectly poised and, I fancied, a trifle
expectant. Drayton eyed them together and in particular. The color streaked his forehead and
faded out. Then he saw me, and, although he never may have murder in his eyes again, it was
there at that choice moment. We weren't at all spectacular, you mustn't think that. It was
all very quick, and there were a lot of people coming and going.

She was in instant command of the situation. Why shouldn't she have been, having created it?
And unexpectedly, suddenly as she had encountered her quarry, equally suddenly she shifted
her position, without the time to take me into her confidence.

"Don't bother about our table, Percy," she said. "Now that we've met friends, it will be jollier
to dine en famille. It will be ever so much nicer than eating in a stuffy restaurant, and the
butler won't have gone to bed yet. Run out and get us a theater wagon."

I went out to the carriage man in a trance. The gods, of a deed, were fighting furiously on
Natica's side--for she could not have foreseen this vantage, readily as she swung her attack
by its aid. Exquisite torture, truly, to flaunt a husband's folly in his own face, over his
own mahogany, with the source of that folly looking on. Drayton's bounden civility to his wife,
and to the other woman, must make him present himself as a target. He knew it, his wife knew
it; as yet the other woman but dimly suspected it--not being over subtle--and it smote me in
the face continuously. The puppet always feels the most cut up at times like these. In a way,
it is because his vanity is being seared. Mine fairly crackled.

So we rattled off up the avenue. The only comfortable ones among us were Natica and Hartopp.
He seemed to think the occurrence a pleasant bit of chance, and he wasn't in the least jealous,
not he.