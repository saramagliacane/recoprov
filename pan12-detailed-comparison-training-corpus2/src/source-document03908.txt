﻿Voltaire says “the character of the mythical gods is ridiculous;” we will add, it is ridiculous
in the extreme. Listen—Hesiod, in his theogony, says: “Chronos, the son of Ouranos, or Saturn,
son of Heaven, in the beginning slew his father, and possessed himself of his rule, and, being
seized with a panic lest he should suffer in the same way, he preferred devouring his children,
but Curetes, a subordinate god, by craft, conveyed Jupiter away in secret and afterwards bound
his brother with chains, and divided the empire, Jupiter receiving the air, and Neptune the
deep, and Pluto Hades.”

Pros-er-pi-ne, Mella-nip-pe, Neptune, Pluto and Jupiter are all set forth in the mythical writings
as adulterers. Jupiter was regarded as more frequently involved in that crime, being set down
as guilty in many instances. For the love of Sem-e-le, it is said that he assumed wings and
proved his own unchastity and her jealousy. These are some of the exploits of the sons of Saturn.
Hercules was celebrated by his three nights, sung by the poets for his successful labors.

The son of Jupiter slew the Lion, and destroyed the many-headed Hydra; was able to kill the
fleet man-eating birds, and brought up from hades the three-headed dog, Cerberus; effectually
cleansed the Augean stable from its refuse; killed the bulls and stag whose nostrils breathed
fire; slew the poisonous serpent and killed Ach-e-lò-us. The guest-slaying Bu-sí-ris was delighted
with being stunned by the cymbals of the Sat-yrs, and to be conquered with the love of women;
and at last, being unable to take the cloak off of Nessus, he kindled his own funeral pile
and died. Such are specimens of the ancient myths. Their character is such as to leave an impassible
gulf between them and the character of the God revealed in our religion. No development theory,
seeking the origin of our religion in the old mythical system, can bridge across this chasm.
It is as deep and broad as the distance between the antipodes. There is no analogy between
these counterfeits or myths and the “true God,” save that remote power of God which is divided
up and parceled out among them. Their morals were the worst. The whole mythical system is simply
one grand demonstration of human apostacy from the “true God.” Homer introduces Zeus in love,
and bitterly complaining and bewailing himself, and plotted against by the other gods. He represents
the gods as suffering at the hands of men. Mars and Venus were wounded by Di-o-me-de. He says,
“Great Pluto’s self the stinging arrow felt when that same son of Jupiter assailed him in the
very gates of hell, and wrought him keenest anguish. Pierced with pain, to the high Olympus,
to the courts of Jupiter groaning he came. The bitter shaft remained deep in his shoulder fixed,
and grieved his soul.” In the mythical system the gods are not presented as creators or first
causes. Homer says, They were in the beginning generated from the waters of the ocean, and
thousands were added by deifying departed heroes and philosophers. The thought of one supreme
Intelligence, the “God of Gods,”, runs through all the system of myths. It is found anterior
to the myths, and, therefore, could not have had its origin with them. The character ascribed
to our God, in our scriptures, has no place among the ancient myths. They hold the “Master
God” before us only in connection with power, being altogether ignorant of His true character.
They even went so far as to attribute much to Him that was ridiculous. One of the ancients
said, “The utmost that a man can do is to attribute to the being he worships his imperfections
and impurities, magnified to infinity, it may be, and then become worse by their reflex action
upon his own nature.” This was verified in the ancient mythical religion, without exception,
and without doubt.

“The character of all the gods was simply human character extended in all its powers, appetites,
lusts and passions. Scholars say there is no language containing words that express the Scriptural
ideas of holiness and abhorrence of sin, except those in which the Scriptures were given, or
into which they have been translated. These attributes must be known in order to salvation
from sin, so God revealed Himself and gave the world a pure religion, as a standard of right
and wrong, and guide in duty, and rule of life.”

The history of the ancient nations of the earth gives a united testimony that their original
progenitors possessed a knowledge of the one true and living God, who was worshiped by them,
and believed to be an infinite, self-existent and invisible spirit. This notion was never entirely
extinguished even among the idolatrous worshipers. Greek and Latin poets were great corrupters
of theology, yet in the midst of all their Gods there is still to be found, in their writings,
the notion of one supreme in power and rule, whom they confound with Jupiter.

The age of myths began with the tenth generation after the flood. The evidence of this is given
by Plato from one of the ancient poets in these words: “It was the generation then the tenth,
of men endowed with speech, since forth the flood had burst upon the men of former times, and
Kronos, Japetus and Titan reigned, whom men of Ouranos proclaimed the noblest sons, and named
them so, because of men endowed with gift of speech, they were the first,” that is to say,
they were orators, “and others for their strength, as Heracles and Perseus, and others for
their art. Those to whom either the subjects gave honor, or the rulers themselves assuming
it, obtained the name, some from fear, others from reverence. Thus Antinous, through the benevolence
of your ancestors toward their subjects, came to be regarded as a god. But those who came after
adopted the worship without examination.” So testifies one who was schooled in philosophy.
Do you say there are points of similitude between the Bible religion and the mythical? It would
be strange if there were none, seeing that the mythical is truly what the term signifies, a
counterfeit upon the genuine, or Biblical.

