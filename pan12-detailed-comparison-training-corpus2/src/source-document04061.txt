﻿We were gathered round the fire, my wife, my daughter and I; Angela seated on what is known,
I believe, in upholstering circles as a humpty, while Peggy lay on her tummy on the floor,
pencil in hand and a sheet of paper before her; she was chewing the pencil with the ruminating
air of one who awaits inspiration. I myself occupied the armchair.

"You know," said Angela presently, "I think Mélisande has seemed worried about something the
last few days. I do hope the poor dear isn't bothering about rabies. One so often hears of
people actually producing a disease merely by thinking a lot about it. By the way, I'm told
that one of the earliest manifestations of rabies is a desire to bite inanimate objects; if
we see her doing that we shall know that the time has come to act."

At this juncture Mélisande entered the room through the open window.

Her manner exhibited a curious blend of dignity and caution; I could more readily have suspected
my own mother of having rabies. She advanced slowly towards us till suddenly her eye lighted
on Peggy, who still chewed her pencil awaiting inspiration.

Mélisande stopped as though she had been shot; I could only surmise that the sight of Peggy
thus occupied had confirmed her darkest suspicions. With one wild shriek of terror she fled
from the room.

       *       *       *       *       *

THE NEGLECTED PROBLEM.

  O dear and delectable journal that daily
    Appeasest my hungering mind
  With items recounted or gravely or gaily
  Of doings at Margate, Mayfair or Old Bailey,
    Or paragraphs rare and refined
  On "Who will the forthcoming cinema star be?"
  "What horse to support with your shirt for the Derby;"
  "How much will the next price of beer at the bar be?"
            "Are halibuts blind?"

  A question arises I prithee examine
    And ponder the pull that it has
  Over headings like "Foch and Parisian Gamine,"
    "Are Bolshevists really believers in Famine?"
    Or "Vocalist Lynched at La Paz."
  I look for it oft and in vain and say, "Blow it!
  There must be an answer and England should know it."
  Here, then, is the problem that's haunting the poet:
            Does Germany Jazz?

       *       *       *       *       *

    "William Henry ----, aged 110, fell off a tree whilst out
    playing with other boys and broke his right leg."--Provincial
    Paper.

We hope it will be a lesson to him for the rest of his life.

       *       *       *       *       *

       *       *       *       *       *

BIRD NOTES.

Nature Study has recently been recommended by a well-known Daily Paper as a means of gradual
relaxation from war-worry. Mr. Punch would therefore like to contribute for so noble an end
a few ornithological notes, having for a long time been addicted to the observation of bird-life.

CUCKOO.--This bird, which obtained its name on account of the similarity of its note to that
of the Cuckoo-clock, was one of the earliest sufferers of the housing problem, which it successfully
solved by depositing its eggs in the nests of other birds.

SEAGULL.--When the eggs of this bird are hatched the mother-parent feeds its young on the glutinous
substance that oozes from sea-weed--hence "Mother Seagull's Syrup."

THROSTLE.--See THRUSH.

PIGEON.--This bird was used as a message-carrier with great success during the War. An attempt
to cross it with the Parrot, to enable it to deliver verbal messages, was unfortunately a failure.

SPARROW.--Bird-fanciers experience great difficulty with this bird when kept in captivity,
as it frequently develops jaundice, in which case it can only be sold under the name of "Canary,"
at a big difference in price.

GUILLEMOT.--The name "Guillemot" is derived from the French word "Guillemot," which means a
Guillemot.

LARK.--The protective instinct in this bird is very marked. Although nesting on the ground
it soars high into the sky for the purpose of leading aviators and balloonists away from its young.

GOLDFINCH.--A favourite cage-bird. The best method of catching the goldfinch is to wait until
it settles on the lowest branch of a tree, then approach it from behind and gently tap its
right wing with your right hand. This causes it immediately to turn its head to see who has
touched it; you can then bring up your left hand unnoticed, into which it falls an easy victim.

BULLFINCH.--Another popular cage-bird. The best method of capturing it, which differs widely
from that in use with the Goldfinch, is as follows:--Hang head downwards from the fork of an
old tree in order to resemble a dead branch, having previously covered yourself with some adhesive
matter. In this position you should wait until as many Bullfinches as you want have settled
on your clothes and stuck there; then climb down from the tree and have them scraped off into
a large cage.

BARN OWL.--This bird invariably builds its nest in empty houses. There will be no nests this year.

STARLING.--Threepence was placed on the head of this destructive bird last year in many parts
of England. The old way was to put salt on its tail.

BLUE TITMOUSE.--The nest of this active little bird is often situated in most extraordinary
places. It is frequently found inside village pumps, and in consequence is much persecuted
by local milkmen. It is feared that unless The Daily Mail can be persuaded to take up the cause
of this unfortunate bird it will soon be faced with extermination.

ROOK.--The chief difference between this bird and the Crow is found in the way in which its
name is spelt.

THRUSH.--See THROSTLE.

       *       *       *       *       *

SONGS OF SIMLA.

II.--SIMLA SOUNDS.

  I have heard the breezes rustle
    O'er a precipice of pines,
  And the half of a Mofussil
    Shiver at a jackal's whines.

  I have heard the monkeys strafing
    Ere the dawn begins to glow,
  And the long-tailed langur laughing
    As he lopes across the snow.

  I have heard the rickshaw varlets
    Clear the road with raucous cries,
  Coolies clad in greens or scarlets,
    As a mistress may devise.

  Well I know the tittle-tattle
    Of the caustic muleteer,
  And the Simla seismic rattle
    Is familiar to my ear.

  Though to-day my feet are climbing
    Bleaker heights and harder roads,
  Still the Christ-church bells are chiming,
    Still the mid-day gun explodes.

  But the sound which echoes loudest
    Is the sound I never knew
  Till I lunched (the very proudest)
    With the Staff at A.H.Q.

  'Twas a scene of peace and plenty,
    Plates a-steam and-spoons a-swoop;
  'Twas a sound of five-and-twenty
    Hungry Generals drinking soup.

  J.M.S.

       *       *       *       *       *

WAITING FOR THE SPARK.

(With thanks to the London Telephone Directory.)

I doubt if you have ever taken the book seriously, dear reader (if any). You dip into it for
a moment, choose a suitable quotation and scribble it down with a blunt pencil on your blotting-pad;
then you wind the lanyard of the listening-box round your neck and start talking to the germ-collector
in that quiet self-assured voice which you believe spells business success. Then you find you
have got on to the Institute of Umbrella-Fanciers instead of the Incorporated Association of
Fly-Swatters, which you wanted, and have to begin all over again. But that is not the way to
treat literature.

In calm hours of reflection, rather, when the mellow sunlight streams into the room and, instead
of the dull gray buildings opposite, you catch a mental glimpse of green tree-tops waving in
the wind, and hear, above the rumbling of the busy 'buses, the buzzes ... the bumbling ...
what I mean to say is you ought to sit down calmly and read the book from cover to cover, as
I am doing now.

For it isn't like a mere Street Directory, which puts all the plot into watertight compartments,
and where possibly all the people in Azalea Terrace know each other by sight, even across the
gap where it says:--

Here begins Aspidistra Avenue, like the lessons in church.

Nor, again, is it like Who's What, where your imagination is hampered and interfered with by
other people butting in to tell you that their recreations are dodging O.B.E.'s and the Income
Tax Commission. Publications: Hanwell Men as I knew Them. Club: The Philanderers, and so forth.
This cramps your style.

But the book before us now is pregnant with half-hidden romances, which you can weave into
any shape that you will, and, what is more, it is written in a noble beautiful English which
you have probably never had time to master. I want you to do that now. Suppose, for instance,
that in private life your hostess introduced you to Museum 88901 Wilkinson Arthur Jas.--let
us say at a Jazz tea. And suppose you were to ask him what his business was, and he told you
that he was an Actnr and Srvyr or a Pprhngr. Probably you would be surprised; possibly even
you wouldn't believe him. But it's all there in the book.

The type too is diversified by sudden changes which intrigue me greatly. All over London I
like to fancy little conversations of this sort are going on:--

Hop 1900 Tomkinson Edward C.-- "Hello, is that TOMKINSON EDWARD C.?"

GERRARD 22001 TOMKINSON EDWARD C.--"SPEAKING."

Hop 1900 Tomkinson Edward C.-- The Whlsl Slvrsmths?"

GERRARD, ETC.--"DON'T SPLUTTER LIKE THAT. WHO ARE YOU?"

Hop, etc.--"I'm Tomkinson Edward C. too. Little Edward C. of Hop. The Tbcnst. I only wanted
to have a talk with you, big brother."

Or sometimes it takes the shape of a novel, starting something like this:--

Kensington 100110 Williams Miss, Tpst., a beautiful but penniless girl, in love with--

Regent 8000 Air Ministry, Ext. 1009, a young aviator who has won the Mlty. Crss. (2 Brs). Their
path is crossed by--

City 66666 (12 lines), BLENKINSOP JEHORAM AND CO., Fnncrs. Blenkinsop wishes to marry Miss
Williams, on account of a large legacy which he has reason to believe will come to her from

Mayfair 5000 Dashwood-Jones H. See Jones H. Dashwood, and so on.

Sometimes, again, as I plunge still deeper into the fascinating volume, a poem seems to fashion
itself and leap from the burning page. Listen.

  She hears not Park appealing
    Nor Gerrard's wail of woe,
  Her heart is on to Ealing
    89200;

  For there her true love (smartest
    Of lcl plmbrs) speaks;
  For him our switch-board artist
    Puts powder on her cheeks.

  For him, the brave, the witty,
    When evening's shadows drop
  She flies from Rank and City
    To tread some Western hop.

  For him her spirit ranges
    Through realms of blissful thrall,
  And that is why Exchange is
    Not getting Lndn Wll.

  Little her mthr----

I'm sorry, reader; I really and truly am. There's my trunk call ... "Hello. No, I can't hear ..."

We must finish it some other time, and you must try READING THE BOOK for yourself srsly please.

"Hello! Hello! Hel-lo!"...

EVOE.

       *       *       *       *       *

THE VISIONARY TRIUMPH.

"This," he said, "is my favourite dream."

We were discussing our favourite dreams and prepared to listen.

"It is always," he went on, "the same--a cricket match: and the older I get and less able to
play cricket, the oftener I have it. It is a real match, you must understand--first-class cricket,
with thousands of spectators and excitement; and it is played a very long way from my home.
That is an important point, as I will explain.

"I am merely one of the spectators. How long I have been watching I cannot say, but the match
is nearing the end and our side--the side which has my sympathies--is nearly all out, but still
needs a few runs to win.

