﻿Dagworth, Sir Thomas.
Damietta, Crusade of.
Damietta, Archbishop of. See Roches, Peter des.
Damme.
Dampierre, Guy, Count of Flanders. See Guy.
Dancaster, John.
Dante.
Darlington, John of, Archbishop of Dublin.
David I., King of Scots.
David II., son of Robert Bruce, King of Scots.
David I., an Llewelyn, Prince of Wales.
David II., ap Griffith, Prince of Wales.
David, Earl of Huntingdon. See Huntingdon.
David of Strathbolgie, Earl of Athol. See Athol.
Dax.
Dean, Forest of.
"Decorated" style of architecture.
Deddington.
Deganwy, Castle of.
Delisle's Histoire de Saint-Sauveur-le-Vicomte.
Denbigh, town, lordship and castle of.
Denifle's Désolation des Églises de France, etc.;
  his Entstehung der Universitäten.
Derby, Henry of Grosmont, Earl of. See also Lancaster.
Derby, Robert Ferrars, Earl of.
Derby, Thomas, Earl of Lancaster and. See Lancaster.
Derby, William of Ferrars, Earl of.
Deschamps, Eustace.
Despenser, Eleanor de, wife of Hugh le Despenser, the younger.
Despenser, Hugh, justiciar.
Despenser, Hugh, the elder, Earl of Winchester, son of the justiciar.
Despenser, Hugh, the younger, Lord of Glamorgan, son of the foregoing.
Devizes, Castle of.
Devon, earldom of, Falkes de Bréauté as warden of.
Devon, Courtenays, earls of.
Dictum de Kenilworth, the.
Dinan.
Disafforestments.
Diserth, Castle of.
Disinherited, the (after Evesham);
  the, Scotch.
Disseisin, novel.
Dolwyddelen Castle.
Dominic, St.
Dominicans.
Don, the river.
Donaldbane, brother of Malcolm Canmore.
Dordogne, the river.
Dordrecht.
Dorking.
Dorsetshire.
Douai.
Douglas, Sir Archibald.
Douglas, Sir James.
Douglas, Sir William.
Douglas, Sir William (at Poitiers).
Dover, town and castle;
  straits of.
Dovey the river.
Dowell's, S., History of Taxation.
Downs, the north;
  the south.
Drokensford, Bishop of Bath and Wells.
Dublin, Castle of.
Dublin, Archbishop of. See Hotham, William of, Archbishop of.
Dubois, Peter.
Dugdale's Monasticon.
Dumfries.
Dunbar, battle of.
Dunfermline.
Dunkeld, Bishop of.
Duns Scotus.
Dunstable.
Dunstanville, house of.
Dupplin Moor, battle of.
Durham;
  bishopric of;
  records of.
Durham, Bishops of.
  See Bek, Anthony;
  Beaumont, Louis de;
  and Bury, Richard of.
Dynevor Castle.

Earn, the river.
Eastminster, the, London.
Eastry, Henry of, prior of Christ Church, Canterbury.
Ebro, the river.
Eccleston, William of, his De adventu fratrum minorun.
Edinburgh, town and castle.
Edington, church of.
Edington, William of, Bishop of Winchester.
Edmund of Almaine, Earl of Cornwall, son of Richard of Cornwall.
Edmund, Earl of Lancaster, Leicester and Derby, some time titular King
of Sicily, son of Henry III.
Edmund of Langley, son of Edward III., Earl of Cambridge, afterward
Duke of York.
Edmund of Woodstock, son of Edward I., Earl of Kent.
Edmund (Rich). St. See Rich, Edmund.
Edmund, St., of East Anglia.
Edward the Confessor, saint and king; translation of.
Edward I.;
  authorities for reign of.
Edward II.;
  sources for the reign of.
Edward III.;
  sources for the reign of.
Edward, son of Henry III. See also Edward I.
Edward of Carnarvon, Prince of Wales. See also Edward II.
Edward of Windsor, Duke of Aquitaine.
Edward, Prince of Wales and of Aquitaine, called the Black Prince.
Education;
  of clergy.
Elbeuf.
Egypt.
Elderslie.
Eleanor of Aquitaine, Queen of Henry II.
Eleanor of Castile, Queen of Edward I.
Eleanor, second daughter of Raymond Berenger IV., Count of Provence,
Queen of Henry III.
Eleanor, younger sister of Henry III., married (1) William Marshal,
(2) Simon de Montfort.
Elgin.
Elizabeth, daughter of Edward I., Countess of Holland, afterwards of
Hereford.
Elizabeth de Burgh, queen of Robert (Bruce), King of Scots.
Ellis, Sir Henry, ed. of Chronica I. De Oxenedes.
Eland, William.
Ely, bishopric of, isle of.
Ely, Bishops of.
  See Marsh, Adam;
  Balsham, Hugh;
  Langham, Simon;
  Hotham, John.
Eltham.
Eltham, John of. See John.
Englefield.
English language;
  in law courts.
Eric, King of Norway.
Escheats.
Esplechin, treaty of.
Essex; earldom of.
Essex, Countess of. See Isabella of Gloucester.
Estates, the three.
Etsi de statu, bull.
Etaples.
Ettrick forest.
Eu, Count of, constable of France.
Eure, the river.
Eulogium Historiarum.
Eustace the Monk.
Evans, J.G., his edition of the Red Book of Hergest.
Eversden, John of.
Evesham, battle of;
  Abbey.
Evreux.
Evreux, Counts of.
  See Charles the Bad, King of Navarre;
  Philip the Bold.
Evreux, Louis, Count of. See Louis.
Exchequer courts for Wales.
Exchequer records.
Exeter, Bishops of.
  See Brantingham, Thomas;
  Stapledon, Walter.
Exeter College, Oxford.
Exports.
Eynsham, Walter of.
Eyville, John d'.

Fair of Lincoln, the. See Lincoln, battle of.
Falkirk;
  battle of.
Famine, of 1316, the;
  of wool, in Flanders.
Farnham.
Farrer's, W., Lancashire Final Concords.
Faucigny.
Fecamp.
Fecamp, Peter Roger, Abbot of. See Clement VI.
Feet of Fines.
Felton, Sir Thomas, Seneschal of Aquitaine.
Ferdinand of Portugal, Count of Flanders.
Ferdinand III. the Saint, King of Cast& [Castile].
Ferrars, house of.
Ferrars, Robert of, Earl of Derby. See Derby.
Ferrars, William of, Earl of Derby. See Derby.
Fife.
Fife, Earl of.
Fifteen, the Council of.
Figeac.
Firstfruits.
Fitzalan, Edmund, and Richard, Earls of Arundel. See Arundel.
Fitzalan of Bedale, Brian.
Fitzalans, the.
FitzAthulf, Constantine, sheriff of London.
FitzGeoffrey, John.
Fitzgerald, governor of Ireland.
Fitzgerald, Maurice, justiciar of Ireland.
Fitzgeralds, the.
Fitzralph, Richard, Archbishop of Armagh.
Fitzthedmar, Arnold.
FitzWalter, Robert.
Flemings, the. See Flanders.
Fleta, law-book.
Fletching.
Flint, county of;
  town and castle of.
Flodden, battle of.
Florence.
Florence, count of Holland.
Florence of Worcester, Continuators of the Chronicle of.
Flores Historiarum, Roger of Wendover's.
Flores Historiarum (fourteenth century).
Flagellants, the.
Flamangrie, La.
Flanders, county of.
Flanders, counts of.
  See Ferdinand of Portugal,
  Guy of Dampierre,
  Louis of Male,
  Louis of Nevers,
  Robert of Béthune
  and Thomas of Savoy.
Flanders, Joan, Countess of. See Joan.
Flanders, Margaret of. See Margaret.
Foedera, Rymer's.
Foix.
Foix, Count of.
Foix, Gaston Phoebus, Count of.
Fontenelles, Cistercian Abbey of.
Fontevraud.
Fordun, John, his Chronicle.
Forests, charter of the;
  perambulation of the;
  enlargement of the.
Fors, William of, Earl of Albemarle. See Albemarle.
Fors, Isabella of. See Albemarle, Countess of.
Forth, the.
Fotheringhay, Castle of.
Foulquois, Guy, Cardinal-bishop of Sabina. See Clement IV.
Fountains Abbey.
Fournier, James. See Benedict XII.
Fournier's Royaume d'Arles.
France;
  records of;
  chronicles of.
France, King of, Edward III. takes title of.
France, Kings of.
  See Philip Augustus,
  Louis VIII.,
  Louis IX.,
  Philip III.,
  Philip IV.,
  Louis X.,
  Philip V.,
  Charles IV.,
  Philip VI.,
  John and
  Charles V.
Francis, St., of Assisi.
Franciscans, the;
  the spiritual.
Franks, the Salian.
Frankton, Stephen of.
Frascati.
Fraser, William, Bishop of St. Andrews.
Frederick II., the emperor.
French language, the.
Frescobaldi, the.
Freynet, Gilbert of. See Gilbert.
Friars, the;
  the four orders of;
  See Austin or hermits of order of St. Augustine;
  Bonhommes;
  Carmelite or White;
  Crutched;
  Dominicans;
  Francisans;
  ---- of the Penance of Jesus Christ or
  ---- of the Sack;
  Trinitarians or Maturins.
Froissart, John.
Froissart, Chroniques, ed. Luce;
  ed. Kervyn.
Fronsac, Viscount of.
Funck-Brentano's, F., editions of the Chronique Artésienne and
Annales Gandenses.
Furness.

Gabaston.
Gaetano, Benedict. See Boniface VIII.
Galeazzo Visconti, Lord of Pavia.
Galloway.
Garonne, the river.
Garter, Order of the.
Gascony, See also Aquitaine.
Gaston, Viscount of Béarn.
Gaveston, Peter, Earl of Cornwall.
Gelderland, Duke of.
Genitours.
Genoa.
Genoese, the;
  crossbowmen.
Geraldines of Leinster, the.
Germany.
Ghent.
Ghent, Gilbert of. See Lincoln, Earls of.
Giffard, Walter, Archbishop of York;
  his register.
Giffords, the.
Gilbert of Freynet.
Gilsland.
Gironde, the river.
Glamorgan, lordship of.
Glamorgan, Lords of. See Gloucester, Earls of.
Glasgow, Robert Wishart, Bishop of. See Wishart.
Glendower, Owen.
Gloucester;
  St. Peter's Church;
  statute of;
  earldom of.
Gloucester, Richard of Clare, Earl of.
Gloucester, Earl of, Gilbert of Clare, son of the above.
Gloucester, Earl of, Gilbert of Clare, son of the above.
Gloucester, Ralph of Monthermer, Earl of.
Gloucester, Audley, Earl of.
Gloucester, Thomas of Woodstock, Earl of. See Thomas.
Gloucester, Isabella, Countess of. See Isabella, Queen of King John.
Gloucester, Robert of.
Gloucestershire.
Gomez, Peter, Cardinal.
Gordon, Adam.
Gothic architecture. See Architecture.
Gough's Itinerary of Edward I.
Gower,
Gower, John;
  his works.
Grampians, the.
Granada.
Grand, Richard le, Archbishop of Canterbury.
Grandisons, the.
Greek, study of.
Greenfield, William, Archbishop of York.
Gregory IX., Pope.
Gregory X., Pope.
Gregory XI, Pope.
Grey, Reginald.
Grey, Richard of.
Grey's Sir T., Scalachronica.
Grey, Walter, Archbishop of York;
  his register.
Griffith ap Gwenwynwyn.
Griffith ap Llewelyn.
Griffith of Welshpool.
Grosmont, castle of.
Grosmont, Henry of, Earl of Derby. See Derby and Lancaster.
Gross's, C., Select Cases from the Coroners' Rolls;
  his Bibliography of British Municipal History;
  his Sources of English History.
Grosseteste, Robert, Bishop of Lincoln.
  his Epistoae.
Gualo the legate.
Guérande, treaty of.
Guernsey. See also Channel Islands.
Guesclin, Bertrand du.
Guienne. See also Aquitaine and Gascony.
Guillon, treaty of.
Guînes.
Guînes, Baldwin of.
Guînes, Count of.
Gurney, Thomas.
Guy of Brittany, Count of Penthièvre.
Guy of Dampierre, Count of Flanders.
Guy of Lusignan, Lord of Cognac.
Gwent.
Gwenwynwyn, house of.
Gwynedd. See also Wales, North.
Gwynedd, house of.

Haddan and Stubbs' Councils.
Haddington.
Hadenham's, Edmund of, Chronicle.
Haggerston.
Hainault.
Hainault, Counts of. See John and William.
Hainault, Countess of, Abbess of Fontenelles.
Hainault, Philippa of. See Philippa Queen.
Hales, Alexander of.
Halidon Hill, battle of.
Halifax, John of.
Hall's, H., Customs Revenue.
Hall's, J, ed. of Minot's Poems.
Hamilton, H.C., ed. of Walter of Hemingburgh.
Hampole.
Hampshire.
Hapsburg, house of.
Hapsburg, Rudolf of. See Rudolf.
Harby.
Harclay, Andrew, governor of Carlisle. See Carlisle, Earl of.
Harcourt, Geoffrey of.
Harcourts, the.
Hardy, Registrum Palatinum Dunelmense.
Harewell, John, Bishop of Bath.
Harlech Castle.
Harry's, Blind, Wallace.
Hastings, battle of.
Hastings, John, first Earl of Pembroke. See Pembroke.
Hastings, John, second Earl of Pembroke. See Pembroke.
Hastingses of Abergavenny, the.
Hathern.
Hauréau's Histoire de la philosophie scholastique.
Haverfordwest.
Hawarden.
Hawkwood, John.
Hay.
Haydon's ed. of Eulogium Historiarum.
Hearne.
Hebrew, study of.
Hebrews. See also Jews.
Hedingham Castle.
Hengham, Justice.
Henley, Walter of.
Hemingburgh, Walter of.
Hennebont.
Henry I., King of England.
Henry II..
Henry III.;
  chroniclers for the reign of.
Henry VIII.
Henry, King of the Romans, son of Frederick II.
Henry II. of Navarre.
Henry II. of Trastarnara, King of Castile.
Henry, Earl of Derby, afterwards King Henry IV.
Henry of Lancaster, younger son of Earl Edmund;
  Earl of Leicester;
  Earl of Lancaster.
Henry of Grosmont, Earl of Derby, then Earl afterwards Duke of
Lancaster.
Hereford;
  earldom of.
Hereford, Bishops of.
  See Aigueblanche, Peter of;
  Cantilupe, St. Thomas of;
  Orleton, Adam.
Hereford, Humphrey Bohun, Earl of.
Hereford, Humphrey Bohun, grandson of above, Earl of.
Hereford, Humphrey Bohun, son of above, Earl of.
Herefordshire.
Heretics, Albigensian.
Hertford.
Hesdin.
Hewlett's editions of Chronicles.
Hexham.
Hexhamshire.
Higden's, Randolph, Polychronicon.
Highlands, the.
Hingeston-Randelph's Exeter Registers.
History, study of.
Hohenstaufen, the.
Holderness, ruled by Counts of Aumâle.
Holland.
Holland, Florence, Count of.
Hollands, Earls of Kent.
Holy Land, the. See Palestine and Crusades.
Holywood, John of. See also Halifax.
Honorius III, Pope.
Honorius IV., Pope.
Hood, Robin.
Horn, Andrew.
Horstmann, Dr., his Legenda Anglie.
Horwood's, A.L., editions of Year Books.
Hospitallers, the.
Hotham, John, Bishop of Ely.
Hotham, William of, Archbishop of Dublin.
Hougue, La.
Hoveden, or Howden, Roger of;
  his continuator.
Howlett's ed. of Monumenta Franciscana.
Howel the Good.
Huelgas, las, monastery of.
Hugh, Choir of St., at Lincoln.
Hugh of Avalon, Bishop of Lincoln, St., Little St. Hugh of Lincoln.
Hugh X., of Lusignan. See also Lusignan.
Hugh XI. of Lusignan. See also Lusignan.
Hull.
Hulme, St. Benet's.
Humanism.
Humber, the.
Hundred Rolls, the.
Hungary, Primate of, visits Canterbury.
Hungerford, Sir Thomas.
Hunter's Leet Jurisdiction of Norwich;
  Rotuli Selecti.
Huntingdon, David, Earl of.
Huntingdon, Honour of.
Huntingdon, Earl of, John the Scot.
Huntingdon, Clinton, Earl of.
Husbandry, Walter of Henley's treatise on.

Imperium, the.
Immunities, baronial.
Indre, the river.
Ingham, Sir Oliver.
Infantry, English;
  French;
  Irish;
  Scotch;
  Welsh.
Innocent III., Pope.
Innocent IV., Pope.
Innocent VI., Pope.
Inquisition, the, in England;
  in the Netherlands.
Interregnum, the Great.
Inverness.
Iolande, daughter of Peter Mauclerc, Count of Brittany.
Ireland.
Ireland, the Butler of, made Earl of Ormonde. See Ormonde.
Irthlingborough, Northamptonshire.
Irvine.
Isabella of Castile, daughter of Peter the Cruel, wife of Edmund, Earl
of Cambridge.
Isabella Marshal, wife of Richard of Cornwall. See Marshal.
Isabella of Angoulême, Queen of John, and wife of Hugh of Lusignan.
Isabella of France, Queen of Edward II..
Isabella of Gloucester, divorced wife of John, wife of Hubert de
Burgh.
Isabella, sister of Henry III., queen of Frederick II.
Isabella, younger sister of Alexander II., wife of Roger Bigod, Earl
of Norfolk.
Islands, the Channel. See Channel Islands, the.
Isleworth.
Isle, the river.
Isle de France, the.
Isle Saint-Jean, Caen.
Islip, Simon, Archbishop of Canterbury.
Italy.

James, King of Sicily, son of Peter of Aragon; afterwards James II. of
Aragon.
Jaudy, the river.
Jedburgh.
Jerusalem, Latin kingdom of.
Jerusalem, Patriarch of. See Bek, Antony.
Jews, in England, the;
  expulsion of the.
Joan of Champagne, Queen of Philip the Fair.
Joan of Ponthieu, Queen of Ferdinand the Saint.
Joan of the Tower, sister of Edward III., Queen of David Bruce.
Joan, sister of Henry III., Queen of Alexander II. of Scotland.
Joan, Countess of Flanders, wife of Thomas of Savoy.
Joan, Countess of Kent, Princess of Wales, wife of Edward the Black
Prince.
Joan, daughter of Edward III.
Joan, eldest daughter of Charles of Valois.
Joan of Acre, daughter of Edward I. and Countess of Gloucester.
Joan of Bar, grand-daughter of Edward I.
Joan of Flanders, Countess of Penthièvre, wife of Charles of Blois.
Joan of Toulouse, daughter of Raymond of Toulouse, wife of Alfonso of
Poitiers.
Joan, Princess of North Wales, wife of Llewelyn ap Iorwerth.
Joan, sister of Richard I., grandmother of Joan of Poitiers.
John, King.
John, King of Bohemia.
John, King of France.
John (Balliol), King of Scots.
John XXII., Pope.
John, Duke of Berri.
John II., Duke of Brabant.
John III., Duke of Brabant.
John II., Duke of Brittany.
John III., Duke of Brittany.
John IV., Duke of Brittany (Montfort).
John V., Duke of Brittany (Montfort).
John, Duke of Normandy.
  See also John, King of France.
John of Avesnes, Count of Hainault.
John of Brittany, Earl of Richmond, son of John II., Duke of Brittany,
and nephew of Edward I.
John of Eltham, son of Edward II., Earl of Cornwall.
John of Gaunt, son of Edward III., Duke of Lancaster.
John of Hainault, brother of William II. of Hainault.
John of Montfort, Earl of Richmond.
  See John V., Duke of Brittany.
John of Montfort, half-brother of John III. of Brittany.
  See John IV., Duke of Brittany.
John the Scot, Earl of Chester. See Chester.
Joinville, Joan of.
Joinvilles, the.
Joinville's History of St. Louis.
Josselin Castle.
Jowel, John.
Judges, the.
Jülich, Dukes of.
Jurisprudence, Anglo-Norman;
  Roman.
Justiciar, office of.
Justiciars.
  See Burgh, Hubert de;
  Marshal, William;
  Roches, Peter des;
  Segrave, Stephen.
Justiciars of Ireland.
  See Marsh, Geoffrey,
  and Fitzgerald, Maurice.
Justiciars of Scotland.
  See Ormesby, William.

Keighley, Henry of, knight of the shire for Lancashire.
Kelso.
Kenilworth, Dictum de.
Kenilworth Castle.
Kennington.
Kensham.
Kent;
  earldom of.
Kent, Earl of, Hubert de Burgh. See Burgh.
Kent, Edmund of Woodstock, Earl of. See Edmund.
Kerry (Wales);
  Vale of;
  scutage of.
Kervyn de Lettenhove's edition of Froissart.
Kesteven, South.
Kidwelly, castle and lordship.
Kildare, Curragh of.
Kildare, Earl of.
Kilkenny, Castle;
  statute of.
Kilwardby, Robert, Archbishop of Canterbury.
Kinghorn.
Kingsford's, C.L., Song of Lewes.
Kingston-on-Thames.
Kinloss.
Kintyre.
Kirk's Accounts of the Obedientiaries of Abingdon.
Kirkby, John, treasurer of Edward I and Bishop of Ely.
Kirkby's Quest.
Kirkcudbright, stewartry of.
Kirkliston, 213.
Klerk, Jan van, his Chronicle.
Knaresborough, castle and town.
Knighton's, Henry, Chronicle.
Knights, of the Shire;
  Templars;
  of St. John;
  of the Garter;
  of the Star.
Knowles, Sir Robert.
Knyvett, Sir John.
Köhler's Entwickelung des Kriegswesens in der Ritterzeit.

Labourers, Statute of.
Lacy, Alice, Countess of Lancaster.
Lacy, Henry, Earl of Lincoln. See Lincoln.
Lacy, Hugh de, Earl of Ulster. See Ulster.
Lacy, John de, Constable of Chester.
  See also Lincoln, Earls of.
Lacy, the house of;
  the house of, in Meath.
Lagny, Abbot of.
Lalinde.
Lamberton, Bishop of St. Andrews.
Lambeth, treaty of.
Lancashire.
Lancaster, Alice, Countess of. See Alice.
Lancaster, Blanche, Duchess of. See Blanche.
Lancaster, Edmund, Earl of. See Edmund.
Lancaster, Henry, Earl of. See Henry.
Lancaster, Henry of Grosmont, Earl and Duke of. See Henry.
Lancaster, honour of;
  town;
  house of;
  records of Duchy of.
Lancaster, John of Gaunt, Duke of. See John.
Lanercost;
  chronicle of.
Langham, Simon, Chancellor and Archbishop of Canterbury.
Langland, William.
Langley.
Langley, Geoffrey of.
Langlois, Charles V., his Philippe le Hardi;
  his Histoire de France.
Langon.
Langtoft's, Peter, Chronicle.
Langton, John, Bishop of Chichester.
Langton, Simon, Archdeacon of Canterbury.
Langton, Stephen, Archbishop of Canterbury.
Langton, Walter, Bishop of Lichfield.
Language, English;
  French;
  German;
  Latin;
  Scottish.
Languedoc.
Laon.
Laon, Robert Lecoq, Bishop of.
Laonnais, the.
Lapsley's County Palatine of Durham.
Latimer, Lord, Chamberlain.
Latin-language.
Lavisse and Rambaud's Histoire Générale.
Lavisse's Histoire de France.
Law, study of English;
  literature of;
  the Salic;
  English.
Laws, Celtic, of Highlanders and Strathclyde Welsh.
Lawyers, Italian;
  English.
Layamon's English version of Wace's Brut.
Lechler's Wycliffe.
Lecoq, Robert, Bishop of Laon.
Leeds Castle (Kent).
Leek, treaty of.
Lehugeur's Philippe le Long.
Leicester;
  earldom of.
Leicester, Abbot of.
Leicester, Countess of. See Eleanor.
Leicester, Henry, Earl of. See Henry, Earl of Lancaster.
Leicester, Robert Beaumont, Earl of.
Leicester, Simon de Montfort, Earl of.
Leicester, Simon de Montfort, the elder, Count of Toulouse and titular
Earl of.
Leicester, Thomas, Earl of. See Thomas, Earl of Lancaster.
Leicestershire.
Leinster.
Leon.
Leon.
L'Estrange, Roger.
Levant, the.
Lewes;
  battle of;
  mise of.
Lewis' Life of Wiclif.
Libellus Famosus, Edward III.'s.
Libourne.
Lichfield, Bishops of.
  See Langton, Walter;
  Northburgh, Roger.
Liddesdale. See also Liddell.
Liddell.
Liebermann, Dr., works by.
Liege, William, Bishop of. See William.
Liege.
Lille.
Limburg.
Limerick.
Limoges;
  sack of.
Limousin.
Lincoln;
  Castle;
  battle of;
  Cathedral;
  parliament of (1301);
  parliament at (1316).
Lincoln, Bishops of.
  See Wells, Hugh of;
  Hugh, St., of Avalon;
  Grosse-teste, Robert;
  Burghersh, Henry.
Lincoln, Richard le Grand, Chancellor of. See Canterbury.
Lincoln, Gilbert of Ghent, Earl of.
Lincoln, Henry Lacy, Earl of.
Lincoln, John de Lacy, Earl of, 45, 47.
Lincoln, Randolph de Blundeville, Earl of. See also Chester.
Lincoln, Thomas of Lancaster, Earl of. See Thomas, Earl of Lancaster.
Lincolnshire.
Linlithgow.
Lionel of Antwerp, son of Edward III., Duke of Clarence and Earl of
Ulster.
Lisieux;
  battle near.
Literature in the thirteenth century;
  French;
  English.
Literature in the fourteenth century;
  English;
  French.
Littleton's Tenures.
Llandaff, Bishop of.
Llandilo.
Llewelyn ap Griffith, Prince of Wales.
Llewelyn ap Iorwerth, Prince of North Wales.
Llewelyn Bren.
Lleyn.
Lloughor.
Lochmaben Castle.
Lodge's Close of the Middle Ages.
Logroño.
Loire, the river.
Lombards.
Lombardy,
  cities of.
London.
London, Bishops of.
  See Sainte-Mère-Eglise, William of;
  Basset, Fulk;
  Baldock, Ralph;
  Courtenay, William.
London, Mayors of.
  See Serlo;
  Waleys, Henry le,
  and Pyel, John.
London, Sheriffs of.
  See FitzAthulf, Constantine.
London, treaty of.
Longjumeau.
Longman's Life and Times of Edward III..
Longnon's Atlas historique de la France.
Longsword, William, Earl of Salisbury. See Salisbury.
Lorraine.
Loserth's Geschichte des späteren Mittelalters.
Lot, the river.
Lothians, the.
Loughborough.
Louis, Count of Evreux.
Louis, Duke of Anjou, brother of Charles V. of France.
Louis of Bavaria, the Emperor.
Louis of France, afterwards Louis VIII.
Louis IX. (St. Louis), King of France.
Louis X., King of France.
Louis of Male, Count of Flanders.
Louis of Nevers, Count of Flanders.
Louth;
  Earldom of.
Louth, John of Bermingham, Earl of.
Louvain.
Luard, Dr. H.R., his Roberti Grosse-teste Epistolæ;
  his editions of Annales Monastici;
  B. Cotton, and Flores Historiarum,
  and Matthew Paris' Chronica Majora.
Luce and Raynouart's edition of Froissart's Chronicle.
Lucy, Anthony.
Ludlow.
Lundy Island.
Lusignan, Alice of.
Lusignan, Aymer of. See Valence, Aymer de.
Lusignan, Guy of.
Lusignan, House of.
Lusignan, Hugh X. of.
Lusignan, Hugh XI. of.
Lusignan (town).
Lusignan, William of. See Valence, William of.
Lussac, bridge of.
Luxemburg, house of.
Lyons, Richard.
Lyons.
Lyons, Council at (1245).
Lyons, Council at (1274).
Lyrics, English.
Lys, the river.

Macaulay's, G.C., edition of Gower's Works.
Mackinnon's History of Edward III.
Macon, league of.
Madden's, Sir F., edition of Matthew Paris' Historia Minor.
Madog ap Llewelyn.
Maelgwn.
Maenan.
Maes Madog, battle of.
Maidstone.
Maine.
Mains. Elector of.
Maitland's, F.W., Memoranda de Parliamento;
  Select Pleas of the Crown;
  Bracton's Note Book;
  Le Mirroir des Justices;
  Select Passages from Bracton, etc.;
  Year Books of Edward II.
  and Canon Law.
Maitland, F.W., and Pollock, Sir F., History of English Law.

Makower's, F., Constitutional History of the Church of England.
Malestroit, truce of.
Malmesbury, the Monk of.
Malmesbury, William of.
Malton.
Maltravers, John.
Mandeville, Geoffrey de.
Manfred, King of Sicily.
Mangonels.
Manny, Sir Walter.
Mannyng, Robert.
Mansel, John.
Mansura.
Maps for period.
Mar, Donald, Earl of.
Marcel, Stephen.
March of Calais.
March (of Scotland), Patrick, Earl of.
March of Wales, the.
March of Wales, Earl of the.
  See also Mortimer, Edmund, and Mortimer, Roger.
March, Edmund Mortimer, Earl of (d. 1381).
March, Roger Mortimer, first Earl of (d. 1330).
  See also Mortimer, Roger, of Wigmore (d. 1330).
Marche, Counts of La.
Marche, La.
Mare, Sir Peter de la.
Margam, annals of abbey of.
Margaret of England, Queen of Alexander III. of Scotland.
Margaret of Flanders.
Margaret of France, sister of Philip the Fair, and second Queen of
Edward I.
Margaret of Hainault, sister of Queen Philippa, Empress of Louis of
Bavaria.
Margaret of Provence, Queen of Louis IX. of France.
Margaret, Queen of Eric, King of Norway, and mother of Margaret, Queen
of Scots.
Margaret, Queen of Scots, the Maid of Norway, daughter of Margaret and
Eric of Norway.
Margaret, sister of Alexander II. of Scotland, wife of Hubert de Burgh.
Margaret, sister of David of Scotland.
Margaret, Viscountess of Limoges.
Margaret, wife of Philip of Burgundy.
Mark, Count of.
Marlborough, statute of.
Marseilles.
Marsh, Adam;
  Letters of.
Marsh, Geoffrey, justiciar of Ireland.
Marshal, office of.
Marshal, house of.
Marshal, the Earls.
  See Pembroke, Earl of;
  Thomas of Brotherton, Earl;
  March, Mortimer, Edmund, Earl of March;
  and Percy, Henry.
Marshal, Gilbert. See Pembroke, Gilbert Marshal, Earl of.
Marshal, Isabella, wife of Richard of Cornwall.
Marshal, Richard. See Pembroke, Richard Marshal, Earl of.
Marshal, William. See Pembroke, William Marshal, the elder, Earl of,
regent of England.
Marshal, William, the younger. See Pembroke, William Marshal, the
younger, Earl of.
Martin IV., Pope.
Martin, papal envoy.
Martin's, C. Trice, Registrum Epistolarum J. Peckham.
Mary of Brabant, Queen of France.
Maturins, the.
Mauclerc, Peter, Count of Brittany. See Peter.
Maud, daughter of Henry, Duke of Lancaster.
Maud of Artois, wife of Otto, Count of Burgundy.
Maud's Castle.
Mauléon, Savary de.
Mauley, Peter de.
Mauleys, the family of.
Maupertuis.
Mauron, battle of.
Maxwell's Robert the Bruce.
Maye, the river.
Meath.
Meaux, treaty of.
Mechlin.
Mediterranean, the.
Melton, William, Archbishop of York.
Melrose Abbey.
Melrose, chronicle of.
Menai Straits, the.
Mendicants, the See also Friars.
Meopham, Simon, Archbishop of Canterbury.
Mercenaries.
Merchants,
  statute of;
  foreign;
  English.
Meredith ap Owen.
Merioneth.
Merionethshire.
Merlin.
Merton.
"Merton, Rule of,".
Merton, Walter of.
Messina, Archbishop of.
Methven, battle of.
Metingham, John of.
Meyer, Paul, his edition of the Histoire de Guillaume le
Maréchal.
Miausson, the river.
Michel, Francisque.
Milan.
Ministers' Accounts.
Minorites, the,
  See also Franciscans.
Minot, Lawrence.
Minsterworth, Sir John.
Miracle plays.
Mirambeau.
Miranda.
Mirroir des Justices, Le.
Mise of Amiens, the.
Mise of Lewes, the.
Model Parliament, the.
  See Parliament.
Mohammedans, the.
Molinier, Auguste, Sources de l'histoire de France.
Monasteries.
Monasticon, Dugdale's.
Monmouth, castle and town of.
Monnow, the river.
Mont Cenis, the.
Montague, Sir William.
  See also Salisbury, Earls of.
Montague;
  the house of.
Montfavence, Bertrand of, Cardinal.
Montfichet, Richard of.
Montfort l'Amaury.
Montfort, county of.
Montfort, Amaury of.
Montfort, the house of (Dukes of Brittany).
  See also John IV. and John V., Dukes of Brittany.
Montfort, the house of (Earls of Leicester).
Montfort, Henry of.
Montfort, John of, the elder. See Brittany, John, Duke of.
Montfort, John of, the younger. See Brittany, John, Duke of.
Montfort, Peter of.
Montfort, Simon of, Count of Toulouse.
  See also Leicester.
Montfort Simon of, Earl of Leicester. See Lester.
Montfort, Simon of, the younger, son of Simon, Earl of Leicester.
Montgomery, castle and town of.
Monthermer, Ralph of.
Monthermer, Thomas of, Montjoie.
Montmorenci, Matthew of.
Montpellier, University of.
Montpezat, lord of.
Montreuil-sur-mer.
  treaty of.
Montrose.
Mont-Saint-Martin, Monastery of.
Monumenta Franciscana, Brewer's.
Monumenta Hist. Germanicae, Scriptores, Pertz'.
Moors of Granada.
Moor, Sir Thomas de la.
Moray.
Moray, Randolph, Earl of.
Moray, Sir Andrew.
Morbihan.
Morgan of Caerleon.
Morgan, leader of Glamorganshire rebels.
Morgarten, battle of.
Morlaix.
  battle of.
Morley, Robert.
Mortimer, Edmund (d. 1303).
Mortimer, Edmund (d. 1381). See March, Edmund Mortimer, Earl of.
Mortimer, Roger, of Chirk.
Mortimer, Roger, of Wigmore (d. 1282).
Mortimer, Roger, of Wigmore (d. 1330).
  See also March, Roger Mortimer, first Earl of.
Mortimer, Roger, grandson of Roger Mortimer, first Earl of March.
Mortimer, Roger, son of Edmund, Earl of March.
Mortimer, the house of.
Mortmain, Statute of.
Moselle, the river.
Mountchensi, Joan of.
Mount Sorrel.
Mowbray, John of (of Scotland).
Mowbray, John of.
Murimuth, Adam.
Myton, battle of.

Najarilla, the river.
Nájera, battle of.
Nantes.
Naples.
Narbonne.
Nassau, Adolf of. King of the Romans. See Adolf, King of the Romans.
Navarre, Blanche of Artois, Queen of. See Blanche.
Navarre, Henry III., King of. See Henry.
Navarre, King of, Charles the Bad. See Charles.
Navarre, Philip of. See Philip.
Navarre, Theobald IV., King of. See Theobald.
Navarre.
Navarete,
Navy, the English;
  the French;
  the Norman.
Neath Abbey.
Netherlands, the.
Neufbourg, house of.
Neufbourg, Henry of, Earl of Warwick. See Warwick.
Nevers, Louis of. See Louis of Nevers, Count of Flanders.
Nevers, the Count of.
Neville of Raby, Lord.
Neville, Ralph, Bishop of Chichester and Chancellor.
Nevilles, the.
Neville's Cross, battle of.
Newark.
Newcastle-on-Tyne.
Newport-on-Usk.
Nicholas IV., Pope.
Nicolas's History of the Royal Navy.
Nine, Council of.
Niort.
Nivernais, the.
Norfolk;
  earldom of.
Norfolk, Roger Bigod, Earl of.
Norfolk, Roger Bigod, Earl of, nephew of above.
Norfolk, Thomas of Brotherton, Earl of See Thomas.
Norham Castle.
Norman architecture.
Normandy.
Normandy, Charles, Duke of. See Charles.
Normandy, John, Duke of. See John, King of France.
Normans, the;
  in Ireland, the.
Norsemen in Scotland, the.
Northallerton.
Northampton;
  parliaments at;
  treaty of Brigham confirmed at;
  treaty of;
  earldom of.
Northampton, William Bohun, Earl of.
Northamptonshire.
Northburgh, Roger, Bishop of Lichfield or Coventry and treasurer.
Northumberland.
Norway, Eric, King of. See Eric.
Norway, Margaret, the Maid of, Queen of Scotland. See Margaret.
Norwich.
Norwich, Bishops of. See Ayermine, William, and Pandulf.
Nottingham.
Nouaillé.

Ochils, the.
Ockham, William of.
O'Connor, Phelim, King of Connaught. See Connaught.
Odiham.
O'Donnells, the.
Oléron, Isle of.
Oliver, illegitimate son of King John.
Oloron, treaty of.
Oman's History of the Art of War in the Middle Ages.
O'Neils, the.
Oise, the river.
Ordainers, the Lords.
Order of the Garter, the.
Order of the Star, the.
Orders, the Religious.
Orders of Friars.
Orewyn Bridge, battle of.
Originalia Rolls, the.
Orkneys, the.
Orleans, Duke of.
Orleton, Adam, Bishop of Hereford.
Ormonde, the Butler of Ireland, made Earl of.
Ormesby, William, justiciar.
Orne, the river.
Orvieto.
Orwell, port and river.
Oseney Abbey;
  Annals of.
Oswestry.
O'Tooles, the.
Otto, nuncio to England;
  legate.
Otto, Count of Burgundy.
Ottobon, Cardinal, legate.
Ottocar, King of Bohemia.
Ouistreham.
Ouse, the river.
Owain Lawgoch. See Owen of Wales.
Owen of Wales, Sir Owen ap Thomas ap Rhodri.
Owen the Red, son of Griffith ap Llewelyn.
Owens College Historical Essays.
Oxford,
  University of,
  Balliol College,
  Merton College,
  the Provisions of,
  parliament at,
  Exeter College.
Oxfordshire.
Oxnead, John of.

Painting in Westminster Abbey.
Palatine, the Elector.
Palermo.
Palestine.
Palestrina, Cardinal-bishop of.
Palgrave's, Sir F.T., Parliamentary Writs and Writs of Military
Service.
  his Documents illustrating the History of Scotland.
Pamplona.
Pandulf, Papal Legate and Bishop of Norwich.
Pantheism.
Papacy, the,
  See also under Popes.
Paris,
  University of,
  College of the Sorbonne in,
  Cathedral of,
  parliament of,
  treaty of (1259),
  treaty of (1303),
  treaty of (1327).
Paris, Matthew.
Parliament, of,
  the mad (1258),
  of Oxford,
  growth Of,
  at Oxford (1264),
  at Northampton (1267),
  at Bury (1267),
  of 1273,
  at Westminster (1275),
  of 1283,
  at Shrewsbury (1284),
  at Acton Burnell (1284),
  of 1289,
  at London (1294),
  the model(1295),
  of the perambulation (1300),
  at Lincoln (1301),
  at Westminster (1305),
  of Carlisle (1307),
  of 1308,
  at Westminster (1309),
  at Stamford (1309),
  of London (1310),
  at London (1315),
  at Lincoln (1316),
  the Irish,
  at York (1318),
  at York (1319),
  in London (July, 1320),
  at York (May, 1322),
  at Westminster (January, 1327),
  at Salisbury (October, 1328),
  at Northampton (1329),
  at Winchester (March, 1330),
  prorogued to Westminster (November, 1330),
  of April 23, 1341,
  of April, 1343,
  of 1347,
  of 1371,
  of 1372,
  the Good (April, 1376),
  of 1377,
  of Paris, see Paris, parliament of.
Parthenai.
Passelewe, Robert.
Pastaureaux, the.
Patrick, Earl of March,
  See also March (Scotland), Earl of.
Pauli's, R., Geschichte von England.
Pavia, Galeazzo Visconti, Lord of.
Paynel, Fulk.
Pearl, the, poem of.
Peasants' revolt, the.
Peasants, revolts of French.
Peckham, John, Archbishop of Canterbury.
Peebles.
Pell Records, the.
Pembroke, earldom of.
Pembroke, Gilbert Marshal, Earl of.
Pembroke, Richard Marshal. Earl of.
Pembroke, William Marshal, the elder, Regent and Earl of,
  History of.
Pembroke, William Marshal, the younger, Earl of.
Pembroke, Aymer of Valence, Earl of.
Pembroke. John Hastings, second Earl of that house.
Pembroke. William of. See William of Valence.
Pembrokeshire, palatine county of.
Penance of Jesus Christ, Friars of the.
Penne.
Penrith.
Penthièvre, county of.
Penthièvre-Tréguier, county of.
Perche, Count of.
Percy, Henry, grandson of Earl Warenne.
Percy, Henry, marshal of England.
Percy, Sir Thomas, seneschal of Poitou.
Percy, the family of.
Périgord.
Périgord, Count of.
Périgueux,
  bishopric of.
Péronne.
Perpendicular style in architecture.
Perrers, Alice.
Perth.
Pertz's Monumenta.
Peruzzi, the.
Perveddwlad.
Peter, Cardinal. See Gomez, Peter.
Peter III., King of Aragon.
Peter Mauclerc, Count of Brittany.
Peter of Aigueblanche, Bishop of Hereford,
  See Aigueblanche.
Peter of Gaveston. See Gaveston.
Peter of Savoy, Earl of Richmond.
Peter of Spain, Cardinal.
Peter Roger, Archbishop of Rouen. See Roger, Peter, and Clement VI.
Peter the Chamberlain.
Peter the Cruel, King of Castile.
Peterhouse, Cambridge.
Peter's Pence.
Petit's Charles de Valois.
Petit-Dutaillis, M.,
  his Étude sur Louis VIII.
Petrarch, Francis.
Petrariae.
Pevensey Castle.
Philip II., Augustus, King of France.
Philip III., the Bold, King of France.
Philip IV., the Fair, King of France.
Philip V., the Long, King of France.
Philip VI. of Valois, King of France.
Philip, Count of Savoy.
Philip, Count of Valois, See also Philip VI., King of France.
Philip of Navarre.
Philip of Rouvres, Duke of Burgundy.
Philip the Bold, Count of Évreux.
Philip the Bold, Duke of Burgundy, son of John, King of France.
Philippa, daughter of Lionel, Duke of Clarence, Countess of March.
Philippa of Hainault, Queen of Edward III.
Philippine, daughter of Guy of Dampierre, Count of Flanders.
Philpots, the.
Philobiblon, the, of Richard of Bury.
Philosophy.
Picardy.
Pike, L.O., his editions of the Year Books.
Pipe, James.
Pipe Rolls.
Pipton, treaty o.
Pirenne's Bibliographie de l'histoire de Belgique.
  Histoire de Belgique.
Pisa, Agnellus of. See Agnellus.
Plague, the. See Black Death.
Plays, miracle.
Plessis, John du, Earl of Warwick.
  See Warwick.
Ploermel.
Plympton.
Poissy.
Poitevins.
Poitiers,
  battle of,
  sources for.
Poitiers, Alfonse of. See Alfonse.
Poitou,
  scutage of.
Poitou, Count of, Richard, son of King John, Count of. See Richard.
Polain's edition of Jean le Bel,
Pole, the house of
Pole, William de la.
Pollock, Sir P., and Maitland's History of English Law,
Polychronicon, Higden's.
Pons.
Pont-Sainte-Maxence.
Pontefract,
  Castle.
Ponthieu.
Pontigny.
Pontoise.
Pontvallain, battle of.
Poole's, R.L., Mediæval Thought,
  his Wycliffe,
  his Oxford Historical Atlas.
Popes.
Port Blanc.
Ports, the Cinque.
Portsmouth.
Portugal, Ferdinand of.
Powys;
  Castle.
Powys, Charltons of. See Charltons.
Praemunire statute of.
Preachers, Order of. See Dominicans.
Pressuti's Registers of Honorius III.
Preston.
Prices, rise in, after the Black Death.
Principality of Wales, the.
Priories, the alien.
Proclamation in English, French and Latin.
Prothero's Simon de Montfort.
Provençals.
Provence.
Provence, Raymond Berengar IV., Count of,
  See Raymond Berengar.
Proving.
Provisions, papal;
  of Oxford, the;
  of Westminster, the;
  of Worcester.
Provisors, statute of.
Public Record Office, the.
Purveyance.
Puymirol.
Pyel, John, mayor of London.
Pyrenees, the.

Quercy Quia Emptores statute. Quièret, Hugh. Quincy, Saer de, Earl of Winchester. See Winchester.

Rageman, statute of.
Ragman. Roll, the.
Ranee, the river.
Randolph, Sir Thomas, Earl of Moray.
Rashdall's Universities of the Middle Ages.
Rathlin Island.
Rationalism.
Ravenspur.
Raymond Berengar IV., Count of Provence.
Raymond VII., Count of Toulouse.
Record of Carnarvon, the.
Record Commission, the.
Red Hills, the.
Redesdale.
Redesdale, Gilbert of Umfraville, Lord of. See Umfraville.
Regalis Devotionis, Bull.
Reginald, Count of Gelderland.
Registers, Bishops;
  Papal Calendars of.
Reims.
Reims, Archbishop of.
Renaissance of the twelfth century, the.
Rennes.
Réole, La.
Reports of Deputy-keeper of the Records;
  of Historical Manuscripts Commission.
Revolt, the peasants'.
Reynolds, Walter, Treasurer of England and Archbishop of Canterbury.
Rhine, the.
Rhine, Count Palatine of the.
Rhineland, the.
Rhos, Cantred of.
Rhone Valley, the.
Rhuddlan Castle.
Rhunoviog, Cantred of.
Rhys ap Howel.
Rhys ap Meredith.
Rhys, J., and J.G. Evans' Red Book of Hergest.
Rich, St. Edmund, Archbishop of Canterbury.
Richard I.
Richard of Bordeaux, son of the Black Prince.
Richard, son of King John, titular Count of Poitou, Earl of Cornwall
and King of the Romans.
Richmond, John, Earl of. See John of Gaunt.
Richmond, John of Brittany, Earl of. See John of Brittany.
Richmond, Peter Mauclerc, Earl of.
  See Peter, Count or Duke of Brittany.
Richmond, Peter of Savoy, Earl of. See Peter of Savoy.
Richmond (place).
Richmond, Simon de Montfort, made Earl of. See Leicester, Earl of
Rievaux.
Rigaud, Bishop of Winchester
Rigaud, Eudes, Archbishop of Rouen.
Rigg's, J.M., Select Pleas of the Jewish Exchequer.
Riley's, H.T., his edition of Rishanger, etc.
Rioms.
Ripon.
Rishanger, William.
Rivaux, Peter of, treasurer.
Robert I, Bruce, King of Scots. See also Bruce, Robert.
Robert II, Steward of Scotland, afterwards King Robert II.
Robert, Steward of Scotland.
Robert, Count of Artois.
Robert of Artois, enemy of Philip VI.
Robert, Count of Namur.
Roberts' Calendarium Genealogicum.
Roche Derien, La, battle of.
Rochelle, La.
Rochelle, battle of La.
Roches, Peter des, Bishop of Winchester.
Rochester, Castle and city.
Rockingham Castle.
Rodez, Bishop of.
Roger, Peter. See also Clement VI Pope.
Rogers, J.E. Thorold, History of Agriculture and Prices.
Roles Gascons. See Rolls
Roll, the Ragman.
Rolle, Richard
Rolls;
  the hundred;
  patent;
  the close;
  of parliament;
  series, the;
  of Court of Chancery;
  Charter;
  Escheat or Inquisitiones post mortem;
  fine;
  Excerpt a e Rotulis Finium (C. Roberts');
  exchequer;
  Assize;
  Coroners;
Romana Mater, bull.
Romances.
Romanesque architecture.
Romans, Adolf of Nassau, King of the, see Adolf of Nassau;
  Charles of Moravia, King of the, see Charles IV;
  Henry, King of the, see Henry;
  Rudolf of Hapsburg, King of the, see Rudolf;
  William of Holland, King of the, see William of Holland.
Rome.
Romney.
Romont.
Romorantin Castle.
Roncesvalles, Pass of.
Roncière, de la, Histoire de la Marine Française.
Rose Castle.
Roslin.
Rostein, the family of.
Rotuli. See Rolls.
Round Table at Windsor.
Rouen,
  Archbishops of. See Rigaud, Eudes, Roger, Peter.
Rouergue,
  Counts of. See Armagnac, Count of.
Roussillon.
Roxburgh, town and castle;
  treaty of.
Royan.
Rudel, Elie, lord of Bergerac.
Rudolf of Hapsburg, King of the Romans.
Runnymede.
Ruthin.
Rye.
Rymer's Foedera.

