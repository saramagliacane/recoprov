package nl.vu.recoprov.baseclasses;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

import org.apache.pdfbox.pdmodel.common.PDMetadata;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.sanselan.common.IImageMetadata;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.Property;

import uk.ac.shef.wit.simmetrics.similaritymetrics.AbstractStringMetric;
import uk.ac.shef.wit.simmetrics.similaritymetrics.Levenshtein;
import com.dropbox.client2.RESTUtility;
import com.dropbox.client2.DropboxAPI.Entry;
/**
 * Wrapper for the various metadata formats
 */
public class RecoMetadata extends Metadata{
	
	private static final long serialVersionUID = 1L;
	public final static String LUCENE_DOCID = "lucene-docid";
	public final static String DROPBOX_PATH = "dropbox-path";
	public final static String DROPBOX_REVISION = "dropbox-revision";
	public final static String DROPBOX_CLIENTMTIME = "dropbox-clientMtime";
	public final static String DROPBOX_BYTES = "dropbox-bytes";
	public final static String DROPBOX_SIZE = "dropbox-size";
	public static final String DROPBOX_MODIFIED = "dropbox-modified";	
	public final static String DROPBOX_MIMETYPE = "dropbox-mimeType";
	public final static String FILESYSTEM_LASTMODIFIED = "filesystem-last-modified";
	public final static String COMPRESSED_SIZE = "compressed-size";

	
	//dropbox
	private Entry DropboxEntry = null;	
	
	//tika
	private Metadata tikaMetadata = null;
	
	//image metadata
	private HashMap<String, PDMetadata> imagePDMetadata = new HashMap<String, PDMetadata>();
	private HashMap<String, IImageMetadata> imageIImageMetadata = new HashMap<String, IImageMetadata>();
	
	//temporal info
	private Date modified = null;
	//used in case of ties in the file modification date;
	//private Date dirModified = null;
	
	//filesystem
	private Date fsmodified = null;
	//used in case of ties in the file modification date;
	private Date fsDirModified = null;
	private long fssize;
	
	private Set<String> images = null;
	

	
//	private ContentType contentType;	
//	private SemanticType semanticType;
	

	public Entry getDropboxEntry() {
		return DropboxEntry;
	}

	public void setDropboxEntry(Entry dropboxEntry) {
		DropboxEntry = dropboxEntry;
		
		if(dropboxEntry == null)
			return;
		
		this.setModified(RESTUtility.parseDate(dropboxEntry.modified));
		this.set(DROPBOX_PATH, dropboxEntry.path);
		this.set(DROPBOX_REVISION, dropboxEntry.rev);
		this.set(Property.externalDate(DROPBOX_CLIENTMTIME), dropboxEntry.clientMtime);
		this.set(Property.externalReal(DROPBOX_BYTES), (double) dropboxEntry.bytes);
		this.set(DROPBOX_MIMETYPE, dropboxEntry.mimeType);
		this.set(DROPBOX_SIZE, dropboxEntry.size);
		
	}
	
	public Date getCreationDate(){
		if(this.get(this.CREATION_DATE) != null){
			String date = this.get(this.CREATION_DATE);		
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			Date val;
			try {
				val = formatter.parse(date);
			} catch (ParseException e) {
				e.printStackTrace();
				return null;
			}
			return val;		
		}	
		return null;
	}


	public Date getModified() {

		if (modified != null)
			return modified;
		else
			return fsmodified;
	}

	public String getMimeType() {
		if (this.tikaMetadata!= null){
			return tikaMetadata.get(Metadata.CONTENT_TYPE);
		}	
		else if (this.DropboxEntry!= null){
			return this.get(DROPBOX_MIMETYPE);
		}
		else{
			return "text/plain";
		}	
	}
	
	public void setModified( Date modified) {
		this.modified = modified;
		this.set(Property.externalDate(DROPBOX_MODIFIED), this.modified);
		
	}

	public void setTikaMetadata(Metadata metadata) {
		tikaMetadata = metadata;
		for (String name: metadata.names()){
			this.set(name, metadata.get(name));
		}
		
	}
	
	public void setFSModified(Date modified){
		this.fsmodified = modified;
		this.set(Property.externalDate(RecoMetadata.FILESYSTEM_LASTMODIFIED), modified);
	}
	
	public String getDropboxPath(){
		return this.get(DROPBOX_PATH);
	}
	
	public String getRevision(){
		return this.get(DROPBOX_REVISION);
	}
	

	public void setLuceneDocNumber(int luceneDocNumber) {
		this.set(Property.externalInteger(LUCENE_DOCID), luceneDocNumber );

	}

	public int getLuceneDocNumber() {
		return this.getInt(Property.externalInteger(LUCENE_DOCID));

	}
	
	public HashMap<String, Double> findSimilarMetadata(DependencyNode d){
		return 	findSimilarMetadata(d.getMetadata());
	}
	
	public HashMap<String, Double> findSimilarMetadata(RecoMetadata r){
		HashMap<String, Double> result = new HashMap<String, Double>();
		for (String name:this.names()){
			if(r.get(name) == null)
				continue;
			String originalAttrib = this.get(name);
			String otherAttrib = r.get(name);
			
			
//			if(name.equals(DROPBOX_PATH))
//				result.put(name, ApproximateSimilarity(originalAttrib.substring(originalAttrib.lastIndexOf("/"), originalAttrib.lastIndexOf(".")), otherAttrib.substring(otherAttrib.lastIndexOf("/"), otherAttrib.lastIndexOf("."))));
//			else
			result.put(name, CrispSimilarity(originalAttrib, otherAttrib));
			
		}
		
		return result;
	}
	
	private Double CrispSimilarity(String originalAttrib, String otherAttrib) {
				
		if(originalAttrib.equals(otherAttrib)){
			
			if(originalAttrib.equals("") || originalAttrib.trim().equals(""))
				return 0.0;
			
			return 1.0;
		}
		else{
			return 0.0;
		}
	}
	
	private Double ApproximateSimilarity(String originalAttrib, String otherAttrib) {
		//using the SimMetrics library
        //AbstractStringMetric metric = new MongeElkan();
		AbstractStringMetric metric = new Levenshtein();
		
        //this single line performs the similarity test
        float result = metric.getSimilarity(originalAttrib, otherAttrib);
        
        if(result < 0.9){
        	result = 0;
        }
        else{
     
        	//System.out.println("Using Metric " + metric.getShortDescriptionString() + " on strings \"" + originalAttrib + "\" & \"" + otherAttrib + "\" gives a similarity score of " + result);
        }
        return (double) result;

		
	}

	public Set<String> getAttributes() {
		Set<String> attrib = new TreeSet<String>();

		for (String name: this.names()){
			if(name.equals(LUCENE_DOCID))
				continue;
			else
				attrib.add(name);
		}
		
		Set<String> insertionOrderedSet = new LinkedHashSet<String>();
		insertionOrderedSet.add(LUCENE_DOCID);
		insertionOrderedSet.addAll(attrib);
		return insertionOrderedSet;
	}

	public void setFSSize(long length) {
		fssize = length;
		
	}
	
	public long getSize(){
		return fssize;
	}

	public Date getFsDirModified() {
		return fsDirModified;
	}

	public void setFsDirModified(Date fsDirModified) {
		this.fsDirModified = fsDirModified;
		//this.set(Property.externalDate(this.FILESYSTEM_DATELASTMODIFIED), fsDirModified);
	}
	
	public void addImages(String filepath, Object metadata){
		if(images==null){
			images = new HashSet<String>();
		}
		
		images.add(filepath);

		if (metadata == null){
			return;
		}
		
		if (metadata instanceof PDMetadata){
			imagePDMetadata.put(filepath, (PDMetadata) metadata);
			
			
                InputStream xmlInputStream = null;
                try {
                    xmlInputStream = ((PDStream) metadata).createInputStream();
                    String mystring = convertStreamToString(xmlInputStream);
                } catch (IOException e) {
                    e.printStackTrace();
                }

		}
		else if (metadata instanceof IImageMetadata){
			imageIImageMetadata.put(filepath, (IImageMetadata) metadata);
		}

	}

	public Set<String> getImages(){
		return images;
	}
	
	public HashMap<String, PDMetadata> getImagePDMetadata(){
		return imagePDMetadata;
	}
	
	public  HashMap<String, IImageMetadata> getIImageMetadata(){
		return imageIImageMetadata;
	}
	
	public long getCompressedSize() {
		if (Property.externalInteger(COMPRESSED_SIZE) == null)
			return 0;
		Integer value = this.getInt(Property.externalInteger(COMPRESSED_SIZE));
		
		if (value == null)
			return 0;
		return (long) value;
	}
	
	public void setCompressedSize(long compressedSize) {
		this.set(Property.externalInteger(COMPRESSED_SIZE), (int) compressedSize);
	}
			
	
	public String convertStreamToString(InputStream is) throws IOException {
		if (is != null) {
			StringBuilder sb = new StringBuilder();
			String line;

			try {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(is, "UTF-8"));
				while ((line = reader.readLine()) != null) {
					sb.append(line).append("\n");
				}
			} finally {
				is.close();
			}
			return sb.toString();
		} else {
			return "";
		}
	}

	public String toString() {
		StringBuffer temp = new StringBuffer();
		for (String name : this.names()) {
			temp.append(name).append(":");
			temp.append(this.get(name)).append("\n");
		}
		return temp.toString();
	}

	public String toCSVString(Set<String>listOfNames) {
		StringBuffer temp = new StringBuffer(getLuceneDocNumber());
		temp.append(DependencyNode.DELIMITER); 
		
		for (String name: listOfNames){
			if(name.equals(LUCENE_DOCID)){
				continue;
			}
			else if (this.get(name)!= null){
				temp.append(this.get(name));
				temp.append(DependencyNode.DELIMITER); 
			}
			else{
				temp.append(DependencyNode.DELIMITER); 
			}
		}
		return temp.toString();
	}

}
