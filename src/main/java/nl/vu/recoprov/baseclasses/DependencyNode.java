package nl.vu.recoprov.baseclasses;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Set;
import org.apache.lucene.search.ScoreDoc;
import org.apache.tika.metadata.Metadata;
import com.dropbox.client2.DropboxAPI.Entry;


public class DependencyNode implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 84L;

	public static final String DELIMITER = "&";

	private String completefilepath;
	private DependencyGraph depGraph;
	//private ScoreDoc[] luceneSimilarity = null;
	private String content = null;
	private RecoMetadata recoMetadata = new RecoMetadata();
	
	private ArrayList<Integer> luceneIdSentences = new ArrayList<Integer>();

	public DependencyNode(DependencyGraph d) {
		setDepGraph(d);
	}

	public String getCompleteFilepath() {
		return completefilepath;
	}

	public void setCompleteFilepath(String filepath) {
		this.completefilepath = filepath;
	}

	public RecoMetadata getMetadata() {
		return this.recoMetadata;
	}

	public void setMetadata(Metadata metadata) {
		recoMetadata.setTikaMetadata(metadata);
	}

	public void addtoMetadata(String name, String value) {
		this.recoMetadata.add(name, value);
	}

	public Entry getDropboxEntry() {
		return recoMetadata.getDropboxEntry();
	}

	public void setDropboxEntry(Entry dropboxEntry) {
		recoMetadata.setDropboxEntry(dropboxEntry);
	}

	public String getDropboxFilepath() {
		return recoMetadata.getDropboxPath();
	}

	public String toCSVString(Set<String> listOfNames) {
		String temp = this.recoMetadata.toCSVString(listOfNames);

		temp += this.completefilepath.replaceAll(DELIMITER, "_") + "\n";
		return temp;
	}

	public Set<String> getAttributes() {
		return recoMetadata.getAttributes();
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getId() {
		return getLuceneDocNumber();
	}

	public int getLuceneDocNumber() {
		return this.recoMetadata.getLuceneDocNumber();
	}

	public void setLuceneDocNumber(int luceneDocNumber) {
		this.recoMetadata.setLuceneDocNumber(luceneDocNumber);
		depGraph.addTranslation(luceneDocNumber, this.getCompleteFilepath());
	}

	public void setDepGraph(DependencyGraph d) {
		depGraph = d;
	}

//	public ScoreDoc[] getLuceneSimilarity() {
//		return luceneSimilarity;
//	}
//
//	public void setLuceneSimilarity(ScoreDoc[] luceneSimilarity) {
//		this.luceneSimilarity = luceneSimilarity;
//	}

	public String getMimeType() {
		return this.recoMetadata.getMimeType();
	}

	public long getSize() {
		return this.recoMetadata.getSize();
	}
	
	public long getCompressedSize() {
		if (this.recoMetadata == null)
			return 0;
		return this.recoMetadata.getCompressedSize();
	}

	public void setCompressedSize(long zipFile) {
		if (this.recoMetadata == null)
			this.recoMetadata = new RecoMetadata();
		this.recoMetadata.setCompressedSize(zipFile);
	}

	public DependencyNode copyInGraph(DependencyGraph depGraph) {

		DependencyNode d = new DependencyNode(depGraph);
		d.setContent(this.getContent());
		d.setCompleteFilepath(this.getCompleteFilepath());
		d.setDropboxEntry(getDropboxEntry());
		//d.setLuceneSimilarity(getLuceneSimilarity());
		d.setLuceneDocNumber(getLuceneDocNumber());
		d.setMetadata(getMetadata());
		d.setLuceneIdSentences(getLuceneIdSentences());
		return d;
	}
	
	public String toString() {
		StringBuffer temp = new StringBuffer("##########################################");
		temp.append("\nMetadata: ");
		temp.append(this.completefilepath);
		temp.append(" \n");

		for (String name : this.recoMetadata.names()) {
			temp.append(name);
			temp.append(": ");
			temp.append(this.recoMetadata.get(name));
			temp.append("\n");
		}

		if (this.content != null) {
			temp.append("Contentfilename: ");
			temp.append(content);
		}
		
		if (this.luceneIdSentences != null) {
			temp.append("LuceneId: ");
			temp.append(luceneIdSentences);
		}

		temp.append("\n");
		return temp.toString();
	}

	public ArrayList<Integer> getLuceneIdSentences() {
		return luceneIdSentences;
	}

	public void setLuceneIdSentences(ArrayList<Integer> luceneIdSentences) {
		this.luceneIdSentences = luceneIdSentences;
	}
	
	public void addLuceneIdSentences(Integer luceneid) {
		this.luceneIdSentences.add(luceneid);
	}



}
