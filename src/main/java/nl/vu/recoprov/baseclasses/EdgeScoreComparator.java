package nl.vu.recoprov.baseclasses;

import java.util.Comparator;

public class EdgeScoreComparator implements Comparator<LabelledEdge> {


	public int compare(LabelledEdge o1, LabelledEdge o2) {
		return (o1.getScore() < o2.getScore() ? -1 : (o1.getScore() == o2
				.getScore() ? 0 : 1));
	}
}
	

