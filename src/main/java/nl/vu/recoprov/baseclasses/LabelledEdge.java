package nl.vu.recoprov.baseclasses;

import java.io.Serializable;

public class LabelledEdge implements Serializable {
	/**
		 * 
		 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String label;
	private double score = 0.0;

	public LabelledEdge(Integer i, String s, double score) {
		setId(i);
		setLabel(s);
		this.score = score;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String toString() {
		return label + " -> " + id + " [ score: " + score + "]";
	}

}
