package nl.vu.recoprov.signaldetectors;

import nl.vu.recoprov.abstractclasses.SignalDetector;
import nl.vu.recoprov.baseclasses.DependencyGraph;

public class LuceneNgramsSignal extends SignalDetector {

	@Override
	public DependencyGraph computeSignal(DependencyGraph input) {
		try{

//			File indexDir = new File(LuceneIndexer.INDEX_DIR);
//			//indexDir.mkdir();
//			
//			FSDirectory store = SimpleFSDirectory.open(indexDir);
//			IndexReader reader = IndexReader.open(store);
//			IndexSearcher searcher = new IndexSearcher(reader);
//			int numdocs = reader.numDocs();
//			
//			for (int i=0; i<numdocs; i++){
//				TermFreqVector[] tfvs = reader.getTermFreqVectors(i);
//				ScoreDoc[] hits = new ScoreDoc[0];
//				
//				if(tfvs == null){
//					System.out.println("No terms vector found for doc " + i);
//				}
//				else{
//					for (TermFreqVector tfv: tfvs){
//						String[] terms = tfv.getTerms();
//				        int[] frequencies = tfv.getTermFrequencies();
//		                String querystring = "";
//		                
//		                int k = -1;
//		                Map<String,Integer> termVector=new TreeMap<String,Integer>();
//		                for (String term: terms){
//		                	k = k+1;
//		                	if(ignoreTerm(term)){
//		                		continue;
//		                	}
//		                	term = modifyTerm(term);
//		                	
//		                    termVector.put(term, frequencies[k]);
//		                }
//		                
//	
//		                // TODO: order by most frequent ones
//		                
//		                for (String term: termVector.keySet()){
//		                	if (querystring.equals("")){
//		                		querystring = term;
//		                		continue;
//		                	}
//		                	querystring = querystring + " OR " + term;
//		                }
//		                
//		                hits = searchForString(i, searcher, querystring);
//		                
//	
//		                    
//					}
//				}	
//					
//				Document doc = searcher.doc(i);
//				String key = doc.getFieldable("name").stringValue();
//				DependencyNode d = input.get(key);
//	  	        d.setLuceneSimilarity(hits);
//	  	        // probably unnecessary
//	  	        input.put(d.getCompleteFilepath(), d);
//	  	        
//	  	        for (int j = 0; j< hits.length; j++){
//	  	        	
//	  	        	if (hits[j].score < 0.1) 
//	  	        		continue;
//	  	        	
//	  	        	if(hits[j].doc == i)
//	  	        		continue;
//	  	        	
//
//	  	  	        //d.addDepNodeSimilarity("lucene_similarity", d2, hits[j].score);
//	  	  	        input.addEdge(i, hits[j].doc, LUCENE_SIMILARITY , hits[j].score);
//	  	  	        
//
//	  	        	
//	  	        }
//
//
//				
//			}
//			
			return input;
			
			} catch(Exception e ){
				System.out.println("Compute similarity: Lucene Directory failing \n");
				e.printStackTrace();
				return null;
			}
		}		

	}

