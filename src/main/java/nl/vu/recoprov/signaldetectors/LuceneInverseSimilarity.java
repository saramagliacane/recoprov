package nl.vu.recoprov.signaldetectors;

import java.util.ArrayList;
import java.util.Date;
import nl.vu.recoprov.abstractclasses.SignalDetector;
import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.baseclasses.LabelledEdge;
import nl.vu.recoprov.baseclasses.DependencyNode;
import nl.vu.recoprov.baseclasses.RecoMetadata;
import nl.vu.recoprov.signaldetectors.LuceneSimilaritySignal;

public class LuceneInverseSimilarity extends SignalDetector {

	@Override
	public DependencyGraph computeSignal(DependencyGraph input) {
		for (String name : input.keySet()) {

			// for each node filter temporally the edges
			DependencyNode d = input.get(name);

			if (d == null) {
				System.out.println("No node with name " + d);
				continue;
			}

			RecoMetadata metadata = d.getMetadata();
			Date lastModified1 = metadata.getModified();
			Date created1 = metadata.getCreationDate();

			ArrayList<LabelledEdge> edgearray = input.getAllEdges(d
					.getLuceneDocNumber());

			if (edgearray == null)
				continue;

			for (LabelledEdge edge : edgearray) {

				if (edge.getLabel() != LuceneSimilaritySignal.LUCENE_SIMILARITY)
					continue;

				DependencyNode d2 = input.get(edge.getId());

				if (d2 == null) {
					System.out.println("No translation for " + edge.getId());
					continue;
				}

				Date lastModified2 = d2.getMetadata().getModified();
				Date created2 = d2.getMetadata().getCreationDate();

				if ((created1 != null) && (created2 != null)) {
					compareModifiedDate(input, edge, d, d2, created1, created2);
					continue;
				}

				if ((lastModified1 != null) && (lastModified2 != null)) {
					compareModifiedDate(input, edge, d, d2, lastModified1,
							lastModified2);
				}

			}

		}

		return input;
	}

	public void compareModifiedDate(DependencyGraph input, LabelledEdge edge,
			DependencyNode d, DependencyNode d2, Date lastModified1,
			Date lastModified2) {

		// if the node in which the arrow is generated is younger than the other
		if (lastModified1.after(lastModified2)) {
			// prune

			// lucene similarity is not symmetric, but it more resembles
			// containment
			// invert edge
			input.addEdge(d2, d,
					LuceneSimilaritySignal.LUCENE_INVERSE_SIMILARITY,
					edge.getScore());

		} else if (lastModified1.compareTo(lastModified2) == 0) {

			Date lastFsModified1 = d.getMetadata().getFsDirModified();
			Date lastFsModified2 = d2.getMetadata().getFsDirModified();

			if (lastFsModified1 != null && lastFsModified2 != null) {

				if (lastFsModified1.after(lastFsModified2)) {
					// prune

					// lucene similarity is not symmetric, but it more resembles
					// containment
					// invert edge
					input.addEdge(d2, d,
							LuceneSimilaritySignal.LUCENE_INVERSE_SIMILARITY,
							edge.getScore());

					// System.out.println("Pruned [directory older] :" +
					// d.getLuceneDocNumber() + "->" + d2.getLuceneDocNumber() +
					// " --- "+ lastModified1 + " vs. " +lastModified2);
				}
			}

		}

	}

}
