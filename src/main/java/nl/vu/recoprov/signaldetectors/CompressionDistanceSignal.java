package nl.vu.recoprov.signaldetectors;
import java.io.*;
import java.util.zip.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
 

import nl.vu.recoprov.abstractclasses.SignalDetector;
import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.baseclasses.DependencyNode;
import nl.vu.recoprov.utils.ConfigurationDefaults;

public class CompressionDistanceSignal extends SignalDetector {

	public final static String COMPRESSION_DISTANCE ="compression-distance";
	
	private Logger logger;

	public CompressionDistanceSignal() {
		logger = LoggerFactory
				.getLogger("nl.vu.recoprov.signaldetectors.CompressionDistanceSignal");
	}

	
	@Override
	public DependencyGraph computeSignal(DependencyGraph input) {
		for (String name1 : input.keySet()) {

			// for each node d1
			DependencyNode d1 = input.get(name1);
			long size1 = computeCompressionSizePerNode(d1);

			for (String name2 : input.keySet()) {

				if (name2.equals(name1))
					continue;

				DependencyNode d2 = input.get(name2);
				long size2 = computeCompressionSizePerNode(d2);
				long size12 = computeCompressionSize(d1, d2);
				long size21 = computeCompressionSize(d2, d1);

				if (size1 != 0 && size2 != 0 && size12!=0 && size21!=0) {
					double score1 =  ((double) size12  - size1)/ (double) size2;
					double score2 =  ((double) size21  - size2)/ (double) size1;
					double score =  1 - ((double) size12  - Math.min(size1,size2))/ (double) Math.max(size1,size2);
					
					if (score1 > score2)
						input.addEdge( d2, d1, COMPRESSION_DISTANCE, score);
					if (score2 > score1)
						input.addEdge(d1, d2, COMPRESSION_DISTANCE, score);

				}
			}

		}
		return input;

	}
	
	private long computeCompressionSize(DependencyNode d1, DependencyNode d2) {


		try {
			FileInputStream file1 = new FileInputStream(d1.getContent());
			FileInputStream file2 = new FileInputStream(d2.getContent());
			SequenceInputStream seq = new SequenceInputStream(file1, file2);
			long result = zipFile(seq, d1.getId()+ "-"+ d2.getId()+ ".gz");
			logger.info("Compression distance computed: " + d1.getId() + "->" + d2.getId());
			return result;

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return 0;

	}
	
	private long computeCompressionSizePerNode(DependencyNode d) {
		FileInputStream fileStream1;

		if (d.getCompressedSize() != 0) {
			return d.getCompressedSize();
		}

		try {
			fileStream1 = new FileInputStream(d.getContent());
			d.setCompressedSize(zipFile(fileStream1, d.getId()+ ".gz"));

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return d.getCompressedSize();

	}
			
	private long zipFile(InputStream fileStream, String name) {
		try {

			FileOutputStream zippedfile = new FileOutputStream(name);
			GZIPOutputStream zip = new GZIPOutputStream(zippedfile);

			// buffer
			int lenght;
			byte[] buffer = new byte[1024];
			while ((lenght = fileStream.read(buffer)) > 0)
				zip.write(buffer, 0, lenght);

			zip.finish();
			zip.close();
			zippedfile.close();
			fileStream.close();

			File tempfile = new File(name);
			long size = tempfile.length();
			tempfile.delete();

			return size;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

}
