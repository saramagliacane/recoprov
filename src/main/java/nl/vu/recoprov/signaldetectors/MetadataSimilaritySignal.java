package nl.vu.recoprov.signaldetectors;

import java.util.HashMap;

import nl.vu.recoprov.abstractclasses.SignalDetector;
import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.baseclasses.DependencyNode;

public class MetadataSimilaritySignal extends SignalDetector {

	public static final String METADATA_SIMILARITY = "-similarity";
	public static final String REVISION_SIMILARITY = "dropbox-path-similarity";
	
	private static final String [] blacklistedMetadata={"dropbox-mimeType", "Content-Type", "Content-Encoding", "xmpTPg:NPages", "dropbox-modified", "dropbox-size", "dropbox-bytes", "producer", "creator", "trapped"};
	
	@Override
	public DependencyGraph computeSignal(DependencyGraph input) {
		for (String name: input.keySet()){
			
			// for each node search if the others have any metadata which is exactly the same
			// TODO: change to approximately similar metadata
			DependencyNode d = input.get(name);
			
			for ( String name2: input.keySet()){
				if(name.equals(name2))
					continue;
			
				DependencyNode d2 = input.get(name2);
				HashMap<String, Double> similarMetadata = d.getMetadata().findSimilarMetadata(d2);
				
				if(!(similarMetadata.isEmpty())){
					for (String value: similarMetadata.keySet()){
						boolean skip = false;
						
						//check BLACK-LIST
						for(int i =0 ; i<blacklistedMetadata.length; i++)
							if (value.equals(blacklistedMetadata[i])) skip = true;

						if (skip) 
							continue;
						
						input.addEdge(d.getLuceneDocNumber(), d2.getLuceneDocNumber(), value + METADATA_SIMILARITY , similarMetadata.get(value));						
					}

				}
			}
				
		}
		
		return input;
	}

}
