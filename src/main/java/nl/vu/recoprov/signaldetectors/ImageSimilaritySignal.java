package nl.vu.recoprov.signaldetectors;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.sanselan.ImageReadException;
import org.apache.sanselan.Sanselan;


import net.semanticmetadata.lire.DocumentBuilder;
import net.semanticmetadata.lire.ImageSearchHits;
import net.semanticmetadata.lire.ImageSearcher;
import net.semanticmetadata.lire.ImageSearcherFactory;
import nl.vu.recoprov.abstractclasses.SignalDetector;
import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.baseclasses.DependencyNode;
import nl.vu.recoprov.utils.ConfigurationDefaults;


public class ImageSimilaritySignal extends SignalDetector {
	public static final String IMAGE_SIMILARITY = "image-similarity";
	public static double IMAGE_THRESHOLD = 0.999;
	@Override
	public DependencyGraph computeSignal(DependencyGraph input) {

		FSDirectory store = null;
		try {
			store = SimpleFSDirectory.open(new File(ConfigurationDefaults.IMAGE_INDEX_DIR));

		IndexReader reader = DirectoryReader.open(store);
		ImageSearcher searcher = ImageSearcherFactory.createDefaultSearcher();

		
		
		for (DependencyNode d: input.values()){
			
			if(d.getMetadata().getImages() == null)
				continue;
			
			for(String imagename: d.getMetadata().getImages()){
				
				BufferedImage bimg = Sanselan.getBufferedImage(new File(imagename));
				
				// required for making tiffs work (todo: download JAI imageIO)
//				BufferedImage bimg = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_RGB);
//			    bimg.createGraphics().drawRenderedImage(img, null);

				
				// searching for an image:
				ImageSearchHits hits = searcher.search(bimg, reader);
				
				
				for (int j = 0; j< hits.length(); j++){
						String originalfilename = hits.doc(j).getField("originalFilename").stringValue();
					
						if (hits.score(j) < IMAGE_THRESHOLD) 
							continue;
		  	        	
		  	        	if(originalfilename.equals(d.getCompleteFilepath()))
		  	        		continue;
		  	        	
		  	        	input.addEdge(d, input.get(originalfilename), IMAGE_SIMILARITY+"_"+imagename+"_"+hits.doc(j).getField(DocumentBuilder.FIELD_NAME_IDENTIFIER).stringValue(), hits.score(j));
		  	        	System.out.println("image-simil based on: "+ imagename + " "+hits.score(j) + ": " + hits.doc(j).getField(DocumentBuilder.FIELD_NAME_IDENTIFIER).stringValue());
  	
		  	        }
				
				
			}
		}
	

		reader.close();
		store.close();
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ImageReadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
				
		return input;
		
	}

}
