package nl.vu.recoprov.signaldetectors;

import java.io.File;
import java.io.IOException;
import org.apache.lucene.analysis.Analyzer;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.vu.recoprov.abstractclasses.SignalDetector;
import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.baseclasses.DependencyNode;
import nl.vu.recoprov.utils.ConfigurationDefaults;
import nl.vu.recoprov.utils.CustomAnalyzer;

public class LuceneSimilaritySignal extends SignalDetector {

	public static final String LUCENE_SIMILARITY = "lucene-similarity";
	public static final String LUCENE_INVERSE_SIMILARITY = LuceneSimilaritySignal.LUCENE_SIMILARITY
			+ "_inverse";

	private String indexDirFilename;
	private static Logger logger;

	public LuceneSimilaritySignal() {
		this(ConfigurationDefaults.INDEX_DIR);
	}

	public LuceneSimilaritySignal(String indexDirFilename) {
		this.indexDirFilename = indexDirFilename;
		logger = LoggerFactory
				.getLogger("nl.vu.recoprov.signaldetectors.LuceneSimilaritySignal");
	}

	public DependencyGraph computeSignal(DependencyGraph input) {

		logger.info("Starting compute signal with index dir: {}",
				indexDirFilename);
		File indexDir = new File(indexDirFilename);

		FSDirectory store;
		IndexReader reader;
		try {
			store = SimpleFSDirectory.open(indexDir);
			reader = DirectoryReader.open(store);
		} catch (IOException e1) {
			logger.error("Lucene Directory failing: {}", indexDir.getName());
			e1.printStackTrace();
			return input;
		}

		IndexSearcher searcher = new IndexSearcher(reader);
		int numdocs = reader.numDocs();

		for (int i = 0; i < numdocs; i++) {
			ScoreDoc[] hits = computeSimilarity( reader,  searcher,  input,  i);
			addEdges(searcher, input, i, hits);
		}
		
		logger.info("Signal computation completed successfully.");

		return input;

	}

	public static ScoreDoc[] searchForString(IndexSearcher searcher,
			StringBuffer queryString) {

		return searchForString("contents", searcher, queryString,
				new CustomAnalyzer(ConfigurationDefaults.LUCENE_VERSION));

	}

	public static ScoreDoc[] searchForString(String fieldname, IndexSearcher searcher,
			StringBuffer queryString, Analyzer analyzer) {

		if (queryString.equals(""))
			return new ScoreDoc[0];

		BooleanQuery.setMaxClauseCount(1000000);
		QueryParser parser = new QueryParser(
				ConfigurationDefaults.LUCENE_VERSION, fieldname, analyzer);
		Query query;
		try {
			query = parser.parse(QueryParser.escape(queryString.toString()));
		} catch (ParseException e) {
			logger.error("Parser exception for queryString: {} ",
					queryString.substring(0, 5));
			e.printStackTrace();
			return new ScoreDoc[0];
		}

		TopScoreDocCollector collector = TopScoreDocCollector.create(
				ConfigurationDefaults.LUCENE_MAX_NUMBER_DOCS, true);
		try {
			searcher.search(query, collector);
		} catch (IOException e) {
			logger.error("Searcher exception for queryString: {} ",
					queryString.substring(0, 100));
			e.printStackTrace();
			return new ScoreDoc[0];
		}
		ScoreDoc[] hits = collector.topDocs().scoreDocs;
		return hits;

	}

	public static ScoreDoc[] searchForPhrase(String fieldname,
			IndexSearcher searcher, String queryString, Analyzer analyzer)
			throws IOException, ParseException {

		if (queryString.equals(""))
			return new ScoreDoc[0];

		QueryParser parser = new QueryParser(
				ConfigurationDefaults.LUCENE_VERSION, fieldname, analyzer);
		Query query = parser.parse(queryString);

		// System.out.println(queryString);

		TopScoreDocCollector collector = TopScoreDocCollector.create(20, true);
		searcher.search(query, collector);
		ScoreDoc[] hits = collector.topDocs().scoreDocs;
		return hits;

	}

	static boolean ignoreTerm(String term) {
		boolean ignore = !(term.length() >= 2 && hasLetters(term));
		// ignore= (term.length()<2 || !hasOnlyLetters(term) ); //best
		// performance
		// if(ignore){
		// System.out.println(term);
		// }

		ignore = false;
		// ignore = term.contains(":"); //more false positive
		return ignore;
	}

	static String modifyTerm(String term) {
		term = term.replace(":", " ");
		// term = term.replace("\\", " ");
		// term = term.replace("{", " ");
		// term = term.replace("}", " ");
		return term;
	}

	static boolean hasOnlyLetters(String str) {
		return str.matches("^[a-zA-Z]+$");
	}

	static boolean hasOnlyNumbers(String str) {
		return str.matches("^[0-9]+$");
	}

	static boolean hasLetters(String str) {
		return str.matches(".*[a-zA-Z]+.*");
	}

	
	public static ScoreDoc[] computeSimilarity(IndexReader reader, IndexSearcher searcher, DependencyGraph input, int i) {
		Terms tfvs;
		ScoreDoc[] hits = new ScoreDoc[0];
		StringBuffer querystring = null;
		try {
			tfvs = reader.getTermVector(i, "contents");
		} catch (IOException e) {
			logger.error("IOException in retrieving term vector for doc {}", i);
			e.printStackTrace();
			return hits;
		}

		if (tfvs == null) {
			logger.error("No term vector for doc {}", i);
		} else {
			logger.trace("LuceneSimilarity: working on doc {} ", i);
			TermsEnum tenum;
			try {
				tenum = tfvs.iterator(null);
			} catch (IOException e) {
				logger.error("No iterator for term vector for doc {}", i);
				e.printStackTrace();
				return hits;
			}

			try {
				while (tenum.next() != null) {

					if (querystring == null) {
						querystring = new StringBuffer(tenum.term()
								.utf8ToString());
					} else {
						querystring.append(" OR ");
						querystring.append(tenum.term().utf8ToString());
					}

				}
			} catch (IOException e) {
				logger.error("Iterator exception for term vector: doc {}", i);
				e.printStackTrace();
				return hits;
			}

			hits = searchForString(searcher, querystring);
		}
		return hits;

		}

		
	private void addEdges(IndexSearcher searcher, DependencyGraph input, int i,
			ScoreDoc[] hits) {
		Document doc;
		try {
			doc = searcher.doc(i);
		} catch (IOException e) {
			logger.error("Searcher exception for doc {}", i);
			e.printStackTrace();
			return;
		}

		String key = doc.getField("name").stringValue();

		for (int j = 0; j < hits.length; j++) {

			// if (hits[j].score <
			// ConfigurationDefaults.LUCENE_THRESHOLD)
			// continue;

			if (hits[j].doc == i){
				continue;
			}
			// d.addDepNodeSimilarity("lucene_similarity", d2,
			// hits[j].score);
			
			double score = hits[j].score/hits[i].score;
			input.addEdge(i, hits[j].doc, LUCENE_SIMILARITY, score);

		}
	}
}
