package nl.vu.recoprov.signaldetectors;

import java.io.File;


import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.SimpleFSDirectory;

import nl.vu.recoprov.abstractclasses.SignalDetector;
import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.baseclasses.RecoMetadata;
import nl.vu.recoprov.utils.ConfigurationDefaults;

public class MatchTitleInContentSignal extends SignalDetector {


	public static final String TITLE_IN_CONTENT_SIMILARITY = "title-in-content-similarity";
	
	@Override
	public DependencyGraph computeSignal(DependencyGraph input) {

		try{

			File indexDir = new File(ConfigurationDefaults.INDEX_DIR);
			//indexDir.mkdir();
			
			FSDirectory store = SimpleFSDirectory.open(indexDir);
			IndexReader reader = DirectoryReader.open(store);
			IndexSearcher searcher = new IndexSearcher(reader);
			int numdocs = reader.numDocs();
			
			for (int i=0; i<numdocs; i++){
			
		                
				Document doc = searcher.doc(i);
//				String key = doc.getFieldable("name").stringValue() ;
//				key = key.substring(key.lastIndexOf("/")+1, key.length());
				
				RecoMetadata metadata = input.get(i).getMetadata();
				if (metadata == null){
					continue;
				}

				String key = metadata.get("title");
				if (key == null || key.trim().isEmpty() || key.equals("untitled"))
					continue;
				
				key = "\""+ metadata.get("title") +"\"";
				//System.out.println("Match title in text : search for "+ key);
				
				//better to have an original index (no strange changes)
				
				ScoreDoc[] hits = null;
				hits = LuceneSimilaritySignal.searchForPhrase("contents", searcher, key, new EnglishAnalyzer(ConfigurationDefaults.LUCENE_VERSION));

				
				
	  	        
	  	        for (int j = 0; j< hits.length; j++){
	  	        	
	  	        	if (hits[j].score < 0.1) 
	  	        		continue;
	  	        	
	  	        	if(hits[j].doc == i)
	  	        		continue;
	  	        	

	  	  	        //d.addDepNodeSimilarity("lucene_similarity", d2, hits[j].score);
	  	        	System.out.println("Match title in text from"+ i + "->" + hits[j]);
	  	  	        input.addEdge(i, hits[j].doc,TITLE_IN_CONTENT_SIMILARITY , hits[j].score);

	  	        	
	  	        }


				
			}
			
			return input;
			
			} catch(Exception e ){
				System.out.println("Compute similarity: Lucene Directory failing \n");
				e.printStackTrace();
				return null;
			}
		}		

	}


