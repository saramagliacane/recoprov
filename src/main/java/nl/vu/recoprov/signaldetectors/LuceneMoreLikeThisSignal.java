package nl.vu.recoprov.signaldetectors;

import java.io.File;
import java.io.IOException;
import org.apache.lucene.analysis.Analyzer;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.PagedBytes.Reader;
import org.apache.lucene.queries.mlt.MoreLikeThis;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.vu.recoprov.abstractclasses.SignalDetector;
import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.baseclasses.DependencyNode;
import nl.vu.recoprov.utils.ConfigurationDefaults;
import nl.vu.recoprov.utils.CustomAnalyzer;

public class LuceneMoreLikeThisSignal extends SignalDetector {

	public static final String LUCENE_MORE_LIKE_THIS_SIMILARITY = "lucene-more-like-this-similarity";
	private Logger logger;
	private String indexDirFilename;

	public LuceneMoreLikeThisSignal() {
		this(ConfigurationDefaults.INDEX_DIR);
	}

	public LuceneMoreLikeThisSignal(String indexDirFilename) {
		this.indexDirFilename = indexDirFilename;
		logger = LoggerFactory
				.getLogger("nl.vu.recoprov.signaldetectors.LuceneMoreLikeThisSignal");
	}

	public DependencyGraph computeSignal(DependencyGraph input) {

		File indexDir = new File(indexDirFilename);
		// indexDir.mkdir();

		FSDirectory store;
		IndexReader reader;
		try {
			store = SimpleFSDirectory.open(indexDir);
			reader = DirectoryReader.open(store);
		} catch (IOException e1) {
			logger.error("Lucene Directory failing: {}", indexDir.getName());
			e1.printStackTrace();
			return input;
		}

		IndexSearcher searcher = new IndexSearcher(reader);
		int numdocs = reader.numDocs();
		MoreLikeThis mlt = new MoreLikeThis(reader);

		for (int i = 0; i < numdocs; i++) {

			Document doc;
			Query query;
			TopDocs hits;
			
			try {
				doc = searcher.doc(i);
				query = mlt.like(i);
				hits = searcher.search(query,
						ConfigurationDefaults.LUCENE_MAX_NUMBER_DOCS);
			} catch (IOException e) {
				logger.error("Search failed for document: {}", i);
				e.printStackTrace();
				continue;
			}
			
			logger.trace("Search succeeded for document: {}", i);

			String key = doc.getField("name").stringValue();
			DependencyNode d = input.get(key);

			for (int j = 0; j < hits.scoreDocs.length; j++) {

				if (hits.scoreDocs[j].doc == i)
					continue;
				
				input.addEdge(i, hits.scoreDocs[j].doc,
						LUCENE_MORE_LIKE_THIS_SIMILARITY,
						hits.scoreDocs[j].score);

			}

		}
		
		logger.info("Signal computation completed successfully.");
		
		return input;

	}

}
