package nl.vu.recoprov.signaldetectors;


//import  name.fraser.neil.plaintext.diff_match_patch;
//import name.fraser.neil.plaintext.diff_match_patch.Diff;
//import name.fraser.neil.plaintext.diff_match_patch.Patch;
//
//import java.io.BufferedReader;
//import java.io.FileReader;
//import java.io.IOException;
//import java.io.StringReader;
//import java.util.LinkedList;
//import java.util.List;
//
import nl.vu.recoprov.abstractclasses.SignalDetector;
import nl.vu.recoprov.baseclasses.DependencyGraph;
//import nl.vu.recoprov.baseclasses.DependencyNode;


public class DiffSignal extends SignalDetector {
	
//		public static final String DIFF_SIMILARITY = "diff-similarity";
//		public static double DIFF_THRESHOLD = 0.000001;
//	  	private diff_match_patch dmp;
//
//
//	  protected void setUp() {
//	    // Create an instance of the diff_match_patch object.
//	    dmp = new diff_match_patch();
//	  }
	

	@Override
	public DependencyGraph computeSignal(DependencyGraph input) {
		
//		setUp();
//		
//		for (DependencyNode d: input.values()){
//			for(DependencyNode d2: input.values()){
//				if(d.getCompleteFilepath().equals(d2.getCompleteFilepath()))
//						continue;
//				
//
//				
////				List<String> original = fileToLines(d.getContent());
////	            List<String> revised  = fileToLines(d2.getContent());
//	            
//	            //TODO change content now is now only first 3 lines
//				LinkedList<Diff> diffs = dmp.diff_main(d.getContent(), d2.getContent(), false);
//
//				
//
//				//computationally trivial operations
//				dmp.diff_cleanupEfficiency(diffs);
//				dmp.diff_cleanupMerge(diffs);
//				// Slide diffs to match logical boundaries.
//				dmp.diff_cleanupSemanticLossless(diffs);
//				// Cleanup semantically trivial equalities.
//				dmp.diff_cleanupSemantic(diffs);
//				
//								
//				LinkedList<Diff> adiffs = new LinkedList<Diff> ();
//				//application specific trimming 
//			    for (Diff aDiff : diffs) {
//		        	  
//		        	aDiff.text = aDiff.text.trim();  
//		        	
//		  	        if(aDiff.text.length()<5)
//		  	        	continue;
//		  	        adiffs.add(aDiff);
//	
//			    }
//				
//				dmp.diff_cleanupMerge(adiffs);
//			    
//				double score = getScore(adiffs, d.getContent().length(), d2.getContent().length());
//				if(score>DIFF_THRESHOLD){
//					//System.out.println(d.getLuceneDocNumber() + " ->" +d2.getLuceneDocNumber() + " :"  + score);
//					input.addEdge(d, d2, DIFF_SIMILARITY, score);
//				}
//				
//				
//				
////				LinkedList<Patch> patches = dmp.patch_make(diffs);
////				
//
////                for (Diff diff: diffs) {
////                	if(diff.operation.equals(diff_match_patch.Operation.EQUAL)){
////                		
////                		diff.text = diff.text.trim(); 
////                		if(diff.text.length()<5)
////         	  	        	continue;
////                		//System.out.println(d.getLuceneDocNumber() + " ->" +d2.getLuceneDocNumber() + " :"  + diff);
////                	}
////
////                        	
////                }
//                
////                for (Patch patch: patches) {
////                	System.out.println(d.getLuceneDocNumber() + " ->" +d2.getLuceneDocNumber() + " :"  + patch);
////                        	
////                }
//
//
//
//                
////                assertEquals("diff_commonOverlap: Overlap.", 3, dmp.diff_commonOverlap("123456xxx", "xxxabcd"));
//			}
//		}
//		
		return input;
	}
	

//	 
//	
//	        // Helper method for get the file content
//	        private static List<String> fileToLines(String content) {
//	                List<String> lines = new LinkedList<String>();
//	                String line = "";
//	                try {
//	                        BufferedReader in = new BufferedReader(new StringReader(content));
//	                        while ((line = in.readLine()) != null) {
//	                                lines.add(line);
//	                        }
//	                } catch (IOException e) {
//	                        e.printStackTrace();
//	                }
//	                return lines;
//	        }
//
//
//	        
//
//	        public double getScore(LinkedList<Diff> diffs, double size1, double size2) {
//	          double levenshtein = 0;
//	          int insertions = 0;
//	          int deletions = 0;
//	          int equals = 0;
//	          for (Diff aDiff : diffs) {
//	        	  
//	        	aDiff.text = aDiff.text.trim();  
//	        	
//	  	        if(aDiff.text.length()<5)
//	  	        	continue;
//
//	  	        
//	            switch (aDiff.operation) {
//	            case INSERT:
//	              insertions += aDiff.text.length();
//	              break;
//	            case DELETE:
//	              deletions += aDiff.text.length();
//	              break;
//	            case EQUAL:
//	              equals += aDiff.text.length();
//	              // A deletion and an insertion is one substitution.
//	              levenshtein += Math.max(insertions, deletions);
//	              insertions = 0;
//	              deletions = 0;
//	              break;
//	            }
//	          }
//	          levenshtein += Math.max(insertions, deletions);
//	          
//	          double minsize = Math.min(size1, size2);
//	          
//	          double levenshteinDistance = 1/(1+levenshtein);
//	          double equalityDistance = equals/minsize;
//
//	          
//	          double score = equalityDistance * levenshteinDistance;
//	          
//	          if(Double.isNaN(score) || Double.isInfinite(score))
//	        	  score = 0.0;
//	          
//	          return score;
//	        }
//	        

	        
}
