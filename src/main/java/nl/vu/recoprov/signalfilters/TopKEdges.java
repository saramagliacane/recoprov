package nl.vu.recoprov.signalfilters;

import java.util.ArrayList;
import java.util.Collections;
import nl.vu.recoprov.abstractclasses.SignalFilterer;
import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.baseclasses.EdgeScoreComparator;
import nl.vu.recoprov.baseclasses.LabelledEdge;
import nl.vu.recoprov.baseclasses.DependencyNode;
import nl.vu.recoprov.signalaggregators.WeightedSumAggregator;


public class TopKEdges extends SignalFilterer {

	public static int K = 10;
	public static String label = WeightedSumAggregator.FINAL_SCORE;

	public TopKEdges(String label) {
		this.label = label;
	}

	public TopKEdges(String label, int K) {
		this.label = label;
		this.K = K;
	}

	public TopKEdges() {}

	@Override
	public DependencyGraph filterNode(DependencyGraph input, DependencyNode d) {
		if (d == null) {
			System.out.println("No node with name " + d);
			return input;
		}

		// get all its outcoming edges
		ArrayList<LabelledEdge> edgearray = input.getAllEdges(d
				.getLuceneDocNumber(), label);

		if (edgearray == null)
			return input;

		Collections.sort(edgearray, new EdgeScoreComparator());

		int count = 0;

		// for each of its outcoming edges
		for (LabelledEdge edge : edgearray) {

			if (count >= K) {
				break;
			}
			input.removeEdge(d.getId(), edge.getId(), edge.getLabel());
			count++;

		}

		return input;
	}

}
