package nl.vu.recoprov.signalfilters;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Terms;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.vu.recoprov.LuceneIndexer;
import nl.vu.recoprov.SentenceSplitter;
import nl.vu.recoprov.abstractclasses.SignalFilterer;
import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.baseclasses.LabelledEdge;
import nl.vu.recoprov.baseclasses.DependencyNode;
import nl.vu.recoprov.baseclasses.RecoMetadata;
import nl.vu.recoprov.signaldetectors.CompressionDistanceSignal;
import nl.vu.recoprov.signaldetectors.LuceneSimilaritySignal;
import nl.vu.recoprov.utils.ConfigurationDefaults;

public class HomogeneityFilter extends SignalFilterer {

	private static final float THRESHOLD = 0.1f;
	private Logger logger;
	
	public HomogeneityFilter(){
		logger = LoggerFactory
				.getLogger("nl.vu.recoprov.signalfilters.HomogeneityFilter");
	}
	
	@Override
	public DependencyGraph filterNode(DependencyGraph input, DependencyNode d) {
		// for each outgoing edge and relative node
		
		if (d == null) {
			System.out.println("No node with name " + d);
			return input;
		}

		// get all its outcoming edges
		ArrayList<LabelledEdge> edgearray = input.getAllEdges(d
				.getLuceneDocNumber());

		if (edgearray == null)
			return input;
		
		IndexReader reader = LuceneIndexer.createIndexReader(SentenceSplitter.SENTENCE_DIRECTORY
				+ ConfigurationDefaults.RELATIVE_INDEX_DIR);
		IndexSearcher searcher = new IndexSearcher(reader);

		// for each of its outcoming edges
		for (LabelledEdge edge : edgearray) {
			int value = compareTwoNodes(input, d, input.get(edge.getId()), reader, searcher);
			if (value == 1){
				logger.info("Removing edge {} -> {}", d.getId(), edge.getId());
				input.removeEdge(d.getId(), edge.getId());
			}
			if (value == -1){
				logger.info("Removing edge {} -> {}", edge.getId(), d.getId());
				input.removeEdge(edge.getId(), d.getId());
			}
		}
		
		
		
		return input;
	}

	private int compareTwoNodes(DependencyGraph input, DependencyNode d1,
			DependencyNode d2, IndexReader reader, IndexSearcher searcher) {
		
		ArrayList<Integer> luceneIdSentences1 = d1.getLuceneIdSentences();
		ArrayList<Integer> luceneIdSentences2 = d2.getLuceneIdSentences();
		double score1 = 0.0;
		double score2 = 0.0;
		

		// for each sentence that is very similar in both docs
		for (Integer i : luceneIdSentences1) {
			ScoreDoc[] hits = LuceneSimilaritySignal.computeSimilarity( reader,  searcher,  input,  i);
			for (ScoreDoc scoredoc: hits){
				
				if(scoredoc.doc == i){
					continue;
				}
				
				if(scoredoc.score < THRESHOLD){
					// they are ordered, so others will be even worse
					break;
				}
				
				// check how many of the sentences are similar in the first
				if(luceneIdSentences1.contains(scoredoc.doc)){
					score1 += scoredoc.score;
				}
				// and how many are similar in the second
				if(luceneIdSentences2.contains(scoredoc.doc)){
					score2 += scoredoc.score;
				}
			}
		}
		
		//normalize by the number of sentences
		score1 =  score1/(luceneIdSentences1.size()+1);
		score2 =  score1/(luceneIdSentences2.size()+1);
		
		logger.info("Comparing two nodes: {} -> {}; scores: {} {}",
				d1.getId(), d2.getId(), score1, score2);
		
		if (score1 > score2){
			return +1;
		}
		
		if (score1 < score2){
			return -1;
		}
		
		return 0;

	}
	

}