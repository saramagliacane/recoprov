package nl.vu.recoprov.signalfilters;

import java.util.ArrayList;
import java.util.Date;
import nl.vu.recoprov.abstractclasses.SignalFilterer;
import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.baseclasses.LabelledEdge;
import nl.vu.recoprov.baseclasses.DependencyNode;
import nl.vu.recoprov.baseclasses.RecoMetadata;
import nl.vu.recoprov.signaldetectors.LuceneMoreLikeThisSignal;
import nl.vu.recoprov.signaldetectors.LuceneSimilaritySignal;
import nl.vu.recoprov.utils.ConfigurationDefaults;

public class LuceneThresholdFilter extends SignalFilterer {

	@Override
	public DependencyGraph filterNode(DependencyGraph input, DependencyNode d) {
		if (d == null) {
			System.out.println("No node with name " + d);
			return input;
		}

		// get all its outcoming edges
		ArrayList<LabelledEdge> edgearray = input.getAllEdges(d
				.getLuceneDocNumber());

		if (edgearray == null)
			return input;

		// for each of its outcoming edges
		for (LabelledEdge edge : edgearray) {

			// consider only text similarity edges
			if (!edge.getLabel().equals(
					LuceneSimilaritySignal.LUCENE_SIMILARITY)
					&& !edge.getLabel().equals(
							LuceneSimilaritySignal.LUCENE_INVERSE_SIMILARITY)
					&& !edge.getLabel()
							.equals(LuceneMoreLikeThisSignal.LUCENE_MORE_LIKE_THIS_SIMILARITY)) {
				continue;
			}

			// remove the ones below the threshold
			if (edge.getScore() < ConfigurationDefaults.LUCENE_THRESHOLD) {
				// remove all edges
				input.removeEdge(d.getId(), edge.getId(), edge.getLabel());
			}

		}

		return input;
	}

}
