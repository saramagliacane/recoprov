package nl.vu.recoprov.signalfilters;

import java.util.ArrayList;
import java.util.Date;
import nl.vu.recoprov.abstractclasses.SignalFilterer;
import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.baseclasses.LabelledEdge;
import nl.vu.recoprov.baseclasses.DependencyNode;
import nl.vu.recoprov.baseclasses.RecoMetadata;

public class BackwardTemporalFilter extends SignalFilterer {

	@Override
	public DependencyGraph filterNode(DependencyGraph input, DependencyNode d) {

		if (d == null) {
			System.out.println("No node with name " + d);
			return input;
		}

		ArrayList<LabelledEdge> edgearray = 
				input.getAllEdges(d.getLuceneDocNumber());

		if (edgearray == null)
			return input;

		for (LabelledEdge edge : edgearray) {

			DependencyNode d2 = input.get(edge.getId());

			if (d2 == null) {
				System.out.println("No translation for " + edge.getId());
				continue;
			}

			int isFirstYounger = compareModifiedDate(d, d2);

			if (isFirstYounger == 1) {
				input.removeEdge(d, d2);
			}

			if (isFirstYounger == -1) {
				input.removeEdge(d2, d);
			}

		}

		return input;
	}


	/**
	 * Compares several of the dates of two DependencyNodes
	 * @param d
	 * @param d2
	 * @return 	0 if they are equal
	 * +1 if the first is younger
	 * -1 if the second is younger
	 */
	public int compareModifiedDate(DependencyNode d, DependencyNode d2) {

		RecoMetadata metadata = d.getMetadata();
		RecoMetadata metadata2 = d2.getMetadata();
		
		Date lastModified1 = metadata.getModified();
		Date created1 = metadata.getCreationDate();

		Date lastModified2 = metadata2.getModified();
		Date created2 = metadata2.getCreationDate();

		if (created1 != null && created2 != null) {
			if (created1.after(created2)) {
				return +1;
			}

			if (created2.after(created1)) {
				return -1;
			}
		}

		if (lastModified1 != null && lastModified2 != null) {
			// if the node in which the arrow is generated is younger than the
			// other
			if (lastModified1.after(lastModified2)) {
				// prune

				// System.out.println("Pruned :" + d.getLuceneDocNumber() + "->"
				// +
				// d2.getLuceneDocNumber() + " --- "+ lastModified1 + " vs. "
				// +lastModified2);

				return +1;
			}

			if (lastModified2.after(lastModified1)) {
				return -1;
			}
		}

		// else (lastModified1.compareTo(lastModified2) == 0)
		// check the filesystem last modified date

		Date lastFsModified1 = metadata.getFsDirModified();
		Date lastFsModified2 = metadata2.getFsDirModified();

		if (lastFsModified1 != null && lastFsModified2 != null) {

			if (lastFsModified1.after(lastFsModified2)) {
				// System.out.println("Pruned [directory older] :" +
				// d.getLuceneDocNumber() + "->" + d2.getLuceneDocNumber() +
				// " --- "+ lastModified1 + " vs. " +lastModified2);
				return +1;
			}

			if (lastFsModified2.after(lastFsModified1)) {
				return -1;
			}
		}

		String stringrev1 = metadata.getRevision();
		String stringrev2 = metadata2.getRevision();

		if (stringrev1 != null && stringrev2 != null) {
			long rev1 = Long.parseLong(stringrev1, 16);
			long rev2 = Long.parseLong(stringrev2, 16);

			if (rev1 > rev2) {
				return +1;
			}

			if (rev2 > rev1) {
				return -1;
			}
		}

		return 0;

	}

}
