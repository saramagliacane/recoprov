package nl.vu.recoprov.signalfilters;

import java.util.ArrayList;
import nl.vu.recoprov.abstractclasses.SignalFilterer;
import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.baseclasses.LabelledEdge;
import nl.vu.recoprov.baseclasses.DependencyNode;


public class PlagiarismCorpusSpecificFilter extends SignalFilterer {

	@Override
	public DependencyGraph filterNode(DependencyGraph input, DependencyNode d) {
			
			// get all its outcoming edges
			ArrayList<LabelledEdge> edgearray = input.getAllEdges(d
					.getLuceneDocNumber());

			if (edgearray == null)
				return input;

			// for each of its outcoming edges
			for (LabelledEdge edge : edgearray) {

				// take the endpoint of the edge
				DependencyNode d2 = input.get(edge.getId());

				if (d2 == null) {
					System.out.println("No translation for " + edge.getId());
					continue;
				}
				
				if (d.getCompleteFilepath().contains("suspicious")){
					if (d2.getCompleteFilepath().contains("suspicious")){
						input.removeEdge(d, d2);
					}
				}
				
				if (d.getCompleteFilepath().contains("source")){
						input.removeEdge(d, d2);
				}
	
			}

			return input;
	}

	
}