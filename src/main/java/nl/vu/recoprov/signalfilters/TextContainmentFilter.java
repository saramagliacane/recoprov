package nl.vu.recoprov.signalfilters;

import java.util.ArrayList;
import java.util.Date;
import nl.vu.recoprov.abstractclasses.SignalFilterer;
import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.baseclasses.LabelledEdge;
import nl.vu.recoprov.baseclasses.DependencyNode;
import nl.vu.recoprov.baseclasses.RecoMetadata;
import nl.vu.recoprov.signaldetectors.CompressionDistanceSignal;
import nl.vu.recoprov.signaldetectors.LuceneSimilaritySignal;

public class TextContainmentFilter extends SignalFilterer {

	@Override
	public DependencyGraph filterSignals(DependencyGraph input) {
		input  = removeCycles(input, CompressionDistanceSignal.COMPRESSION_DISTANCE);
		return removeCycles(input,LuceneSimilaritySignal.LUCENE_SIMILARITY);
	}

	private DependencyGraph removeCycles(DependencyGraph input, String label) {
	
		for (String name : input.keySet()) {

			// for each node filter 
			DependencyNode d = input.get(name);

			if (d == null) {
				System.out.println("No node with name " + d);
				continue;
			}
		
			
			// get all its outcoming edges
			ArrayList<LabelledEdge> edgearray = input.getAllEdges(d
					.getLuceneDocNumber());

			if (edgearray == null)
				continue;

			// for each of its outcoming edges
			for (LabelledEdge edge : edgearray) {

				// consider only text similarity edges
				if (! edge.getLabel().equals(label)){
					continue;
				}
				
				// take the endpoint of the edge
				DependencyNode d2 = input.get(edge.getId());

				if (d2 == null) {
					System.out.println("No translation for " + edge.getId());
					continue;
				}
				
				// check if there are the outcoming edges from this endpoint back to the previous node
				ArrayList<LabelledEdge> backedges = input.getAllEdges(d2.getLuceneDocNumber(), d.getLuceneDocNumber(), label);
				
				// if there aren't, everything is fine
				if (backedges == null){
					continue;
				}
				
				// else we have a cycle in the graph, that we have to resolve
				double backScore = 0;
				
				// in case there are multiple edges between the nodes
				// take the one with the max score
				for (LabelledEdge backedge: backedges){
					
					
					double tempscore = backedge.getScore();
					if (tempscore > backScore){
						backScore = tempscore;
					}
				}
				
				// check which of the two directions has the bigger score
				// and remove the edges in the other direction
				if (backScore > edge.getScore()){
					// remove all edges
					input.removeEdge(d, d2);
				}
				else{
					// remove all edges
					input.removeEdge(d2, d);
				}
				


			}

		}

		return input;
	}
}