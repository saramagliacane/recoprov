package nl.vu.recoprov;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.sql.Date;

import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.baseclasses.DependencyNode;
import nl.vu.recoprov.utils.ConfigurationDefaults;

import org.apache.tika.Tika;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.sax.BodyContentHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.ContentHandler;


public class TikaReader {

	private String currentDir;
	private Tika tika = new Tika();
	private AutoDetectParser parser = new AutoDetectParser();
	private ParseContext context = new ParseContext();
	private String contentdirname = ConfigurationDefaults.CONTENTDIR;
	private Logger logger;

	public TikaReader(String dir) {
		currentDir = dir;
		tika.setMaxStringLength(-1);
		context.set(Parser.class, parser);
		logger = LoggerFactory
				.getLogger("nl.vu.recoprov.TikaReader");
	}

	public DependencyGraph read(DependencyGraph depNodeMap, String[] params) {
		return read(currentDir, depNodeMap, params);
	}

	public DependencyGraph read(DependencyGraph depNodeMap) {
		return read(currentDir, depNodeMap, null);
	}

	public DependencyGraph read(String dirname, DependencyGraph depNodeMap,
			String[] params) {

		// make content dir - where you store the contents of the files
		File contentdir = new File(contentdirname);
		if (!contentdir.exists())
			contentdir.mkdir();
		
		File dir = new File(dirname);

		for (String filename : dir.list()) {
			try {

				if (ConfigurationDefaults.ignoreFile(filename)) {
					continue;
				}

				if (ConfigurationDefaults.isInBlackList(filename, params)) {
					continue;
				}

				File f = new File(dir + "/" + filename);

				if (f.isDirectory()) {
					read(f.getAbsolutePath(), depNodeMap, params);
					continue;
				}

				FileInputStream fin = new FileInputStream(f);

				String mimeType = tika.detect(fin);

				Metadata metadata = new Metadata();
				metadata.set(Metadata.CONTENT_TYPE, mimeType);

				// if(false && mimeType.contains("zip")){
				// fin.close();
				// TikaInputStream tin = TikaInputStream.get(f, metadata);
				//
				// Matcher MATCHER =
				// (new XPathParser("xhtml",
				// XHTMLContentHandler.XHTML)).parse("/descendant::node()");
				//
				// MatchingContentHandler h = new MatchingContentHandler(new
				// WriteOutContentHandler(-1),MATCHER );
				//
				//
				// AutoDetectParser parser = new AutoDetectParser();
				// ParseContext context = new ParseContext();
				// context.set(Parser.class, parser);
				//
				//
				//
				//
				// parser.parse(tin, h, metadata, context);
				// content = h.toString();
				//
				// //System.out.println(f.getAbsolutePath());
				// //System.out.println(content);
				//
				//
				//
				// tin.close();
				// }
				//

				fin.close();
				TikaInputStream tin = TikaInputStream.get(f, metadata);
				//ByteArrayOutputStream outputstream = new ByteArrayOutputStream();
				ContentHandler textHandler = new BodyContentHandler(-1);

				parser.parse(tin, textHandler, metadata, context);

				tin.close();

				String filepath = f.getAbsolutePath();
				DependencyNode d = depNodeMap.get(filepath);

				if (d == null) {
					logger.error("DependencyNode not found: {}", filepath);
					continue;
				}
				else{
					logger.trace("Processing dependency node: {}", filepath);
				}
				
				String contentFilename = filepath;

				// if they are not only txt files, copy text content
				if (!mimeType.contains("text/plain")){
				
					File contentFile = new File(contentdirname, filename +".txt");
					contentFilename = contentFile.getAbsolutePath();
					FileWriter out = new FileWriter(contentFile);		
					out.write(textHandler.toString());
					out.close();
				}
				
				//otherwise refer to the files itself
				d.setContent(contentFilename);		
				
				metadata.add(Metadata.CONTENT_TYPE, mimeType);
				d.setMetadata(metadata);
				d.getMetadata().setFSModified(new Date(f.lastModified()));
				d.getMetadata().setFSSize(f.length());
				
				

				if (f.getParentFile().isDirectory()) {
					d.getMetadata().setFsDirModified(
							new Date(f.getParentFile().lastModified()));
				}

				//depNodeMap.put(filepath, d);
				//
				// System.out.println("-----------------------------------------------------");
				// System.out.println(filename);
				// System.out.println(content);
				// System.out.println(metadata);
				//
				// listAvailableMetaDataFields(metadata);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		return depNodeMap;

	}

//	private static void listAvailableMetaDataFields(Metadata metadata) {
//		for (int i = 0; i < metadata.names().length; i++) {
//			String name = metadata.names()[i];
//			System.out.println(name + " : " + metadata.get(name));
//		}
//	}

}
