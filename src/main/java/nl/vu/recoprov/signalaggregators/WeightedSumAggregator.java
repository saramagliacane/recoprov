package nl.vu.recoprov.signalaggregators;

import java.util.ArrayList;
import java.util.HashMap;
import nl.vu.recoprov.abstractclasses.SignalAggregator;
import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.baseclasses.DependencyNode;
import nl.vu.recoprov.baseclasses.LabelledEdge;

public class WeightedSumAggregator extends SignalAggregator {

	public static final String FINAL_SCORE = "aggregated-score";

	@Override
	public DependencyGraph aggregateSignals(DependencyGraph input) {
		for (String name : input.keySet()) {

			// for each node merge the edges
			DependencyNode d = input.get(name);

			HashMap<Integer, HashMap<Integer, LabelledEdge>> addEdgesArray = new HashMap<Integer, HashMap<Integer, LabelledEdge>>();
			ArrayList<LabelledEdge> edgearray = input.getAllEdges(d
					.getLuceneDocNumber());

			if (edgearray == null)
				continue;

			for (LabelledEdge edge : edgearray) {
				DependencyNode d2 = input.get(edge.getId());
				ArrayList<LabelledEdge> edgearrayBetweenTheTwoNodes = input
						.getAllEdges(d.getLuceneDocNumber(),
								d2.getLuceneDocNumber());
				Double finalScore = 0.0;
				Integer count = 0;
				for (LabelledEdge edgeBetweenNodes : edgearrayBetweenTheTwoNodes) {
					if (edgeBetweenNodes.getLabel().equals(FINAL_SCORE)) {
						count = 0;
						break;
					}
					finalScore += edgeBetweenNodes.getScore();
					count++;
				}
				if (count == 0)
					continue;
				else {
					LabelledEdge newedge = new LabelledEdge(
							edge.getId(), FINAL_SCORE, finalScore / count);

					HashMap<Integer, LabelledEdge> temp = addEdgesArray.get(d
							.getLuceneDocNumber());

					if (temp == null)
						temp = new HashMap<Integer, LabelledEdge>();

					temp.put(d2.getLuceneDocNumber(), newedge);
					addEdgesArray.put(d.getLuceneDocNumber(), temp);
				}

			}

			for (Integer i : addEdgesArray.keySet()) {

				for (Integer i2 : addEdgesArray.get(i).keySet()) {
					input.addEdge(i, addEdgesArray.get(i).get(i2));

				}
			}

		}

		return input;
	}

}
