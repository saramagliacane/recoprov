package nl.vu.recoprov;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;


import org.apache.sanselan.*;
import org.apache.sanselan.common.IImageMetadata;

import net.semanticmetadata.lire.DocumentBuilder;
import net.semanticmetadata.lire.DocumentBuilderFactory;
import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.baseclasses.DependencyNode;
import nl.vu.recoprov.utils.ConfigurationDefaults;


import org.apache.commons.io.FileUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.common.PDMetadata;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObject;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectForm;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;


public class ImageReader {
	
	static  int counter = 0;
	static String directoryname;


	
	public static DependencyGraph  read(String dirname,  DependencyGraph depNodeMap ){
		File dir = new File(dirname);
		directoryname = dirname;
		
		File imageDir = new File(ConfigurationDefaults.IMAGE_DIRECTORY);
		imageDir.mkdir();
		
		return read(dir, depNodeMap);
	}

	public static DependencyGraph  read(File dir, DependencyGraph input ){

		DocumentBuilder docbuilder = DocumentBuilderFactory.getDefaultDocumentBuilder();
		
		File indexDir = new File(ConfigurationDefaults.IMAGE_INDEX_DIR);
		//IMAGE_INDEX_DIR = indexDir.getAbsolutePath();

		
		if (!indexDir.exists()) {
			indexDir.mkdir();
		}	
		else {
			try {
				FileUtils.deleteDirectory(indexDir);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		FSDirectory store = null;
		try {
			store = SimpleFSDirectory.open(indexDir);
			store.clearLock(ConfigurationDefaults.IMAGE_INDEX_DIR);
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		Analyzer analyzer = new EnglishAnalyzer(ConfigurationDefaults.LUCENE_VERSION);
		IndexWriterConfig config = new IndexWriterConfig(ConfigurationDefaults.LUCENE_VERSION, analyzer );
		
		IndexWriter writer = null;
		try {
			writer = new IndexWriter(store, config);
		} catch (CorruptIndexException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (LockObtainFailedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		

		for (String depname: input.keySet()){
			try {
      		
				DependencyNode d = input.get(depname);
				
				
				if (ConfigurationDefaults.ignoreFile(depname)){
					continue;
				}

				
				if(d.getMimeType().contains("image")){
					
					File imgfile = new File(d.getCompleteFilepath());
					
					BufferedImage img = Sanselan.getBufferedImage(imgfile);
					IImageMetadata metadata = Sanselan.getMetadata(imgfile);
										
					//TODO: check if still problems with tiff also with Sanselan
					
//					// trying to get the tiffs to work
//					BufferedImage bimg = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_RGB);
//				    bimg.createGraphics().drawRenderedImage(img, null);
					
					
					if(img == null){
						continue;
						
//						img = findSubstituteImage(d.getCompleteFilepath().replace("tiff", "pdf"));
					}
					
					
					d = addImageToIndex(d, img, metadata, docbuilder, writer);
					input.put(depname, d);
					
				}
	    		
				if (d.getMimeType().contains("pdf")) {
					input = readImagesFromPDF(input, d, depname, docbuilder,
							writer);
				}
	      	

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
      	
		}
		
		try {
			writer.close();
			store.close();
		} catch (CorruptIndexException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	

		counter = 0;
		return input;
	}

	public static DependencyGraph readImagesFromPDF(DependencyGraph input, DependencyNode d, String depname, DocumentBuilder docbuilder, IndexWriter writer) throws IOException{
		File f = new File(d.getCompleteFilepath());
		PDDocument document = null;
		try{
			document = PDDocument.load(f);
		}catch(IOException e){
				
			return input;
		}
		List<PDPage> pages = document.getDocumentCatalog().getAllPages();

		
		for (PDPage page: pages){
			PDResources resources = page.getResources();
			input = elaborateResources( resources,  input,  d,  depname,  docbuilder,  writer);
			
			
		}
		
		document.close();
		
		return input;
	}
	
	public static DependencyGraph elaborateResources(PDResources resources, DependencyGraph input, DependencyNode d, String depname, DocumentBuilder docbuilder, IndexWriter writer) throws IOException{
		
		//has to be recursive (images inside resources)
		
		//System.out.println( "Resources: "+ d.getCompleteFilepath() + resources);
		
		//System.out.println( "Resource Properties: "+ d.getCompleteFilepath() + resources.getProperties());
		
		Map<String,PDXObjectImage> images = resources.getImages();
		if( images !=null && !images.isEmpty() ){
			
			for (String key: images.keySet()){

				PDXObjectImage image = images.get(key);
				PDMetadata metadata = image.getMetadata();
				//System.out.println( "Image: "+ d.getCompleteFilepath() + image);
				
				BufferedImage imagebuf = null;
				
				try{
					imagebuf = image.getRGBImage();
				}catch (Throwable e){
					e.printStackTrace();
					continue;
				}
				
				if(imagebuf != null){
					d = addImageToIndex(d, imagebuf, metadata, docbuilder, writer);
					input.put(depname, d);
				}
				

			}

			images.clear();
			
		 }
		
		Map<String, PDXObject> xobjects = resources.getXObjects();
		for (String key: xobjects.keySet()){
			PDXObject xobject = xobjects.get(key);
			//System.out.println( "Objects: "+ d.getCompleteFilepath() + xobject.getClass());
			
			if( xobject instanceof PDXObjectForm){
				PDXObjectForm form = (PDXObjectForm) xobject;
				input = elaborateResources( form.getResources(),  input,  d,  depname,  docbuilder,  writer);
	
				
			}
			


		}
		
		//System.out.println( "ResourceGraphicStates: "+ d.getCompleteFilepath() + resources.getGraphicsStates());
		
		
		return input;
		
	}
	

	

	public static DependencyNode addImageToIndex(DependencyNode d, BufferedImage image, Object metadata, DocumentBuilder docbuilder, IndexWriter writer) throws IOException{
		
		if (isImageWhite (image)){
			return d;
		}
		
		String name = ConfigurationDefaults.IMAGE_DIRECTORY + counter++ + ".png" ;
		System.out.println( "Adding image: " + name  +  " " + d.getCompleteFilepath() );
		File newfile = new File(name );
		//image.write2file( newfile );
		try {
			Sanselan.writeImage(image, newfile, ImageFormat.IMAGE_FORMAT_PNG, null);
		} catch (ImageWriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Document doc = docbuilder.createDocument(image, name);
		doc.add(new StringField("name", name, Field.Store.YES));
		doc.add(new StringField("originalFilename", d.getCompleteFilepath() , Field.Store.YES));
		writer.addDocument(doc);
		
		image.flush();
		image = null;

		d.getMetadata().addImages(name, metadata);
		return d;
	}
	
	
	/**
	 * Check if the image is empty (completely white), 
	 * so we don't add it to the index.
	 * @param img
	 * @return true = image is completely white
	 */
	public static Boolean isImageWhite(BufferedImage img){
		int width = img.getWidth()-1;
		int height = img.getHeight()-1;
		Boolean white = true;
		int whiteColor = Color.WHITE.getRGB();
		
		for (int i = 0; i < width; i++){
			for (int j = 0; j < height; j++){
				int rgb = 0;
				try{
					rgb = img.getRGB(i,j);
				}catch(Exception e){
					continue;
				}
				
				
				if (rgb != whiteColor)
					white = false;
			}
		}
		
		return white;
	}
}

