package nl.vu.recoprov;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.baseclasses.DependencyNode;
import nl.vu.recoprov.utils.ConfigurationDefaults;
import nl.vu.recoprov.utils.CustomAnalyzer;
import nl.vu.recoprov.utils.CustomFileReader;
import org.apache.commons.io.FileUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import com.dropbox.client2.exception.DropboxException;


/**
 * Create the Lucene index for the files in the DependencyGraph.
 * @author saramagliacane
 *
 */

public class LuceneIndexer {

	private static Logger logger;
	private Boolean cleanupIndex = false;
	private String indexParentDir = "";
	private SentenceSplitter sentenceSplitter = new SentenceSplitter();
	
	public LuceneIndexer() {
		this("");
	}
	
	public LuceneIndexer( String indexParentDir){
		this.indexParentDir = indexParentDir;
		logger = LoggerFactory
				.getLogger("nl.vu.recoprov.signaldetector.LuceneIndexer");
	}

	
	public void indexFiles(DependencyGraph input) throws IOException {

		File indexDir;

		if (indexParentDir.equals(""))
			indexDir = new File(ConfigurationDefaults.RELATIVE_INDEX_DIR);
		else
			indexDir = new File(indexParentDir,
					ConfigurationDefaults.RELATIVE_INDEX_DIR);

		if (!indexDir.exists()) {
			// if index directory doesn't exist, create the index
			createIndex(input);
		} 
		else {
			// the index directory exists, do we want to remove it?
			if (cleanupIndex) {
				try {
					FileUtils.deleteDirectory(indexDir);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		// now the index is created we have the lucene docid that we can assign
		// to the nodes
		assignLuceneNumbers(input);

	}


			
	private void createIndex(DependencyGraph input) throws IOException {
		
		int count = 0;

		File indexDir;

		if (indexParentDir.equals(""))
			indexDir = new File(ConfigurationDefaults.RELATIVE_INDEX_DIR);
		else
			indexDir = new File(indexParentDir,
					ConfigurationDefaults.RELATIVE_INDEX_DIR);

		ConfigurationDefaults.INDEX_DIR = indexDir.getAbsolutePath();
		
		FSDirectory store = SimpleFSDirectory.open(indexDir);
		IndexWriter writer = createIndexWriter(store);
		

		for (String name : input.keySet()) {

			if (ConfigurationDefaults.ignoreFile(name)) {
				continue;
			}

			logger.info(count++ + ": adding " + name);

			DependencyNode node = input.get(name);
			StringBuffer content = readContentFile(node);
			Document doc = createLuceneDocument(node, content);
			writer.addDocument(doc);
		}

		writer.close();
		store.close();

	}

	private void assignLuceneNumbers(DependencyGraph input) throws IOException {

		File indexDir;

		if (indexParentDir.equals(""))
			indexDir = new File(ConfigurationDefaults.RELATIVE_INDEX_DIR);
		else
			indexDir = new File(indexParentDir,
					ConfigurationDefaults.RELATIVE_INDEX_DIR);

		FSDirectory store;

		store = SimpleFSDirectory.open(indexDir);

		IndexReader reader = DirectoryReader.open(store);
		IndexSearcher searcher = new IndexSearcher(reader);
		int numdocs = reader.numDocs();

		for (int i = 0; i < numdocs; i++) {
			Document doc = searcher.doc(i);
			String key = doc.getField("name").stringValue();
			DependencyNode d = input.get(key);
			d.setLuceneDocNumber(i);
			
			// split sentences		
			if (!key.contains(SentenceSplitter.SENTENCE_DIRECTORY))
				sentenceSplitter.createFileForKNgrams(d, key, readContentFile(d).toString());
			
			input.put(d.getCompleteFilepath(), d);
		}

		reader.close();
		store.close();

	}
	
	
	private StringBuffer readContentFile(DependencyNode node) {
		
		StringBuffer content = new StringBuffer();

		if ((node.getContent() != null) && (!node.getContent().isEmpty())) {
			CustomFileReader contentFile;
			try {
				contentFile = new CustomFileReader(node.getContent());
			} catch (FileNotFoundException e) {
				System.out.println("Content file not found "
						+ node.getContent());
				e.printStackTrace();
				return null;
			}

			while (true) {
				String line = contentFile.readLine();
				if (line == null)
					break;
				content.append(line);
			}
			contentFile.close();
		}
		return content;
	}
	
	private Document createLuceneDocument (DependencyNode node, StringBuffer content){
		
		Document doc = new Document();
		doc.add(new StringField("name", node.getCompleteFilepath(),
				Field.Store.YES));
		doc.add(new StringField("path", node.getCompleteFilepath(),
				Field.Store.YES));
		if (content != null && content.length() !=  0) {
			
			FieldType type = new FieldType();
			type.setIndexed(true);
			type.setStored(true);
			type.setStoreTermVectors(true);
			Field field = new Field("contents", new String(content), type);
			doc.add(field);

		} 
		else {
			System.out.println("No content in file "
					+ node.getCompleteFilepath());
		}
		
		logger.info("Document {} created", node.getCompleteFilepath());
		
		return doc;
		
	}
	
	private IndexWriter createIndexWriter(FSDirectory store) throws IOException {

		Analyzer analyzer = new CustomAnalyzer(
				ConfigurationDefaults.LUCENE_VERSION);
		IndexWriterConfig config = new IndexWriterConfig(
				ConfigurationDefaults.LUCENE_VERSION, analyzer);

		IndexWriter writer = new IndexWriter(store, config);

		return writer;

	}
	
	public static IndexReader createIndexReader(String indexDirFilename) {
		File indexDir = new File(indexDirFilename);

		FSDirectory store;
		IndexReader reader;
		try {
			store = SimpleFSDirectory.open(indexDir);
			reader = DirectoryReader.open(store);
		} catch (IOException e1) {
			logger.error("Lucene Directory failing: {}", indexDir.getName());
			e1.printStackTrace();
			return null;
		}

		return reader;
	}


}
	

