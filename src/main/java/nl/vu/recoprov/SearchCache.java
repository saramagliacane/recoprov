package nl.vu.recoprov;

import com.dropbox.client2.session.AppKeyPair;
import com.dropbox.client2.session.AccessTokenPair;
import com.dropbox.client2.jsonextract.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * A modified version of the SearchCache from the Dropbox SDK.
 * It handles the caching of the user authorization.
 *
 */

public class SearchCache
{
 
    private static RuntimeException die(String message)
    {
        System.err.println(message);
        return die();
    }

    private static RuntimeException die()
    {
        System.exit(1);
        return new RuntimeException();
    }


    /**
     * Represent a path as a list of ancestors and a leaf name.
     *
     * For example, "/a/b/c" -> Path(["a", "b"], "c")
     */
    public static final class Path
    {
        public final String[] branch;
        public final String leaf;

        public Path(String[] branch, String leaf)
        {
            assert branch != null;
            assert leaf != null;
            this.branch = branch;
            this.leaf = leaf;
        }

        public static Path parse(String s)
        {
            assert s.startsWith("/");
            String[] parts = s.split("/");
            assert parts.length > 0;

            String[] branch = new String[parts.length-2];
            System.arraycopy(parts, 1, branch, 0, branch.length);
            String leaf = parts[parts.length-1];
            return new Path(branch, leaf);
        }
    }


    // ------------------------------------------------------------------------
    // State model (load+save to JSON)

    public static final class State
    {
        public final AppKeyPair appKey;
        public final AccessTokenPair accessToken;
        public final Content.Folder tree;

        public State(AppKeyPair appKey, AccessTokenPair accessToken, Content.Folder tree)
        {
            this.appKey = appKey;
            this.accessToken = accessToken;
            this.tree = tree;
        }

        public String cursor;

        public void save(String fileName)
        {
            JSONObject jstate = new JSONObject();

            // Convert app key
            JSONArray japp = new JSONArray();
            japp.add(appKey.key);
            japp.add(appKey.secret);
            jstate.put("app_key", japp);

            // Convert access token
            JSONArray jaccess = new JSONArray();
            jaccess.add(accessToken.key);
            jaccess.add(accessToken.secret);
            jstate.put("access_token", jaccess);

            // Convert tree
            JSONObject jtree = tree.toJson();
            jstate.put("tree", jtree);

            // Convert cursor, if present.
            if (cursor != null) {
                jstate.put("cursor", cursor);
            }

            try {
                FileWriter fout = new FileWriter(fileName);
                try {
                    jstate.writeJSONString(fout);
                }
                finally {
                    fout.close();
                }
            }
            catch (IOException ex) {
                throw die("ERROR: unable to save to state file \"" + fileName + "\": " + ex.getMessage());
            }
        }

        public static State load(String fileName)
        {
            JsonThing j;
            try {
                FileReader fin = new FileReader(fileName);
                try {
                    j = new JsonThing(new JSONParser().parse(fin));
                } catch (ParseException ex) {
                    throw die("ERROR: State file \"" + fileName + "\" isn't valid JSON: " + ex.getMessage());
                } finally {
                    fin.close();
                }
            }
            catch (IOException ex) {
                throw die("ERROR: unable to load state file \"" + fileName + "\": " + ex.getMessage());
            }

            try {
                JsonMap jm = j.expectMap();

                JsonList japp = jm.get("app_key").expectList();
                AppKeyPair appKey = new AppKeyPair(japp.get(0).expectString(), japp.get(1).expectString());

                JsonList jaccess = jm.get("access_token").expectList();
                AccessTokenPair accessToken = new AccessTokenPair(jaccess.get(0).expectString(), jaccess.get(1).expectString());

                JsonMap jtree = jm.get("tree").expectMap();
                Content.Folder tree = Content.Folder.fromJson(jtree);

                State state = new State(appKey, accessToken, tree);

                JsonThing jcursor = jm.getMaybe("cursor");
                if (jcursor != null) {
                    state.cursor = jcursor.expectString();
                }

                return state;
            }
            catch (JsonExtractionException ex) {
                throw die ("ERROR: State file has incorrect structure: " + ex.getMessage());
            }
        }
    }

    // ------------------------------------------------------------------------
    // We represent our local cache as a tree of 'Node' objects.
    public static final class Node
    {
        /**
         * The original path of the file.  We track this separately because
         * Folder.children only contains lower-cased names.
         */
        public String path;

        /**
         * The node content (either Content.File or Content.Folder)
         */
        public Content content;

        public Node(String path, Content content)
        {
            this.path = path;
            this.content = content;
        }

        public final JSONArray toJson()
        {
            JSONArray array = new JSONArray();
            array.add(path);
            array.add(content.toJson());
            return array;
        }

        public static Node fromJson(JsonThing t)
            throws JsonExtractionException
        {
            JsonList l = t.expectList();
            String path = l.get(0).expectStringOrNull();
            JsonThing jcontent = l.get(1);
            Content content;
            if (jcontent.isList()) {
                content = Content.File.fromJson(jcontent.expectList());
            } else if (jcontent.isMap()) {
                content = Content.Folder.fromJson(jcontent.expectMap());
            } else {
                throw jcontent.unexpected();
            }
            return new Node(path, content);
        }
    }

    public static abstract class Content
    {
        public abstract Object toJson();

        public static final class Folder extends Content
        {
            public final HashMap<String,Node> children = new HashMap<String,Node>();

            public JSONObject toJson()
            {
                JSONObject o = new JSONObject();
                for (Map.Entry<String,Node> c : children.entrySet()) {
                    o.put(c.getKey(), c.getValue().toJson());
                }
                return o;
            }

            public static Folder fromJson(JsonMap j)
                throws JsonExtractionException
            {
                Folder folder = new Folder();
                for (Map.Entry<String,JsonThing> e : j) {
                    folder.children.put(e.getKey(), Node.fromJson(e.getValue()));
                }
                return folder;
            }
        }

        public static final class File extends Content
        {
            public final String size;
            public final String lastModified;

            public File(String size, String lastModified)
            {
                this.size = size;
                this.lastModified = lastModified;
            }

            public JSONArray toJson()
            {
                JSONArray j = new JSONArray();
                j.add(size);
                j.add(lastModified);
                return j;
            }

            public static File fromJson(JsonList l)
                throws JsonExtractionException
            {
                return new File(l.get(0).expectString(), l.get(1).expectString());
            }
        }
    }

}
