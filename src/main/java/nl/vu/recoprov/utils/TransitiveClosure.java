package nl.vu.recoprov.utils;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import nl.vu.recoprov.abstractclasses.SignalAggregator;
import nl.vu.recoprov.abstractclasses.SignalFilterer;
import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.baseclasses.DependencyNode;
import nl.vu.recoprov.baseclasses.LabelledEdge;
import nl.vu.recoprov.signalaggregators.WeightedSumAggregator;

public class TransitiveClosure extends SignalAggregator{

	
	public static final String INFERRED  = WeightedSumAggregator.FINAL_SCORE;

		public DependencyGraph aggregateSignals(DependencyGraph input) {
			
//			DependencyGraph input =  (DependencyGraph) original.clone();
			
			HashMap<Integer, ArrayList<LabelledEdge>> incidencyMatrix = input.getIncidencyMatrix();
			Boolean modified = false;


			
			for (String name1: input.keySet()){
				DependencyNode x = input.get(name1);
				for (String name2: input.keySet()){
					
					if(name1.equals(name2))
						continue;
					
					DependencyNode y = input.get(name2);
					for (String name3: input.keySet()){
						
						if(name2.equals(name3) )
							continue;
						
						DependencyNode z = input.get(name3);
						
						// add edge xz if edges xy and yz exist
						ArrayList<LabelledEdge> edgearrayXY = input.getAllEdges(x.getLuceneDocNumber(), y.getLuceneDocNumber());
						ArrayList<LabelledEdge> edgearrayYZ = input.getAllEdges(y.getLuceneDocNumber(), z.getLuceneDocNumber());
						//ArrayList<LabelledEdge> edgearrayXZ = input.getAllEdges(x.getLuceneDocNumber(), z.getLuceneDocNumber());
						
						
							
						// if two edges with the same label exist between xy and yz
						//Boolean found = false;
							
						for( LabelledEdge e2: edgearrayXY){
							if(!e2.getLabel().equals(WeightedSumAggregator.FINAL_SCORE))
								continue;
							if(e2.getScore()<= 0.0)
									continue;
							for( LabelledEdge e3: edgearrayYZ){
								if(e3.getLabel().equals(e2.getLabel())){
										if(e3.getScore()<= 0.0)
											continue;
										
										LabelledEdge result =input.getAggregatedEdge(x.getLuceneDocNumber(), z.getLuceneDocNumber());
										LabelledEdge result2 =input.getInferredEdge(x.getLuceneDocNumber(), z.getLuceneDocNumber());
										if((result != null)||(result2!=null)){
											System.out.println("Found " +e2.getLabel()+  ":" + x.getLuceneDocNumber() + "->" + z.getLuceneDocNumber() + " - because there are: " +x.getLuceneDocNumber() + "->" + y.getLuceneDocNumber() + "->" + z.getLuceneDocNumber() );
											break;
										}	
										input.addEdge(x, z, INFERRED, 1.0);
										System.out.println("Added " +e2.getLabel()+  ":" + x.getLuceneDocNumber() + "->" + z.getLuceneDocNumber() + " - because there are: " +x.getLuceneDocNumber() + "->" + y.getLuceneDocNumber() + "->" + z.getLuceneDocNumber() );
										modified = true;
										break;
								}	
							}
						}
						
					}
				}
			}	
	
			
			// should be recursvie
			
			if(modified) 
				return aggregateSignals(input);
			else 
				return input;
		}
}

	

