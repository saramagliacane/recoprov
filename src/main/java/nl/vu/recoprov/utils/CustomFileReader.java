package nl.vu.recoprov.utils;
/**
 * Custom file reader that extends BufferedReader to accept filenames as Strings
 * and Files directly.
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class CustomFileReader extends BufferedReader{
	
	String filename;

	public CustomFileReader(String filename) throws FileNotFoundException{
		super(new InputStreamReader(new FileInputStream(filename)));
		this.filename = filename;
	}

	public CustomFileReader(File file) throws FileNotFoundException {
		super(new InputStreamReader(new FileInputStream(file)));
		this.filename = file.getName();
	}

	
	@Override
	public void close(){
		try {
			super.close();
		} catch (IOException e) {
			System.out.println("Error closing file: " + filename);
			e.printStackTrace();
		}
	
	}
	
	@Override
	public String readLine(){
		String line = null;
		try{
			line = super.readLine();
		} catch (final IOException e) {
			System.out.println("Error processing file: " + filename);
			e.printStackTrace();
		}
		return line;
	}
	
}
