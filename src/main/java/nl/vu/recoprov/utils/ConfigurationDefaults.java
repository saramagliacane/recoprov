package nl.vu.recoprov.utils;

import java.io.File;
import java.util.ArrayList;

import org.apache.lucene.util.Version;

public class ConfigurationDefaults {
	
	public static final String CONFIG_FILE = "config.txt";
	
	public static final Boolean useNewFeatures = false;
	
	/**
	 * Dropbox configuration defaults
	 */
	public static final String STATE_FILE = "SearchCache.json";
	public static final String SECRET_FILE = "AppKeySecret.txt";
	public static final String TEMPDIR = "temp/";
	public static final String CONTENTDIR = "content/";
	
	/** 
	 * Image Reader defaults
	 */
	
	public static final String IMAGE_DIRECTORY = "images/";
	public static final String IMAGE_INDEX_DIR = IMAGE_DIRECTORY + "lucene_index/";



	public static String[] PLAGIARISMDETECTIONDIRS = {"01_no_plagiarism", "02_no_obfuscation", "03_artificial_low", "04_artificial_high", "05_translation", "06_simulated_paraphrase"};

	
	/**
	 * Lucene indexer defaults
	 */
	public static final String RELATIVE_INDEX_DIR = "lucene_index/";
	public static String INDEX_DIR = "lucene_index/";
	public static final Version LUCENE_VERSION = Version.LUCENE_42;
	
	/** 
	 * Lucene similarity defaults
	 */
	public static double LUCENE_THRESHOLD = 0.1;
	public static int LUCENE_MAX_NUMBER_DOCS = 100;
	
	
	/**
	 * Writing results
	 */
	
	public static String RESULTS = "log.txt";
	
	public static Boolean ignoreFile(String filename) {
		if (filename.startsWith(".") || filename.startsWith("_")
				|| filename.contains("/_") || filename.contains("/.")) {
			return true;
		}
		if (filename.contains("blg") || filename.contains("log")
				|| filename.contains("aux") || filename.contains("synctex")
				|| filename.contains("Icon") || filename.contains("xml")
				|| filename.contains("content") || filename.contains("readme.txt") )
			return true;

		//plagiarism detection corpus 
		// TODO: better specify it as a parameter in the experiment
		if (filename.contains("pairs"))
			return true;
		
		return false;

	}

	public static boolean isInBlackList(String filename,
			String[] params) {
		if (params != null){
			for (String s: params){
				if (filename.contains(s))
					return true;
			}
		}
		
		if (filename.contains(ConfigurationDefaults.RELATIVE_INDEX_DIR))
			return true;
		
		if (filename.contains(ConfigurationDefaults.IMAGE_DIRECTORY))
			return true;
		
		
		if (filename.contains(ConfigurationDefaults.CONTENTDIR))
			return true;
		
		
		return false;
	}

}
