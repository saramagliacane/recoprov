package nl.vu.recoprov.utils;

import java.io.FileNotFoundException;

import nl.vu.recoprov.CompletePipeline;

public class ConfigurationReader {
	

	private String configfile = ConfigurationDefaults.CONFIG_FILE;
	private Boolean online = false;
	private String currentDir = "";
	
	public ConfigurationReader(){}
	
	public ConfigurationReader(String filename){
		configfile = filename;
	}
	
	public CompletePipeline readParameters() {
		readConfigFile();
		return new CompletePipeline(online, currentDir);
	}
	

	public void readParameters(CompletePipeline pipeline) {
		readConfigFile();
		pipeline.setCurrentDir(currentDir);
		pipeline.setConnectToInternet(online);
		
	}
		
	private void readConfigFile(){
		System.out.println("Reading config file: " + configfile);


		String line = null;

		CustomFileReader reader;
		try {
			reader = new CustomFileReader(configfile);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			return;
		}

		while (true) {

			line = reader.readLine();
			if (line == null)
				break;

			if (line.startsWith("folder=")) {
				currentDir = line.split("=")[1].replace("\"", "");

				if (!currentDir.startsWith("/")) {
					currentDir = "/" + currentDir;
				}
				if (!currentDir.endsWith("/")) {
					currentDir = currentDir + "/";
				}

				continue;
			}

			line = line.toLowerCase().trim();

			if (line.startsWith("dropbox")) {
				online = true;
			}

			if (line.startsWith("local")) {
				online = false;
			}

		}

		reader.close();



	}
}
