package nl.vu.recoprov.experiments;

/**
 * Plagiarism detection experiment using the PAN 2012 corpus
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import nl.vu.recoprov.CompletePipeline;
import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.signaldetectors.CompressionDistanceSignal;
import nl.vu.recoprov.signalfilters.HomogeneityFilter;
import nl.vu.recoprov.utils.ConfigurationDefaults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dropbox.client2.exception.DropboxException;

public class Experiment3 {

	private static String dirfile = "pan12-detailed-comparison-training-corpus/";
	private static String jsonfile = "pan.json";

	private static DependencyGraph baselineGraph = null;
	private static Logger logger;

	public static final String RESULTS_DIRECTORY = "results/";
	
	public static void main(String[] args) throws Exception {
		
		// pass the directory and the json of the reference graph
		
		logger = LoggerFactory
				.getLogger("nl.vu.recoprov.experiments.Experiment3");

		if (!checkInitialParameters(args)) {
			System.exit(0);
		}
		
		File resultDir = new File (RESULTS_DIRECTORY);
		if (!resultDir.exists())
			resultDir.mkdir();
		
		// experimenting to get perfect recall
		ConfigurationDefaults.LUCENE_MAX_NUMBER_DOCS = 6000;
		CompletePipeline pipeline = new CompletePipeline(false, dirfile);
		
		FileWriter writer = createFileResultsWriter();
		DependencyGraph depGraphLucene = createLuceneGraph(pipeline);
		DependencyGraph depGraph = createReferenceGraph(pipeline);
		//DependencyGraph depGraphSentencesLucene = createLuceneGraphForSentences(pipeline, depGraphLucene);
		experimentWithThresholds(pipeline, writer, depGraph, depGraphLucene, null);
		
		
		writer.flush();
		writer.close();

	}
		
	public static void experimentWithThresholds(CompletePipeline pipeline,
			FileWriter writer, DependencyGraph depGraph,
			DependencyGraph depGraphLucene, DependencyGraph depGraphSentencesLucene) throws Exception {

		DependencyGraph depGraphLuceneMore = createLuceneMoreGraph(pipeline);
		
		//add compression
		//DependencyGraph depGraphLuceneCompression = createCompressionGraph(pipeline);
	
		double[] thresholds = { 0.0, 0.01, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35,
				0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7 };

		for (double threshold : thresholds) {
			try {

				ConfigurationDefaults.LUCENE_THRESHOLD = threshold;

				DependencyGraph depGraphLuceneThreshold = depGraphLucene
						.copyGraph();
				pipeline.filterLuceneThreshold(depGraphLuceneThreshold);
				

				DependencyGraph depGraph1 = depGraphLuceneThreshold.copyGraph();
				pipeline.aggregateSignals(depGraph1);
				writeResults(pipeline, depGraph, depGraph1, "Lucene", writer);

				depGraph1 = depGraphLuceneThreshold.copyGraph();
				pipeline.filterTextContainment(depGraph1);
				pipeline.aggregateSignals(depGraph1);
				writeResults(pipeline, depGraph, depGraph1, "LuceneTextFilter",
						writer);

				depGraph1 = depGraphLuceneThreshold.copyGraph();
				pipeline.filterPlagiarismCorpus(depGraph1);
				pipeline.aggregateSignals(depGraph1);
				writeResults(pipeline, depGraph, depGraph1, "LucenePANFilter",
						writer);

				depGraph1 = depGraphLuceneThreshold.copyGraph();
				pipeline.filterTextContainment(depGraph1);
				pipeline.filterPlagiarismCorpus(depGraph1);
				pipeline.aggregateSignals(depGraph1);
				writeResults(pipeline, depGraph, depGraph1,
						"LucenePAN2Filters", writer);
				
//				depGraph1 = depGraphLuceneCompression.copyGraph();
//				pipeline.filterTopKEdges(depGraph1, CompressionDistanceSignal.COMPRESSION_DISTANCE);
//				pipeline.filterPlagiarismCorpus(depGraph1);
//				pipeline.filterLuceneThreshold(depGraphLuceneThreshold);
//				pipeline.aggregateSignals(depGraph1);
//				writeResults(pipeline, depGraph, depGraph1,
//						"LuceneCompressionPANFilter", writer);
				
				
				
				DependencyGraph depGraphLuceneMoreThreshold = depGraphLuceneMore
						.copyGraph();
				pipeline.filterLuceneThreshold(depGraphLuceneMoreThreshold);

				depGraph1 = depGraphLuceneMoreThreshold.copyGraph();
				pipeline.aggregateSignals(depGraph1);
				writeResults(pipeline, depGraph, depGraph1, "LuceneMore", writer);

				depGraph1 = depGraphLuceneMoreThreshold.copyGraph();
				pipeline.filterTextContainment(depGraph1);
				pipeline.aggregateSignals(depGraph1);
				writeResults(pipeline, depGraph, depGraph1, "LuceneMoreTextFilter",
						writer);

				depGraph1 = depGraphLuceneMoreThreshold.copyGraph();
				pipeline.filterPlagiarismCorpus(depGraph1);
				pipeline.aggregateSignals(depGraph1);
				writeResults(pipeline, depGraph, depGraph1, "LuceneMorePANFilter",
						writer);

				depGraph1 = depGraphLuceneMoreThreshold.copyGraph();
				pipeline.filterTextContainment(depGraph1);
				pipeline.filterPlagiarismCorpus(depGraph1);
				pipeline.aggregateSignals(depGraph1);
				writeResults(pipeline, depGraph, depGraph1,
						"LuceneMorePAN2Filters", writer);
				
				
//				// dependency graph with lucene sentences
//				
//				DependencyGraph depGraphSentencesLuceneThreshold = depGraphSentencesLucene
//						.copyGraph();
//				pipeline.filterLuceneThreshold(depGraphSentencesLuceneThreshold);
//				pipeline.aggregateSignals(depGraphSentencesLuceneThreshold);
//				writeResults(pipeline, depGraph, depGraphSentencesLuceneThreshold, "LuceneSentences", writer);


			} catch (Exception e) {
				e.printStackTrace();
				logger.error("Error in the main loop, threshold {}", threshold);
				throw e;
			}
		}
	

	}


	private static void writeResults(CompletePipeline pipeline,
			DependencyGraph depGraph, DependencyGraph predicted,
			String message, FileWriter writer) throws FileNotFoundException {

		String temp = depGraph.similarToGraph(predicted).toString();
		try {
			writer.append("\n\n" + message + " \nLucene Threshold: "
					+ ConfigurationDefaults.LUCENE_THRESHOLD + "\n");
			writer.append(temp);
			writer.flush();
		} catch (IOException e) {
			logger.error(
					"Results file IOException (cannot write) for experiment {}",
					message);
			e.printStackTrace();
		}

		logger.info("{} graph created, threshold {}", message,
				ConfigurationDefaults.LUCENE_THRESHOLD);
		pipeline.translateToPROVDM(predicted, RESULTS_DIRECTORY+ "graph" + message + "_"
				+ ConfigurationDefaults.LUCENE_THRESHOLD + ".gv");

	}

	public static DependencyGraph createGraph() throws DropboxException,
			IOException {
		try {
			if (baselineGraph == null) {
				baselineGraph = new DependencyGraph();
				CompletePipeline pipeline = new CompletePipeline(false,
						dirfile, ConfigurationDefaults.PLAGIARISMDETECTIONDIRS);

				baselineGraph = pipeline.initDependencyGraph();
				logger.info("Baseline graph created.");
			}

		} catch (DropboxException e) {
			e.printStackTrace();
			logger.error("Could not create baseline graph.");
			throw e;
		}

		DependencyGraph copyOfBaseline = baselineGraph.copyGraph();

		return copyOfBaseline;
	}


	public static DependencyGraph createLuceneGraph(CompletePipeline pipeline)
			throws Exception {
		DependencyGraph depGraphLucene = DependencyGraph.deserializeDependencyGraph("depGraphLucene.ser");

		if (depGraphLucene == null) {
			depGraphLucene = createGraph();

			try {
				pipeline.luceneSimilaritySignal(depGraphLucene);
				logger.info("Lucene graph created.");
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("Could not create Lucene graph.");
				throw e;
			}

			depGraphLucene.serialize("depGraphLucene.ser");
		}
		return depGraphLucene;
	}
	
	
	public static DependencyGraph createLuceneMoreGraph(CompletePipeline pipeline)
			throws Exception {
		DependencyGraph depGraphLuceneMore = DependencyGraph.deserializeDependencyGraph("depGraphLuceneMore.ser");

		if (depGraphLuceneMore == null) {
			depGraphLuceneMore = createGraph();

			try {
				pipeline.luceneMoreLikeThisSignal(depGraphLuceneMore);
				logger.info("Lucene More graph created.");
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("Could not create Lucene More graph.");
				throw e;
			}

			depGraphLuceneMore.serialize("depGraphLuceneMore.ser");
		}
		return depGraphLuceneMore;
	}


	
	public static DependencyGraph createReferenceGraph(CompletePipeline pipeline)
			throws Exception {

		DependencyGraph depGraph1 = DependencyGraph.deserializeDependencyGraph("depGraphReference.ser");

		if (depGraph1 != null)
			return depGraph1;

		PROVReader provreader = new PROVReader(dirfile, jsonfile, "src/", "susp/");
		try {
			depGraph1 = provreader.generateDepGraph();
			pipeline.translateToPROVDM(depGraph1, "graphCorpus.gv");
			depGraph1.serialize("depGraphReference.ser");
			logger.info("Reference graph created.");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Could not create reference graph.");
			throw e;
		}

		return depGraph1;

	}

	public static FileWriter createFileResultsWriter() throws IOException {
		String resultsfilename = "results" + System.currentTimeMillis()
				+ ".txt";
		FileWriter writer;
		try {
			writer = new FileWriter(resultsfilename);
		} catch (IOException e) {
			logger.error(
					"Results file IOException (cannot open FileWriter) {}",
					resultsfilename);
			e.printStackTrace();
			throw e;
		}
		return writer;
	}
	
	

	public static Boolean checkInitialParameters(String [] args) {

		
		if (args.length >= 1) {
			dirfile = args [0];
		}
		
		if (args.length >= 2){
			jsonfile = args [1];
		}
		
		logger.info("Starting experiment: {} {}", dirfile, jsonfile);

		File dir = new File(dirfile);

		if (!dir.exists()) {
			logger.error("Directory doesn't exist {}", dirfile);
			return false;
		}

		if (!dir.isDirectory()) {
			logger.error("Directory is not a directory {}", dirfile);
			return false;
		}
		dirfile = dir.getAbsolutePath();

		File json = new File(jsonfile);

		if (!json.exists()) {
			logger.error("json doesn't exist {}", jsonfile);
			return false;
		}
		jsonfile = json.getAbsolutePath();

		return true;

	}
	
	private static DependencyGraph createCompressionGraph(
			CompletePipeline pipeline, DependencyGraph depGraphLucene) {
		
		DependencyGraph depGraph1 = DependencyGraph.deserializeDependencyGraph("depGraphCompression.ser");

		if (depGraph1 != null)
			return depGraph1;
		
		DependencyGraph depGraphLuceneCompression = depGraphLucene.copyGraph();
		pipeline.compressionDistanceSignal(depGraphLuceneCompression);

		try {
			depGraphLuceneCompression.serialize("depGraphCompression.ser");
			logger.info("Compression graph created.");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Could not create compression graph.");
		}

		return depGraphLuceneCompression;
	}
	
	@Deprecated
	private static DependencyGraph createDependencyGraphForSentences (CompletePipeline pipeline, DependencyGraph input) {
		
//		DependencyGraph depGraph1 = DependencyGraph.deserializeDependencyGraph("depGraphSentences.ser");
//
//		if (depGraph1 != null)
//			return depGraph1;

		try {
			pipeline.createDependencyGraphForSentences(input);
//			input.serialize("depGraphSentences.ser");
			logger.info("depGraphSentences graph created.");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Could not create depGraphSentences graph.");
		}

		return input;
	}

	
	@Deprecated
	private static DependencyGraph createLuceneGraphForSentences (CompletePipeline pipeline, DependencyGraph input) {
		
		logger.info("depGraphSentencesLucene search started.");
		DependencyGraph depGraph1 = DependencyGraph.deserializeDependencyGraph("depGraphSentencesLucene.ser");

		if (depGraph1 != null)
			return depGraph1;
		
		depGraph1 = input.copyGraph();
		
		try {
			createDependencyGraphForSentences(pipeline, depGraph1);
			new HomogeneityFilter().filterSignals(depGraph1);
			//pipeline.aggregateSignals(depGraph1);
			input.serialize("depGraphSentencesLucene.ser");
			logger.info("depGraphSentencesLucene graph created.");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Could not create depGraphSentencesLucene graph.");
		}

		return depGraph1;
	}

}
