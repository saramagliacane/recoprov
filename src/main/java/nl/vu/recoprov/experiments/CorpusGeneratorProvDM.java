package nl.vu.recoprov.experiments;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;

import nl.vu.recoprov.CompletePipeline;
import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.baseclasses.DependencyNode;
import nl.vu.recoprov.signalaggregators.WeightedSumAggregator;
import nl.vu.recoprov.utils.ConfigurationDefaults;



import org.openprovenance.prov.dot.ProvToDot;
import org.openprovenance.prov.xml.Activity;
import org.openprovenance.prov.xml.ActivityRef;
import org.openprovenance.prov.xml.Agent;
import org.openprovenance.prov.xml.Document;
import org.openprovenance.prov.xml.Entity;
import org.openprovenance.prov.xml.EntityRef;
import org.openprovenance.prov.xml.ProvFactory;
import org.openprovenance.prov.xml.Statement;
import org.openprovenance.prov.xml.Used;
import org.openprovenance.prov.xml.WasAssociatedWith;
import org.openprovenance.prov.xml.WasGeneratedBy;
//import org.openprovenance.prov.rdf.RdfConstructor;

// prov to dot uses the XML representation

//import org.openprovenance.prov.rdf.Entity;
//import org.openprovenance.prov.rdf.Derivation;
//import org.openprovenance.prov.rdf.Revision;
//import org.openprovenance.prov.rdf.EntityInvolvement;
//import org.openprovenance.prov.rdf.TimeInstant;

/// track the time in which an entity was generated


// further annotation is in Notes - an entity can be linked to a note by hasAnnotation

// entity attributes - [attr1=val] prov:type="document'
// derivation attributes - prov:type = "physical transform" wasDerivedFrom
// types of derivation:
// revision - newer and older   wasRevisionOf
// quotation - quote(partial copy), original     wasQuotedFrom
// original source  - derived (entity), source   hadOriginalSource

// derivation is a particular form of Trace - tracedTo - entity, ancestor

// specialization is a more constrained entity  specializationOf

// attrib - prov:label, prov:type, prov:value (score)


public class CorpusGeneratorProvDM {

	static HashMap<String, Entity> listOfAvailableEntities = new HashMap<String, Entity> ();
	static HashMap<String, Object> listOfAvailableRelations =  new HashMap<String, Object> ();
	static  HashMap<String,Agent> listOfAvailableAgents = new  LinkedHashMap<String, Agent> ();
	static  HashMap<String,Activity> listOfAvailableActivities = new  LinkedHashMap<String, Activity> ();

	
//	static 		ArrayList<String> docNumbers = new ArrayList<String>();
	static int counter = 0;
	
	static Boolean trackCreation = false;
	
	public static DependencyGraph depGraph;
	public final static String dir = "/Users/saramagliacane/Dropbox/reconstructing-dataprep-provenance/";
	
	public static void main(String args[]) throws Exception {
		
		ProvFactory factory = initFactory();
		CompletePipeline pipeline = new CompletePipeline(false, dir);
		
		depGraph = pipeline.initDependencyGraph(); 
		pipeline.loadMetadaAndIndexes(depGraph);
		createEntityFromDepGraph(factory, depGraph);
		
		manualAnnotation();
		
		convertToDot(factory);

		//System.out.println(depGraph);
		
		
		
	}
		
	
	public static ProvFactory initFactory() {
		ProvFactory factory = ProvFactory.getFactory();

		//initDocNumbers();
		
		Hashtable<String, String> namespace = new Hashtable<String, String>();
		//namespace.put("_", "https://www.dropbox.com/home/reconstructing-dataprep-provenance/");		
		namespace.put("_", "");
		factory.setNamespaces(namespace);

		
		addAgent(factory, "yolanda");
		addAgent(factory, "paul");
		addAgent(factory, "sara");
		return factory;
	}
	
	

	
	
	private static void addAgent(ProvFactory factory, String name){
		
		Agent agent = factory.newAgent(name);
		listOfAvailableAgents.put(name, agent);

		

	}

	
public static void createEntityFromDepGraph(ProvFactory factory, DependencyGraph depGraph){
	for (String name: depGraph.keySet()){
		// create an entity for each node in the dependency graph
		if(ConfigurationDefaults.ignoreFile(name))
			continue;
		// for each node take the edges
		DependencyNode d = depGraph.get(name);
		createEntity(factory, d);
		
		
	}
}


public static void createEntity(ProvFactory factory, DependencyNode d){

	Agent a = listOfAvailableAgents.get("paul");

	if(d.getCompleteFilepath().contains("camera-ready")){
		a = listOfAvailableAgents.get("sara");
	}
	
	String dir = "/Users/saramagliacane/Dropbox/reconstructing-dataprep-provenance/";
	String name = d.getCompleteFilepath().replace(dir, "");
	

	
	String sanitizedname= name.replace("/", "_");
	sanitizedname= sanitizedname.replace(" ", "_");
	
	String id = ""+d.getLuceneDocNumber();
	
	//System.out.println("Created entity: "+ sanitizedname);
	
	Entity entity = listOfAvailableEntities.get(name);
	if(entity == null){
		entity = factory.newEntity(sanitizedname,id );
		listOfAvailableEntities.put(name, entity);
		
		if(trackCreation){
			Activity creation = factory.newActivity("creation"+ counter, "creation"+ counter);
	
			
			try {		
			// convert Date to XMLGregorianCalendar
				GregorianCalendar c = new GregorianCalendar();
				c.setTime(d.getMetadata().getModified());
				XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
				creation.setStartTime(date2);
			} catch (DatatypeConfigurationException e) {
				e.printStackTrace();
			}
	
			listOfAvailableActivities.put("creation"+ counter++, creation);
			
	
			WasAssociatedWith assoc = factory.newWasAssociatedWith(creation.getId(), creation, a);
			WasGeneratedBy generation = factory.newWasGeneratedBy(entity,"author", creation);
	
			
			listOfAvailableRelations.put("creation"+creation.getLabel(), assoc);
			listOfAvailableRelations.put("author"+creation.getLabel(), generation);
		}
	
	}		

	
	

	
}
	


private static ActivityRef createActivityAndGetRef(String name){
	ActivityRef creationRef = new ActivityRef();
	
	Activity creation = listOfAvailableActivities.get(name);
	
	if(creation== null){
		
		creation = new Activity();
		creation.setId(new QName(name));
		listOfAvailableActivities.put(name, creation);
	}


	creationRef.setRef(creation.getId());
	
	return creationRef;
}


//private static void createGenerationActivityAndEntity(String activity, String entityname){
//	ActivityRef actref= createActivityAndGetRef(activity);
//	
//	Entity entity = 	listOfAvailableEntities.get(entityname);
//	EntityRef entityref = new EntityRef();
//	entityref.setRef(entity.getId());
//	
//	WasGeneratedBy generation = new WasGeneratedBy();
//	generation.setActivity(actref);
//	generation.setEntity(entityref);
//
//	//generation.setId(new QName("generated"));
//	
//	listOfAvailableRelations.add(generation);
//}

private static void createDerivationActivityAndEntity( String activity, String usedEntity, String generatedEntity){
	ActivityRef actref= createActivityAndGetRef(activity);
	//System.out.print(usedEntity);
	
	
	Entity used = 	listOfAvailableEntities.get(usedEntity);
	EntityRef usedref = new EntityRef();
	usedref.setRef(used.getId());
	
	Entity generated = 	listOfAvailableEntities.get(generatedEntity);
	EntityRef generatedref = new EntityRef();
	generatedref.setRef(generated.getId());
	
	
	WasGeneratedBy generation = new WasGeneratedBy();
	generation.setActivity(actref);
	generation.setEntity(usedref);
	listOfAvailableRelations.put(activity + usedEntity,generation);
	
	Used usation = new Used();
	usation.setActivity(actref);
	usation.setEntity(generatedref);
	listOfAvailableRelations.put(activity + generatedEntity,usation);

//	WasDerivedFrom derivation = new WasDerivedFrom();
//	derivation.setActivity(actref);
//	derivation.setGeneratedEntity(generatedref);
//	derivation.setUsedEntity(usedref);
//	derivation.getType().add(activity);
//	derivation.setId(new QName("generated"));
	


	
	//depGraph.addEdge(docNumbers.indexOf(generatedEntity), docNumbers.indexOf(usedEntity),WeightedSumAggregator.FINAL_SCORE, 1.0);
	
	//System.out.println(dir+generatedEntity);
	depGraph.addEdge(depGraph.get(dir+usedEntity), depGraph.get(dir+generatedEntity),WeightedSumAggregator.FINAL_SCORE, 1.0);
	

}


public static void manualAnnotation(){
	String eswc2012 = "eswc2012-swpm-reconstructing/";
	
	
	// ############ ESWC2012 folder
	createDerivationActivityAndEntity("latex-compile1",eswc2012 + "reconstructing.tex", eswc2012 + "reconstructing.pdf");
	
//	createDerivationActivityAndEntity("latex-compile1",eswc2012 + "reconstructing.tex", eswc2012 + "reconstructing.log");
//	createDerivationActivityAndEntity("latex-compile1",eswc2012 + "reconstructing.tex", eswc2012 + "reconstructing.aux");
//	createDerivationActivityAndEntity("bibtex1",eswc2012 + "incompleteProvRelWork.bib", eswc2012 + "reconstructing.blg");
//	
	
	//createDerivationActivityAndEntity("bibtex1",eswc2012 + "incompleteProvRelWork.bib", eswc2012 + "reconstructing.bbl");
	createDerivationActivityAndEntity("latex-compile1", eswc2012 + "reconstructing.bbl", eswc2012 + "reconstructing.pdf");


	createDerivationActivityAndEntity("latex-compile1", eswc2012 + "llncs.cls", eswc2012 + "reconstructing.pdf");
	createDerivationActivityAndEntity("latex-compile1", eswc2012 + "splncs03.bst", eswc2012 + "reconstructing.pdf");
	
	createDerivationActivityAndEntity("tiff2pdf", eswc2012 + "example-reconstruction.tiff", eswc2012 + "example-reconstruction.pdf");
	createDerivationActivityAndEntity("latex-compile1", eswc2012 + "example-reconstruction.pdf", eswc2012 + "reconstructing.pdf");
	
	
	createDerivationActivityAndEntity("latex-compile2",eswc2012 + "incompleteprovenanceRelWork.tex", eswc2012 + "incompleteprovenanceRelWork.pdf");
	//an older version of the biblio
	// revision tudi of incompleteProvRelWork.bib - 17 aprila 2012  11.57 (ob isti uri .bbl in .blg)
	// tex je menjal pole   - revision od bibliografije je prej ku una od texa
	//citation use with an older version of the biblio (inside the tex)
	//createDerivationActivityAndEntity("citationUse3",eswc2012 + "incompleteProvRelWork.bib", eswc2012 + "reconstructing.tex");
	//createDerivationActivityAndEntity("bibtex1",eswc2012 + "incompleteProvRelWork.bib", "eswc2012_swpm_reconstructing_incompleteprovenanceRelWork.bbl");
	createDerivationActivityAndEntity("latex-compile2",eswc2012 + "incompleteprovenanceRelWork.bbl", eswc2012 + "incompleteprovenanceRelWork.pdf");
	createDerivationActivityAndEntity("latex-compile2", eswc2012 + "llncs.cls", eswc2012 + "incompleteprovenanceRelWork.pdf");
	createDerivationActivityAndEntity("latex-compile2", eswc2012 + "splncs03.bst", eswc2012 + "incompleteprovenanceRelWork.pdf");

	
	createDerivationActivityAndEntity("doc2tex", eswc2012 + "incomplete provenance state of the art.docx", eswc2012 + "incompleteprovenanceRelWork.tex");
	createDerivationActivityAndEntity("doc2bib", eswc2012 + "incomplete provenance state of the art.docx", eswc2012 + "incompleteProvRelWork.bib");
	createDerivationActivityAndEntity("inclusion-tex2tex",eswc2012 + "incompleteprovenanceRelWork.tex",eswc2012 + "reconstructing.tex");

	
	
	// ############ camera ready folder

	String camera_ready = "camera-ready/";
			
	createDerivationActivityAndEntity("review-tex",eswc2012 + "reconstructing.tex", camera_ready + eswc2012 + "reconstructing.tex");
	createDerivationActivityAndEntity("copy-tiff", eswc2012 + "example-reconstruction.tiff", camera_ready + eswc2012 + "example-reconstruction.tiff");
	createDerivationActivityAndEntity("copy-pdf1", eswc2012 + "example-reconstruction.pdf", camera_ready + eswc2012 + "example-reconstruction.pdf");
	createDerivationActivityAndEntity("copy-llncs", eswc2012 + "llncs.cls", camera_ready + eswc2012 + "llncs.cls");
	createDerivationActivityAndEntity("copy-splncs", eswc2012 + "splncs03.bst", camera_ready + eswc2012 + "splncs03.bst");
	createDerivationActivityAndEntity("update-bib",eswc2012 + "incompleteProvRelWork.bib", camera_ready + eswc2012 + "incompleteProvRelWork.bib");
	
	createDerivationActivityAndEntity("latex-compile3",camera_ready + eswc2012 + "reconstructing.tex", camera_ready + eswc2012 + "reconstructing.pdf");

	
	createDerivationActivityAndEntity("use-citation-in-tex",camera_ready + eswc2012 + "incompleteProvRelWork.bib", camera_ready + eswc2012 + "reconstructing.tex");
	
	createDerivationActivityAndEntity("bibtex-compile3",camera_ready + eswc2012 + "incompleteProvRelWork.bib", camera_ready + eswc2012 + "reconstructing.bbl");
	createDerivationActivityAndEntity("latex-compile3", camera_ready + eswc2012 + "reconstructing.bbl", camera_ready + eswc2012 + "reconstructing.pdf");
	createDerivationActivityAndEntity("latex-compile3", camera_ready + eswc2012 + "llncs.cls", camera_ready + eswc2012 + "reconstructing.pdf");
	createDerivationActivityAndEntity("latex-compile3", camera_ready + eswc2012 + "splncs03.bst", camera_ready + eswc2012 + "reconstructing.pdf");
	
	createDerivationActivityAndEntity("latex-compile3", camera_ready + eswc2012 + "example-reconstruction.pdf", camera_ready + eswc2012 + "reconstructing.pdf");
	
	createDerivationActivityAndEntity("zip", camera_ready + eswc2012 + "reconstructing.tex", camera_ready + eswc2012 + "swpm2012-camera-ready.zip");
	createDerivationActivityAndEntity("zip", camera_ready + eswc2012 + "reconstructing.bbl", camera_ready + eswc2012 + "swpm2012-camera-ready.zip");
	createDerivationActivityAndEntity("zip", camera_ready + eswc2012 + "incompleteProvRelWork.bib", camera_ready + eswc2012 + "swpm2012-camera-ready.zip");
	createDerivationActivityAndEntity("zip", camera_ready + eswc2012 + "llncs.cls", camera_ready + eswc2012 + "swpm2012-camera-ready.zip");
	createDerivationActivityAndEntity("zip", camera_ready + eswc2012 + "splncs03.bst", camera_ready + eswc2012 + "swpm2012-camera-ready.zip");
	createDerivationActivityAndEntity("zip", camera_ready + eswc2012 + "example-reconstruction.tiff", camera_ready + eswc2012 + "swpm2012-camera-ready.zip");
	createDerivationActivityAndEntity("zip", camera_ready + eswc2012 + "example-reconstruction.pdf", camera_ready + eswc2012 + "swpm2012-camera-ready.zip");
	createDerivationActivityAndEntity("zip", camera_ready + eswc2012 + "reconstructing.pdf", camera_ready + eswc2012 + "swpm2012-camera-ready.zip");
	

	// ############ camera ready root folder
	createDerivationActivityAndEntity("copy-pdf2", camera_ready + eswc2012 + "reconstructing.pdf", camera_ready + "SWPM_camera_ready.pdf");
	
//	// ############ root folder
	createDerivationActivityAndEntity("review-doc",   "reconstruct.docx", "ReconstructingProvenance.docx" );
	createDerivationActivityAndEntity("inclusion-doc2tex",   "ReconstructingProvenance.docx", eswc2012+ "reconstructing.tex" );
	
	// an older version of it, actually
	//createDerivationActivityAndEntity("imageInclusion",   eswc2012+ "example-reconstruction.tiff", "ReconstructingProvenance.docx" );
	//System.out.println(listOfAvailableRelations);


	
	
	// CODE je kazin
	
	//presidents.xlsx -> presidents.csv?
	
	
	
	
}


public static void convertToDot(ProvFactory factory){

	ProvToDot provtodot = new ProvToDot();
	Document container;
	// container = factory.newNamedBundle("", listOfAvailableActivities.values(), listOfAvailableEntities.values(),  new LinkedList<Agent>(), new LinkedList<Statement>());
	container = factory.newDocument(listOfAvailableActivities.values(), listOfAvailableEntities.values(),  new LinkedList<Agent>(), new LinkedList<Statement>());
	
	try {
		provtodot.convert(container, new File("graphCorpus.gv"));
	} catch (FileNotFoundException e) {
	
		e.printStackTrace();
	}
}


//@Deprecated	
//public static EntityRef createEntityAndGetEntityRef(DependencyNode d){
//	
//	Agent a = listOfAvailableAgents.get("paul");
//	
//	if(d.getCompleteFilepath().contains("camera-ready")){
//		a = listOfAvailableAgents.get("sara");
//	}
//	
//	String dir = "/Users/saramagliacane/Dropbox/reconstructing-dataprep-provenance/";
//    String name = d.getCompleteFilepath().replace(dir, "");
////    name= name.replace("/", "_");
////    name= name.replace(" ", "_");
//    System.out.println("Created entity: "+ name);
//	
//	return createEntityAndGetEntityRef(name ,  name, d.getMimeType(), a, d.getMetadata().getModified(), d.getSize());
//}
//
//
//
//@Deprecated	
//private static EntityRef createEntityAndGetEntityRef(String name, String id, String mimeType, Agent author, Date time, long size){
//	EntityRef ref = new EntityRef();
//	
//	Entity entity = listOfAvailableEntities.get(id);
//	if(entity == null){
//		entity = new Entity();
//		entity.setLabel(name);
//		entity.setId(new QName(id));
////		entity.getType().add(mimeType);
////		entity.getAny().add(time);
////		entity.getAny().add(size);
//		listOfAvailableEntities.put(id, entity);
//	
//		Activity creation = new Activity();
//		creation.setId(new QName("creation"+ counter));
//		creation.setLabel("creation"+ counter);
//
//		
//		try {		
//		// convert Date to XMLGregorianCalendar
//			GregorianCalendar c = new GregorianCalendar();
//			c.setTime(time);
//			XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
//			creation.setStartTime(date2);
//		} catch (DatatypeConfigurationException e) {
//			e.printStackTrace();
//		}
//
//		listOfAvailableActivities.put("creation"+ counter++, creation);
//		
//		//listOfAvailableActivities.put("creation"+ name, creation);
//		
//		ActivityRef creationRef = new ActivityRef();
//		creationRef.setRef(creation.getId());
//		
//		AgentRef agentref = new AgentRef(); 
//		agentref.setRef(author.getId());
//		
//		
//		
//		WasAssociatedWith assoc = new WasAssociatedWith();
//		
//		assoc.setAgent(agentref);
//		assoc.getRole().add("author");
//		assoc.setActivity(creationRef);
//		//assoc.setId(new QName("author"));
//		
//		WasGeneratedBy generation = new WasGeneratedBy();
//		generation.setActivity(creationRef);
//		generation.setEntity(ref);
//		//generation.setId(new QName("generated"));
//		
//		listOfAvailableRelations.add(assoc);
//		listOfAvailableRelations.add(generation);
//	
//	}		
//
//	
//	
//
//	
//	ref.setRef(entity.getId());
//	return ref;
//}
	



//	private static void initDocNumbers(){
//		docNumbers.add("camera-ready_eswc2012-swpm-reconstructing_incompleteProvRelWork.bib");
//		docNumbers.add("eswc2012-swpm-reconstructing_reconstructing.bbl");
//		docNumbers.add("camera-ready_eswc2012-swpm-reconstructing_reconstructing.tex");
//		docNumbers.add("eswc2012-swpm-reconstructing_splncs03.bst");
//		docNumbers.add("eswc2012-swpm-reconstructing_reconstructing.tex");
//
//
//		docNumbers.add("eswc2012-swpm-reconstructing_example-reconstruction.tiff");
//		docNumbers.add("camera-ready_eswc2012-swpm-reconstructing_reconstructing.pdf");
//		docNumbers.add("ReconstructingProvenance.docx");
//		docNumbers.add("eswc2012-swpm-reconstructing_incomplete_provenance_state_of_the_art.docx");
//		docNumbers.add("eswc2012-swpm-reconstructing_incompleteprovenanceRelWork.bbl");
//	
//	
//		docNumbers.add("camera-ready_eswc2012-swpm-reconstructing_splncs03.bst");
//		docNumbers.add("eswc2012-swpm-reconstructing_incompleteprovenanceRelWork.tex");
//		docNumbers.add("eswc2012-swpm-reconstructing_incompleteProvRelWork.bib");
//		docNumbers.add("eswc2012-swpm-reconstructing_example-reconstruction.pdf");
//		docNumbers.add("camera-ready_eswc2012-swpm-reconstructing_example-reconstruction.tiff");
//		docNumbers.add("camera-ready_eswc2012-swpm-reconstructing_llncs.cls");
//		docNumbers.add("eswc2012-swpm-reconstructing_incompleteprovenanceRelWork.pdf");
//		docNumbers.add("eswc2012-swpm-reconstructing_reconstructing.pdf");
//		docNumbers.add("eswc2012-swpm-reconstructing_llncs.cls");
//		docNumbers.add("reconstruct.docx");
//		docNumbers.add("camera-ready_SWPM_camera_ready.pdf");
//		docNumbers.add("camera-ready_eswc2012-swpm-reconstructing_swpm2012-camera-ready.zip");
//		docNumbers.add("camera-ready_eswc2012-swpm-reconstructing_example-reconstruction.pdf");
//		docNumbers.add("camera-ready_eswc2012-swpm-reconstructing_reconstructing.bbl");
//		docNumbers.add("camera-ready_SWPM copyright_form.pdf");
//
//	}
//	
}	
	
