package nl.vu.recoprov.experiments;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import nl.vu.recoprov.CompletePipeline;
import nl.vu.recoprov.ProvDMtranslator;
import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.signalaggregators.WeightedSumAggregator;
import nl.vu.recoprov.signaldetectors.ImageSimilaritySignal;
import nl.vu.recoprov.signaldetectors.LuceneInverseSimilarity;
import nl.vu.recoprov.signaldetectors.LuceneSimilaritySignal;
import nl.vu.recoprov.signaldetectors.MatchTitleInContentSignal;
import nl.vu.recoprov.signaldetectors.MetadataSimilaritySignal;
import nl.vu.recoprov.signalfilters.BackwardTemporalFilter;
import nl.vu.recoprov.utils.TransitiveClosure;

import org.openprovenance.prov.xml.ProvFactory;

public class Experiment2 {
	
	public final static String dirTemp = "/Users/saramagliacane/Documents/workspace/recoprov/temp/Data2Semantics/Philips-Elsevier-Usecase/IDSA Qs/";
	public final static String dir = "/Data2Semantics/Philips-Elsevier-Usecase/IDSA Qs/";
	
	private static DependencyGraph baselineGraph = null;
	
	public static DependencyGraph createReferenceGraph(){
		
		
		ProvFactory factory = CorpusGeneratorBiomed.initFactory();

		CompletePipeline pipeline = new CompletePipeline(false, dirTemp);

		try {
			CorpusGeneratorBiomed.depGraph = pipeline.initDependencyGraph();
			pipeline.indexFiles(CorpusGeneratorBiomed.depGraph);

			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		CorpusGeneratorBiomed.createEntityFromDepGraph(factory, CorpusGeneratorBiomed.depGraph);
		CorpusGeneratorBiomed.manualAnnotation();
		CorpusGeneratorBiomed.convertToDot(factory);

		return CorpusGeneratorBiomed.depGraph;
	}
	
	public static DependencyGraph createGraph(){
		
		if (baselineGraph == null){
		
			baselineGraph = new DependencyGraph();
			CompletePipeline pipeline = new CompletePipeline(false, dirTemp);
			
			try {
				baselineGraph = pipeline.initDependencyGraph();
				pipeline.loadMetadaAndIndexes(baselineGraph);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		DependencyGraph copyOfBaseline = baselineGraph.copyGraph();
		
		
		
		return copyOfBaseline;
	}
	
	
	
	
	public static void main(String[] args) throws IOException,
			InterruptedException {

		File logfile = new File("log" + System.currentTimeMillis() + ".txt");
		FileWriter writer = new FileWriter(logfile);

		DependencyGraph depGraph = createReferenceGraph();

		// Compare with Transitive Closure
		DependencyGraph depGraphT = new TransitiveClosure()
				.aggregateSignals(depGraph);
		new ProvDMtranslator().translate(depGraphT, "graphCorpus_Trans.gv");

		// use only Lucene
		DependencyGraph depGraph1 = createGraph();
		depGraph1 = new LuceneSimilaritySignal().computeSignal(depGraph1);
		depGraph1 = new BackwardTemporalFilter().filterSignals(depGraph1);
		depGraph1 = new WeightedSumAggregator().aggregateSignals(depGraph1);
		writer.append("\n\nLucene - Filter: \n");
		String temp = depGraph.similarToGraph(depGraph1).toString();
		writer.append(temp);
		
		writer.append("\n" + depGraph1.toBooleanArray() + "\n");
		
		writer.flush();
		new ProvDMtranslator()
				.translate(depGraph1, "graphLucene_BackFilter.gv");

		
		
		writer.append("\n\nLucene- TransitiveClosure: \n");
		DependencyGraph depGraph11 = new TransitiveClosure()
				.aggregateSignals(depGraph1);
		writer.append((depGraphT.similarToGraph(depGraph11).toString()));
		new ProvDMtranslator().translate(depGraph11,
				"graphLucene_BackFilter_Trans.gv");
		
		depGraph1 = null;
		depGraph11 = null;

		// use Lucene and metadata similarity
		DependencyGraph depGraph2 = createGraph();
		depGraph2 = new LuceneSimilaritySignal().computeSignal(depGraph2);
		depGraph2 = new MetadataSimilaritySignal().computeSignal(depGraph2);
		depGraph2 = new BackwardTemporalFilter().filterSignals(depGraph2);
		depGraph2 = new WeightedSumAggregator().aggregateSignals(depGraph2);
		writer.append("\n\nLucene, Metadata - Filter: \n");
		writer.append(depGraph.similarToGraph(depGraph2).toString());
		writer.append("\n" + depGraph2.toBooleanArray() + "\n");
		new ProvDMtranslator().translate(depGraph2,
				"graphLuceneMetadata_BackFilter.gv");
		writer.flush();

		writer.append("\n\nLucene, Metadata- TransitiveClosure: \n");
		DependencyGraph depGraph12 = new TransitiveClosure()
				.aggregateSignals(depGraph2);
		System.out.println(depGraphT.similarToGraph(depGraph12).toString());
		writer.append((depGraphT.similarToGraph(depGraph12).toString()));
		new ProvDMtranslator().translate(depGraph12,
				"graphLuceneMetadata_BackFilter_Trans.gv");
		
		depGraph2 = null;
		depGraph12 = null;

		DependencyGraph depGraph3 = createGraph();
		depGraph3 = new LuceneSimilaritySignal().computeSignal(depGraph3);
		depGraph3 = new MetadataSimilaritySignal().computeSignal(depGraph3);
		depGraph3 = new LuceneInverseSimilarity().computeSignal(depGraph3);
		depGraph3 = new BackwardTemporalFilter().filterSignals(depGraph3);
		depGraph3 = new WeightedSumAggregator().aggregateSignals(depGraph3);
		writer.append("\n\nLucene, Metadata, Inverse - Filter: \n");
		writer.append(depGraph.similarToGraph(depGraph3).toString());
		writer.append("\n" + depGraph3.toBooleanArray() + "\n");
		new ProvDMtranslator().translate(depGraph3,
				"graphLuceneMetadata_Inverse_BackFilter.gv");
		writer.flush();

		writer.append("\n\nLucene, Metadata- TransitiveClosure: \n");
		DependencyGraph depGraph13 = new TransitiveClosure()
				.aggregateSignals(depGraph3);
		System.out.println(depGraphT.similarToGraph(depGraph13).toString());
		writer.append((depGraphT.similarToGraph(depGraph13).toString()));
		new ProvDMtranslator().translate(depGraph13,
				"graphLuceneMetadataInverse_BackFilter_Trans.gv");
		
		depGraph3 = null;
		depGraph13 = null;
		
		System.gc();

		DependencyGraph depGraph4 = createGraph();
		depGraph4 = new LuceneSimilaritySignal().computeSignal(depGraph4);
		depGraph4 = new MetadataSimilaritySignal().computeSignal(depGraph4);
		depGraph4 = new LuceneInverseSimilarity().computeSignal(depGraph4);
		depGraph4 = new ImageSimilaritySignal().computeSignal(depGraph4);
		depGraph4 = new MatchTitleInContentSignal().computeSignal(depGraph4);
		depGraph4 = new BackwardTemporalFilter().filterSignals(depGraph4);
		depGraph4 = new WeightedSumAggregator().aggregateSignals(depGraph4);
		writer.append("\n\nLucene, Metadata, Image, Inverse - Filter: \n");
		writer.append(depGraph.similarToGraph(depGraph4).toString());
		writer.append("\n" + depGraph4.toBooleanArray() + "\n");
		new ProvDMtranslator().translate(depGraph4,
				"graphLuceneMetadataImageInverse_BackFilter.gv");
		writer.flush();

		writer.append("\n\nLucene, Metadata, Image, Inverse - TransitiveClosure: \n");
		DependencyGraph depGraph14 = new TransitiveClosure()
				.aggregateSignals(depGraph4);
		System.out.println(depGraphT.similarToGraph(depGraph14).toString());
		writer.append((depGraphT.similarToGraph(depGraph14).toString()));
		new ProvDMtranslator().translate(depGraph14,
				"graphLuceneMetadataInverseImage_BackFilter_Trans.gv");

		// transitive reduction

		// String cmd = "tred " + ;
		// Runtime run = Runtime.getRuntime();
		// Process pr = run.exec(cmd);
		// pr.waitFor();
		// BufferedReader buf = new BufferedReader(new
		// InputStreamReader(pr.getInputStream()));
		// String line = "";
		// while ((line=buf.readLine())!=null) {
		// System.out.println(line);
		// }

		// Compare with Transitive Closure - currently not working
		// DependencyGraph depGraphTR = new
		// TransitiveReductionFilter().filterSignals(depGraphT);
		// new ProvDMtranslator().translate(depGraphTR,
		// "graphCorpus_TransR.gv");
		//
		// writer.append("\n\nLucene, Metadata, Match title and Image- TransitiveClosure - Transitive Reduction: \n");
		// depGraph14 = new
		// TransitiveReductionFilter().filterSignals(depGraph14);
		// writer.append(depGraphTR.similarToGraph(depGraph14).toString());
		// new ProvDMtranslator().translate(depGraph14,
		// "graphLuceneMetadataTitleImage_BackFilter_TransR.gv");
		//
		// writer.append("\n\nLucene, Metadata, Match title and Image- TransitiveClosure - Transitive Reduction - compared to original: \n");
		// writer.append(depGraph.similarToGraph(depGraph14).toString());

		writer.flush();
		writer.close();

	}


 		


}
