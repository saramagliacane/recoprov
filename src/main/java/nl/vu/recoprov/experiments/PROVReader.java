package nl.vu.recoprov.experiments;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;

import nl.vu.recoprov.CompletePipeline;
import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.baseclasses.DependencyNode;
import nl.vu.recoprov.signalaggregators.WeightedSumAggregator;
import nl.vu.recoprov.utils.ConfigurationDefaults;
import org.openprovenance.prov.dot.ProvToDot;
import org.openprovenance.prov.json.Converter;
import org.openprovenance.prov.xml.Activity;
import org.openprovenance.prov.xml.Document;
import org.openprovenance.prov.xml.Entity;
import org.openprovenance.prov.xml.ProvFactory;
import org.openprovenance.prov.xml.StatementOrBundle;
import org.openprovenance.prov.xml.WasDerivedFrom;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;


import com.dropbox.client2.exception.DropboxException;


public class PROVReader {

	private static HashMap<String, Entity> listOfAvailableEntities = new HashMap<String, Entity> ();
//	private static HashMap<String, Object> listOfAvailableRelations =  new HashMap<String, Object> ();
//	private static  HashMap<String,Activity> listOfAvailableActivities = new  LinkedHashMap<String, Activity> ();
//	private static int counter = 0;
	private  Logger logger;
	
	private String dir;
	private String jsonfile;
	private String src = "";
	private String susp = "";
	private Document provdoc = new Document();
	

	

	public PROVReader(String dir, String jsonfile){
		this.dir = dir;
		this.jsonfile = jsonfile;
		logger = LoggerFactory
				.getLogger("nl.vu.recoprov.experiments.PROVReader");
	}
	
	public PROVReader(String dir, String jsonfile, String src, String susp){
		this.dir = dir;
		this.jsonfile = jsonfile;
		this.src = src;
		this.susp = susp;
		logger = LoggerFactory
				.getLogger("nl.vu.recoprov.experiments.PROVReader");
	}
	
	public DependencyGraph generateDepGraph() throws DropboxException, IOException {

		logger.info("Reading PROV description of folder: {} in JSON {}: ", dir, jsonfile);
		
		ProvFactory factory = initFactory();
		CompletePipeline pipeline = new CompletePipeline(false, dir);
		DependencyGraph depGraph = pipeline.initDependencyGraph();

		// and get the lucene identifiers
		pipeline.indexFiles(depGraph);
		
		createEntityFromDepGraph(factory, depGraph);

		readJSON(jsonfile, depGraph, factory);

		//convertToDot(factory);

		return depGraph;

	}
		
	
	private ProvFactory initFactory() {
		ProvFactory factory = ProvFactory.getFactory();
		Hashtable<String, String> namespace = new Hashtable<String, String>();	
		namespace.put("_", "");
		factory.setNamespaces(namespace);		
		return factory;
	}
	
	

	
	private void createEntityFromDepGraph(ProvFactory factory,
			DependencyGraph depGraph) {
		for (String name : depGraph.keySet()) {
			// create an entity for each node in the dependency graph
			if (ConfigurationDefaults.ignoreFile(name))
				continue;
			// for each node take the edges
			DependencyNode d = depGraph.get(name);
			createEntity(factory, d);

		}
	}


	private void createEntity(ProvFactory factory, DependencyNode d) {

		String name = d.getCompleteFilepath();
		// name = name.replace(dir, "");
		String id = "" + d.getLuceneDocNumber();

		Entity entity = listOfAvailableEntities.get(name);
		if (entity == null) {
			entity = factory.newEntity(name, id);
			listOfAvailableEntities.put(name, entity);

		}

	}
	



	private void readJSON(String jsonfile, DependencyGraph depGraph, ProvFactory factory) {
		Converter conv = new Converter();

		File sourcedir = new File(dir, src);
		File suspdir = new File(dir, susp);
		
		try {
			provdoc = conv.readDocument(jsonfile);
		} catch (Exception e) {
			// ignore}
		}
		List<StatementOrBundle> provlist = provdoc
				.getEntityAndActivityAndWasGeneratedBy();
		for (StatementOrBundle s : provlist) {
			// assume are all statements
			// if (s instanceof Entity){
			// String entityName = ((Entity) s).getId().toString();
			// entityName = entityName.replace(namespace, "");
			// System.out.println("Entity: " + entityName);
			// }
			//
			// if (s instanceof Activity){
			// String actName = ((Activity) s).getId().toString();
			// actName = actName.replace(namespace, "");
			// System.out.println("Activity: " + actName);
			// }

			// if (s instanceof WasGeneratedBy){
			// String actName = ((WasGeneratedBy) s).getId().toString();
			// actName = actName.replace(namespace, "");
			// System.out.println("WasGeneratedBy: " + actName);
			// }

			if (s instanceof WasDerivedFrom) {
				String used = ((WasDerivedFrom) s).getUsedEntity().getRef()
						.getLocalPart();
				String generated = ((WasDerivedFrom) s).getGeneratedEntity()
						.getRef().getLocalPart();
				// System.out.println("WasDerivedFrom: " + used + " ->"+
				// generated);

				File usedFile = new File(sourcedir, used);
				File genFile = new File(suspdir, generated);
				
				depGraph.addEdge(depGraph.get(usedFile.getAbsolutePath()),depGraph.get(genFile.getAbsolutePath()),
						WeightedSumAggregator.FINAL_SCORE, 1.0);
			}

		}

	}




	private void convertToDot(ProvFactory factory) {

		ProvToDot provtodot = new ProvToDot();
//		Document container;
//		container = factory.newDocument(listOfAvailableActivities.values(),
//				listOfAvailableEntities.values(), new LinkedList<Agent>(),
//				new LinkedList<Statement>());

		try {	
			provtodot.convert(provdoc, new File("graphCorpus.gv"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}


}	

	
/*********************************************************************	
//	 Old code
	
	
	
	
//	public void readPROVN(String provnfile, DependencyGraph depGraph, ProvFactory factory) {
//		Converter conv = new Converter();
//
//		File sourcedir = new File(dir, sourceFolder);
//		File suspdir = new File(dir, suspiciousFolder);
//		
//		try {
//			provdoc = conv.readDocument(provnfile);
//		} catch (Exception e) {
//			// ignore}
//		}
//		List<StatementOrBundle> provlist = provdoc
//				.getEntityAndActivityAndWasGeneratedBy();
//		for (StatementOrBundle s : provlist) {
//			// assume are all statements
//			// if (s instanceof Entity){
//			// String entityName = ((Entity) s).getId().toString();
//			// entityName = entityName.replace(namespace, "");
//			// System.out.println("Entity: " + entityName);
//			// }
//			//
//			// if (s instanceof Activity){
//			// String actName = ((Activity) s).getId().toString();
//			// actName = actName.replace(namespace, "");
//			// System.out.println("Activity: " + actName);
//			// }
//
//			// if (s instanceof WasGeneratedBy){
//			// String actName = ((WasGeneratedBy) s).getId().toString();
//			// actName = actName.replace(namespace, "");
//			// System.out.println("WasGeneratedBy: " + actName);
//			// }
//
//			if (s instanceof WasDerivedFrom) {
//				String used = ((WasDerivedFrom) s).getUsedEntity().getRef()
//						.getLocalPart();
//				String generated = ((WasDerivedFrom) s).getGeneratedEntity()
//						.getRef().getLocalPart();
//				// System.out.println("WasDerivedFrom: " + used + " ->"+
//				// generated);
//
//				File usedFile = new File(sourcedir, used);
//				File genFile = new File(suspdir, generated);
//				
//				
//				depGraph.addEdge(depGraph.get(genFile.getAbsolutePath()),
//						depGraph.get(usedFile.getAbsolutePath()),
//						WeightedSumAggregator.FINAL_SCORE, 1.0);
//			}
//
//		}
//
//	}
*/






	
