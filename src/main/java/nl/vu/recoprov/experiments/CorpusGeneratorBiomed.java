package nl.vu.recoprov.experiments;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import nl.vu.recoprov.CompletePipeline;
import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.baseclasses.DependencyNode;
import nl.vu.recoprov.signalaggregators.WeightedSumAggregator;
import nl.vu.recoprov.utils.ConfigurationDefaults;

import org.openprovenance.prov.dot.ProvToDot;
import org.openprovenance.prov.xml.Agent;
import org.openprovenance.prov.xml.Document;
import org.openprovenance.prov.xml.Entity;
import org.openprovenance.prov.xml.EntityRef;
import org.openprovenance.prov.xml.ProvFactory;
import org.openprovenance.prov.xml.WasDerivedFrom;

// prov to dot uses the XML representation

//import org.openprovenance.prov.rdf.Entity;
//import org.openprovenance.prov.rdf.Derivation;
//import org.openprovenance.prov.rdf.Revision;
//import org.openprovenance.prov.rdf.EntityInvolvement;
//import org.openprovenance.prov.rdf.TimeInstant;

/// track the time in which an entity was generated


// further annotation is in Notes - an entity can be linked to a note by hasAnnotation

// entity attributes - [attr1=val] prov:type="document'
// derivation attributes - prov:type = "physical transform" wasDerivedFrom
// types of derivation:
// revision - newer and older   wasRevisionOf
// quotation - quote(partial copy), original     wasQuotedFrom
// original source  - derived (entity), source   hadOriginalSource

// derivation is a particular form of Trace - tracedTo - entity, ancestor

// specialization is a more constrained entity  specializationOf

// attrib - prov:label, prov:type, prov:value (score)


public class CorpusGeneratorBiomed {

	static HashMap<String, Entity> listOfAvailableEntities = new HashMap<String, Entity> ();
	static HashMap<String, Object> listOfAvailableRelations =  new HashMap<String, Object> ();
	static  HashMap<String,Agent> listOfAvailableAgents = new  LinkedHashMap<String, Agent> ();


	
//	static 		ArrayList<String> docNumbers = new ArrayList<String>();
	static int counter = 0;
	
	static Boolean trackCreation = false;
	
	public static DependencyGraph depGraph;
	public final static String dir = "/Users/saramagliacane/Documents/workspace/recoprov/temp/Data2Semantics/Philips-Elsevier-Usecase/IDSA Qs/";
	//public final static String dir = "/Data2Semantics/Philips-Elsevier-Usecase/IDSA Qs/";
	
	public static void main(String args[]) throws Exception {
		
		
		ProvFactory factory = initFactory();
		CompletePipeline pipeline = new CompletePipeline(false, dir);
		
		depGraph = pipeline.initDependencyGraph(); 
		
		

		// System.out.println("### THIRD PHASE: Index the contents with Apache Lucene. \n");
		pipeline.indexFiles(depGraph);
		

		createEntityFromDepGraph(factory, depGraph);
		
		manualAnnotation();
		
		convertToDot(factory);

		//System.out.println(depGraph);
		
		
		
	}
		
	
	public static ProvFactory initFactory() {
		ProvFactory factory = ProvFactory.getFactory();

		//initDocNumbers();
		
		Hashtable<String, String> namespace = new Hashtable<String, String>();
		//namespace.put("_", "https://www.dropbox.com/home/reconstructing-dataprep-provenance/");		
		namespace.put("_", "");
		factory.setNamespaces(namespace);

		
		return factory;
	}
	
	

	
	
	private static void addAgent(ProvFactory factory, String name){
		
		Agent agent = factory.newAgent(name);
		listOfAvailableAgents.put(name, agent);

		

	}

	
public static void createEntityFromDepGraph(ProvFactory factory, DependencyGraph depGraph){
	for (String name: depGraph.keySet()){
		// create an entity for each node in the dependency graph
		if(ConfigurationDefaults.ignoreFile(name))
			continue;
		// for each node take the edges
		DependencyNode d = depGraph.get(name);
		createEntity(factory, d);
		
		
	}
}


public static void createEntity(ProvFactory factory, DependencyNode d){

	
	String name = d.getCompleteFilepath().replace(dir, "");
	

	
	String sanitizedname= name.replace("/", "_");
	sanitizedname= sanitizedname.replace(" ", "_");
	
	String id = ""+d.getLuceneDocNumber();
	
	//System.out.println("Created entity: "+ sanitizedname);
	
	Entity entity = listOfAvailableEntities.get(name);
	if(entity == null){
		entity = factory.newEntity(sanitizedname,id );
		listOfAvailableEntities.put(name, entity);
		//System.out.println(name);
	
	}		

	
	

	
}
	



//private static void createGenerationActivityAndEntity(String activity, String entityname){
//	ActivityRef actref= createActivityAndGetRef(activity);
//	
//	Entity entity = 	listOfAvailableEntities.get(entityname);
//	EntityRef entityref = new EntityRef();
//	entityref.setRef(entity.getId());
//	
//	WasGeneratedBy generation = new WasGeneratedBy();
//	generation.setActivity(actref);
//	generation.setEntity(entityref);
//
//	//generation.setId(new QName("generated"));
//	
//	listOfAvailableRelations.add(generation);
//}

private static void createDerivationActivityAndEntity( String activity, String usedEntity, String generatedEntity){
	//System.out.print(usedEntity);
	
	
	Entity used = 	listOfAvailableEntities.get(usedEntity);
	EntityRef usedref = new EntityRef();
	usedref.setRef(used.getId());
	
	Entity generated = 	listOfAvailableEntities.get(generatedEntity);
	EntityRef generatedref = new EntityRef();
	generatedref.setRef(generated.getId());
	



	
	WasDerivedFrom derivation = new WasDerivedFrom();
	derivation.setGeneratedEntity(generatedref);
	derivation.setUsedEntity(usedref);
	derivation.getType().add(activity);
	
	listOfAvailableRelations.put(activity + usedEntity+ generatedEntity,derivation);
	
//	derivation.setId(new QName("generated"));

	//depGraph.addEdge(docNumbers.indexOf(generatedEntity), docNumbers.indexOf(usedEntity),WeightedSumAggregator.FINAL_SCORE, 1.0);
	
	//System.out.println(dir+generatedEntity);
	depGraph.addEdge(depGraph.get(dir+usedEntity), depGraph.get(dir+generatedEntity),WeightedSumAggregator.FINAL_SCORE, 1.0);
	

}


public static void manualAnnotation(){

	String Q02 ="Q02/";
	String Q10 ="Q10/";
	
	// ############ Basis
	createDerivationActivityAndEntity("revision",  "Practice_Guideline_fever_in_neutropenia_2010_Clin_Infect_Dis.-2011-Freifeld-e56-93_93102d93bd4.pdf", "Practice_Guideline_fever_in_neutropenia_2010_Clin_Infect_Dis.-2011-Freifeld-e56-93_dc302d93bd4.pdf");
	
	String q02_50_new = Q02+"50_93902d93bd4.pdf";
	
	//empty derivations
//	createDerivationActivityAndEntity("revision", Q02+"47_93402d93bd4.pdf", Q02+"47_93a02d93bd4.pdf");
//	createDerivationActivityAndEntity("revision", Q02+"50_93502d93bd4.pdf", q02_50_new);
//	createDerivationActivityAndEntity("revision", Q02+"51_93602d93bd4.pdf", Q02+"51_93802d93bd4.pdf");
//	createDerivationActivityAndEntity("revision",  Q02+"52_93702d93bd4.pdf", Q02+"52_93b02d93bd4.pdf");
	
	createDerivationActivityAndEntity("revision", Q02+"EvidenceQII_94402d93bd4.xlsx", Q02+"EvidenceQII_94c02d93bd4.xlsx");
	//createDerivationActivityAndEntity("revision", Q02+"EvidenceQII_94402d93bd4.xlsx", Q02+"EvidenceQII_99e02d93bd4.xlsx");
	createDerivationActivityAndEntity("revision", Q02+"EvidenceQII_94402d93bd4.xlsx", Q02 +"EvidenceQII_99f02d93bd4.xlsx");
	
	//createDerivationActivityAndEntity("revision",  Q02+"EvidenceQII_94c02d93bd4.xlsx", Q02+"EvidenceQII_99e02d93bd4.xlsx");
	createDerivationActivityAndEntity("revision",  Q02+"EvidenceQII_94c02d93bd4.xlsx", Q02 +"EvidenceQII_99f02d93bd4.xlsx");
	
	//createDerivationActivityAndEntity("revision",  Q02+"EvidenceQII_99e02d93bd4.xlsx", Q02 +"EvidenceQII_99f02d93bd4.xlsx");
	

	
	
	// cluster 1 - blood
	createDerivationActivityAndEntity("cite", Q02+"47_93a02d93bd4.pdf", q02_50_new);
	createDerivationActivityAndEntity("cite", Q02+"52_93b02d93bd4.pdf", q02_50_new);
	createDerivationActivityAndEntity("cite", Q02+"52_93b02d93bd4.pdf", Q02+"51_93802d93bd4.pdf");	
	createDerivationActivityAndEntity("cite", Q02+"52_93b02d93bd4.pdf", Q02+"49_92502d93bd4.pdf");
	createDerivationActivityAndEntity("cite", q02_50_new, Q02+"49_92502d93bd4.pdf");
	
	// cluster 2 - 55,56,57
	createDerivationActivityAndEntity("cite", Q02+"55_92602d93bd4.pdf", Q02+"56_92702d93bd4.pdf");
	createDerivationActivityAndEntity("cite", Q02+"56_92702d93bd4.pdf", Q02+"57_92402d93bd4.pdf");
	createDerivationActivityAndEntity("cite", Q02+"55_92602d93bd4.pdf", Q02+"57_92402d93bd4.pdf");
	
	// cluster 3 - filgrastim
	createDerivationActivityAndEntity("cite", Q10+"301_93302d93bd4.pdf", Q10+"303_92a02d93bd4.pdf");
	createDerivationActivityAndEntity("cite", Q10+"301_93302d93bd4.pdf", Q10+"298_92902d93bd4.pdf");
	createDerivationActivityAndEntity("cite", Q10+"303_92a02d93bd4.pdf", Q10+"298_92902d93bd4.pdf");
	createDerivationActivityAndEntity("cite", Q10+"303_92a02d93bd4.pdf", Q10+"292 update 2010_92f02d93bd4.pdf");
	createDerivationActivityAndEntity("cite", Q10+"303_92a02d93bd4.pdf", Q10+"292_92e02d93bd4.pdf");
	createDerivationActivityAndEntity("cite", Q10+"303_92a02d93bd4.pdf", Q10+"293_93202d93bd4.pdf");
	createDerivationActivityAndEntity("cite", Q10+"303_92a02d93bd4.pdf", Q10+"294_93002d93bd4.pdf");
	
	createDerivationActivityAndEntity("cite", Q10+"292_92e02d93bd4.pdf", Q10+"294_93002d93bd4.pdf");
	createDerivationActivityAndEntity("cite", Q10+"293_93202d93bd4.pdf", Q10+"294_93002d93bd4.pdf");
	createDerivationActivityAndEntity("cite", Q10+"293_93202d93bd4.pdf", Q10+"296_92d02d93bd4.pdf");	
	createDerivationActivityAndEntity("cite", Q10+"293_93202d93bd4.pdf", Q10+"299_92b02d93bd4.pdf");	
	createDerivationActivityAndEntity("cite", Q10+"293_93202d93bd4.pdf", Q10+"297_92802d93bd4.pdf");	
	
	createDerivationActivityAndEntity("cite", Q10+"295_92c02d93bd4.pdf", Q10+"297_92802d93bd4.pdf");	
	createDerivationActivityAndEntity("cite", Q10+"295_92c02d93bd4.pdf", Q10+"293_93202d93bd4.pdf");	
	createDerivationActivityAndEntity("cite", Q10+"295_92c02d93bd4.pdf", Q10+"299_92b02d93bd4.pdf");	
	createDerivationActivityAndEntity("cite", Q10+"295_92c02d93bd4.pdf", Q10+"296_92d02d93bd4.pdf");
	
	createDerivationActivityAndEntity("cite",  Q10+"298_92902d93bd4.pdf", Q10+ "292 update 2010_92f02d93bd4.pdf");
	createDerivationActivityAndEntity("cite",  Q10+"298_92902d93bd4.pdf", Q10+ "299_92b02d93bd4.pdf");
	
	createDerivationActivityAndEntity("cite",  Q10+"296_92d02d93bd4.pdf", Q10+ "299_92b02d93bd4.pdf");
	createDerivationActivityAndEntity("cite",  Q10+"296_92d02d93bd4.pdf", Q10+ "294_93002d93bd4.pdf");
	
	
	
	
	createDerivationActivityAndEntity("basedOn",  "Practice_Guideline_fever_in_neutropenia_2010_Clin_Infect_Dis.-2011-Freifeld-e56-93_dc302d93bd4.pdf", Q10+ "EvidenceQX_d8302d93bd4.xlsx");
	createDerivationActivityAndEntity("cite",  Q10+"292 update 2010_92f02d93bd4.pdf", Q10+ "EvidenceQX_d8302d93bd4.xlsx");
	createDerivationActivityAndEntity("cite",  Q10+"292_92e02d93bd4.pdf", Q10+ "EvidenceQX_d8302d93bd4.xlsx");
	createDerivationActivityAndEntity("cite",  Q10+"293_93202d93bd4.pdf", Q10+ "EvidenceQX_d8302d93bd4.xlsx");
	createDerivationActivityAndEntity("cite",  Q10+"294_93002d93bd4.pdf", Q10+ "EvidenceQX_d8302d93bd4.xlsx");
	createDerivationActivityAndEntity("cite",  Q10+"295_92c02d93bd4.pdf", Q10+ "EvidenceQX_d8302d93bd4.xlsx");
	createDerivationActivityAndEntity("cite",  Q10+"296_92d02d93bd4.pdf", Q10+ "EvidenceQX_d8302d93bd4.xlsx");
	createDerivationActivityAndEntity("cite",  Q10+"297_92802d93bd4.pdf", Q10+ "EvidenceQX_d8302d93bd4.xlsx");
	createDerivationActivityAndEntity("cite",  Q10+"298_92902d93bd4.pdf", Q10+ "EvidenceQX_d8302d93bd4.xlsx");
	createDerivationActivityAndEntity("cite",  Q10+"299_92b02d93bd4.pdf", Q10+ "EvidenceQX_d8302d93bd4.xlsx");
	createDerivationActivityAndEntity("cite",  Q10+"301_93302d93bd4.pdf", Q10+ "EvidenceQX_d8302d93bd4.xlsx");
	createDerivationActivityAndEntity("cite",  Q10+"303_92a02d93bd4.pdf", Q10+ "EvidenceQX_d8302d93bd4.xlsx");
	
	
	createDerivationActivityAndEntity("basedOn", "Practice_Guideline_fever_in_neutropenia_2010_Clin_Infect_Dis.-2011-Freifeld-e56-93_dc302d93bd4.pdf", Q02+"EvidenceQII_99f02d93bd4.xlsx");
	createDerivationActivityAndEntity("cite", Q02+"47_93a02d93bd4.pdf", Q02+"EvidenceQII_99f02d93bd4.xlsx");
	createDerivationActivityAndEntity("cite", Q02+"51_93802d93bd4.pdf", Q02+"EvidenceQII_99f02d93bd4.xlsx");
	createDerivationActivityAndEntity("cite", Q02+"52_93b02d93bd4.pdf", Q02+"EvidenceQII_99f02d93bd4.xlsx");	
	createDerivationActivityAndEntity("cite", Q02+"49_92502d93bd4.pdf", Q02+"EvidenceQII_99f02d93bd4.xlsx");
	createDerivationActivityAndEntity("cite", q02_50_new, Q02+"EvidenceQII_99f02d93bd4.xlsx");
	createDerivationActivityAndEntity("cite", Q02+"55_92602d93bd4.pdf", Q02+"EvidenceQII_99f02d93bd4.xlsx");
	createDerivationActivityAndEntity("cite", Q02+"56_92702d93bd4.pdf", Q02+"EvidenceQII_99f02d93bd4.xlsx");
	createDerivationActivityAndEntity("cite", Q02+"57_92402d93bd4.pdf", Q02+"EvidenceQII_99f02d93bd4.xlsx");
	
//	createDerivationActivityAndEntity("basedOn", "Practice_Guideline_fever_in_neutropenia_2010_Clin_Infect_Dis.-2011-Freifeld-e56-93_dc302d93bd4.pdf",Q02+"EvidenceQII_99e02d93bd4.xlsx");
//	createDerivationActivityAndEntity("cite", Q02+"47_93a02d93bd4.pdf", Q02+"EvidenceQII_99e02d93bd4.xlsx");
//	createDerivationActivityAndEntity("cite", Q02+"51_93802d93bd4.pdf", Q02+"EvidenceQII_99e02d93bd4.xlsx");
//	createDerivationActivityAndEntity("cite", Q02+"52_93b02d93bd4.pdf", Q02+"EvidenceQII_99e02d93bd4.xlsx");	
//	createDerivationActivityAndEntity("cite", Q02+"49_92502d93bd4.pdf", Q02+"EvidenceQII_99e02d93bd4.xlsx");
//	createDerivationActivityAndEntity("cite", Q02+"50_93902d93bd4.pdf", Q02+"EvidenceQII_99e02d93bd4.xlsx");
//	createDerivationActivityAndEntity("cite", Q02+"55_92602d93bd4.pdf", Q02+"EvidenceQII_99e02d93bd4.xlsx");
//	createDerivationActivityAndEntity("cite", Q02+"56_92702d93bd4.pdf", Q02+"EvidenceQII_99e02d93bd4.xlsx");
//	createDerivationActivityAndEntity("cite", Q02+"57_92402d93bd4.pdf", Q02+"EvidenceQII_99e02d93bd4.xlsx");
	
	createDerivationActivityAndEntity("basedOn", "Practice_Guideline_fever_in_neutropenia_2010_Clin_Infect_Dis.-2011-Freifeld-e56-93_dc302d93bd4.pdf",Q02+"EvidenceQII_94c02d93bd4.xlsx");
	createDerivationActivityAndEntity("cite", Q02+"47_93a02d93bd4.pdf", Q02+"EvidenceQII_94c02d93bd4.xlsx");
	createDerivationActivityAndEntity("cite", Q02+"51_93802d93bd4.pdf", Q02+"EvidenceQII_94c02d93bd4.xlsx");
	createDerivationActivityAndEntity("cite", Q02+"52_93b02d93bd4.pdf", Q02+"EvidenceQII_94c02d93bd4.xlsx");	
	createDerivationActivityAndEntity("cite", Q02+"49_92502d93bd4.pdf", Q02+"EvidenceQII_94c02d93bd4.xlsx");
	createDerivationActivityAndEntity("cite", q02_50_new, Q02+"EvidenceQII_94c02d93bd4.xlsx");
	createDerivationActivityAndEntity("cite", Q02+"55_92602d93bd4.pdf", Q02+"EvidenceQII_94c02d93bd4.xlsx");
	createDerivationActivityAndEntity("cite", Q02+"56_92702d93bd4.pdf", Q02+"EvidenceQII_94c02d93bd4.xlsx");
	createDerivationActivityAndEntity("cite", Q02+"57_92402d93bd4.pdf", Q02+"EvidenceQII_94c02d93bd4.xlsx");
	
	
	createDerivationActivityAndEntity("basedOn", "Practice_Guideline_fever_in_neutropenia_2010_Clin_Infect_Dis.-2011-Freifeld-e56-93_dc302d93bd4.pdf",Q02+"EvidenceQII_94402d93bd4.xlsx");
	createDerivationActivityAndEntity("cite", Q02+"47_93a02d93bd4.pdf", Q02+"EvidenceQII_94402d93bd4.xlsx");
	createDerivationActivityAndEntity("cite", Q02+"51_93802d93bd4.pdf", Q02+"EvidenceQII_94402d93bd4.xlsx");
	createDerivationActivityAndEntity("cite", Q02+"52_93b02d93bd4.pdf", Q02+"EvidenceQII_94402d93bd4.xlsx");	
	createDerivationActivityAndEntity("cite", Q02+"49_92502d93bd4.pdf", Q02+"EvidenceQII_94402d93bd4.xlsx");
	createDerivationActivityAndEntity("cite", q02_50_new, Q02+"EvidenceQII_94402d93bd4.xlsx");

	
	createDerivationActivityAndEntity("cite", Q02+"47_93a02d93bd4.pdf", "Practice_Guideline_fever_in_neutropenia_2010_Clin_Infect_Dis.-2011-Freifeld-e56-93_dc302d93bd4.pdf");
	createDerivationActivityAndEntity("cite", Q02+"51_93802d93bd4.pdf", "Practice_Guideline_fever_in_neutropenia_2010_Clin_Infect_Dis.-2011-Freifeld-e56-93_dc302d93bd4.pdf");
	createDerivationActivityAndEntity("cite", Q02+"52_93b02d93bd4.pdf", "Practice_Guideline_fever_in_neutropenia_2010_Clin_Infect_Dis.-2011-Freifeld-e56-93_dc302d93bd4.pdf");
	createDerivationActivityAndEntity("cite", Q02+"49_92502d93bd4.pdf", "Practice_Guideline_fever_in_neutropenia_2010_Clin_Infect_Dis.-2011-Freifeld-e56-93_dc302d93bd4.pdf");
	createDerivationActivityAndEntity("cite", q02_50_new, "Practice_Guideline_fever_in_neutropenia_2010_Clin_Infect_Dis.-2011-Freifeld-e56-93_dc302d93bd4.pdf");
	createDerivationActivityAndEntity("cite", Q02+"55_92602d93bd4.pdf", "Practice_Guideline_fever_in_neutropenia_2010_Clin_Infect_Dis.-2011-Freifeld-e56-93_dc302d93bd4.pdf");
	createDerivationActivityAndEntity("cite", Q02+"56_92702d93bd4.pdf", "Practice_Guideline_fever_in_neutropenia_2010_Clin_Infect_Dis.-2011-Freifeld-e56-93_dc302d93bd4.pdf");
	createDerivationActivityAndEntity("cite", Q02+"57_92402d93bd4.pdf", "Practice_Guideline_fever_in_neutropenia_2010_Clin_Infect_Dis.-2011-Freifeld-e56-93_dc302d93bd4.pdf");
	
	
	createDerivationActivityAndEntity("cite",  Q10+"292 update 2010_92f02d93bd4.pdf","Practice_Guideline_fever_in_neutropenia_2010_Clin_Infect_Dis.-2011-Freifeld-e56-93_dc302d93bd4.pdf");
	createDerivationActivityAndEntity("cite",  Q10+"292_92e02d93bd4.pdf", "Practice_Guideline_fever_in_neutropenia_2010_Clin_Infect_Dis.-2011-Freifeld-e56-93_dc302d93bd4.pdf");
	createDerivationActivityAndEntity("cite",  Q10+"293_93202d93bd4.pdf", "Practice_Guideline_fever_in_neutropenia_2010_Clin_Infect_Dis.-2011-Freifeld-e56-93_dc302d93bd4.pdf");
	createDerivationActivityAndEntity("cite",  Q10+"294_93002d93bd4.pdf", "Practice_Guideline_fever_in_neutropenia_2010_Clin_Infect_Dis.-2011-Freifeld-e56-93_dc302d93bd4.pdf");
	createDerivationActivityAndEntity("cite",  Q10+"295_92c02d93bd4.pdf", "Practice_Guideline_fever_in_neutropenia_2010_Clin_Infect_Dis.-2011-Freifeld-e56-93_dc302d93bd4.pdf");
	createDerivationActivityAndEntity("cite",  Q10+"296_92d02d93bd4.pdf", "Practice_Guideline_fever_in_neutropenia_2010_Clin_Infect_Dis.-2011-Freifeld-e56-93_dc302d93bd4.pdf");
	createDerivationActivityAndEntity("cite",  Q10+"297_92802d93bd4.pdf", "Practice_Guideline_fever_in_neutropenia_2010_Clin_Infect_Dis.-2011-Freifeld-e56-93_dc302d93bd4.pdf");
	createDerivationActivityAndEntity("cite",  Q10+"298_92902d93bd4.pdf", "Practice_Guideline_fever_in_neutropenia_2010_Clin_Infect_Dis.-2011-Freifeld-e56-93_dc302d93bd4.pdf");
	createDerivationActivityAndEntity("cite",  Q10+"299_92b02d93bd4.pdf", "Practice_Guideline_fever_in_neutropenia_2010_Clin_Infect_Dis.-2011-Freifeld-e56-93_dc302d93bd4.pdf");
	createDerivationActivityAndEntity("cite",  Q10+"301_93302d93bd4.pdf", "Practice_Guideline_fever_in_neutropenia_2010_Clin_Infect_Dis.-2011-Freifeld-e56-93_dc302d93bd4.pdf");
	createDerivationActivityAndEntity("cite",  Q10+"303_92a02d93bd4.pdf", "Practice_Guideline_fever_in_neutropenia_2010_Clin_Infect_Dis.-2011-Freifeld-e56-93_dc302d93bd4.pdf");
	
	
	String guidelineOld = "Practice_Guideline_fever_in_neutropenia_2010_Clin_Infect_Dis.-2011-Freifeld-e56-93_93102d93bd4.pdf";
	
	
	
	createDerivationActivityAndEntity("cite", Q02+"47_93a02d93bd4.pdf", guidelineOld);	
	createDerivationActivityAndEntity("cite", Q02+"51_93802d93bd4.pdf", guidelineOld);
	createDerivationActivityAndEntity("cite", Q02+"52_93b02d93bd4.pdf", guidelineOld);
	createDerivationActivityAndEntity("cite", Q02+"49_92502d93bd4.pdf", guidelineOld);
	createDerivationActivityAndEntity("cite", Q02+"50_93902d93bd4.pdf", guidelineOld);
	createDerivationActivityAndEntity("cite", Q02+"55_92602d93bd4.pdf", guidelineOld);
	createDerivationActivityAndEntity("cite", Q02+"56_92702d93bd4.pdf", guidelineOld);
	createDerivationActivityAndEntity("cite", Q02+"57_92402d93bd4.pdf", guidelineOld);
	
	
	createDerivationActivityAndEntity("cite",  Q10+"292 update 2010_92f02d93bd4.pdf",guidelineOld);
	createDerivationActivityAndEntity("cite",  Q10+"292_92e02d93bd4.pdf", guidelineOld);
	createDerivationActivityAndEntity("cite",  Q10+"293_93202d93bd4.pdf", guidelineOld);
	createDerivationActivityAndEntity("cite",  Q10+"294_93002d93bd4.pdf", guidelineOld);
	createDerivationActivityAndEntity("cite",  Q10+"295_92c02d93bd4.pdf", guidelineOld);
	createDerivationActivityAndEntity("cite",  Q10+"296_92d02d93bd4.pdf", guidelineOld);
	createDerivationActivityAndEntity("cite",  Q10+"297_92802d93bd4.pdf", guidelineOld);
	createDerivationActivityAndEntity("cite",  Q10+"298_92902d93bd4.pdf", guidelineOld);
	createDerivationActivityAndEntity("cite",  Q10+"299_92b02d93bd4.pdf", guidelineOld);
	createDerivationActivityAndEntity("cite",  Q10+"301_93302d93bd4.pdf", guidelineOld);
	createDerivationActivityAndEntity("cite",  Q10+"303_92a02d93bd4.pdf", guidelineOld);
}


public static void convertToDot(ProvFactory factory){

	ProvToDot provtodot = new ProvToDot();
	Document container;
	container = factory.newDocument(null, listOfAvailableEntities.values(),  null, null);
		try {
		provtodot.convert(container, new File("graphCorpusBiomed.gv"));
	} catch (FileNotFoundException e) {
	
		e.printStackTrace();
	}
}

}	
	
