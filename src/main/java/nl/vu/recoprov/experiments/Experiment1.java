package nl.vu.recoprov.experiments;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import nl.vu.recoprov.CompletePipeline;
import nl.vu.recoprov.ProvDMtranslator;
import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.signalaggregators.WeightedSumAggregator;
import nl.vu.recoprov.signaldetectors.ImageSimilaritySignal;
import nl.vu.recoprov.signaldetectors.LuceneInverseSimilarity;
import nl.vu.recoprov.signaldetectors.LuceneSimilaritySignal;
import nl.vu.recoprov.signaldetectors.MatchTitleInContentSignal;
import nl.vu.recoprov.signaldetectors.MetadataSimilaritySignal;
import nl.vu.recoprov.signalfilters.BackwardTemporalFilter;
import nl.vu.recoprov.utils.ConfigurationDefaults;
import nl.vu.recoprov.utils.TransitiveClosure;

import org.openprovenance.prov.xml.ProvFactory;

public class Experiment1 {
	
	public static String dirTemp = "/Users/saramagliacane/Dropbox/reconstructing-dataprep-provenance/";
	public final static String dir = "/reconstructing-dataprep-provenance/";
	
	public static DependencyGraph createReferenceGraph(){
		ProvFactory factory = CorpusGeneratorProvDM.initFactory();

		CompletePipeline pipeline = new CompletePipeline(false, dirTemp);

		try {
			CorpusGeneratorProvDM.depGraph = pipeline.initDependencyGraph();
			pipeline.indexFiles(CorpusGeneratorProvDM.depGraph);
					
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		CorpusGeneratorProvDM.createEntityFromDepGraph(factory, CorpusGeneratorProvDM.depGraph);
		CorpusGeneratorProvDM.manualAnnotation();
		CorpusGeneratorProvDM.convertToDot(factory);

		return CorpusGeneratorProvDM.depGraph;
	}
	
	public static DependencyGraph createGraph(){
		DependencyGraph depGraphDerived = new DependencyGraph();
		CompletePipeline pipeline = new CompletePipeline(true, dir);
		
		try {
			depGraphDerived = pipeline.initDependencyGraph();
			pipeline.loadMetadaAndIndexes(depGraphDerived);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return depGraphDerived;
	}
	
	
	
	
	public static void main(String[] args) throws IOException, InterruptedException{
		Experiment1();
	}
	
	public static void Experiment1() throws IOException{

		DependencyGraph depGraph = createReferenceGraph();
		
		File logfile = new File("log"+ System.currentTimeMillis()+".txt");
		FileWriter writer = new FileWriter(logfile);
		
		//use only Lucene
		DependencyGraph depGraph1 = createGraph();
		depGraph1 = new LuceneSimilaritySignal().computeSignal(depGraph1);
//		depGraph1 = new WeightedSumAggregator().aggregateSignals(depGraph1);
//		writer.append("\n\nLucene: \n");
//		writer.append(depGraph.similarToGraph(depGraph1).toString());
//		new ProvDMtranslator().translate(depGraph1, "graphLucene.gv");
		

		//use  Lucene and metadata similarity
		DependencyGraph depGraph2 = createGraph();
		depGraph2 = new LuceneSimilaritySignal().computeSignal(depGraph2);
		depGraph2  =  new MetadataSimilaritySignal().computeSignal(depGraph2);
//		depGraph2 = new WeightedSumAggregator().aggregateSignals(depGraph2);
//		writer.append("\n\nLucene and Metadata: \n");
//		writer.append(depGraph.similarToGraph(depGraph2).toString());
//		new ProvDMtranslator().translate(depGraph2, "graphLuceneMetadata.gv");
		
		//use  Lucene metadata and image similarity
		DependencyGraph depGraph3 = createGraph();
		depGraph3 = new LuceneSimilaritySignal().computeSignal(depGraph3);
		depGraph3  =  new MetadataSimilaritySignal().computeSignal(depGraph3);
		depGraph3  =  new ImageSimilaritySignal().computeSignal(depGraph3);
//		depGraph3 = new WeightedSumAggregator().aggregateSignals(depGraph3);
//		writer.append("\n\nLucene and Metadata and Match title: \n");
//		writer.append(depGraph.similarToGraph(depGraph3).toString());
//		new ProvDMtranslator().translate(depGraph3, "graphLuceneMetadataTitle.gv");
		

		//use  Lucene metadata and image similarity
		DependencyGraph depGraph4 = createGraph();
		depGraph4 = new LuceneSimilaritySignal().computeSignal(depGraph4);
		depGraph4  =  new MetadataSimilaritySignal().computeSignal(depGraph4);
		depGraph4  =  new ImageSimilaritySignal().computeSignal(depGraph4);
		depGraph4 = new LuceneInverseSimilarity().computeSignal(depGraph4);
		depGraph4  =  new MatchTitleInContentSignal().computeSignal(depGraph4);
//		depGraph4 = new WeightedSumAggregator().aggregateSignals(depGraph4);
//		writer.append("\n\nLucene, Metadata, Match title and Image: \n");
//		writer.append(depGraph.similarToGraph(depGraph4).toString());
//		new ProvDMtranslator().translate(depGraph4, "graphLuceneMetadataTitleImage.gv");

		
		//BackwardFilter
		writer.append("\n\nLucene - Filter: \n");
		depGraph1 = new BackwardTemporalFilter().filterSignals(depGraph1);
		depGraph1 = new WeightedSumAggregator().aggregateSignals(depGraph1);
		writer.append(depGraph.similarToGraph(depGraph1).toString());
		new ProvDMtranslator().translate(depGraph1, "graphLucene_BackFilter.gv");
		
		writer.append("\n\nLucene, Metadata - Filter: \n");
		depGraph2 = new BackwardTemporalFilter().filterSignals(depGraph2);
		depGraph2 = new WeightedSumAggregator().aggregateSignals(depGraph2);
		writer.append(depGraph.similarToGraph(depGraph2).toString());
		new ProvDMtranslator().translate(depGraph2, "graphLuceneMetadata_BackFilter.gv");
		
		writer.append("\n\nLucene, Metadata, Image - Filter: \n");
		depGraph3 = new BackwardTemporalFilter().filterSignals(depGraph3);
		depGraph3 = new WeightedSumAggregator().aggregateSignals(depGraph3);
		writer.append(depGraph.similarToGraph(depGraph3).toString());
		new ProvDMtranslator().translate(depGraph3, "graphLuceneMetadataImage_BackFilter.gv");
		
		writer.append("\n\nLucene, Metadata, Match title and Image - Filter: \n");
		depGraph4 = new BackwardTemporalFilter().filterSignals(depGraph4);
		depGraph4 = new WeightedSumAggregator().aggregateSignals(depGraph4);
		writer.append(depGraph.similarToGraph(depGraph4).toString());
		new ProvDMtranslator().translate(depGraph4, "graphLuceneMetadataTitleImage_BackFilter.gv");
		
		
		//LOWER THRESHOLD
		ConfigurationDefaults.LUCENE_THRESHOLD = 0.01;
		ImageSimilaritySignal.IMAGE_THRESHOLD =0.01;
		
		//use  Lucene metadata and image similarity
		DependencyGraph depGraph5 = createGraph();
		depGraph5 = new LuceneSimilaritySignal().computeSignal(depGraph5);
		depGraph5  =  new MetadataSimilaritySignal().computeSignal(depGraph5);
		depGraph5  =  new ImageSimilaritySignal().computeSignal(depGraph5);
		depGraph5  =  new MatchTitleInContentSignal().computeSignal(depGraph5);
		depGraph5 = new WeightedSumAggregator().aggregateSignals(depGraph5);
		depGraph5 = new BackwardTemporalFilter().filterSignals(depGraph5);
		writer.append("\n\nLucene, Metadata, Match title and Image -Filter - Low Threshold : \n");
		writer.append(depGraph.similarToGraph(depGraph5).toString());
		new ProvDMtranslator().translate(depGraph5, "graphLuceneMetadataTitleImage_BackFilterLow.gv");
		
		
//		writer.close();
//		writer = new FileWriter(logfile+"_trans.txt");

		writer.append("\n**************************");
		
		//Compare with Transitive Closure
 		DependencyGraph depGraphT = new TransitiveClosure().aggregateSignals(depGraph);
		new ProvDMtranslator().translate(depGraphT, "graphCorpus_Trans.gv");
		
		writer.append("\n\nLucene- TransitiveClosure: \n");
		DependencyGraph depGraph11 = new  TransitiveClosure().aggregateSignals(depGraph1);
		writer.append((depGraphT.similarToGraph(depGraph11).toString()));
		new ProvDMtranslator().translate(depGraph11, "graphLucene_BackFilter_Trans.gv");
		

		
		writer.append("\n\nLucene, Metadata- TransitiveClosure: \n");
		DependencyGraph depGraph12 = new  TransitiveClosure().aggregateSignals(depGraph2);
		writer.append((depGraphT.similarToGraph(depGraph12).toString()));
		new ProvDMtranslator().translate(depGraph12, "graphLuceneMetadata_BackFilter_Trans.gv");
		
		writer.append("\n\nLucene, Metadata, Image - TransitiveClosure: \n");
		DependencyGraph depGraph13 = new  TransitiveClosure().aggregateSignals(depGraph3);
		writer.append((depGraphT.similarToGraph(depGraph13).toString()));
		new ProvDMtranslator().translate(depGraph13, "graphLuceneMetadataImage_BackFilter_Trans.gv");
		
		writer.append("\n\nLucene, Metadata, Match title and Image- TransitiveClosure: \n");
		DependencyGraph depGraph14 = new  TransitiveClosure().aggregateSignals(depGraph4);
		writer.append((depGraphT.similarToGraph(depGraph14).toString()));
		new ProvDMtranslator().translate(depGraph14, "graphLuceneMetadataTitleImage_BackFilter_Trans.gv");
		
		writer.append("\n\nLucene, Metadata, Match title and Image Low- TransitiveClosure: \n");
		DependencyGraph depGraph15 = new  TransitiveClosure().aggregateSignals(depGraph5);
		writer.append((depGraphT.similarToGraph(depGraph15).toString()));
		new ProvDMtranslator().translate(depGraph15, "graphLuceneMetadataTitleImage_BackFilterLow_Trans.gv");
		
		
//transitive reduction
		
		
//		String cmd = "tred " + ;
//		Runtime run = Runtime.getRuntime();
//		Process pr = run.exec(cmd);
//		pr.waitFor();
//		BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
//		String line = "";
//		while ((line=buf.readLine())!=null) {
//			System.out.println(line);
//		}
		
		
		//Compare with Transitive Closure - currently not working
// 		DependencyGraph depGraphTR = new TransitiveReductionFilter().filterSignals(depGraphT);
//		new ProvDMtranslator().translate(depGraphTR, "graphCorpus_TransR.gv");
//		
//		writer.append("\n\nLucene, Metadata, Match title and Image- TransitiveClosure - Transitive Reduction: \n");
//		depGraph14 = new TransitiveReductionFilter().filterSignals(depGraph14);
//		writer.append(depGraphTR.similarToGraph(depGraph14).toString());
//		new ProvDMtranslator().translate(depGraph14, "graphLuceneMetadataTitleImage_BackFilter_TransR.gv");
//		
//		writer.append("\n\nLucene, Metadata, Match title and Image- TransitiveClosure - Transitive Reduction - compared to original: \n");
//		writer.append(depGraph.similarToGraph(depGraph14).toString());

		writer.close();
		
		}

 		


}
