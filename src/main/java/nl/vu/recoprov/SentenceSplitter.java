package nl.vu.recoprov;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import org.apache.lucene.analysis.core.KeywordAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.SimpleFSDirectory;

import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.baseclasses.DependencyNode;
import nl.vu.recoprov.signaldetectors.LuceneSimilaritySignal;
import nl.vu.recoprov.utils.ConfigurationDefaults;

import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;

public class SentenceSplitter {
	private SentenceDetectorME sentenceDetector = null;

	public static final String SENTENCE_DIRECTORY = "sentences/";
	public static final int DEFAULT_K = 1000;
	
	public SentenceSplitter() {
		initialize();
	}
		
	public void initialize(){

		InputStream modelIn = null;
		SentenceModel model = null;
		
		File sentenceDir = new File (SENTENCE_DIRECTORY);
		if (!sentenceDir.exists())
			sentenceDir.mkdir();
		
		try {
			modelIn = new FileInputStream("en-sent.bin");
			model = new SentenceModel(modelIn);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		} finally {
			if (modelIn != null) {
				try {
					modelIn.close();
				} catch (IOException e) {
				}
			}
		}
		
		sentenceDetector = new SentenceDetectorME(model);
	}
	
	
	public String[] splitIntoStrings(String text){
		return sentenceDetector.sentDetect(text);
	}
	
	public void createFilePerSentence(String filename, String content){
		int counter = 0;
		FileWriter writer = null;
		for (String s :splitIntoStrings(content)){
			try {
				writer = new FileWriter(new File(SENTENCE_DIRECTORY + filename+"_"+counter));
				writer.append(s);
			} catch (IOException e) {
				e.printStackTrace();
			} finally{
				if (writer != null)
					try{
						writer.close();
					} catch (IOException e){
					}
			}
			counter++;
		}
	}

	// may be better to use standard Lucene Shingles
	public void createFileForKNgrams(DependencyNode d, String filename, String content){
		int counter = 0;
		int k = 0;
		StringBuffer buffer = new StringBuffer();
		FileWriter writer = null;
		
		for (String s : content.split(" ")){
			
			if (k < DEFAULT_K){
				buffer.append(s + " ");
				k++;
			}
			
			else{			
				File file = new File(filename);
				File outfile = new File(SENTENCE_DIRECTORY + file.getName()+"_"+counter);

				if (!outfile.exists()){
				
					try {
						writer = new FileWriter(outfile);
						writer.append(buffer);
					} catch (IOException e) {
						e.printStackTrace();
					} finally{
						if (writer != null)
							try{
								writer.close();
							} catch (IOException e){
							}
					}
				}
				buffer = new StringBuffer();
				k = 0;
				counter++;

			}
		}
	}
	
	public static void main (String[] args){
		SentenceSplitter s = new SentenceSplitter();
		
		String content = "prova. che voglia.";
		
		for (String string: s.splitIntoStrings(content))
			System.out.println(string);

		for (String string: content.split(" "))
			System.out.println(string);
	}
	
	public static ScoreDoc[] getLuceneIdForSentence(StringBuffer filename, DependencyNode d){
		IndexReader reader  = LuceneIndexer.createIndexReader(SENTENCE_DIRECTORY + ConfigurationDefaults.RELATIVE_INDEX_DIR);
		IndexSearcher searcher = new IndexSearcher(reader);		
		File file = new File(filename.toString());
		File dir = new File(SENTENCE_DIRECTORY);
		StringBuffer querystring = new StringBuffer(dir.getAbsolutePath()+ "*"+ file.getName()+"*");

		QueryParser parser = new QueryParser(
				ConfigurationDefaults.LUCENE_VERSION, "name", new KeywordAnalyzer());
		Query query;
		try {
			query = parser.parse(QueryParser.escape(querystring.toString()));
		} catch (ParseException e) {
			e.printStackTrace();
			return new ScoreDoc[0];
		}

		TopScoreDocCollector collector = TopScoreDocCollector.create(
				ConfigurationDefaults.LUCENE_MAX_NUMBER_DOCS, true);
		try {
			searcher.search(query, collector);
		} catch (IOException e) {
			e.printStackTrace();
			return new ScoreDoc[0];
		}
		ScoreDoc[] hits = collector.topDocs().scoreDocs;
		return hits;
	}

	public static void setLuceneIdForSentence(DependencyGraph input) {
		IndexReader reader  = LuceneIndexer.createIndexReader(SENTENCE_DIRECTORY + ConfigurationDefaults.RELATIVE_INDEX_DIR);
		IndexSearcher searcher = new IndexSearcher(reader);	
		
		int numdocs = reader.numDocs();
		for (int i = 0; i < numdocs; i++) {
			Document doc;
			try {
				doc = searcher.doc(i);
			} catch (IOException e) {
				e.printStackTrace();
				continue;
			}

			String key = doc.getField("name").stringValue();
			key = key.substring(key.lastIndexOf("/")+1, key.lastIndexOf("_"));
			
			DependencyNode node = input.getSimilar(key);
			node.addLuceneIdSentences(i);
		}
		
		
		
	}

}
