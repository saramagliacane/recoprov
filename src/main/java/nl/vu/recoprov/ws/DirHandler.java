package nl.vu.recoprov.ws;

import java.io.IOException; 

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nl.vu.recoprov.CompletePipeline;
import nl.vu.recoprov.ProvDMtranslator;
import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.signalaggregators.WeightedSumAggregator;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

public class DirHandler  extends AbstractHandler
	{
	    
		public void handle(String target, Request baseRequest,
				HttpServletRequest request, HttpServletResponse response)
				throws IOException, ServletException {
			 
	    	try {
		    	
		    	CompletePipeline pipeline = new CompletePipeline(false, "/Users/saramagliacane/Dropbox/reconstructing-dataprep-provenance");
		    	
	    		//System.out.println("### FIRST PHASE: Fetch the documents from Dropbox.\n");
	    		DependencyGraph depGraph = new DependencyGraph();
	
	    		depGraph = pipeline.initDependencyGraph();
	    		
	    		pipeline.loadMetadaAndIndexes(depGraph);
	    		
	    		pipeline.computeSignals(depGraph);
	     		
	    		pipeline.filterSignals(depGraph);
	    		
	     		//System.out.println(depGraph);
	     		//System.out.println(depGraph.getAttributes());
	
	//     		System.out.println(depGraph.toCSVString());
	    		
	    		//AGGREGATE
	    		depGraph = new WeightedSumAggregator().aggregateSignals(depGraph);
	    		System.out.println(depGraph);
	    		
	    		ProvDMtranslator pdmt = new ProvDMtranslator();
	    		String out = pdmt.translate(depGraph);
	    		System.out.println(out);
	    		
		    	response.setContentType("text/html;charset=utf-8");
			    response.setStatus(HttpServletResponse.SC_OK);
			    baseRequest.setHandled(true);
			    response.getWriter().println(out);
			   
	    	
	    	} catch (Exception e) {
	    		e.printStackTrace();
	    		throw new ServletException(e);
	    	}
			
		}
	
}
