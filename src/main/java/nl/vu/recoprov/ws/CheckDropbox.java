package nl.vu.recoprov.ws;

import java.io.IOException; 

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nl.vu.recoprov.CompletePipeline;
import nl.vu.recoprov.DropboxClient;
import nl.vu.recoprov.ProvDMtranslator;
import nl.vu.recoprov.SearchCache.Content;
import nl.vu.recoprov.SearchCache.State;
import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.signalaggregators.WeightedSumAggregator;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.util.MultiMap;
import org.eclipse.jetty.util.UrlEncoded;

import com.dropbox.client2.session.AccessTokenPair;
import com.dropbox.client2.session.WebAuthSession;

public class CheckDropbox  extends AbstractHandler
	{
	    
	
		public void handle(String target, Request baseRequest,
				HttpServletRequest request, HttpServletResponse response)
				throws IOException, ServletException {
			
			try {
				System.out.println("Dropbox check got called");
				
				DropboxClient client = new DropboxClient();
				
				if (request.getQueryString() != null) {
					MultiMap<String> params = new MultiMap<String>();
					UrlEncoded.decodeTo(request.getQueryString(), params, "UTF-8");
				}
				
				if (client.isLinked()) {
					System.out.println("dropbox is linked");
					response.setContentType("text/html;charset=utf-8");
				    response.setStatus(HttpServletResponse.SC_OK);
				    baseRequest.setHandled(true);
				    response.getWriter().println("Already Approved");
				}
				else {
					System.out.println("redirecting...");
					WebAuthSession auth = client.getDropboxApproval();
					
					System.out.println("Should be redirecting...");
					String authurl = auth.getAuthInfo("http://localhost:8080/checkDropbox/").url;
					System.out.println(authurl);
					
					response.sendRedirect(response.encodeRedirectURL(authurl));
					
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw new ServletException(e);
			}
			
		}
	
}
