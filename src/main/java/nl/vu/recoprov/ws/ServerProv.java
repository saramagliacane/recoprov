package nl.vu.recoprov.ws;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;

public class ServerProv {
	
	public static void main (String args[]) throws Exception {
		
		Server server = new Server(8080);
		
		ResourceHandler resource_handler = new ResourceHandler();
	    resource_handler.setDirectoriesListed(true);
	    resource_handler.setWelcomeFiles(new String[]{ "index.html" });
	    resource_handler.setResourceBase("./web/");
	    
	    ContextHandler context = new ContextHandler();
        context.setContextPath("/getProvenance");
        context.setResourceBase(".");
        context.setClassLoader(Thread.currentThread().getContextClassLoader());
        context.setHandler(new DirHandler());
        
        ContextHandler checkDropbox = new ContextHandler();
        checkDropbox.setContextPath("/checkDropbox");
        checkDropbox.setResourceBase(".");
        checkDropbox.setClassLoader(Thread.currentThread().getContextClassLoader());
        checkDropbox.setHandler(new CheckDropbox());
	    
        HandlerList handlers = new HandlerList();
	    handlers.setHandlers(new Handler[] { resource_handler, context, checkDropbox});
	    server.setHandler(handlers);
		
		
		
        
        server.start();
        server.join();
	}
	
	

}
