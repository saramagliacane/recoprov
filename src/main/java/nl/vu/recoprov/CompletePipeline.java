package nl.vu.recoprov;

/**
 * The complete pipeline class calls all the components of the system in the right order.
 */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.lucene.search.ScoreDoc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dropbox.client2.exception.DropboxException;

import nl.vu.recoprov.ProvDMtranslator;
import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.baseclasses.DependencyNode;
import nl.vu.recoprov.signalaggregators.WeightedSumAggregator;
import nl.vu.recoprov.signaldetectors.CompressionDistanceSignal;
import nl.vu.recoprov.signaldetectors.ImageSimilaritySignal;
import nl.vu.recoprov.signaldetectors.LuceneInverseSimilarity;
import nl.vu.recoprov.signaldetectors.LuceneMoreLikeThisSignal;
import nl.vu.recoprov.signaldetectors.LuceneSimilaritySignal;
import nl.vu.recoprov.signaldetectors.MatchTitleInContentSignal;
import nl.vu.recoprov.signaldetectors.MetadataSimilaritySignal;
import nl.vu.recoprov.signalfilters.BackwardTemporalFilter;
import nl.vu.recoprov.signalfilters.LuceneThresholdFilter;
import nl.vu.recoprov.signalfilters.PlagiarismCorpusSpecificFilter;
import nl.vu.recoprov.signalfilters.TextContainmentFilter;
import nl.vu.recoprov.signalfilters.TopKEdges;
import nl.vu.recoprov.utils.ConfigurationDefaults;
import nl.vu.recoprov.utils.ConfigurationReader;


public class CompletePipeline {

	private String currentDir = null;
	private Boolean connectToInternet = true;
	private String[] params = null;
	private Logger logger;
	
	private LuceneThresholdFilter luceneThresholdFilter = new LuceneThresholdFilter();
	private WeightedSumAggregator aggregator = new WeightedSumAggregator();
	private TextContainmentFilter textContainmentFilter = new TextContainmentFilter();
	private PlagiarismCorpusSpecificFilter plagiarismCorpusSpecificFilter = new PlagiarismCorpusSpecificFilter();
	private ProvDMtranslator ProvDMtranslator = new ProvDMtranslator();
	private DropboxClient client = new DropboxClient();
	private BackwardTemporalFilter backwardTemporalFilter = new BackwardTemporalFilter();
	private LuceneSimilaritySignal luceneSimilaritySignal = new LuceneSimilaritySignal();
	private LuceneInverseSimilarity luceneInverseSimilarity = new LuceneInverseSimilarity();
	private MetadataSimilaritySignal metadataSimilaritySignal = new MetadataSimilaritySignal();
	private MatchTitleInContentSignal matchTitleInContentSignal = new MatchTitleInContentSignal();
	private ImageSimilaritySignal imageSimilaritySignal = new ImageSimilaritySignal();
	private LuceneMoreLikeThisSignal luceneMoreLikeThisSignal = new LuceneMoreLikeThisSignal();
	private TopKEdges topKEdges = new TopKEdges();
	private CompressionDistanceSignal compressionDistanceSignal = new CompressionDistanceSignal();
	
	private TikaReader tika;
	private LuceneIndexer indexer;
	
	public CompletePipeline() {
		this (false, "", null);
	}


	public CompletePipeline(Boolean online, String dir) {
		this (online, dir, null);
	}
	public CompletePipeline(Boolean online, String dir, String[] params) {
		this.connectToInternet = online;
		this.currentDir = dir;
		this.params = params;
		logger = LoggerFactory
				.getLogger("nl.vu.recoprov.CompletePipeline");
		tika = new TikaReader(currentDir);
		indexer = new LuceneIndexer();
	}
	
	public static void main(String[] args) throws Exception {

		ConfigurationReader confreader = new ConfigurationReader();
		CompletePipeline pipeline = confreader.readParameters();
		
		DependencyGraph depGraph = pipeline.initDependencyGraph();
		pipeline.computeSignals(depGraph);
		pipeline.filterSignals(depGraph);
		pipeline.aggregateSignals(depGraph);
		pipeline.writeToFile(depGraph);
		pipeline.translateToPROVDM(depGraph);

	}
	
	public void translateToPROVDM(DependencyGraph depGraph, String name) throws FileNotFoundException {
		ProvDMtranslator.translate(depGraph, name);	
	}


	public void translateToPROVDM(DependencyGraph depGraph) throws FileNotFoundException {
		ProvDMtranslator.translate(depGraph);	
	}

	public DependencyGraph initDependencyGraph() throws DropboxException, IOException {
		DependencyGraph input = getDropboxMetadata();
		loadMetadaAndIndexes(input);
		return input;
	}
		
	public DependencyGraph getDropboxMetadata() throws DropboxException{
		logger.info("Initializing DependencyGraph for directory: {}", currentDir);
		DependencyGraph depGraph = new DependencyGraph();
		
		if (connectToInternet) {
			if (!client.isLinked()) {
				client.linkToAccount();
			}
			client.getAllRevs(currentDir, depGraph);
			currentDir = ConfigurationDefaults.TEMPDIR;
		} else {
			client.getAllRevsOffline(new File(currentDir), depGraph);
		}		
		return depGraph;
	}
	
	public void loadMetadaAndIndexes(DependencyGraph depGraph) throws IOException {
		tikaRead(depGraph);
		indexFiles(depGraph);
		depGraph = ImageReader.read(currentDir, depGraph);
	}
	
	public void tikaRead(DependencyGraph depGraph){
		tika.read(depGraph, params);
	}

	public void indexFiles(DependencyGraph depGraph) throws IOException{
		indexer.indexFiles(depGraph);
	}
	
	// TODO: compare nouns, verbs, named entities
	// preprocessing get rid of the tags
	// overlap of words
	
	public void computeSignals(DependencyGraph depGraph) {
		luceneSimilaritySignal(depGraph);
		luceneInverseSimilarity(depGraph);
		metadataSimilaritySignal(depGraph);
		matchTitleInContentSignal(depGraph);
		imageSimilaritySignal(depGraph);
	}
	
	public void luceneSimilaritySignal(DependencyGraph depGraph){
		luceneSimilaritySignal.computeSignal(depGraph);
	}
	
	public void luceneSimilaritySignal(DependencyGraph depGraph, String indexDir){
		new LuceneSimilaritySignal(indexDir).computeSignal(depGraph);
	}
	
	
	public void luceneMoreLikeThisSignal(DependencyGraph depGraph){
		luceneMoreLikeThisSignal.computeSignal(depGraph);
	}
	
	public void luceneInverseSimilarity(DependencyGraph depGraph) {
		luceneInverseSimilarity.computeSignal(depGraph);
	}

	public void metadataSimilaritySignal(DependencyGraph depGraph) {
		metadataSimilaritySignal.computeSignal(depGraph);
	}

	public void matchTitleInContentSignal(DependencyGraph depGraph) {
		matchTitleInContentSignal.computeSignal(depGraph);
	}

	public void imageSimilaritySignal(DependencyGraph depGraph) {
		imageSimilaritySignal.computeSignal(depGraph);
	}
	
	public void compressionDistanceSignal(DependencyGraph depGraph){
		compressionDistanceSignal.computeSignal(depGraph);
	}

	public void filterBackWards(DependencyGraph depGraph) {
		backwardTemporalFilter.filterSignals(depGraph);
	}

	public void filterLuceneThreshold(DependencyGraph depGraph) {
		luceneThresholdFilter.filterSignals(depGraph);
	}

	public void filterTextContainment (DependencyGraph depGraph){
		textContainmentFilter.filterSignals(depGraph);
	}
	
	public void filterPlagiarismCorpus (DependencyGraph depGraph){
		plagiarismCorpusSpecificFilter.filterSignals(depGraph);
	}
	
	public void filterTopKEdges(DependencyGraph input){
		topKEdges.filterSignals(input);
	}
	
	public void filterTopKEdges(DependencyGraph input, String label){
		new TopKEdges(label).filterSignals(input);
	}
	
	public void filterTopKEdges(DependencyGraph input, String label, int k ){
		new TopKEdges(label, k).filterSignals(input);
	}
	
	public void filterSignals(DependencyGraph depGraph) {
		filterBackWards(depGraph);
		filterLuceneThreshold (depGraph);
		filterTextContainment (depGraph);	
	}

	public void aggregateSignals(DependencyGraph depGraph) {
		aggregator.aggregateSignals(depGraph);
	}

	public void writeToFile(DependencyGraph depGraph){
		File file = new File(ConfigurationDefaults.RESULTS);
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(file));
			writer.write(depGraph.toString());
			writer.close();
		} catch (IOException e) {
			logger.error("Could not write to file: {}", ConfigurationDefaults.RESULTS);
			e.printStackTrace();
		}
	}
	

	public String getCurrentDir(){
		return currentDir;
	}
	
	public void setCurrentDir(String dir){
		currentDir = dir;
	}
	
	public void setConnectToInternet(Boolean online){
		connectToInternet = online;
	}
	
	public void createDependencyGraphForSentences(DependencyGraph input) throws IOException, DropboxException{
		if (ConfigurationDefaults.useNewFeatures == true) {
			createDependencyGraphForSentences();
			SentenceSplitter.setLuceneIdForSentence(input);
		}
	}
	
	public DependencyGraph createDependencyGraphForSentences() throws IOException, DropboxException{		
		if (ConfigurationDefaults.useNewFeatures == false) {
			return new DependencyGraph();
		}
		
		File sentenceDir = new File (SentenceSplitter.SENTENCE_DIRECTORY);
		if (!sentenceDir.exists()){
			initDependencyGraph();
		}
		
		CompletePipeline pipeline = new CompletePipeline(false, SentenceSplitter.SENTENCE_DIRECTORY);		
		DependencyGraph depGraph = pipeline.getDropboxMetadata();
		pipeline.tikaRead(depGraph);
		LuceneIndexer luceneIndexer = new LuceneIndexer(SentenceSplitter.SENTENCE_DIRECTORY);
		luceneIndexer.indexFiles(depGraph);
		
		return depGraph;

	}
	
	
}
