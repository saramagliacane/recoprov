package nl.vu.recoprov.abstractclasses;
/**
 * Abstract class for the signal aggregation phase
 */
import nl.vu.recoprov.baseclasses.DependencyGraph;

public abstract class SignalAggregator {
	
	public abstract DependencyGraph aggregateSignals(DependencyGraph input);

}
