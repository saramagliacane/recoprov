package nl.vu.recoprov.abstractclasses;
/**
 * Abstract class for the signal detection phase 
 */
import nl.vu.recoprov.baseclasses.DependencyGraph;

public abstract class SignalDetector {
	
	public abstract DependencyGraph computeSignal(DependencyGraph input);

}
