package nl.vu.recoprov.abstractclasses;
/**
 * Abstract class for the signal filter phase
 */
import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.baseclasses.DependencyNode;

public abstract class SignalFilterer {

	/**
	 * A function that represents the API of the filter.
	 * Given an input DependencyGraph, it outputs the filtered version.
	 * @param input
	 * @return
	 */
	public DependencyGraph filterSignals(DependencyGraph input){
		return filterGraph(input);
	}
	
	public DependencyGraph filterGraph (DependencyGraph input){
		for (String name : input.keySet()) {
			DependencyNode d = input.get(name);
			input = filterNode(input, d);			
		}	
		return input;
	}
	
	public DependencyGraph filterNode (DependencyGraph input, DependencyNode d){
		return input;
	}
}
