package nl.vu.recoprov;

/**
 * Dropbox client based on the example client in the SDK
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.baseclasses.DependencyNode;
import nl.vu.recoprov.utils.ConfigurationDefaults;
import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.DropboxAPI.Entry;
import com.dropbox.client2.exception.DropboxException;
import com.dropbox.client2.session.AccessTokenPair;
import com.dropbox.client2.session.AppKeyPair;
import com.dropbox.client2.session.Session;
import com.dropbox.client2.session.WebAuthSession;

public class DropboxClient extends SearchCache {

	private String APPKEY;
	private String SECRET;

	/**
	 * Get permission to access a person's Dropbox folder.
	 * @throws DropboxException
	 */
	public void linkToAccount() throws DropboxException {
		
		File f = new File(ConfigurationDefaults.STATE_FILE);
		AppKeyPair appKeyPair = null;

		if (!f.exists())
			try {
				f.createNewFile();
				appKeyPair = readSecretFile(ConfigurationDefaults.SECRET_FILE);
			} catch (IOException e) {
				System.err.println("Could not read secret file: " + ConfigurationDefaults.SECRET_FILE);
				e.printStackTrace();
			}

		// Make the user log in and authorize us.
		WebAuthSession was = new WebAuthSession(appKeyPair,
				Session.AccessType.APP_FOLDER);
		WebAuthSession.WebAuthInfo info = was.getAuthInfo();
		System.out.println("1. Go to: " + info.url);
		System.out.println("2. Allow access to this app.");
		System.out.println("3. Press ENTER.");

		try {
			while (System.in.read() != '\n') {
			}
		} catch (IOException ex) {
			System.err.println("I/O error: " + ex.getMessage());
			System.exit(1);
			throw new RuntimeException();
		}

		// This will fail if the user didn't visit the above URL and hit
		// 'Allow'.
		was.retrieveWebAccessToken(info.requestTokenPair);
		AccessTokenPair accessToken = was.getAccessTokenPair();
		System.out.println("Link successful.");

		// Save state
		State state = new State(appKeyPair, accessToken, new Content.Folder());
		state.save(ConfigurationDefaults.STATE_FILE);

	}

	public WebAuthSession getDropboxApproval() throws DropboxException {
		File f = new File(ConfigurationDefaults.STATE_FILE);
		if (!f.exists())
			try {
				f.createNewFile();
				readSecretFile(ConfigurationDefaults.SECRET_FILE);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		System.out.println("Creating web auth session");
		AppKeyPair appKeyPair = new AppKeyPair(APPKEY, SECRET);
		WebAuthSession was = new WebAuthSession(appKeyPair,
				Session.AccessType.APP_FOLDER);
		return was;
	}

	public void saveRequest(WebAuthSession was) throws Exception {
		was.retrieveWebAccessToken(was.getAuthInfo().requestTokenPair);
		AccessTokenPair accessToken = was.getAccessTokenPair();
		System.out.println("Link successful.");

		AppKeyPair appKeyPair = new AppKeyPair(APPKEY, SECRET);
		// Save state
		State state = new State(appKeyPair, accessToken, new Content.Folder());
		state.save(ConfigurationDefaults.STATE_FILE);
	}

	/**
	 * Read the file containing the APPKEY and SECRET
	 * @param filename
	 * @return the pair APPKEY and SECRET
	 */
	private AppKeyPair readSecretFile(String filename) {
		System.out.println("Reading secret file: " + filename);
		final File file = new File(filename);

		try {
			final BufferedReader reader = new BufferedReader(
					new InputStreamReader(new FileInputStream(file)));

			String line = null;
			while ((line = reader.readLine()) != null) {

				if (line.startsWith("APPKEY=")) {
					APPKEY = line.split("=")[1];
				}
				if (line.startsWith("SECRET=")) {
					SECRET = line.split("=")[1];
				}
			}

			reader.close();

		} catch (final IOException e) {
			System.out.println("Error processing secret file: " + filename);
		}

		AppKeyPair appKeyPair = new AppKeyPair(APPKEY, SECRET);
		return appKeyPair;
	}

	public DependencyGraph getAllRevsOffline(File dir, DependencyGraph depGraph)
			throws DropboxException {

		for (File f : dir.listFiles()) {

			if (ConfigurationDefaults.ignoreFile(f.getAbsolutePath())) {
				continue;
			}

			if (f.isDirectory()) {
				depGraph.putAll(getAllRevsOffline(f, depGraph));
				continue;

			}

			DependencyNode d = new DependencyNode(depGraph);
			d.setCompleteFilepath(f.getAbsolutePath());
			depGraph.put(d.getCompleteFilepath(), d);

		}

		return depGraph;

	}

	/**
	 * Get all reviews of the files for the complete Dropbox 
	 * @param depGraph
	 * @return
	 * @throws DropboxException
	 */
	public void getAllRevs(DependencyGraph depGraph)
			throws DropboxException {
		getAllRevs("", depGraph);
	}

	/**
	 * Get all reviews of the files for a specific Dropbox folder
	 * @param depGraph
	 * @param dropboxFolder
	 * @return
	 * @throws DropboxException
	 */
	public void getAllRevs(String dropboxFolder, 
			DependencyGraph depGraph) throws DropboxException {

		// Load state.
		State state = State.load(ConfigurationDefaults.STATE_FILE);

		// Connect to Dropbox.
		// WebAuthSession session = new WebAuthSession(state.appKey,
		// WebAuthSession.AccessType.APP_FOLDER);
		WebAuthSession session = new WebAuthSession(state.appKey,
				WebAuthSession.AccessType.DROPBOX);
		session.setAccessTokenPair(state.accessToken);
		DropboxAPI<?> client = new DropboxAPI<WebAuthSession>(session);

		// make dirs
		File dir = new File(ConfigurationDefaults.TEMPDIR);
		dir.delete();
		dir.mkdir();

		File dropboxDir = new File(dir.getAbsolutePath() + dropboxFolder);
		dropboxDir.mkdirs();

		List<Entry> result = getRevisions(client, dropboxFolder, dropboxDir);

		// print the obtained metadata
		// printMetadata(result);

		// copy all the files to the temporary folder
		copyToTempFolder(client, result, dir, dropboxFolder);

		for (Entry e : result) {

			if (ConfigurationDefaults.ignoreFile(e.path)) {
				continue;
			}

			if(e.bytes == 0)
				continue;
			
			DependencyNode d = new DependencyNode(depGraph);
			d.setDropboxEntry(e);
			d.setCompleteFilepath(createTempFilename(dir, e, dropboxFolder));
			// System.out.println(d.getCompleteFilepath() + " "+ d);
			depGraph.put(d.getCompleteFilepath(), d);

		}

	}

	private void copyToTempFolder(DropboxAPI client, List<Entry> result,
			File dir, String dropboxFolder) {

		// copy files
		for (Entry e : result) {

			if (ConfigurationDefaults.ignoreFile(e.path)) {
				continue;
			}
			
			if(e.bytes == 0)
				continue;

			try {
				String filename = createTempFilename(dir, e, dropboxFolder);
				FileOutputStream fout = new FileOutputStream(filename);
				client.getFile(e.path, e.rev, fout, null);
				fout.close();

			} catch (Exception e1) {
				e1.printStackTrace();
				continue;
			}

		}
	}

	private List<Entry> getRevisions(DropboxAPI client, String filepath,
			File dir) {

		Entry e = null;
		List<Entry> revisions = new ArrayList<Entry>();

		try {
			e = client.metadata(filepath, 0, null, true, null);
		} catch (DropboxException e2) {
			e2.printStackTrace();
			return null;
		}

		for (Entry file : e.contents) {
			String filename = file.path;
			List<Entry> temp = null;

			if (file.isDir == false) {

				try {
					temp = client.revisions(filename, 0);
					System.out.println("Received Dropbox metadata for file: "
							+ filename);
				} catch (DropboxException e1) {
					e1.printStackTrace();
				}

			} else {
				if (dir != null) {
					File newdir = new File(dir.getAbsolutePath() + "/"
							+ file.fileName());
					newdir.mkdirs();
					temp = getRevisions(client, filename, newdir);
				} else {
					temp = getRevisions(client, filename, dir);
				}
			}

			if (temp != null) {
				revisions.addAll(temp);

			}
		}

		return revisions;
	}

	public Boolean isLinked()  {

		System.out.println("Checking linkage");
		File f = new File(ConfigurationDefaults.STATE_FILE);
		if (!f.exists())
			return false;

		// Load state.
		State state = State.load(ConfigurationDefaults.STATE_FILE);

		// Connect to Dropbox.
		WebAuthSession session = new WebAuthSession(state.appKey,
				WebAuthSession.AccessType.APP_FOLDER);
		session.setAccessTokenPair(state.accessToken);

		// System.out.println("Got to before returning " + session.isLinked());
		return session.isLinked();

	}

	public static String createTempFilename(File dir, Entry e,
			String dropboxFolder) {
		
		String temp;
		
		if (!e.path.contains(".")){
			temp = dir.getAbsolutePath()
					+ e.path + "_" + e.rev;
		}
		else{
			temp = dir.getAbsolutePath()
					+ e.path.substring(0, e.path.lastIndexOf(".")) + "_" + e.rev
					+ e.path.substring(e.path.lastIndexOf("."), e.path.length());
			// System.out.println(temp);
		}
		return temp;
	}

	/**
	 * Used for debugging purposes - prints out the metadata
	 * @param result
	 */
	private static void printMetadata(List<Entry> result) {
		for (Entry e : result) {
			System.out.println(e.fileName() + "\n" + e.rev + " " + e.modified
					+ " " + e.bytes + " bytes " + e.mimeType + " " + e.path);
		}
	}

}