package nl.vu.recoprov;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import nl.vu.recoprov.baseclasses.DependencyGraph;
import nl.vu.recoprov.baseclasses.DependencyNode;
import nl.vu.recoprov.baseclasses.LabelledEdge;
import nl.vu.recoprov.signalaggregators.WeightedSumAggregator;
import nl.vu.recoprov.signaldetectors.MetadataSimilaritySignal;
import nl.vu.recoprov.utils.TransitiveClosure;
import org.openprovenance.prov.dot.ProvToDot;
import org.openprovenance.prov.xml.Activity;
import org.openprovenance.prov.xml.Agent;
import org.openprovenance.prov.xml.Document;
import org.openprovenance.prov.xml.Entity;
import org.openprovenance.prov.xml.EntityRef;
import org.openprovenance.prov.xml.ProvFactory;
import org.openprovenance.prov.xml.SpecializationOf;
import org.openprovenance.prov.xml.Statement;
import org.openprovenance.prov.xml.WasDerivedFrom;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProvDMtranslator {

	private HashMap<Integer, Entity> listOfAvailableEntities;
	private Collection<Statement> listOfAvailableRelations;
	private boolean useTred = false;
	private ProvFactory factory;
	private Logger logger;
	
	public String translate(DependencyGraph input) throws FileNotFoundException {
		System.out.println("Got called...");
		String result = translate(input, "graph.gv");
		System.out.println("Got someting back");
		return result;
	}
	 
	public String translate(DependencyGraph input, String graphfilename) throws FileNotFoundException {
		
		logger = LoggerFactory
				.getLogger("nl.vu.recoprov.ProvDMTranslator");
		
		logger.info("Translate to PROVDM.");
		
		factory = new ProvFactory();		
		listOfAvailableEntities = new HashMap<Integer, Entity> ();
		listOfAvailableRelations = new ArrayList<Statement> ();

		for (String name: input.keySet()){
			
			// for each node take the edges
			DependencyNode d = input.get(name);
			
			EntityRef originEntity = getEntityRefFromDependencyNode(factory, d);		
			ArrayList<LabelledEdge> edgearray = input.getAllEdges(d.getLuceneDocNumber());
			
			if(edgearray == null)
				continue;

			
			for(LabelledEdge edge: edgearray){
				
				double score = edge.getScore();
				if(score <= 0.0)
					continue;
				
				if(edge.getLabel().equals(WeightedSumAggregator.FINAL_SCORE) || edge.getLabel().equals(TransitiveClosure.INFERRED)){
					DependencyNode d2 = input.get(edge.getId());
					EntityRef generatedEntity = getEntityRefFromDependencyNode(factory, d2);
					
					String scoreString = ""+score;
					
					WasDerivedFrom derivationRelation = factory.newWasDerivedFrom(scoreString, generatedEntity, originEntity);
//					derivationRelation.setGeneratedEntity(generatedEntity);
//					derivationRelation.setUsedEntity(originEntity);
					
				
					derivationRelation.getType().add(WeightedSumAggregator.FINAL_SCORE);
					listOfAvailableRelations.add(derivationRelation);}
				else if (edge.getLabel().equals(MetadataSimilaritySignal.REVISION_SIMILARITY)){
					DependencyNode d2 = input.get(edge.getId());
					EntityRef generatedEntity = getEntityRefFromDependencyNode(factory, d2);
				
					
					SpecializationOf revisionRelation = new SpecializationOf();
//					revisionRelation.setSpecializedEntity(generatedEntity);
//					revisionRelation.setGeneralEntity(originEntity);

					
					//TODO: we remove for graph purposes
				
					
					listOfAvailableRelations.add(revisionRelation);
					}
				
			}	
		}


		// Transitive reduction
		if (useTred)
			try {
				callTredOnDotFile(graphfilename);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		convertToDot(graphfilename);

		String svg = "didn't work";
		// svg = convertToSVG("graph2.gv");

		return svg;
	}
	
	
	public String convertToSVG(String filename) throws Exception {
		String cmd = "dot -Tsvg " + filename;

		Runtime run = Runtime.getRuntime();
		Process pr = run.exec(cmd);
		pr.waitFor();
		BufferedReader buf = new BufferedReader(new InputStreamReader(
				pr.getInputStream()));
		String line = "";
		String out = "";
		while ((line = buf.readLine()) != null) {
			System.out.println(line);
			out = out + line + "\n";

		}

		buf.close();
		return out;
	}

	public  EntityRef getEntityRefFromDependencyNode(ProvFactory factory, DependencyNode d){
		EntityRef ref = new EntityRef();
		
		Entity entity = listOfAvailableEntities.get(d.getLuceneDocNumber());
		if(entity == null){
			entity = factory.newEntity(""+d.getLuceneDocNumber(), ""+d.getLuceneDocNumber());
			//entity.setId(new QName(d.getCompleteFilepath()));
			entity.getType().add("document");
			//entity.getAny().add(d.getCompleteFilepath());
			listOfAvailableEntities.put(d.getLuceneDocNumber(), entity);
		}		
		
		ref.setRef(entity.getId());
		return ref;
	}
	
	public void convertToDot(String graphfilename) throws FileNotFoundException {

		logger.info("Going to build a dot file");
		ProvToDot provtodot = new ProvToDot();

		Document container = factory.newDocument(new LinkedList<Activity>(),
				listOfAvailableEntities.values(), new LinkedList<Agent>(),
				listOfAvailableRelations);

		provtodot.convert(container, new File(graphfilename));

	}
	
	
	public void callTredOnDotFile( String graphfilename) throws IOException, InterruptedException{
	
		String cmd = "tred " + graphfilename;
		BufferedWriter fout = new BufferedWriter(new FileWriter("Tred"
						+ graphfilename));
		Runtime run = Runtime.getRuntime();
		Process pr = run.exec(cmd);
		pr.waitFor();
		BufferedReader buf = new BufferedReader(new InputStreamReader(
						pr.getInputStream()));
		String line = "";
		while ((line = buf.readLine()) != null) {
			fout.write(line);
		}
		fout.flush();
		fout.close();
		buf.close();
		pr.destroy();
	}

}
